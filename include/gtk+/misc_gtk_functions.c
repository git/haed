/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
void haed_alert_dialog(const char *message)
{
	GtkWidget *dialogWindow;
	GtkWidget *dialogBox;
	GtkWidget *messageLabel;
	
	//define the window
	dialogWindow=gtk_dialog_new();
	g_signal_connect (dialogWindow, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(dialogWindow));
	
		//containments
		messageLabel = gtk_label_new(message);
		gtk_label_set_selectable(GTK_LABEL(messageLabel),1);
		gtk_box_pack_start(GTK_BOX(dialogBox),messageLabel,0,1,0);
	
		gtk_dialog_add_button(GTK_DIALOG(dialogWindow),GTK_STOCK_OK,GTK_RESPONSE_YES);
	
	//show the dialog
	gtk_widget_show_all(dialogWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(dialogWindow))==GTK_RESPONSE_YES)
	{
		gtk_widget_destroy(dialogWindow);
		return;
	}
	else
	{
		gtk_widget_destroy(dialogWindow);
		return;
	}
}

int haed_confirmation_dialog(const char *message)
{
	GtkWidget *dialogWindow;
	GtkWidget *dialogBox;
	GtkWidget *messageLabel;
	
	//define the window
	dialogWindow=gtk_dialog_new();
	g_signal_connect (dialogWindow, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(dialogWindow));
	
		//containments
		messageLabel = gtk_label_new(message);
		gtk_label_set_selectable(GTK_LABEL(messageLabel),1);
		gtk_box_pack_start(GTK_BOX(dialogBox),messageLabel,0,1,0);
	
		gtk_dialog_add_button(GTK_DIALOG(dialogWindow),GTK_STOCK_OK,GTK_RESPONSE_YES);
		gtk_dialog_add_button(GTK_DIALOG(dialogWindow),GTK_STOCK_CANCEL,GTK_RESPONSE_NO);
	
	//show the dialog
	gtk_widget_show_all(dialogWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(dialogWindow))==GTK_RESPONSE_YES)
	{
		gtk_widget_destroy(dialogWindow);
		return 1;
	}
	else
	{
		gtk_widget_destroy(dialogWindow);
		return 0;
	}
}
