/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#pragma once

#ifndef USE_BLIBC
#define USE_BLIBC

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <dirent.h>

/**
	Will print:
	===
	file:line: MESSAGE_LEVEL: (function)
	→ @MESSAGE
	===
	Will print out in stdout
	
	@msg NO_FREE
		The format-input string
	@... VA_LIST NO_FREE
		VA_LIST with values to msg, @see printf
	@return
		void
*/
#define b_critical_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;37m\e[41mCRITICAL\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)
#define b_error_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;31mERROR\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)
#define b_warning_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;33mWARNING\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)
#define b_debug_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;36mDEBUG\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)

///this will just print out MESSAGE_LEVEL: @MESSAGE
#define b_tm(msg, ...) printf("\e[0m\e[1;35mTEST:\e[0m\e[1m " msg "\e[0m\n", ##__VA_ARGS__)

#ifdef __GNUC__
#define B_DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#define B_DEPRECATED __declspec(deprecated)
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define B_DEPRECATED
#endif

char* b_substr_tl(const char* str,size_t strlen, size_t begin, size_t len);
#define b_substr(str, begin, len) b_substr_tl(str,strlen(str), begin, len)

char *concat(const char *str, ...);
char *concata(char *str, ...);

int b_string_count_chars(char* p, char character );

int log10s(int num);

char* b_int_to_string(int num);
int b_string_to_int(char *String, size_t StringLength);
char* b_float_to_string(float num);
float b_string_to_float(char *text,size_t tempInt);

//seach for a string in a list
int b_is_string_in_list(char *needle,char **haystack,int haystack_amount);

void **sortdalloc(void **pointer, int amount_without_removed);
void *b_array_remove_element(void **pointerToRem,int *pointerLength, int pos);
void *b_array_remove_elements_from_list(void **pointerToRem,int *pointerLength, int *posList, int posListLen);

char **b_explode_tl(char *text,int textLength,int *returnLength,const char *explode);
#define b_explode(text, returnLength,explode) b_explode_tl(text,strlen(text),returnLength,explode)
char *b_implode(char **explodedStr,int length,const char *implodeIn);
char *b_replace_string(char *string,const char *replace,const char *replaceWith);
char **b_explode_char_vector_tl(char *text,int textLength,int *returnLength,const char *explode,int explodeLength);

#define b_explode_char_vector(text,returnLength,explode) ExplodeCharVectorTl(text,strlen(text),returnLength,explode,strlen(explode))

int b_is_dir(char *fileName);
int b_is_file(char *fileName);

void save_file_tl(char *Name, char *Text,size_t textlen);
#define save_file(name,text) save_file_tl(name,text,strlen(text)) 

char *get_file_contents(char *filename,long *contentsLength);
char *get_file_type(const char *filename);

int *GetIntVector(char* text,int *returnL);

float *GetFloatVector(char* text,int *returnL);

#endif
