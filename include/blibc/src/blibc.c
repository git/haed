/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "blibc.h"

/**
	Substr, this one is kinda old, can be reproduced with strndup

	@param str
		input string
	@strlen
		length of the string
	@begin
		where in the string you want to start
	@len
		the length of the string
		
	@return
		the new string
*/
char *b_substr_tl(const char* str,size_t strlen, size_t begin, size_t len)
{	
	if (str == 0 || strlen == 0 || len<=0 || strlen < begin || strlen < (begin+len))
	return 0;

	return strndup(str + begin, len);
}

/**
	Will make a new concentrated string, ( @param logical str+str+str+... )
	
	@param str NO_FREE
		the first string
	@param ... VA_LIST NULL_TERMINATED
		a va list with strings.
	@return
		the pointer to the new string
*/
char *concat(const char *str, ...)
{
	va_list ap, ap2;
	size_t total = 1;
	const char *s;
	char *result;
     
	va_start (ap, str);
	va_copy (ap2, ap);
     
	//Determine how much space we need. 
	for (s = str; s != NULL; s = va_arg (ap, const char *))
		total += strlen (s);
     
	va_end (ap);
     
	result = (char *) malloc (total);
	if (result != NULL)
	{
		result[0] = '\0';
     
		//Copy the strings.
		for (s = str; s != NULL; s = va_arg (ap2, const char *))
			strcat (result, s);
	}
     
	va_end (ap2);
     
	return result;
}

/**
	@param see concat
	This will create a new string, but the first value, will be realloced and get all contents.
	
	@param str REALLOC
		the first string
	@param ... VA_LIST NULL_TERMINATED
		va list with strings.
	@return
		the pointer to the new string
*/
char *concata(char *str, ...)
{
	va_list ap, ap2;
	size_t total = 1;
	const char *s;
	char *result;
     
	va_start (ap, str);
	va_copy (ap2, ap);
     
	//Determine how much space we need. 
	if(str)
     s = str;
   else
     s = va_arg (ap, const char *);
	//Copy the strings.
	while (s != NULL)
	{
		total += strlen (s);
	
		s = va_arg (ap, const char *);
	}
     
	va_end (ap);
     
	result = malloc(total);
		
	if (result != NULL)
	{
		result[0] = '\0';
     
      if(str)
        s = str;
      else
        s = va_arg (ap2, const char *);
		//Copy the strings.
		while (s != NULL)
		{
			strcat (result, s);
			
			s = va_arg (ap2, const char *);
		}
	}
     
	va_end (ap2);
    
   free(str);
     
	return result;
}

/**
	counting number of char occurances in a string.
	
	@param p NO_FREE
		the first string
	@param character .
		the char to search for
	@return
		returns the number of occurances in the string
*/
int b_string_count_chars(char* p, char character )
{
	//const char *p = str;
	int count = 0;

	do
	{
		if(*p == character)
		{
			count++;
		}
	}while (*(p++));

	return count;
}

/**
	Fast log(num)/log(10)
	
	@param num .
		the value to calculate
	@return
		the successful value
*/
int log10s(int num)
{
	int res=0;
	while(num>=10)
	{
		num=(num/10);
		res++;
	}
	return res;
}

/**
	Converts int to string
	
	@param num .
		The value to convert
	@return NEW
		returns pointer to the new string
*/
char *b_int_to_string(int num)
{
	int isNegative=0;
	if(num<0)
	{
		isNegative=1;
		num=-num;
	}
	int i=0,length=log10s(num);
	i=length;
	char* endS=(char*)malloc((length+2+isNegative)*8);
	endS[i+1+isNegative]='\0';
	if(isNegative)
	{
		endS[0]='-';
	}
	while(0<=i)
	{
		endS[i+isNegative]='0'+num%10;
		num=(int)(num/10);
		i--;
	}
	return endS;
}

/**
	Converts string to int
	
	@param num NO_FREE
		The value to convert
	@return .
		returns the converted value
*/

int b_string_to_int(char *String, size_t StringLength)
{
	int FinalInt=0;
	int Negative=0;
	
	for(int i=0;i<StringLength;i++)
	{
		if(Negative==0 && FinalInt==0)
		{
			Negative=(String[i]=='-');
		}
		
		//add each number
		if(String[i]>='0' && String[i]<='9')
		{
			FinalInt=(FinalInt*10)+(String[i]-'0');
		}
		else if(String[i]!=' ' && !Negative)
		{
			b_error_message("Cannot convert a non-number int (must be in decimal form) [%s]",String);
			break;
		}
	}
	
	return ((-Negative*2)+1)*FinalInt;
}

/**
	Converts float to string
	
	@param num .
		The value to convert
	@return NEW
		returns pointer to the new string
*/
char *b_float_to_string(float num)
{
	/*
	float decimals=num-(int)num;
	char *cdecimals=(char*)malloc(sizeof(char));
	//memset(cdecimals,'\0',16);
	if(decimals<0)
	{
		decimals=-decimals;
	}
	for(int i=0;(i<15 && decimals>0);i++)
	{
		cdecimals=concat(cdecimals,b_int_to_string((int)(decimals*10)),NULL);
		decimals=(decimals*10)-((int)(decimals*10));
	}
	return concat(b_int_to_string((int)num),".",b_int_to_string((int)(decimals*100000)),NULL);
	*/
	char *cdecimals=(char*)malloc(sizeof(char)*20);
	sprintf(cdecimals,"%f",num);
	
	int i=0;
	do
	{
		if(cdecimals[i]==',')
		{
			cdecimals[i]='.';
		}
		i++;
	}while(i!=strlen(cdecimals)+1);
	
	return cdecimals;
}

/**
	Converts string to float
	
	@param num NO_FREE
		The value to convert
	@return .
		returns the converted value
*/
float b_string_to_float(char *text,size_t tempInt)
{
	float returnFloat=0;
	size_t newi=tempInt;
	
	for(int i=0;i<tempInt;i++)
	{
		//printf("[%c]\n",*(text+i));
		if(text[i]!='.' && text[i]!='-' && text[i]<'0' && text[i]>'9')
		{
			char *errorstr=strndup(text,tempInt);
			b_error_message("Error when extracting the floating value! %s",errorstr);
			free(errorstr);
			return 0;
		}
		if(text[i]=='.')
		{
			newi=i;
			break;
		}
	}
	
	returnFloat=(float)b_string_to_int(text,newi);
	
	if(newi<tempInt)
		returnFloat+=(((float)b_string_to_int(text+newi+1,tempInt-newi-1))/pow(10,tempInt-newi-1));
	
	return returnFloat;
	
	//return atof(text);
}

//seach for a string in a list
int b_is_string_in_list(char *needle,char **haystack,int haystack_amount)
{
	for(int i=0;i<haystack_amount;i++)
	{
		if(strcmp(needle,haystack[i])==0)
		{
			return 1;
		}
	}
	return 0;
}

/**
	Sort on decreaseAlloc
	amount can be 3
	pointer
	NULL
	pointer
	pointer
	OUT_OF_AREA
	--will become--
	amount =3
	pointer
	pointer
	pointer
	OUT_OF_AREA
	
	@param pointer
		the pointer to change its value
	@param amount_without_removed
		the length of the pointer when its not changed
	@return
		returns the changed pointer
*/
void **sortdalloc(void **pointer, int amount_without_removed)
{
	//printf("...\n");
	
	if(amount_without_removed>0)
	{
		int WHERE=0;
		for(int i=0;i<amount_without_removed;i++)
		{
			if(pointer[i]==NULL)
			{
				WHERE=i;
				goto sortalloc_ENDLOOP;
			}
		}
		//ERROR
		b_error_message("cannot find nulled pointer!");
		return NULL;
		
		sortalloc_ENDLOOP:
		
		for(int i=0;i<(amount_without_removed-WHERE);i++)
		{
			pointer[WHERE+i]=pointer[WHERE+i+1];
		}
	
		//qsort (pointer, amount_without_removed+1, sizeof(void**), compare);
		
		pointer=(void**)realloc((void*)pointer,sizeof(void*)*(amount_without_removed));
	}
	else
	{
		pointer=NULL;
	}
	
	//will return NULL if realloc failed
	return pointer;
}

/**
	this function will remove a value from a pointer-list and return the new, realloced pointer.
	if the value is a pointer which you want to free, then do it before this function

	@param pointerToRem REALLOC
		the pointer to resize
	@param pointerLength RESIZE
		pointer to the Length of pointerToRem, will modify the length to the new one
	@param pos .
		where it should be removed
	@return 
		the realloced pointerToRem list.
*/
void *b_array_remove_element(void **pointerToRem,int *pointerLength, int pos)
{
	int newLength=(*pointerLength)-1;
	
	//b_tm("?");
	
	for(int i=pos;i<(newLength-pos);i++)
	{
		pointerToRem[i]=pointerToRem[i+1];
	}
	
	(*pointerLength)=newLength;
	
	return realloc(pointerToRem,sizeof(void*)*newLength);
}
/**
	this function will remove a list of values from a pointer-list and return the new, realloced pointer.

	@param pointerToRem REALLOC
		the pointer to resize
	@param pointerLength RESIZE
		pointer to the Length of pointerToRem, will modify the length to the new one
	@param posList NO_FREE
		a list of values that should be removed. similar to b_array_remove_element, except that this is a list
	@param posListLen .
		the length of posList
	@return 
		the realloced pointerToRem list.
*/
void *b_array_remove_elements_from_list(void **pointerToRem,int *pointerLength, int *posList, int posListLen)
{
	int newLength=(*pointerLength)-posListLen;
	int move=0;
	
	for(int i=0;i<(*pointerLength);i++)
	{
		for(int j=0;j<posListLen;j++)
		{
			if(posList[j]==i+move)
			{
				int counter=1;
				
				while(1)
				{
					for(int k=0;k<posListLen;k++)
					{
						if(posList[j]+counter==posList[k])
						{
							counter++;
							goto try_again;
						}
					}
					move+=counter;
					
					break;
					try_again:
					continue;
				}
				
				goto continue_loop;
			}
		}
		
		continue_loop:
		
		pointerToRem[i]=pointerToRem[i+move];
	}
	
	(*pointerLength)=newLength;
	
	return realloc(pointerToRem,sizeof(void*)*newLength);
}

/**
	Works like PHPs explode
	returns NULL on fail

	@param text
		the string you want to explode
	@param textLength
		the length of the text
	@param returnLength
		a secondary output parameter.
	@param explode
		what you want to divide the string with

	@return
		a string vector
*/
char **b_explode_tl(char *text,int textLength,int *returnLength,const char *explode)
{
	int explodeLength=strlen(explode);
	
	char **returnString=NULL;
	
	int explodeCount=0;
	int replaceWithLength=0;
	int prevStart=0;
	
	for(int i=0;i<textLength;i++)
	{
		//printf("{%c %c}",text[i],explode[explodeCount]);
		if(text[i]==explode[explodeCount])
		{
			if(explodeCount<explodeLength)
			{
				explodeCount++;
			}
			
			if(explodeCount==explodeLength)
			{
				explodeCount=0;
				
				replaceWithLength++;
				
				returnString=realloc(returnString,sizeof(char*)*(replaceWithLength+1));
				
				returnString[replaceWithLength-1]=b_substr_tl(text,textLength,prevStart,i-explodeLength+1-prevStart);
				//printf("(%d %d %d %d)\n",i,prevStart,explodeLength,i-explodeLength+1-prevStart);
				prevStart=i+1;
			}
		}
		else
		{
			explodeCount=0;
		}
	}
	
	//lägg till sista elementet
	if(replaceWithLength>0)
	{
		returnString[replaceWithLength]=b_substr_tl(text,textLength,prevStart,textLength-prevStart);
		//lägg till en för element istället för separatorer
		replaceWithLength++;
	}
	
	(*returnLength)=replaceWithLength;
	return returnString;
}

/**
	PHPs Explode men utan att man skickar in textlängden
*/

#define b_explode(text, returnLength,explode) b_explode_tl(text,strlen(text),returnLength,explode)

/**
	PHPs implode

	@param explodedStr en strängvektor
	@param length strängvektorns dimension
	@param implodeIn det du vill klistra ihop strängen med...

	@param EJFRIGES return, explodedStr
*/
char *b_implode(char **explodedStr,int length,const char *implodeIn)
{
	int implodeInLength=strlen(implodeIn);
	
	char *returnString=NULL;
	
	int totalLength=0;
	
	for(int i=0;i<length;i++)
	{
		if(explodedStr[i]!=NULL)
		{
			int expLen=strlen(explodedStr[i]);
			
			totalLength+=expLen;
			
			returnString=realloc(returnString,sizeof(char)*totalLength);
			memcpy(returnString+totalLength-expLen,explodedStr[i],expLen);
			
			//printf("%s\n",returnString);
		}
		
		if(i+1<length)
		{
			totalLength+=implodeInLength;
			
			returnString=realloc(returnString,sizeof(char)*totalLength);
			memcpy(returnString+totalLength-implodeInLength,implodeIn,implodeInLength);
		}
	}
	
	returnString=realloc(returnString,sizeof(char)*(totalLength+1));
	returnString[totalLength]='\0';
	
	//printf("[%s %d]\n",returnString,totalLength);
	
	return returnString;
}

/**
	Works like an explode in an implode

	@param string
		a string you want to fix
	@param replace
		what you want to replace
	@param replaceWith
		what you want to replace with

	@return
*/
char *b_replace_string(char *string,const char *replace,const char *replaceWith)
{
	char *returnString=NULL;
	char **explodedStr;
	int len;

	if((explodedStr=b_explode(string,&len,replace)) && len>0)
	{
		returnString=b_implode(explodedStr,len,replaceWith);
		
		for(int i=0;i<len;i++)
		{
			if(explodedStr[i]!=NULL)
			{
				free(explodedStr[i]);
			}
		}
		free(explodedStr);
		//free(string);
	}
	else
	{
		returnString=string;
	}
	
	return returnString;
}

/**
	This function works approximatly like the explode function. But each char here represents a char you want to explode with

	@param text
		input text
	@param textLength
		length of the text
	@param returnLength
		the pointer to the return length (2nd return value)
	@param explode
		char list to use to split the strings with
	@param explodeLength
		the amount of chars in that list
		
	@return
		returns a vector of strings
*/

char **b_explode_char_vector_tl(char *text,int textLength,int *returnLength,const char *explode,int explodeLength)
{
	//int explodeLength=strlen(explode);
	
	char **returnString=NULL;

	int replaceWithLength=0;
	int prevStart=0;
	
	for(int i=0;i<textLength;i++)
	{
		//printf("[%c]",text[i]);
		
		for(int ii=0;ii<explodeLength;ii++)
		{
			if(text[i]==explode[ii])
			{
				replaceWithLength++;
				
				if((returnString=(char**)realloc(returnString,sizeof(char*)*(replaceWithLength+1))))
				{
					returnString[replaceWithLength-1]=b_substr_tl(text,textLength,prevStart,i-prevStart);
				}
				else
				{
					b_error_message("Could not realloc!\n");
				}
				
				
				prevStart=i+1;
				
				break;
			}
		}
	}
	
	//lägg till sista elementet
	if(replaceWithLength>0)
	{
		returnString[replaceWithLength]=b_substr_tl(text,textLength,prevStart,textLength-prevStart);
		//lägg till en för element istället för separatorer
		replaceWithLength++;
	}
	
	(*returnLength)=replaceWithLength;
	return returnString;
}

/**
	Simplified version
*/

#define b_explode_char_vector(text,returnLength,explode) ExplodeCharVectorTl(text,strlen(text),returnLength,explode,strlen(explode))

/**
	Checks if it is a folder

	@param filename NO_FREE
		input filename
	@return .
		boolean 1=if found, 0= if not found
*/
int b_is_dir(char *fileName)
{
	//b_tm("Filename %s",fileName);
	DIR *dir=opendir(fileName);

	if(dir) 
	{
		closedir(dir);
		return 1;
	}
	return 0;
}

/**
	Checks if it is a file

	@param filename NO_FREE
		input filename
	@return .
		boolean 1=if found, 0= if not found
*/
int b_is_file(char *fileName)
{
	FILE *f= fopen(fileName,"r");

	if(f) 
	{
		fclose(f);
		return 1;
	}
	return 0;
}

/**
	Simple save file function.

	@param Name NO_FREE
		name of the file/position
	@param Text NO_FREE
		the text in which will be saved
	@return
		void
*/
void save_file_tl(char *Name, char *Text,size_t textlen)
{
	FILE *fp;
	
	fp = fopen(Name, "w+");

	if(fp!=NULL)
	{
		fwrite(Text,sizeof(char),textlen,fp);
		b_tm("%s\n",Text);
	}
	else
	{
		b_error_message("Could not write to file!");
	}
	
	fclose (fp);
}

/**
	This will just get the filecontents, very simple function
	
	@param filename
		the name of the file, absolute or relative path
	@param contentsLength
		secondary output parameter. Will return the length of that file
	@return
		returns the entire contents, remember to handle the the string as is.
		Eg dont use strlen() on this output. Use contentsLength instead.
*/
char *get_file_contents(char *filename,long *contentsLength) 
{
	char *buffer;
	long length;
	FILE *f;
	
	f = fopen (filename, "rb");

	if(f)
	{
		fseek(f, 0, SEEK_END);
		length = ftell(f);
		fseek(f, 0, SEEK_SET);
		buffer = calloc(length+1,sizeof(char));
		if(buffer)
		{
			fread(buffer, 1, length, f);
		}
		fclose(f);
		
		if(contentsLength)
			*contentsLength=length;
		
		return buffer;
	}
	else
	{
		b_error_message("Could not open the file %s!\n",filename);
		return 0;
	}
}

/**
	get the extension of a filetype
	
	@param filename
		the name of a file, as a string
	@return
		the filetype eg .txt, .bin etc
	
*/
char *get_file_type(const char *filename)
{
	int len;
	char *tmp=(char*)filename;
	char *tmp_last_dot=NULL;
	
	while(*tmp)
	{
		if(*tmp=='.')
		{
			tmp_last_dot=tmp;
		}
	
		tmp++;
	}
	
	if(tmp_last_dot && *(tmp_last_dot+1)!='\0')
	{
		return strdup(tmp_last_dot+1);
	}
	
	return NULL;
}

/**
	This function will divide as this "234234,123124,23356"
	This converts into {234234,123124,23356}
	
	@param text
		the string you want to convert into the vector
	@param returnL
		secondary return variable, will return the length of the string.
	@return
		integervector, like int val[returnL]={your stuff}
*/
int *GetIntVector(char* text,int *returnL)
{
	int *returnInt=NULL;
	size_t amount=0;
	
	char *newtext=text;
	
	//go through the string
	do
	{
		if(*newtext==',' || *newtext=='\0')
		{
			amount++;
			returnInt=realloc(returnInt,sizeof(int)*amount);
			returnInt[amount-1]=b_string_to_int(text,newtext-text);
			text=newtext+1;
		}
		//b_tm("[%c]\n",*newtext);
	}while(*newtext++);
	
	(*returnL)=amount;
	
	return returnInt;
}

/**
	This function will divide as this "234234.345,123124.121234,23356.23423"
	This converts into {234234.345,123124.121234,23356.23423}
	
	@param text
		the string you want to convert into the vector
	@param returnL
		secondary return variable, will return the length of the string.
	@return
		integervector, like int val[returnL]={your stuff}
*/
float *GetFloatVector(char* text,int *returnL)
{
	float *returnInt=NULL;
	size_t amount=0;
	
	char *newtext=text;
	
	//go through the string
	do
	{
		if(*newtext==',' || *newtext=='\0')
		{
			amount++;
			returnInt=realloc(returnInt,sizeof(int)*amount);
			returnInt[amount-1]=b_string_to_float(text,newtext-text);
			printf("%f\n",b_string_to_float(text,newtext-text));
			text=newtext+1;
		}
		//b_tm("[%c]\n",*newtext);
	}while(*newtext++);
	
	(*returnL)=amount;
	
	return returnInt;
}
