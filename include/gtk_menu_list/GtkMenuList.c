/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct GtkMenuListInner_
{
	/*
	item is with mnemonic
	*/
	const char *item;
	void *function;
	void *data;
	struct GtkMenuListInner_ *innerList;
	int innerListAmount;
	
}GtkMenuListInner;

/*
typedef struct
{
	GtkMenuListInner *list;
	int amount;
	
}GtkMenuList;
*/

GtkWidget *gtk_menu_new_from_list(GtkMenuListInner *list,int size)
{
	GtkWidget *returnMenu;
	GtkWidget **items;
	
	items=(GtkWidget**)malloc(sizeof(GtkWidget*)*size);
	
	returnMenu=gtk_menu_new();
	
	for(int i=0;i<size;i++)
	{
		if(list[i].item!=NULL)
		{
			items[i]=gtk_menu_item_new_with_label(list[i].item);
			//if there is a function
			if(list[i].function!=NULL)
			{
				g_signal_connect(items[i], "activate",G_CALLBACK (list[i].function),list[i].data);
			}
			
			if(list[i].innerList!=NULL)
			{
				//printf("WAS HERE!\n");
				gtk_menu_item_set_submenu(GTK_MENU_ITEM(items[i]),gtk_menu_new_from_list(list[i].innerList, list[i].innerListAmount));
			}
			
			gtk_menu_shell_append(GTK_MENU_SHELL(returnMenu), items[i]);
		}
	}
	
	return returnMenu;
}
