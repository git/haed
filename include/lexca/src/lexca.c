/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "lexca.h"
 
#include <stdio.h>
#include <string.h>

int lexca_test(const char *str)
{
	//yy_scan_string(str);
	struct yy_buffer_state * bufstate=yy_scan_string(str);

	yy_switch_to_buffer(bufstate);
	
	int ret=0;
	
	while((ret=yylex())!=0)
	{
		printf("%d %s \n",ret,yytext);
	}
	
	yy_delete_buffer(bufstate);
	
	//check if the syntax was carried out accordingly

	bufstate=yy_scan_string(str);

	yy_switch_to_buffer(bufstate);
	
	int error=yyparse();
	
	printf("\nThe syntax is: %s\n",!error?"good":"bad");
	
	yy_delete_buffer(bufstate);
	
	return error;
}

/**
	Replace a value in a function
	
	@param haystack
		the string to search in
	@param needle
		the thing you want to replace
	@param replace
		the thing you want to put there instead
	@return
		returns a new allocated string with replaced values
*/
char *lexca_replace_value(const char *haystack, const char *needle,const char *replace)
{
	const char *thisStr;
	char *output=NULL;

	size_t curLen=0,thisLen;
	int ret=0;

	struct yy_buffer_state * bufstate=yy_scan_string(haystack);
	
	yy_switch_to_buffer(bufstate);
	
	while((ret=yylex())!=0)
	{
		if(strcmp(yytext,needle)==0)
		{
			thisStr=replace;
		}
		else
			thisStr=yytext;
			
		thisLen=strlen(thisStr)+1;
		output=realloc(output,sizeof(char)*(thisLen+curLen));
		memcpy(output+curLen,thisStr,thisLen-1);
		
		curLen+=thisLen;
		output[curLen-1]=' ';
	}
	
	if(output)
		output[curLen-1]='\0';
		
	yy_delete_buffer(bufstate);
	//yypop_buffer_state();
	//yyterminate();
	
	return output;
}

int lexca_replace_at_pos(const char *str, size_t pos, const char *replace, size_t *output_len, char **output)
{
	int ret;
	
	FILE *output_fp=open_memstream(output,output_len);
	
	if(output_fp==NULL)
	{
		return 1;
	}
	
	struct yy_buffer_state * bufstate=yy_scan_string(str);
	
	yy_switch_to_buffer(bufstate);
	
	size_t i=0;
	
	while((ret=yylex())!=0)
	{
		if(i==pos)
		{
			fprintf(output_fp,"%s",replace);
		}
		else if(yytext)
		{
			fprintf(output_fp,"%s",yytext);
		}
		
		i++;
	}
	
	yy_delete_buffer(bufstate);
	//yypop_buffer_state();
	//yyterminate();
	
	fflush(output_fp);
	fclose(output_fp);
	
	return 0;
}

/**
	For each version
	
	@param str
		input string to parse
	@param funct
		function to run
	@param param
		parameter to the function
*/
int lexca_for_each(const char *str, int (*dofunct)(const char*,LexcaStrType,size_t,void*),void *param)
{
	int ret;

	struct yy_buffer_state * bufstate=yy_scan_string(str);
	
	yy_switch_to_buffer(bufstate);
	
	size_t i=0;
	
	while((ret=yylex())!=0)
	{
		if(dofunct(yytext,ret,i,param)!=0)
		{
			//fail
			yy_delete_buffer(bufstate);
			return 1;
		}
		i++;
	}
	
	yy_delete_buffer(bufstate);
	//yypop_buffer_state();
	//yyterminate();
	
	return 0;
}
