/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#pragma once

#ifndef LEXCA_HEADER
#define LEXCA_HEADER

#include "defs/parser.h"
#include "defs/lexer.h"

typedef enum yytokentype LexcaStrType;

int lexca_test(const char *str);

char *lexca_replace_value(const char *haystack, const char *needle,const char *replace);
int lexca_replace_at_pos(const char *str, size_t pos, const char *replace, size_t *output_len, char **output);

//void (*dofunct)(const char*,LexcaStrType,void*)
int lexca_for_each(const char *str, int (*dofunct)(const char*,LexcaStrType,size_t,void*),void *param);

#endif
