/*
	This is a modified version of http://www.quut.com/c/ANSI-C-grammar-y-2011.html

	The MIT License (MIT)
	
	Copyright (c) 1985 Jeff Lee
	Copyright (c) 2012 jutta@pobox.com
	Copyright (c) 2012 DAGwyn@aol.com
	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
%{
#include "defs/lexer.h"

void yyerror(const char *s)
{
	fflush(stdout);
	fprintf(stderr, "*** %s\n", s);
}
 
%}

%token	LEXCA_TYPE_IDENTIFIER LEXCA_TYPE_I_CONSTANT LEXCA_TYPE_F_CONSTANT LEXCA_TYPE_STRING_LITERAL LEXCA_TYPE_FUNC_NAME LEXCA_TYPE_SIZEOF
%token	LEXCA_TYPE_PTR_OP LEXCA_TYPE_INC_OP LEXCA_TYPE_DEC_OP LEXCA_TYPE_LEFT_OP LEXCA_TYPE_RIGHT_OP LEXCA_TYPE_LE_OP LEXCA_TYPE_GE_OP LEXCA_TYPE_EQ_OP LEXCA_TYPE_NE_OP
%token	LEXCA_TYPE_AND_OP LEXCA_TYPE_OR_OP LEXCA_TYPE_MUL_ASSIGN LEXCA_TYPE_DIV_ASSIGN LEXCA_TYPE_MOD_ASSIGN LEXCA_TYPE_ADD_ASSIGN
%token	LEXCA_TYPE_SUB_ASSIGN LEXCA_TYPE_LEFT_ASSIGN LEXCA_TYPE_RIGHT_ASSIGN LEXCA_TYPE_AND_ASSIGN
%token	LEXCA_TYPE_XOR_ASSIGN LEXCA_TYPE_OR_ASSIGN
%token	LEXCA_TYPE_TYPEDEF_NAME LEXCA_TYPE_ENUMERATION_CONSTANT

%token	LEXCA_TYPE_TYPEDEF LEXCA_TYPE_EXTERN LEXCA_TYPE_STATIC LEXCA_TYPE_AUTO LEXCA_TYPE_REGISTER LEXCA_TYPE_INLINE
%token	LEXCA_TYPE_CONST LEXCA_TYPE_RESTRICT LEXCA_TYPE_VOLATILE
%token	LEXCA_TYPE_BOOL LEXCA_TYPE_CHAR LEXCA_TYPE_SHORT LEXCA_TYPE_INT LEXCA_TYPE_LONG LEXCA_TYPE_SIGNED LEXCA_TYPE_UNSIGNED LEXCA_TYPE_FLOAT LEXCA_TYPE_DOUBLE LEXCA_TYPE_VOID
%token	LEXCA_TYPE_COMPLEX LEXCA_TYPE_IMAGINARY 
%token	LEXCA_TYPE_STRUCT LEXCA_TYPE_UNION LEXCA_TYPE_ENUM LEXCA_TYPE_ELLIPSIS

%token	LEXCA_TYPE_CASE LEXCA_TYPE_DEFAULT LEXCA_TYPE_IF LEXCA_TYPE_ELSE LEXCA_TYPE_SWITCH LEXCA_TYPE_WHILE LEXCA_TYPE_DO LEXCA_TYPE_FOR LEXCA_TYPE_GOTO LEXCA_TYPE_CONTINUE LEXCA_TYPE_BREAK LEXCA_TYPE_RETURN

%token	LEXCA_TYPE_ALIGNAS LEXCA_TYPE_ALIGNOF LEXCA_TYPE_ATOMIC LEXCA_TYPE_GENERIC LEXCA_TYPE_NORETURN LEXCA_TYPE_STATIC_ASSERT LEXCA_TYPE_THREAD_LOCAL

%start translation_unit
%%

primary_expression
	: LEXCA_TYPE_IDENTIFIER
	| constant
	| string
	| '(' expression ')'
	| generic_selection
	;

constant
	: LEXCA_TYPE_I_CONSTANT		/* includes character_constant */
	| LEXCA_TYPE_F_CONSTANT
	| LEXCA_TYPE_ENUMERATION_CONSTANT	/* after it has been defined as such */
	;

enumeration_constant		/* before it has been defined as such */
	: LEXCA_TYPE_IDENTIFIER
	;

string
	: LEXCA_TYPE_STRING_LITERAL
	| LEXCA_TYPE_FUNC_NAME
	;

generic_selection
	: LEXCA_TYPE_GENERIC '(' assignment_expression ',' generic_assoc_list ')'
	;

generic_assoc_list
	: generic_association
	| generic_assoc_list ',' generic_association
	;

generic_association
	: type_name ':' assignment_expression
	| LEXCA_TYPE_DEFAULT ':' assignment_expression
	;

postfix_expression
	: primary_expression
	| postfix_expression '[' expression ']'
	| postfix_expression '(' ')'
	| postfix_expression '(' argument_expression_list ')'
	| postfix_expression '.' LEXCA_TYPE_IDENTIFIER
	| postfix_expression LEXCA_TYPE_PTR_OP LEXCA_TYPE_IDENTIFIER
	| postfix_expression LEXCA_TYPE_INC_OP
	| postfix_expression LEXCA_TYPE_DEC_OP
	| '(' type_name ')' '{' initializer_list '}'
	| '(' type_name ')' '{' initializer_list ',' '}'
	;

argument_expression_list
	: assignment_expression
	| argument_expression_list ',' assignment_expression
	;

unary_expression
	: postfix_expression
	| LEXCA_TYPE_INC_OP unary_expression
	| LEXCA_TYPE_DEC_OP unary_expression
	| unary_operator cast_expression
	| LEXCA_TYPE_SIZEOF unary_expression
	| LEXCA_TYPE_SIZEOF '(' type_name ')'
	| LEXCA_TYPE_ALIGNOF '(' type_name ')'
	;

unary_operator
	: '&'
	| '*'
	| '+'
	| '-'
	| '~'
	| '!'
	;

cast_expression
	: unary_expression
	| '(' type_name ')' cast_expression
	;

multiplicative_expression
	: cast_expression
	| multiplicative_expression '*' cast_expression
	| multiplicative_expression '/' cast_expression
	| multiplicative_expression '%' cast_expression
	;

additive_expression
	: multiplicative_expression
	| additive_expression '+' multiplicative_expression
	| additive_expression '-' multiplicative_expression
	;

shift_expression
	: additive_expression
	| shift_expression LEXCA_TYPE_LEFT_OP additive_expression
	| shift_expression LEXCA_TYPE_RIGHT_OP additive_expression
	;

relational_expression
	: shift_expression
	| relational_expression '<' shift_expression
	| relational_expression '>' shift_expression
	| relational_expression LEXCA_TYPE_LE_OP shift_expression
	| relational_expression LEXCA_TYPE_GE_OP shift_expression
	;

equality_expression
	: relational_expression
	| equality_expression LEXCA_TYPE_EQ_OP relational_expression
	| equality_expression LEXCA_TYPE_NE_OP relational_expression
	;

and_expression
	: equality_expression
	| and_expression '&' equality_expression
	;

exclusive_or_expression
	: and_expression
	| exclusive_or_expression '^' and_expression
	;

inclusive_or_expression
	: exclusive_or_expression
	| inclusive_or_expression '|' exclusive_or_expression
	;

logical_and_expression
	: inclusive_or_expression
	| logical_and_expression LEXCA_TYPE_AND_OP inclusive_or_expression
	;

logical_or_expression
	: logical_and_expression
	| logical_or_expression LEXCA_TYPE_OR_OP logical_and_expression
	;

conditional_expression
	: logical_or_expression
	| logical_or_expression '?' expression ':' conditional_expression
	;

assignment_expression
	: conditional_expression
	| unary_expression assignment_operator assignment_expression
	;

assignment_operator
	: '='
	| LEXCA_TYPE_MUL_ASSIGN
	| LEXCA_TYPE_DIV_ASSIGN
	| LEXCA_TYPE_MOD_ASSIGN
	| LEXCA_TYPE_ADD_ASSIGN
	| LEXCA_TYPE_SUB_ASSIGN
	| LEXCA_TYPE_LEFT_ASSIGN
	| LEXCA_TYPE_RIGHT_ASSIGN
	| LEXCA_TYPE_AND_ASSIGN
	| LEXCA_TYPE_XOR_ASSIGN
	| LEXCA_TYPE_OR_ASSIGN
	;

expression
	: assignment_expression
	| expression ',' assignment_expression
	;

constant_expression
	: conditional_expression	/* with constraints */
	;

declaration
	: declaration_specifiers ';'
	| declaration_specifiers init_declarator_list ';'
	| static_assert_declaration
	;

declaration_specifiers
	: storage_class_specifier declaration_specifiers
	| storage_class_specifier
	| type_specifier declaration_specifiers
	| type_specifier
	| type_qualifier declaration_specifiers
	| type_qualifier
	| function_specifier declaration_specifiers
	| function_specifier
	| alignment_specifier declaration_specifiers
	| alignment_specifier
	;

init_declarator_list
	: init_declarator
	| init_declarator_list ',' init_declarator
	;

init_declarator
	: declarator '=' initializer
	| declarator
	;

storage_class_specifier
	: LEXCA_TYPE_TYPEDEF	/* identifiers must be flagged as TYPEDEF_NAME */
	| LEXCA_TYPE_EXTERN
	| LEXCA_TYPE_STATIC
	| LEXCA_TYPE_THREAD_LOCAL
	| LEXCA_TYPE_AUTO
	| LEXCA_TYPE_REGISTER
	;

type_specifier
	: LEXCA_TYPE_VOID {/*printf("void!\n");*/}
	| LEXCA_TYPE_CHAR
	| LEXCA_TYPE_SHORT
	| LEXCA_TYPE_INT
	| LEXCA_TYPE_LONG
	| LEXCA_TYPE_FLOAT
	| LEXCA_TYPE_DOUBLE
	| LEXCA_TYPE_SIGNED
	| LEXCA_TYPE_UNSIGNED
	| LEXCA_TYPE_BOOL
	| LEXCA_TYPE_COMPLEX
	| LEXCA_TYPE_IMAGINARY	  	/* non-mandated extension */
	| atomic_type_specifier
	| struct_or_union_specifier
	| enum_specifier
	| LEXCA_TYPE_TYPEDEF_NAME		/* after it has been defined as such */
	;

struct_or_union_specifier
	: struct_or_union '{' struct_declaration_list '}'
	| struct_or_union LEXCA_TYPE_IDENTIFIER '{' struct_declaration_list '}'
	| struct_or_union LEXCA_TYPE_IDENTIFIER
	;

struct_or_union
	: LEXCA_TYPE_STRUCT
	| LEXCA_TYPE_UNION
	;

struct_declaration_list
	: struct_declaration
	| struct_declaration_list struct_declaration
	;

struct_declaration
	: specifier_qualifier_list ';'	/* for anonymous struct/union */
	| specifier_qualifier_list struct_declarator_list ';'
	| static_assert_declaration
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list
	| type_specifier
	| type_qualifier specifier_qualifier_list
	| type_qualifier
	;

struct_declarator_list
	: struct_declarator
	| struct_declarator_list ',' struct_declarator
	;

struct_declarator
	: ':' constant_expression
	| declarator ':' constant_expression
	| declarator
	;

enum_specifier
	: LEXCA_TYPE_ENUM '{' enumerator_list '}'
	| LEXCA_TYPE_ENUM '{' enumerator_list ',' '}'
	| LEXCA_TYPE_ENUM LEXCA_TYPE_IDENTIFIER '{' enumerator_list '}'
	| LEXCA_TYPE_ENUM LEXCA_TYPE_IDENTIFIER '{' enumerator_list ',' '}'
	| LEXCA_TYPE_ENUM LEXCA_TYPE_IDENTIFIER
	;

enumerator_list
	: enumerator
	| enumerator_list ',' enumerator
	;

enumerator	/* identifiers must be flagged as ENUMERATION_CONSTANT */
	: enumeration_constant '=' constant_expression
	| enumeration_constant
	;

atomic_type_specifier
	: LEXCA_TYPE_ATOMIC '(' type_name ')'
	;

type_qualifier
	: LEXCA_TYPE_CONST
	| LEXCA_TYPE_RESTRICT
	| LEXCA_TYPE_VOLATILE
	| LEXCA_TYPE_ATOMIC
	;

function_specifier
	: LEXCA_TYPE_INLINE
	| LEXCA_TYPE_NORETURN
	;

alignment_specifier
	: LEXCA_TYPE_ALIGNAS '(' type_name ')'
	| LEXCA_TYPE_ALIGNAS '(' constant_expression ')'
	;

declarator
	: pointer direct_declarator
	| direct_declarator
	;

direct_declarator
	: LEXCA_TYPE_IDENTIFIER
	| '(' declarator ')'
	| direct_declarator '[' ']'
	| direct_declarator '[' '*' ']'
	| direct_declarator '[' LEXCA_TYPE_STATIC type_qualifier_list assignment_expression ']'
	| direct_declarator '[' LEXCA_TYPE_STATIC assignment_expression ']'
	| direct_declarator '[' type_qualifier_list '*' ']'
	| direct_declarator '[' type_qualifier_list LEXCA_TYPE_STATIC assignment_expression ']'
	| direct_declarator '[' type_qualifier_list assignment_expression ']'
	| direct_declarator '[' type_qualifier_list ']'
	| direct_declarator '[' assignment_expression ']'
	| direct_declarator '(' parameter_type_list ')'
	| direct_declarator '(' ')'
	| direct_declarator '(' identifier_list ')'
	;

pointer
	: '*' type_qualifier_list pointer
	| '*' type_qualifier_list
	| '*' pointer
	| '*'
	;

type_qualifier_list
	: type_qualifier
	| type_qualifier_list type_qualifier
	;


parameter_type_list
	: parameter_list ',' LEXCA_TYPE_ELLIPSIS
	| parameter_list
	;

parameter_list
	: parameter_declaration
	| parameter_list ',' parameter_declaration
	;

parameter_declaration
	: declaration_specifiers declarator
	| declaration_specifiers abstract_declarator
	| declaration_specifiers
	;

identifier_list
	: LEXCA_TYPE_IDENTIFIER
	| identifier_list ',' LEXCA_TYPE_IDENTIFIER
	;

type_name
	: specifier_qualifier_list abstract_declarator
	| specifier_qualifier_list
	;

abstract_declarator
	: pointer direct_abstract_declarator
	| pointer
	| direct_abstract_declarator
	;

direct_abstract_declarator
	: '(' abstract_declarator ')'
	| '[' ']'
	| '[' '*' ']'
	| '[' LEXCA_TYPE_STATIC type_qualifier_list assignment_expression ']'
	| '[' LEXCA_TYPE_STATIC assignment_expression ']'
	| '[' type_qualifier_list LEXCA_TYPE_STATIC assignment_expression ']'
	| '[' type_qualifier_list assignment_expression ']'
	| '[' type_qualifier_list ']'
	| '[' assignment_expression ']'
	| direct_abstract_declarator '[' ']'
	| direct_abstract_declarator '[' '*' ']'
	| direct_abstract_declarator '[' LEXCA_TYPE_STATIC type_qualifier_list assignment_expression ']'
	| direct_abstract_declarator '[' LEXCA_TYPE_STATIC assignment_expression ']'
	| direct_abstract_declarator '[' type_qualifier_list assignment_expression ']'
	| direct_abstract_declarator '[' type_qualifier_list LEXCA_TYPE_STATIC assignment_expression ']'
	| direct_abstract_declarator '[' type_qualifier_list ']'
	| direct_abstract_declarator '[' assignment_expression ']'
	| '(' ')'
	| '(' parameter_type_list ')'
	| direct_abstract_declarator '(' ')'
	| direct_abstract_declarator '(' parameter_type_list ')'
	;

initializer
	: '{' initializer_list '}'
	| '{' initializer_list ',' '}'
	| assignment_expression
	;

initializer_list
	: designation initializer
	| initializer
	| initializer_list ',' designation initializer
	| initializer_list ',' initializer
	;

designation
	: designator_list '='
	;

designator_list
	: designator
	| designator_list designator
	;

designator
	: '[' constant_expression ']'
	| '.' LEXCA_TYPE_IDENTIFIER
	;

static_assert_declaration
	: LEXCA_TYPE_STATIC_ASSERT '(' constant_expression ',' LEXCA_TYPE_STRING_LITERAL ')' ';'
	;

statement
	: labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
	;

labeled_statement
	: LEXCA_TYPE_IDENTIFIER ':' statement
	| LEXCA_TYPE_CASE constant_expression ':' statement
	| LEXCA_TYPE_DEFAULT ':' statement
	;

compound_statement
	: '{' '}'
	| '{'  block_item_list '}'
	;

block_item_list
	: block_item
	| block_item_list block_item
	;

block_item
	: declaration
	| statement
	;

expression_statement
	: ';'
	| expression ';'
	;

selection_statement
	: LEXCA_TYPE_IF '(' expression ')' statement LEXCA_TYPE_ELSE statement
	| LEXCA_TYPE_IF '(' expression ')' statement
	| LEXCA_TYPE_SWITCH '(' expression ')' statement
	;

iteration_statement
	: LEXCA_TYPE_WHILE '(' expression ')' statement
	| LEXCA_TYPE_DO statement LEXCA_TYPE_WHILE '(' expression ')' ';'
	| LEXCA_TYPE_FOR '(' expression_statement expression_statement ')' statement
	| LEXCA_TYPE_FOR '(' expression_statement expression_statement expression ')' statement
	| LEXCA_TYPE_FOR '(' declaration expression_statement ')' statement
	| LEXCA_TYPE_FOR '(' declaration expression_statement expression ')' statement
	;

jump_statement
	: LEXCA_TYPE_GOTO LEXCA_TYPE_IDENTIFIER ';'
	| LEXCA_TYPE_CONTINUE ';'
	| LEXCA_TYPE_BREAK ';'
	| LEXCA_TYPE_RETURN ';'
	| LEXCA_TYPE_RETURN expression ';'
	;

translation_unit
	: external_declaration
	| translation_unit external_declaration
	;

external_declaration
	: function_definition
	| declaration
	;

function_definition
	: declaration_specifiers declarator declaration_list compound_statement
	| declaration_specifiers declarator compound_statement
	;

declaration_list
	: declaration
	| declaration_list declaration
	;

