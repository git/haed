/*
	This is a modified version of http://www.quut.com/c/ANSI-C-grammar-l.html

	The MIT License (MIT)
	
	Copyright (c) 1985 Jeff Lee
	Copyright (c) 2012 jutta@pobox.com
	Copyright (c) 2012 DAGwyn@aol.com
	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
%e  1019
%p  2807
%n  371
%k  284
%a  1213
%o  1117

O   [0-7]
D   [0-9]
NZ  [1-9]
L   [a-zA-Z_]
A   [a-zA-Z_0-9]
H   [a-fA-F0-9]
HP  (0[xX])
E   ([Ee][+-]?{D}+)
P   ([Pp][+-]?{D}+)
FS  (f|F|l|L)
IS  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP  (u|U|L)
SP  (u8|u|U|L)
ES  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS  [ \t\v\n\f]

%{

#include <stdio.h>
#include "defs/parser.h"

void yyerror(const char *);  /* prints grammar violation message */

int sym_type(const char *);  /* returns type from symbol table */

#define sym_type(identifier) LEXCA_TYPE_IDENTIFIER /* with no symbol table, fake it */

static void comment(void);
static int check_type(void);
%}

%%
"/*"                                    { comment(); }
"//".*                                    { /* consume //-comment */ }

"auto"					{ return(LEXCA_TYPE_AUTO); }
"break"					{ return(LEXCA_TYPE_BREAK); }
"case"					{ return(LEXCA_TYPE_CASE); }
"char"					{ return(LEXCA_TYPE_CHAR); }
"const"					{ return(LEXCA_TYPE_CONST); }
"continue"				{ return(LEXCA_TYPE_CONTINUE); }
"default"				{ return(LEXCA_TYPE_DEFAULT); }
"do"					{ return(LEXCA_TYPE_DO); }
"double"				{ return(LEXCA_TYPE_DOUBLE); }
"else"					{ return(LEXCA_TYPE_ELSE); }
"enum"					{ return(LEXCA_TYPE_ENUM); }
"extern"				{ return(LEXCA_TYPE_EXTERN); }
"float"					{ return(LEXCA_TYPE_FLOAT); }
"for"					{ return(LEXCA_TYPE_FOR); }
"goto"					{ return(LEXCA_TYPE_GOTO); }
"if"					{ return(LEXCA_TYPE_IF); }
"inline"				{ return(LEXCA_TYPE_INLINE); }
"int"					{ return(LEXCA_TYPE_INT); }
"long"					{ return(LEXCA_TYPE_LONG); }
"register"				{ return(LEXCA_TYPE_REGISTER); }
"restrict"				{ return(LEXCA_TYPE_RESTRICT); }
"return"				{ return(LEXCA_TYPE_RETURN); }
"short"					{ return(LEXCA_TYPE_SHORT); }
"signed"				{ return(LEXCA_TYPE_SIGNED); }
"sizeof"				{ return(LEXCA_TYPE_SIZEOF); }
"static"				{ return(LEXCA_TYPE_STATIC); }
"struct"				{ return(LEXCA_TYPE_STRUCT); }
"switch"				{ return(LEXCA_TYPE_SWITCH); }
"typedef"				{ return(LEXCA_TYPE_TYPEDEF); }
"union"					{ return(LEXCA_TYPE_UNION); }
"unsigned"				{ return(LEXCA_TYPE_UNSIGNED); }
"void"					{ return(LEXCA_TYPE_VOID); }
"volatile"				{ return(LEXCA_TYPE_VOLATILE); }
"while"					{ return(LEXCA_TYPE_WHILE); }
"_Alignas"                              { return LEXCA_TYPE_ALIGNAS; }
"_Alignof"                              { return LEXCA_TYPE_ALIGNOF; }
"_Atomic"                               { return LEXCA_TYPE_ATOMIC; }
"_Bool"                                 { return LEXCA_TYPE_BOOL; }
"_Complex"                              { return LEXCA_TYPE_COMPLEX; }
"_Generic"                              { return LEXCA_TYPE_GENERIC; }
"_Imaginary"                            { return LEXCA_TYPE_IMAGINARY; }
"_Noreturn"                             { return LEXCA_TYPE_NORETURN; }
"_Static_assert"                        { return LEXCA_TYPE_STATIC_ASSERT; }
"_Thread_local"                         { return LEXCA_TYPE_THREAD_LOCAL; }
"__func__"                              { return LEXCA_TYPE_FUNC_NAME; }

{L}{A}*					{ return check_type(); }

{HP}{H}+{IS}?				{ return LEXCA_TYPE_I_CONSTANT; }
{NZ}{D}*{IS}?				{ return LEXCA_TYPE_I_CONSTANT; }
"0"{O}*{IS}?				{ return LEXCA_TYPE_I_CONSTANT; }
{CP}?"'"([^'\\\n]|{ES})+"'"		{ return LEXCA_TYPE_I_CONSTANT; }

{D}+{E}{FS}?				{ return LEXCA_TYPE_F_CONSTANT; }
{D}*"."{D}+{E}?{FS}?			{ return LEXCA_TYPE_F_CONSTANT; }
{D}+"."{E}?{FS}?			{ return LEXCA_TYPE_F_CONSTANT; }
{HP}{H}+{P}{FS}?			{ return LEXCA_TYPE_F_CONSTANT; }
{HP}{H}*"."{H}+{P}{FS}?			{ return LEXCA_TYPE_F_CONSTANT; }
{HP}{H}+"."{P}{FS}?			{ return LEXCA_TYPE_F_CONSTANT; }

({SP}?\"([^"\\\n]|{ES})*\"{WS}*)+	{ return LEXCA_TYPE_STRING_LITERAL; }

"..."					{ return LEXCA_TYPE_ELLIPSIS; }
">>="					{ return LEXCA_TYPE_RIGHT_ASSIGN; }
"<<="					{ return LEXCA_TYPE_LEFT_ASSIGN; }
"+="					{ return LEXCA_TYPE_ADD_ASSIGN; }
"-="					{ return LEXCA_TYPE_SUB_ASSIGN; }
"*="					{ return LEXCA_TYPE_MUL_ASSIGN; }
"/="					{ return LEXCA_TYPE_DIV_ASSIGN; }
"%="					{ return LEXCA_TYPE_MOD_ASSIGN; }
"&="					{ return LEXCA_TYPE_AND_ASSIGN; }
"^="					{ return LEXCA_TYPE_XOR_ASSIGN; }
"|="					{ return LEXCA_TYPE_OR_ASSIGN; }
">>"					{ return LEXCA_TYPE_RIGHT_OP; }
"<<"					{ return LEXCA_TYPE_LEFT_OP; }
"++"					{ return LEXCA_TYPE_INC_OP; }
"--"					{ return LEXCA_TYPE_DEC_OP; }
"->"					{ return LEXCA_TYPE_PTR_OP; }
"&&"					{ return LEXCA_TYPE_AND_OP; }
"||"					{ return LEXCA_TYPE_OR_OP; }
"<="					{ return LEXCA_TYPE_LE_OP; }
">="					{ return LEXCA_TYPE_GE_OP; }
"=="					{ return LEXCA_TYPE_EQ_OP; }
"!="					{ return LEXCA_TYPE_NE_OP; }
";"					{ return ';'; }
("{"|"<%")				{ return '{'; }
("}"|"%>")				{ return '}'; }
","					{ return ','; }
":"					{ return ':'; }
"="					{ return '='; }
"("					{ return '('; }
")"					{ return ')'; }
("["|"<:")				{ return '['; }
("]"|":>")				{ return ']'; }
"."					{ return '.'; }
"&"					{ return '&'; }
"!"					{ return '!'; }
"~"					{ return '~'; }
"-"					{ return '-'; }
"+"					{ return '+'; }
"*"					{ return '*'; }
"/"					{ return '/'; }
"%"					{ return '%'; }
"<"					{ return '<'; }
">"					{ return '>'; }
"^"					{ return '^'; }
"|"					{ return '|'; }
"?"					{ return '?'; }

{WS}					{ /* whitespace separates tokens */ }
.					{ /* discard bad characters */ }

%%

int yywrap(void)        /* called at end of input */
{
    return 1;           /* terminate now */
}

static void comment(void)
{
    int c;

    while ((c = input()) != 0)
        if (c == '*')
        {
            while ((c = input()) == '*')
                ;

            if (c == '/')
                return;

            if (c == 0)
                break;
        }
    yyerror("unterminated comment");
}

static int check_type(void)
{
    switch (sym_type(yytext))
    {
    case LEXCA_TYPE_TYPEDEF_NAME:                /* previously defined */
        return LEXCA_TYPE_TYPEDEF_NAME;
    case LEXCA_TYPE_ENUMERATION_CONSTANT:        /* previously defined */
        return LEXCA_TYPE_ENUMERATION_CONSTANT;
    default:                          /* includes undefined */
        return LEXCA_TYPE_IDENTIFIER;
    }
}

