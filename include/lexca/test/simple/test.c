/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <lexca.h>

GString *retStr=NULL;

char buf[100]={0};
LexcaStrType prevtype=0;

void lexca_test_for_each(const char *str, LexcaStrType type, void *param)
{
	printf("value found here: %s %d %s\n",str,type,(type=='(' && prevtype==LEXCA_TYPE_IDENTIFIER)?buf:"not a function!");
	
	if(!!strcmp(str,"int"))
		g_string_append(retStr,str);
	else
		g_string_append(retStr,"size_t");
	
	if(type==LEXCA_TYPE_IDENTIFIER)
		memcpy(buf,str,strlen(str));
	
	prevtype=type;
}

int main(void)
{
	const char *input="int hello=10;void hello(void){int a=0;if(a==0){a=a+a;}printf(\"im a mighty pirate\");}";
	const char *needle="hello";
	const char *replace="bye";
	
	retStr=g_string_sized_new(200);

	lexca_test(input);

	char *output=lexca_replace_value(input,needle,replace);
	//lets print that it was successful
	printf("input [%s] output [%s] needle [%s] replace[%s]\n",input,output,needle,replace);
	
	lexca_test(output);
	
	printf("testing for each ...\n");
	
	lexca_for_each(output,lexca_test_for_each,NULL);
	
	gchar *result=g_string_free(retStr,FALSE);
	printf("Resulting string:\n%s\n",result);
	
	free(output);
	g_free(result);
	
	return 0;
}
