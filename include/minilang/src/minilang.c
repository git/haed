/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "minilang.h"

//#include "etc.h"

/**
	Init the parsing
	
	@return
	returns a newly created parsing object
*/
HamsParser *hams_parse_init()
{
	return calloc(1,sizeof(HamsParser));
}

/**
	@deprecated byt till bison!
	Used to loop through a string, or a list of chars.
	This should support unicode chars (i hope :P)
	
	@param parser
		the parsing object, containig info of the parsing
	@param c
		a char
*/
void hams_parse_char(HamsParser *parser,char c)
{
	//printf("%c\n",c);
	
	if((c==',' || c=='\n' || c=='\0' || c=='(' || c==')' || c=='=') && parser->slashon==0 && parser->commenton==0)
	{
		if(c=='=' && parser->funct.amount>0 && strcmp(parser->funct.parameters[parser->funct.amount-1].name,"=")==0)
		{
			parser->funct.parameters[parser->funct.amount-1].name=realloc(parser->funct.parameters[parser->funct.amount-1].name,3);
			parser->funct.parameters[parser->funct.amount-1].name[1]='=';
			parser->funct.parameters[parser->funct.amount-1].name[2]='\0';
			
			return;
		}
		
		if(parser->parsewlen>0)
		{
			parser->funct.amount++;
			parser->funct.parameters=realloc(parser->funct.parameters,sizeof(HamsParameter)*parser->funct.amount);
			
			parser->funct.parameters[parser->funct.amount-1].name=realloc(parser->parsew,sizeof(char)*(parser->parsewlen+1));
			parser->funct.parameters[parser->funct.amount-1].name[parser->parsewlen]='\0';
			parser->funct.parameters[parser->funct.amount-1].type=HAMS_VARIABLE;
			
			//printf("%zu %s\n",parser->funct.amount,parser->funct.parameters[parser->funct.amount-1].name);
			
			parser->parsewlen=0;
			parser->parsew=NULL;
		}
		else if(c=='=')
		{
			parser->funct.amount++;
			parser->funct.parameters=realloc(parser->funct.parameters,sizeof(HamsParameter)*parser->funct.amount);
			
			parser->funct.parameters[parser->funct.amount-1].name=malloc(sizeof(char)*(2));
			parser->funct.parameters[parser->funct.amount-1].name[0]='=';
			parser->funct.parameters[parser->funct.amount-1].name[1]='\0';
			parser->funct.parameters[parser->funct.amount-1].type=HAMS_OPERATOR;
			
			parser->parsewlen=0;
			parser->parsew=NULL;
		}
		
/*		if(c=='(' || c==')')*/
/*		{*/
/*			parser->funct.amount++;*/
/*			parser->funct.parameters=realloc(parser->funct.parameters,sizeof(HamsParameter)*parser->funct.amount);*/
/*			*/
/*			parser->funct.parameters[parser->funct.amount-1].name=malloc(sizeof(char)*2);*/
/*			parser->funct.parameters[parser->funct.amount-1].name[0]=c;*/
/*			parser->funct.parameters[parser->funct.amount-1].name[1]='\0';*/
/*		}*/

		if(c=='(' && parser->funct.amount>0)
		{
			parser->funct.parameters[parser->funct.amount-1].type=HAMS_FUNCTION;
		}
	}
	else if(c=='\\' && parser->slashon==0)
	{
		parser->slashon=1;
	}
	else if(c=='\'' && parser->slashon==0)
	{
		parser->commenton^=1;
	}
	else if(c!=' ' && c!='\t')
	{
		if(parser->slashon==1)
			parser->slashon=0;
		
		//add c
		parser->parsewlen++;
		parser->parsew=realloc(parser->parsew,parser->parsewlen);
		parser->parsew[parser->parsewlen-1]=c;
	}
}

/**
	The default function for ending the parsing
	
	@param parser
		the parser to quote "destory"
	@return
		returns a HamsFunction
*/
HamsFunction *hams_parse_exit(HamsParser *parser)
{
	HamsFunction *ret=realloc(parser,sizeof(HamsFunction));
	
	return ret;
}

/**
	Dump the information from a HamsFunction in a correct manner
	
	@param func
		the function you want to dump the information from
	@return
		the string with the informations
*/
char *hams_dump_text(HamsFunction *func)
{
	char *finalstr=NULL;
	uint8_t mode=0;
	
	for(int i=0;i<func->amount;i++)
	{
		//b_tm("string %i %s",i,func->parameters[i].name);
		
		if(func->parameters[i].type==HAMS_FUNCTION)
		{
			finalstr=concata(finalstr,func->parameters[i].name,"(",NULL);
			
			mode=1;
			//if you only wanted the function
			if(i+1>=func->amount || func->parameters[i+1].type!=HAMS_VARIABLE)
			{
				finalstr=concata(finalstr,")",NULL);
				
				mode=0;
			}
		}
		else if(mode==1)
		{
			if(func->parameters[i].type==HAMS_VARIABLE)
			{
				if(func->parameters[i-1].type==HAMS_FUNCTION)
				{
					finalstr=concata(finalstr,func->parameters[i].name,NULL);
				}
				else
				{
					finalstr=concata(finalstr,", ",func->parameters[i].name,NULL);
				}
				
				if(i+1>=func->amount || func->parameters[i+1].type!=HAMS_VARIABLE)
				{
					finalstr=concata(finalstr,")",NULL);
					
					mode=0;
				}
			}
			else
			{
				b_error_message("SERIOUS ERROR!");
			}
		}
		else
		{
			if(i==0)
				finalstr=concata(finalstr,func->parameters[i].name,NULL);
			else
				finalstr=concata(finalstr," ",func->parameters[i].name,NULL);
		}
	}
	
	return finalstr;
	
}

void hams_destroy(HamsFunction *func)
{
	for(int i=0;i<func->amount;i++)
	{
		free(func->parameters[i].name);
	}
	
	free(func->parameters);
	free(func);
}

/**
	Create a Hamsfunction from a list of strings, like "COLLISION","OBJ1","OBJ2"
	
	@param str
		the first string
	@param __VA_LIST__
		a list of strings
	@return
		a cute HamsFunction
*/
HamsFunction *hams_vcreate(const char *str, va_list ap)
{
	const char *s;
	
	HamsFunction *ret=calloc(1,sizeof(HamsFunction));
	
	//va_start (ap, str);
	
	//Determine how much space we need. 
	for (s = str; s != NULL; s = va_arg (ap, const char *))
	{
		HamsTypes intype;
		
		ret->amount++;
		ret->parameters=realloc(ret->parameters,sizeof(HamsParameter)*ret->amount);
		ret->parameters[ret->amount-1].name=strndup(s,strlen(s));
		//b_tm("string in: %s",s);
		if((intype=va_arg(ap, HamsTypes)))
			ret->parameters[ret->amount-1].type=intype;
		else
		{
			b_error_message("Bad parameter!");
			return NULL;
		}
	}
	
	va_end (ap);
	
	return ret;
}

char *hams_get_from_type(HamsFunction *func, HamsTypes type, int pos)
{
	int thispos=0;

	for(int i=0;i<func->amount;i++)
	{
		if(func->parameters[i].type==type)
		{
			if(thispos>=pos)
			{
				return func->parameters[i].name;
			}
			else
				thispos++;
		}
	}
	
	if(thispos>0)
		b_error_message("You did not specify a correct position!");
	else
		b_error_message("Found nothing!!");
	
	return NULL;
}

HamsFunction *hams_create(const char *str, ...)
{
	va_list ap;
	va_start (ap, str);
	
	return hams_vcreate(str,ap);
}

char *hams_create_readable_string(const char *str, ...)
{
	va_list ap;
	va_start (ap, str);
	
	HamsFunction *tempf=hams_vcreate(str,ap);
	
	char *retstr=hams_dump_text(tempf);
	
	hams_destroy(tempf);
	
	//b_tm("TEST2 [%s]",retstr);
	
	return retstr;
}

/**
	Parses a string
	
	@param buffer
		a buffer to parse
*/
HamsFunction *hams_parse_text(const char *buffer)
{
	HamsParser *test=hams_parse_init();
	
	while(*buffer)
	{
		hams_parse_char(test,*buffer);
		buffer++;
	}
	
	hams_parse_char(test,'\0');
	
	return hams_parse_exit(test);
}
