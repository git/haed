/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
//conditions
#define HAMS_DEF_COLLISION "COLLISION"
#define HAMS_DEF_GET_AMOUNT "GET_AMOUNT"
#define HAMS_DEF_GET_TICKS "GET_TICKS"

//actions
#define HAMS_DEF_STOP "STOP"
#define HAMS_DEF_BOUNCE "BOUNCE"
#define HAMS_DEF_SET_X "SET_X"
#define HAMS_DEF_SET_Y "SET_Y"
#define HAMS_DEF_DESTROY "DESTROY"

//different types of configurations
typedef enum _HamsTriggerInfo
{
	HAMS_TRIGGER_FUNC,
	HAMS_TRIGGER_VALUE
}HamsTriggerInfo;

typedef struct _HamsTo
{
	const char *def;
	//int parameters;
	const char *to;
	HamsTriggerInfo inf;
}HamsTo;
