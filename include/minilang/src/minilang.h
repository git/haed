/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <blibc.h>

typedef enum HamsTypes
{
	HAMS_FUNCTION=1, //som typedef
	HAMS_VARIABLE=2, //som a
	HAMS_OPERATOR=3, //som +, * osv
	HAMS_END=4 // ;
}HamsTypes;

typedef struct _HamsParameter
{
	char *name;
	HamsTypes type;
}HamsParameter;

typedef struct _HamsFunction
{
	size_t amount;
	HamsParameter *parameters;
}HamsFunction;

typedef struct _HamsParser
{
	//public
	HamsFunction funct;
	
	//private
	uint8_t commenton;
	uint8_t slashon;
	
	char *parsew;
	size_t parsewlen;
}HamsParser;

HamsParser *hams_parse_init();

void hams_parse_char(HamsParser *parser,char c);

HamsFunction *hams_parse_exit(HamsParser *parser);

char *hams_dump_text(HamsFunction *func);

char *hams_get_from_type(HamsFunction *func, HamsTypes type, int pos);

HamsFunction *hams_vcreate(const char *str, va_list ap);
HamsFunction *hams_create(const char *str, ...);

HamsFunction *hams_parse_text(const char *buffer);

void hams_destroy(HamsFunction *func);

char *hams_create_readable_string(const char *str, ...);
