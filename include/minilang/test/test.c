/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <blibc.h>

#include "minilang.h"

int main()
{
	b_tm("RUNNING TEST...");

	//const char *teststr="(COLLISION OBJ1 OBJ2)";
	//const char *teststr="COLLISION OBJ1 OBJ2";
	
	const char *str=hams_create_readable_string("fun1",HAMS_FUNCTION,"var1",HAMS_VARIABLE,"var2",HAMS_VARIABLE,"==",HAMS_OPERATOR,"fun2",HAMS_FUNCTION,"var1",HAMS_VARIABLE,"var2",HAMS_VARIABLE,NULL);
	
	const char *str2=hams_create_readable_string("ANIMATION_ROLLER",HAMS_VARIABLE,"==",HAMS_OPERATOR,"123",HAMS_VARIABLE,NULL);
	
	printf("[%s]\n",str);
	
	printf("[%s]\n",str2);
	
	//printf("[%s]\n",hams_c_trigger_to_string(test));
	
	b_tm("DONE TEST 1, BEGINNING TEST 2:");
	
	const char *teststrsimp="GET_TICKS == 100";
	
	HamsFunction *test=hams_parse_text(teststrsimp);
	
	printf("Simple: [%s] Res:\n%s\n",teststrsimp,hams_dump_text(test));
	
	const char *teststrmedium="fun1(var1,var2) == fun2(var1,var2)";
	
	HamsFunction *test2=hams_parse_text(teststrmedium);
	
	printf("Simple: [%s] Res:\n%s\n",teststrmedium,hams_dump_text(test2));
	
	
	return 0;
}
