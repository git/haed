/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/

#include "includes.h"
//Haed program!//Program name
const char* PROGRAM_NAME="Program";

//Program screen width
const int SCREEN_WIDTH=1024;
//Program screen height
const int SCREEN_HEIGHT=768;

AOHaedP *hattis_init;
AOHaedP *hinder_init;
AOHaedP *mal_init;
AOHaedP *ao1431_init;
AOHaedP *ao14358_init;
AOHaedP *ao_15_6_27_11_6_11_init;

//temp temp temp
TTF_Font *font;
SDL_Surface *text;

Mix_Music *music = NULL;

Mix_Chunk *scratch = NULL;
Mix_Chunk *death = NULL;
Mix_Chunk *hitgoal = NULL;
Mix_Chunk *win = NULL;

//INPUT DEFINITIONS
SDL_Keycode player1Keys[]={SDLK_UP,SDLK_DOWN,SDLK_LEFT,SDLK_RIGHT,SDLK_RSHIFT,SDLK_z};
int Player1Input[6];
SDL_Keycode player2Keys[]={0xe4,SDLK_o,SDLK_a,SDLK_e,SDLK_2,SDLK_1};
int Player2Input[6];

//INIT RULES
void INIT_PROGRAM(void)
{
	animationvect hattis_AnimationVect;
	SDL_Rect ***hattis_AnimationSettings=create_rect_vector(&hattis_AnimationVect,"stopped","ALL_MIRROR",3,&(SDL_Rect){0,0,32,32},&(SDL_Rect){0,32,32,32},&(SDL_Rect){0,64,32,32},"running","ALL_MIRROR",3,&(SDL_Rect){0,0,32,32},&(SDL_Rect){0,32,32,32},&(SDL_Rect){0,64,32,32},"disappearing","ALL_MIRROR",3,&(SDL_Rect){0,0,32,32},&(SDL_Rect){0,32,32,32},&(SDL_Rect){0,64,32,32},NULL);
	hattis_init=init_ActiveObject("hattis",hattis_AnimationSettings,hattis_AnimationVect);

	hattis_init->speed[0]=2.500000;
	hattis_init->speed[1]=2.500000;
	hattis_init->accel[0]=0.500000;
	hattis_init->accel[1]=0.500000;
	hattis_init->deaccel[0]=0.500000;
	hattis_init->deaccel[1]=0.500000;
	
	animationvect hinder_AnimationVect;
	SDL_Rect ***hinder_AnimationSettings=create_rect_vector(&hinder_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,32,32},NULL);
	hinder_init=init_ActiveObject("hinder",hinder_AnimationSettings,hinder_AnimationVect);

	
	animationvect mal_AnimationVect;
	SDL_Rect ***mal_AnimationSettings=create_rect_vector(&mal_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,32,32},NULL);
	mal_init=init_ActiveObject("mal",mal_AnimationSettings,mal_AnimationVect);

	
	animationvect ao1431_AnimationVect;
	SDL_Rect ***ao1431_AnimationSettings=create_rect_vector(&ao1431_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,368,64},NULL);
	ao1431_init=init_ActiveObject("ao1431",ao1431_AnimationSettings,ao1431_AnimationVect);

	
	animationvect ao14358_AnimationVect;
	SDL_Rect ***ao14358_AnimationSettings=create_rect_vector(&ao14358_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,367,64},NULL);
	ao14358_init=init_ActiveObject("ao14358",ao14358_AnimationSettings,ao14358_AnimationVect);

	
	animationvect ao_15_6_27_11_6_11_AnimationVect;
	SDL_Rect ***ao_15_6_27_11_6_11_AnimationSettings=create_rect_vector(&ao_15_6_27_11_6_11_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,32,32},NULL);
	ao_15_6_27_11_6_11_init=init_ActiveObject("ao_15_6_27_11_6_11",ao_15_6_27_11_6_11_AnimationSettings,ao_15_6_27_11_6_11_AnimationVect);

	font = TTF_OpenFont("./ttftest/FreeSans.ttf", 24);
	
	music = Mix_LoadMUS("/home/caf2/Musik/Strauss/RadetzkyMarchStraus_TCD11.ogg");
	
	if(!music)
		printf("could not load music! %s\n",Mix_GetError());
	
	scratch = Mix_LoadWAV("/usr/share/hedgewars/Data/Sounds/whipcrack.ogg");
	
	if(!scratch)
		printf("could not load sound!\n");
		
	death = Mix_LoadWAV("/usr/share/hedgewars/Data/Sounds/Hellish.ogg");
	hitgoal = Mix_LoadWAV("./ttftest/mine_metal_03.ogg");
	win = Mix_LoadWAV("./ttftest/alarmvictory_1.ogg");
}

//HAPPENS ON INPUT
void INPUT_PROGRAM(SDL_Event *tempEvent)
{
	SetInputPlayer(Player1Input,player1Keys,tempEvent);
	SetInputPlayer(Player2Input,player2Keys,tempEvent);
}

void map1(void);
void New_level(void);
void New_level_3(void);
//WHERE ALL THE FUN STUFF HAPPENS...
void map1(void)
{
	HageUnit *hattis;
	HageUnit *hinder;
	HageUnit *mal;
	HageUnit *ao1431;
	HageUnit *ao14358;
	HageUnit *ao_15_6_27_11_6_11;
	if(LEVEL_START)
	{
		destroy_all(TOPLEVEL_UNIT);
		CREATE_OBJECT(mal_init,311,522);
		CREATE_OBJECT(mal_init,471,298);
		CREATE_OBJECT(mal_init,567,106);
		CREATE_OBJECT(mal_init,631,106);
		CREATE_OBJECT(mal_init,471,330);
		CREATE_OBJECT(mal_init,503,362);
		CREATE_OBJECT(mal_init,471,362);
		CREATE_OBJECT(mal_init,503,330);
		CREATE_OBJECT(mal_init,503,298);
		CREATE_OBJECT(mal_init,567,138);
		CREATE_OBJECT(mal_init,535,106);
		CREATE_OBJECT(mal_init,535,138);
		CREATE_OBJECT(mal_init,663,106);
		CREATE_OBJECT(mal_init,663,138);
		CREATE_OBJECT(mal_init,631,138);
		CREATE_OBJECT(mal_init,311,554);
		CREATE_OBJECT(mal_init,279,554);
		CREATE_OBJECT(mal_init,279,522);
		CREATE_OBJECT(mal_init,759,426);
		CREATE_OBJECT(mal_init,759,458);
		CREATE_OBJECT(mal_init,791,458);
		CREATE_OBJECT(mal_init,791,426);
		CREATE_OBJECT(mal_init,855,74);
		CREATE_OBJECT(mal_init,887,74);
		CREATE_OBJECT(mal_init,887,106);
		CREATE_OBJECT(mal_init,855,106);
		CREATE_OBJECT(hinder_init,247,362);
		CREATE_OBJECT(hinder_init,247,330);
		CREATE_OBJECT(hinder_init,247,394);
		CREATE_OBJECT(hinder_init,247,298);
		CREATE_OBJECT(hinder_init,247,266);
		CREATE_OBJECT(hinder_init,247,234);
		CREATE_OBJECT(hinder_init,343,394);
		CREATE_OBJECT(hinder_init,343,362);
		CREATE_OBJECT(hinder_init,343,330);
		CREATE_OBJECT(hinder_init,343,298);
		CREATE_OBJECT(hinder_init,343,266);
		CREATE_OBJECT(hinder_init,343,234);
		CREATE_OBJECT(hinder_init,343,202);
		CREATE_OBJECT(hinder_init,343,170);
		CREATE_OBJECT(hinder_init,439,170);
		CREATE_OBJECT(hinder_init,407,170);
		CREATE_OBJECT(hinder_init,375,170);
		CREATE_OBJECT(hinder_init,471,170);
		CREATE_OBJECT(hinder_init,503,170);
		CREATE_OBJECT(hinder_init,535,170);
		CREATE_OBJECT(hinder_init,567,170);
		CREATE_OBJECT(hinder_init,599,138);
		CREATE_OBJECT(hinder_init,599,170);
		CREATE_OBJECT(hinder_init,695,170);
		CREATE_OBJECT(hinder_init,727,170);
		CREATE_OBJECT(hinder_init,727,202);
		CREATE_OBJECT(hinder_init,727,234);
		CREATE_OBJECT(hinder_init,727,266);
		CREATE_OBJECT(hinder_init,727,298);
		CREATE_OBJECT(hinder_init,727,362);
		CREATE_OBJECT(hinder_init,695,394);
		CREATE_OBJECT(hinder_init,599,394);
		CREATE_OBJECT(hinder_init,663,394);
		CREATE_OBJECT(hinder_init,567,394);
		CREATE_OBJECT(hinder_init,535,394);
		CREATE_OBJECT(hinder_init,503,394);
		CREATE_OBJECT(hinder_init,439,362);
		CREATE_OBJECT(hinder_init,439,330);
		CREATE_OBJECT(hinder_init,439,298);
		CREATE_OBJECT(hinder_init,439,266);
		CREATE_OBJECT(hinder_init,503,266);
		CREATE_OBJECT(hinder_init,535,266);
		CREATE_OBJECT(hinder_init,567,266);
		CREATE_OBJECT(hinder_init,599,266);
		CREATE_OBJECT(hinder_init,631,266);
		CREATE_OBJECT(hinder_init,631,298);
		CREATE_OBJECT(hinder_init,407,490);
		CREATE_OBJECT(hinder_init,439,490);
		CREATE_OBJECT(hinder_init,471,490);
		CREATE_OBJECT(hinder_init,503,490);
		CREATE_OBJECT(hinder_init,535,490);
		CREATE_OBJECT(hinder_init,567,490);
		CREATE_OBJECT(hinder_init,599,490);
		CREATE_OBJECT(hinder_init,791,490);
		CREATE_OBJECT(hinder_init,823,490);
		CREATE_OBJECT(hinder_init,823,458);
		CREATE_OBJECT(hinder_init,823,426);
		CREATE_OBJECT(hinder_init,823,330);
		CREATE_OBJECT(hinder_init,823,298);
		CREATE_OBJECT(hinder_init,823,266);
		CREATE_OBJECT(hinder_init,823,170);
		CREATE_OBJECT(hinder_init,823,106);
		CREATE_OBJECT(hinder_init,695,74);
		CREATE_OBJECT(hinder_init,663,74);
		CREATE_OBJECT(hinder_init,631,74);
		CREATE_OBJECT(hinder_init,599,74);
		CREATE_OBJECT(hinder_init,567,74);
		CREATE_OBJECT(hinder_init,535,74);
		CREATE_OBJECT(hinder_init,503,74);
		CREATE_OBJECT(hinder_init,471,74);
		CREATE_OBJECT(hinder_init,439,74);
		CREATE_OBJECT(hinder_init,343,74);
		CREATE_OBJECT(hinder_init,311,74);
		CREATE_OBJECT(hinder_init,279,74);
		CREATE_OBJECT(hinder_init,247,74);
		CREATE_OBJECT(hinder_init,247,106);
		CREATE_OBJECT(hinder_init,247,426);
		CREATE_OBJECT(hinder_init,247,458);
		CREATE_OBJECT(hinder_init,247,490);
		CREATE_OBJECT(hinder_init,247,522);
		CREATE_OBJECT(hinder_init,247,554);
		CREATE_OBJECT(hinder_init,247,586);
		CREATE_OBJECT(hinder_init,279,586);
		CREATE_OBJECT(hinder_init,503,586);
		CREATE_OBJECT(hinder_init,535,586);
		CREATE_OBJECT(hinder_init,631,586);
		CREATE_OBJECT(hinder_init,663,586);
		CREATE_OBJECT(hinder_init,695,586);
		CREATE_OBJECT(hinder_init,727,586);
		CREATE_OBJECT(hinder_init,759,586);
		CREATE_OBJECT(hinder_init,791,586);
		CREATE_OBJECT(hinder_init,823,586);
		CREATE_OBJECT(hinder_init,919,458);
		CREATE_OBJECT(hinder_init,919,426);
		CREATE_OBJECT(hinder_init,919,394);
		CREATE_OBJECT(hinder_init,919,362);
		CREATE_OBJECT(hinder_init,919,330);
		CREATE_OBJECT(hinder_init,919,298);
		CREATE_OBJECT(hinder_init,919,266);
		CREATE_OBJECT(hinder_init,919,234);
		CREATE_OBJECT(hinder_init,919,202);
		CREATE_OBJECT(hinder_init,919,170);
		CREATE_OBJECT(hinder_init,375,74);
		CREATE_OBJECT(hinder_init,407,74);
		CREATE_OBJECT(hinder_init,599,106);
		CREATE_OBJECT(hinder_init,311,586);
		CREATE_OBJECT(hinder_init,407,586);
		CREATE_OBJECT(hinder_init,439,586);
		CREATE_OBJECT(hinder_init,471,586);
		CREATE_OBJECT(hinder_init,567,586);
		CREATE_OBJECT(hinder_init,599,586);
		CREATE_OBJECT(hinder_init,855,586);
		CREATE_OBJECT(hinder_init,887,586);
		CREATE_OBJECT(hinder_init,919,586);
		CREATE_OBJECT(hinder_init,919,554);
		CREATE_OBJECT(hinder_init,919,522);
		CREATE_OBJECT(hinder_init,919,490);
		CREATE_OBJECT(hinder_init,247,138);
		CREATE_OBJECT(hinder_init,727,74);
		CREATE_OBJECT(hinder_init,759,74);
		CREATE_OBJECT(hinder_init,791,74);
		CREATE_OBJECT(hinder_init,823,74);
		CREATE_OBJECT(hinder_init,919,138);
		CREATE_OBJECT(hinder_init,919,106);
		CREATE_OBJECT(hinder_init,919,74);
		CREATE_OBJECT(hinder_init,823,138);
		CREATE_OBJECT(hinder_init,823,202);
		CREATE_OBJECT(hinder_init,823,234);
		CREATE_OBJECT(hinder_init,727,490);
		CREATE_OBJECT(hinder_init,759,490);
		CREATE_OBJECT(hinder_init,695,490);
		CREATE_OBJECT(hinder_init,663,490);
		CREATE_OBJECT(hinder_init,631,490);
		CREATE_OBJECT(hinder_init,375,490);
		CREATE_OBJECT(hinder_init,343,490);
		CREATE_OBJECT(hinder_init,631,170);
		CREATE_OBJECT(hinder_init,663,170);
		CREATE_OBJECT(hinder_init,727,330);
		CREATE_OBJECT(hinder_init,727,394);
		CREATE_OBJECT(hinder_init,631,394);
		CREATE_OBJECT(hinder_init,471,394);
		CREATE_OBJECT(hinder_init,439,394);
		CREATE_OBJECT(hinder_init,471,266);
		CREATE_OBJECT(ao1431_init,1294,693);
		CREATE_OBJECT(ao14358_init,1304,708);
		CREATE_OBJECT(hattis_init,128,190);
		LEVEL_START=0;
		
		if( Mix_PlayingMusic() == 0 )
		{
			if(Mix_PlayMusic( music, -1 )==-1)printf("ERROR!\n");
		}
	}
	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format,140,255,255));
	
	//for SDL_ttf
	
	SDL_Color text_color = {100, 100, 100};
	char str[33];
	sprintf(str,"%d",2000-ANIMATION_ROLLER);
   text = TTF_RenderText_Solid(font,str, text_color);
	apply_surface(0,0,text, screen, NULL);
	SDL_FreeSurface(text);
	
	//for SDL_ttf
	
	hage_move_objects(hattis_init,Player1Input);
	FOR_EACH_START(hattis){FOR_EACH_START(hinder){do{do{
		if(COLLISION(hattis,hinder))
		{
			STOP(hattis);
			SET_X(hattis,0);
			Mix_PlayChannel( -1, scratch, 0 );
			printf("again!\n");
		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(hinder);}}
	FOR_EACH_START(hattis){FOR_EACH_START(mal){do{do{
		if(COLLISION(hattis,mal))
		{
			STOP(hattis);
			DESTROY_OBJECT(mal);
			Mix_PlayChannel( -1, hitgoal, 0 );
		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(mal);}}
	FOR_EACH_START(hattis){FOR_EACH_START(ao1431){FOR_EACH_START(ao14358){do{do{do{
		if(COUNT(mal)==0)
		{
			DESTROY_OBJECT(hattis);
			SET_X(ao1431,0);
			SET_Y(ao1431,0);
			DESTROY_OBJECT(ao14358);
			SET_LEVEL(New_level);
			Mix_PlayChannel( -1, win, 0 );
		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(ao1431);}FOR_EACH_OBJECT(ao14358);}}}
	FOR_EACH_START(ao14358){FOR_EACH_START(hattis){FOR_EACH_START(ao1431){do{do{do{
		if(ANIMATION_ROLLER==2000)
		{
			SET_X(ao14358,0);
			SET_Y(ao14358,0);
			DESTROY_OBJECT(hattis);
			DESTROY_OBJECT(ao1431);
			Mix_PlayChannel( -1, death, 0 );
		}
	}FOR_EACH_OBJECT(ao14358);}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(ao1431);}}}

	hage_apply_movement(hattis_init);
}
//WHERE ALL THE FUN STUFF HAPPENS...
void New_level(void)
{
	HageUnit *hattis;
	HageUnit *hinder;
	HageUnit *mal;
	HageUnit *ao1431;
	HageUnit *ao14358;
	HageUnit *ao_15_6_27_11_6_11;
	if(LEVEL_START)
	{
		destroy_all(TOPLEVEL_UNIT);
		CREATE_OBJECT(hattis_init,402,190);
		CREATE_OBJECT(hinder_init,327,190);
		CREATE_OBJECT(hinder_init,359,254);
		CREATE_OBJECT(hinder_init,327,222);
		CREATE_OBJECT(hinder_init,423,254);
		CREATE_OBJECT(hinder_init,423,126);
		CREATE_OBJECT(hinder_init,391,126);
		CREATE_OBJECT(hinder_init,359,126);
		CREATE_OBJECT(hinder_init,327,158);
		CREATE_OBJECT(hinder_init,391,254);
		CREATE_OBJECT(hinder_init,455,94);
		CREATE_OBJECT(hinder_init,455,286);
		CREATE_OBJECT(mal_init,832,260);
		CREATE_OBJECT(mal_init,428,576);
		CREATE_OBJECT(mal_init,112,281);
		CREATE_OBJECT(mal_init,112,249);
		CREATE_OBJECT(mal_init,832,228);
		CREATE_OBJECT(mal_init,689,497);
		CREATE_OBJECT(mal_init,657,497);
		CREATE_OBJECT(mal_init,396,576);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,800,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,832,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,260);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,228);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,832,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,800,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,768,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,768,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,363,361);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,487,518);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,490,408);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,724,409);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,902,603);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,656,580);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,584,422);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,776,124);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,670,221);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,554,295);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,579,172);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,278,102);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,234,237);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,80,131);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,65,373);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,146,477);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,219,557);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,272,433);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,181,357);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,350,548);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,495,622);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,866,480);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,921,351);
		CREATE_OBJECT(mal_init,290,333);
		CREATE_OBJECT(mal_init,101,573);
		CREATE_OBJECT(mal_init,803,602);
		CREATE_OBJECT(mal_init,676,318);
		CREATE_OBJECT(mal_init,660,61);
		CREATE_OBJECT(mal_init,170,32);
		LEVEL_START=0;
	}
	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format,255,255,255));
	hage_move_objects(hattis_init,Player1Input);
	FOR_EACH_START(hattis){FOR_EACH_START(hinder){do{do{
		if(COLLISION(hattis, hinder))
		{
			STOP(hattis);
			Mix_PlayChannel( -1, scratch, 0 );

		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(hinder);}}
	FOR_EACH_START(hattis){FOR_EACH_START(mal){do{do{
		if(COLLISION(hattis, mal))
		{
			DESTROY(mal);
			Mix_PlayChannel( -1, hitgoal, 0 );

		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(mal);}}
	
		if(COUNT(mal) == 0)
		{
			SET_LEVEL(New_level_3);
			Mix_PlayChannel( -1, win, 0 );

		}
	
	FOR_EACH_START(hattis){FOR_EACH_START(ao_15_6_27_11_6_11){do{do{
		if(COLLISION(hattis, ao_15_6_27_11_6_11))
		{
			SET_LEVEL(map1);
			Mix_PlayChannel( -1, death, 0 );

		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(ao_15_6_27_11_6_11);}}
	
		if(ANIMATION_ROLLER == 4000)
		{
			SET_LEVEL(map1);
			Mix_PlayChannel( -1, death, 0 );

		}
	

	hage_apply_movement(hattis_init);
}
//WHERE ALL THE FUN STUFF HAPPENS...
void New_level_3(void)
{
	HageUnit *hattis;
	HageUnit *hinder;
	HageUnit *mal;
	HageUnit *ao1431;
	HageUnit *ao14358;
	HageUnit *ao_15_6_27_11_6_11;
	if(LEVEL_START)
	{
		destroy_all(TOPLEVEL_UNIT);
		CREATE_OBJECT(ao1431_init,235,54);
		CREATE_OBJECT(ao14358_init,190,119);
		CREATE_OBJECT(hattis_init,342,246);
		CREATE_OBJECT(hinder_init,328,309);
		CREATE_OBJECT(hinder_init,264,277);
		CREATE_OBJECT(hinder_init,264,213);
		CREATE_OBJECT(hinder_init,424,245);
		CREATE_OBJECT(hinder_init,392,309);
		CREATE_OBJECT(hinder_init,360,309);
		CREATE_OBJECT(hinder_init,296,309);
		CREATE_OBJECT(hinder_init,264,245);
		CREATE_OBJECT(hinder_init,342,181);
		CREATE_OBJECT(hinder_init,424,277);
		CREATE_OBJECT(hinder_init,424,213);
		LEVEL_START=0;
	}
	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format,255,255,255));
	hage_move_objects(hattis_init,Player1Input);
	FOR_EACH_START(hattis){FOR_EACH_START(hinder){do{do{
		if(COLLISION(hattis, hinder))
		{
			STOP(hattis);

		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(hinder);}}

	hage_apply_movement(hattis_init);
}


void *FIRST_LEVEL=map1;
