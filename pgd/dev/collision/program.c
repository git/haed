/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/

#include "includes.h"
//Haed program!//Program name
const char* PROGRAM_NAME="Program";

//Program screen width
const int SCREEN_WIDTH=1024;
//Program screen height
const int SCREEN_HEIGHT=768;

AOHaedP *hattis_init;
AOHaedP *hinder_init;
AOHaedP *mal_init;
AOHaedP *ao1431_init;
AOHaedP *ao14358_init;
AOHaedP *ao_15_6_27_11_6_11_init;

//INPUT DEFINITIONS
SDL_Keycode player1Keys[]={SDLK_UP,SDLK_DOWN,SDLK_LEFT,SDLK_RIGHT,SDLK_RSHIFT,SDLK_z};
int Player1Input[6];
SDL_Keycode player2Keys[]={0xe4,SDLK_o,SDLK_a,SDLK_e,SDLK_2,SDLK_1};
int Player2Input[6];

//INIT RULES
void INIT_PROGRAM(void)
{
	animationvect hattis_AnimationVect;
	SDL_Rect ***hattis_AnimationSettings=create_rect_vector(&hattis_AnimationVect,"stopped","ALL_MIRROR",3,&(SDL_Rect){0,0,32,32},&(SDL_Rect){0,32,32,32},&(SDL_Rect){0,64,32,32},"running","ALL_MIRROR",3,&(SDL_Rect){0,0,32,32},&(SDL_Rect){0,32,32,32},&(SDL_Rect){0,64,32,32},"disappearing","ALL_MIRROR",3,&(SDL_Rect){0,0,32,32},&(SDL_Rect){0,32,32,32},&(SDL_Rect){0,64,32,32},NULL);
	hattis_init=init_ActiveObject("hattis",hattis_AnimationSettings,hattis_AnimationVect);

	hattis_init->speed[0]=2.500000;
	hattis_init->speed[1]=2.500000;
	hattis_init->accel[0]=0.500000;
	hattis_init->accel[1]=0.500000;
	hattis_init->deaccel[0]=0.500000;
	hattis_init->deaccel[1]=0.500000;
	
	animationvect hinder_AnimationVect;
	SDL_Rect ***hinder_AnimationSettings=create_rect_vector(&hinder_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,32,32},NULL);
	hinder_init=init_ActiveObject("hinder",hinder_AnimationSettings,hinder_AnimationVect);

	
	animationvect mal_AnimationVect;
	SDL_Rect ***mal_AnimationSettings=create_rect_vector(&mal_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,32,32},NULL);
	mal_init=init_ActiveObject("mal",mal_AnimationSettings,mal_AnimationVect);

	
	animationvect ao1431_AnimationVect;
	SDL_Rect ***ao1431_AnimationSettings=create_rect_vector(&ao1431_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,368,64},NULL);
	ao1431_init=init_ActiveObject("ao1431",ao1431_AnimationSettings,ao1431_AnimationVect);

	
	animationvect ao14358_AnimationVect;
	SDL_Rect ***ao14358_AnimationSettings=create_rect_vector(&ao14358_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,367,64},NULL);
	ao14358_init=init_ActiveObject("ao14358",ao14358_AnimationSettings,ao14358_AnimationVect);

	
	animationvect ao_15_6_27_11_6_11_AnimationVect;
	SDL_Rect ***ao_15_6_27_11_6_11_AnimationSettings=create_rect_vector(&ao_15_6_27_11_6_11_AnimationVect,"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,32,32},NULL);
	ao_15_6_27_11_6_11_init=init_ActiveObject("ao_15_6_27_11_6_11",ao_15_6_27_11_6_11_AnimationSettings,ao_15_6_27_11_6_11_AnimationVect);

	
}

//HAPPENS ON INPUT
void INPUT_PROGRAM(SDL_Event *tempEvent)
{
	SetInputPlayer(Player1Input,player1Keys,tempEvent);
	SetInputPlayer(Player2Input,player2Keys,tempEvent);
}

void New_level(void);
void *LEVEL_VECTOR[] = {New_level};
//WHERE ALL THE FUN STUFF HAPPENS...
void New_level(void)
{
	HageUnit *hattis;
	HageUnit *hinder;
	HageUnit *mal;
	HageUnit *ao1431;
	HageUnit *ao14358;
	HageUnit *ao_15_6_27_11_6_11;
	if(LEVEL_START)
	{
		destroy_all(TOPLEVEL_UNIT);
		CREATE_OBJECT(hattis_init,402,190);
		CREATE_OBJECT(hinder_init,327,190);
		CREATE_OBJECT(hinder_init,359,254);
		CREATE_OBJECT(hinder_init,327,222);
		CREATE_OBJECT(hinder_init,423,254);
		CREATE_OBJECT(hinder_init,423,126);
		CREATE_OBJECT(hinder_init,391,126);
		CREATE_OBJECT(hinder_init,359,126);
		CREATE_OBJECT(hinder_init,327,158);
		CREATE_OBJECT(hinder_init,391,254);
		CREATE_OBJECT(hinder_init,455,94);
		CREATE_OBJECT(hinder_init,455,286);
		CREATE_OBJECT(mal_init,832,260);
		CREATE_OBJECT(mal_init,428,576);
		CREATE_OBJECT(mal_init,112,281);
		CREATE_OBJECT(mal_init,112,249);
		CREATE_OBJECT(mal_init,832,228);
		CREATE_OBJECT(mal_init,689,497);
		CREATE_OBJECT(mal_init,657,497);
		CREATE_OBJECT(mal_init,396,576);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,800,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,832,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,260);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,228);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,864,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,832,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,800,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,768,196);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,768,292);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,363,361);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,487,518);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,490,408);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,724,409);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,902,603);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,656,580);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,584,422);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,776,124);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,670,221);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,554,295);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,579,172);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,278,102);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,234,237);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,80,131);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,65,373);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,146,477);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,219,557);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,272,433);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,181,357);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,350,548);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,495,622);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,866,480);
		CREATE_OBJECT(ao_15_6_27_11_6_11_init,921,351);
		CREATE_OBJECT(mal_init,290,333);
		CREATE_OBJECT(mal_init,101,573);
		CREATE_OBJECT(mal_init,803,602);
		CREATE_OBJECT(mal_init,676,318);
		CREATE_OBJECT(mal_init,660,61);
		CREATE_OBJECT(mal_init,170,32);
		LEVEL_START=0;
	}
	
	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format,78,154,6));
	
	
	hage_move_objects(hattis_init,Player1Input);
	
	hage_apply_movement(hattis_init);
	
	FOR_EACH_START(hattis){FOR_EACH_START(hinder){do{do{
		if(COLLISION_TRACK(hattis, hinder))
		{
			STOP(hattis);
		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(hinder);}}
	FOR_EACH_START(hattis){FOR_EACH_START(mal){do{do{

		if(COLLISION(hattis, mal))
		{
			DESTROY(mal);

		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(mal);}}
	
		if(COUNT(mal) == 0)
		{
			//SET_LEVEL(New_level_3);

		}
	
	FOR_EACH_START(hattis){FOR_EACH_START(ao_15_6_27_11_6_11){do{do{
		if(COLLISION(hattis, ao_15_6_27_11_6_11))
		{
			//SET_LEVEL(map1);
		}
	}FOR_EACH_OBJECT(hattis);}FOR_EACH_OBJECT(ao_15_6_27_11_6_11);}}
	
		if(ANIMATION_ROLLER == 4000)
		{
			//SET_LEVEL(map1);

		}
}


void *FIRST_LEVEL=New_level;
