/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "includes.h"

//create a active object of a sort
//temp coords={x,y,(z)}
void hage_unit_add_unit(HageUnit **firstUnitP,void *unitparent,int x,int y)
{
	HageUnit *firstUnit=*firstUnitP;
	
	HageUnit *newunit=calloc(1,sizeof(HageUnit));
	
	//1bit(1)=controllable,2bit(2)=showable,3bit(4)=on
	newunit->on=1;
	newunit->showable=1;
	newunit->controllable=1;
	newunit->type=unitparent;
	
	newunit->coord[0]=x;
	newunit->coord[1]=y;
	
	if(firstUnit==NULL)
	{
		//b_tm("NEW FIRST UNIT");
		
		newunit->prevUnit=newunit;
		newunit->nextUnit=newunit;
		
		(*firstUnitP)=newunit;
	}
	else
	{
		newunit->prevUnit=firstUnit->prevUnit;
		newunit->nextUnit=firstUnit;
		firstUnit->prevUnit->nextUnit=newunit;
		firstUnit->prevUnit=newunit;
	}
	
	//do this better
	AOHaedP *tempUnit=unitparent;
	
	HageUnit *firstSUnit=tempUnit->firstunit;
	
	if(firstSUnit==NULL)
	{
		newunit->prevSUnit=newunit;
		newunit->nextSUnit=newunit;
		
		tempUnit->firstunit=newunit;
	}
	else
	{
		newunit->prevSUnit=firstSUnit->prevSUnit;
		newunit->nextSUnit=firstSUnit;
		firstSUnit->prevSUnit->nextSUnit=newunit;
		firstSUnit->prevSUnit=newunit;
	}
	//b_tm("ADD AMOUNT %d %s",hage_object_get_unit_amount(unitparent),unitparent->name);
}

#define create_ActiveObject(obj,coords) hage_unit_add_unit(TOPLEVEL_UNIT,obj,coords[0],coords[1])

void hage_unit_destroy_unit(HageUnit **firstUnit,HageUnit *unittodestroy)
{
	if(!unittodestroy)
		return;
	
	HageUnit *firstunit=*firstUnit;
	
	if(firstunit==NULL)
	{
		b_error_message("The first unit is already destroyed!");
		return;
	}
	
	if(firstunit==unittodestroy)
	{
		if(firstunit->nextUnit!=firstunit)
			(*firstUnit)=firstunit->nextUnit;
		else
			(*firstUnit)=NULL;
	}
		
	unittodestroy->prevSUnit->nextSUnit=unittodestroy->nextSUnit;
	unittodestroy->nextSUnit->prevSUnit=unittodestroy->prevSUnit;
	unittodestroy->prevUnit->nextUnit=unittodestroy->nextUnit;
	unittodestroy->nextUnit->prevUnit=unittodestroy->prevUnit;
	
	AOHaedP *parentobject=unittodestroy->type;
	
	if(parentobject->firstunit==unittodestroy)
	{
		if(unittodestroy->nextSUnit!=unittodestroy)
			parentobject->firstunit=unittodestroy->nextSUnit;
		else
			parentobject->firstunit=NULL;
	}
	
	free(unittodestroy);
}

/**
	Destroy all units
	
	@param firstUnit
		the topmost unit
*/
void destroy_all(HageUnit **firstUnit)
{
	HageUnit *firstU=*firstUnit;
	HageUnit *loopUnit=firstU;
	HageUnit *sloopUnit;
	
	if(!firstU)
		return;
		
	do
	{
		sloopUnit=loopUnit->nextUnit;
	
		AOHaedP *parentobject=loopUnit->type;
		
		parentobject->firstunit=NULL;
		free(loopUnit);
	
		loopUnit=sloopUnit;
	}while(loopUnit!=firstU);

	*firstUnit=NULL;
}

/**
	destroys the unit, and returns the next unit. If such object does not exist, then return null.
	
	@param firstUnit
		the first unit in the linked list (not the unit to destroy)
	@param unittodestroy
		the unit you want to destroy
	@returns
		the next unit
*/
HageUnit *hage_unit_destroy_give_next(HageUnit **firstUnit,HageUnit *unittodestroy)
{
	HageUnit *nextunit;
	
	if(unittodestroy->nextUnit==unittodestroy)
	{
		nextunit=NULL;
	}
	else
	{
		nextunit=unittodestroy->nextUnit;
	}
	
	hage_unit_destroy_unit(firstUnit,unittodestroy);
	
	return nextunit;
}

/**
	destroys the unit, and returns the next unit. If such object does not exist, then return null.
	
	@param firstUnit
		the first unit in the linked list (not the unit to destroy)
	@param unittodestroy
		the unit you want to destroy
	@returns
		next unit from your type
*/
HageUnit *hage_unit_destroy_give_next_s(HageUnit **firstUnit,HageUnit *unittodestroy)
{
	HageUnit *nextunit;
	
	if(unittodestroy->nextSUnit==unittodestroy)
	{
		nextunit=NULL;
	}
	else
	{
		nextunit=unittodestroy->nextSUnit;
	}
	
	hage_unit_destroy_unit(firstUnit,unittodestroy);
	
	return nextunit;
}


int hage_count(HageUnit *firstunit)
{
	int amount=0;
	
	if(!firstunit)
		return 0;
	
	HageUnit *loopunit=firstunit;
	
	do
	{
		amount++;
		loopunit=loopunit->nextSUnit;
	}while(loopunit!=firstunit);
	
	return amount;
}


void hage_units_show(HageUnit **firstUnit)
{
	HageUnit *firstunit=*firstUnit;
	
	if(!firstunit)
		return;
	
	HageUnit *loopunit=firstunit;
	//so we can free objects
	HageUnit *nextunit;
	
	//OTHERS
	do
	{
		AOHaedP *parentobject=loopunit->type;
	
		//if reached max frames
		if(loopunit->animation==PGD_ANIMATION_KILLED && loopunit->animation_frame>=parentobject->maxFrames[PGD_ANIMATION_KILLED])
		{
			//DESTROY
			nextunit=hage_unit_destroy_give_next(firstUnit,loopunit);
		}
		//if its visible
		else
		{
			nextunit=loopunit->nextUnit;
			
			//printf("roller: %d, maxframes: %d, animation frame: %d\n",ANIMATION_ROLLER,loopunit->parentobject->maxFrames[loopunit->animation],loopunit->animation_frame);
			
			if(ANIMATION_ROLLER % 10 == 0 && parentobject->maxFrames[loopunit->animation]>1)
			{
				loopunit->animation_frame++;
			}
			
			if((loopunit->velocity[0]==0 && loopunit->velocity[1]==0 && loopunit->animation==PGD_ANIMATION_RUNNING) || loopunit->animation_frame>=parentobject->maxFrames[loopunit->animation])
			{
				loopunit->animation_frame=0;
				loopunit->animation=PGD_ANIMATION_STOPPED;
			}
			else if(hage_check_animation(parentobject,PGD_ANIMATION_RUNNING) && (loopunit->velocity[0]!=0 || loopunit->velocity[1]!=0) && loopunit->animation!=PGD_ANIMATION_RUNNING)
			{
				loopunit->animation_frame=0;
				loopunit->animation=PGD_ANIMATION_RUNNING;
			}
			
			//printf("anim: %d %d\n",loopunit->animation,hage_check_animation(parentobject,loopunit->animation));
			//BLIT OBJECT
			if(hage_check_animation(parentobject,loopunit->animation))
				apply_surface(loopunit->coord[0], loopunit->coord[1], parentobject->images[loopunit->animation],screen,parentobject->rect[loopunit->animation][loopunit->animation_frame] );
			else
				apply_surface(loopunit->coord[0], loopunit->coord[1], parentobject->images[PGD_ANIMATION_STOPPED],screen,parentobject->rect[PGD_ANIMATION_STOPPED][0] );
		}
		
		loopunit=nextunit;
	}while(loopunit!=firstunit);
}

/*
int CollisionAO(AOHaedP *tempAO1,int object1,AOHaedP *tempAO2,int object2)
{
	if(((tempAO1->coord[object1][0])<=(tempAO2->coord[object2][0])+(tempAO2->rect[tempAO2->animation[object2]][tempAO2->animation_frame[object2]]->w))
		&& ((tempAO2->coord[object2][0])<=(tempAO1->coord[object1][0])+(tempAO1->rect[tempAO1->animation[object1]][tempAO1->animation_frame[object1]]->w))
		&& ((tempAO1->coord[object1][1])<=(tempAO2->coord[object2][1])+(tempAO2->rect[tempAO2->animation[object2]][tempAO2->animation_frame[object2]]->h))
		&& ((tempAO2->coord[object2][1])<=(tempAO1->coord[object1][1])+(tempAO1->rect[tempAO1->animation[object1]][tempAO1->animation_frame[object1]]->h)))
	{
		return 1;
	}
	return 0;
}
*/

/**
	Check collision
	
	@param tempAO1
		object 1
	@param tempAO2
		object 2
*/
int hage_collision(HageUnit *tempAO1,HageUnit *tempAO2,int tracking)
{
	if(tempAO1!=tempAO2 && tempAO1 && tempAO2)
	{
		AOHaedP *parentobject1=tempAO1->type;
		AOHaedP *parentobject2=tempAO2->type;

		SDL_Rect *A01frame=parentobject1->rect[tempAO1->animation][tempAO1->animation_frame];
		SDL_Rect *A02frame=parentobject2->rect[tempAO2->animation][tempAO2->animation_frame];

		int collisionres[DIMENSION];
		int returncol=1;

		for(int xyz=DIMENSION-1;xyz>=0;xyz--)
		{
			double obj1len=(xyz==0?A01frame->w:A01frame->h);
			double obj2len=(xyz==0?A02frame->w:A02frame->h);
		
			collisionres[xyz]=((tempAO1->coord[xyz])<=(tempAO2->coord[xyz])+obj2len)
								&& ((tempAO2->coord[xyz])<=(tempAO1->coord[xyz])+obj1len);
			
			//(if you want to stop)
			if(tracking && collisionres[xyz])
			{
				//reverse
				int invxyz=DIMENSION-1-xyz;
			
				tempAO1->collisionvect[xyz]=collisionres[xyz];
				tempAO2->collisionvect[xyz]=collisionres[xyz];
				
				//check y (if x was done above)
				tempAO1->fcollisionvect[invxyz]=(((tempAO1->coord[xyz])-(tempAO1->velocity[xyz])<=(tempAO2->coord[xyz])-(tempAO2->velocity[xyz])+obj2len)
								&& ((tempAO2->coord[xyz])-(tempAO2->velocity[xyz])<=(tempAO1->coord[xyz])-(tempAO1->velocity[xyz])+obj1len));
			
				if(xyz==0)
				{
					tempAO1->fcollisionvect[0]&=collisionres[1];
					tempAO1->fcollisionvect[1]&=collisionres[0];
					tempAO2->fcollisionvect[0]=tempAO1->fcollisionvect[0];
					tempAO2->fcollisionvect[1]=tempAO1->fcollisionvect[1];
				}
			}
		
/*		int collisionres = (*/
/*				((tempAO1->coord[0])+(tempAO1->velocity[0])<=(tempAO2->coord[0])+(tempAO2->velocity[0])+(A02frame->w))*/
/*			&& ((tempAO2->coord[0])+(tempAO2->velocity[0])<=(tempAO1->coord[0])+(tempAO1->velocity[0])+(A01frame->w))*/
/*			&& ((tempAO1->coord[1])+(tempAO1->velocity[1])<=(tempAO2->coord[1])+(tempAO2->velocity[1])+(A02frame->h))*/
/*			&& ((tempAO2->coord[1])+(tempAO2->velocity[1])<=(tempAO1->coord[1])+(tempAO1->velocity[1])+(A01frame->h)));*/

			returncol=(returncol && collisionres[xyz]);
		}
		
		return returncol;
	}
	
	return 0;
}
/*
bool hage_get_alpha_from_pos(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8* p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;
    Uint32 pixelColor;
     
    switch(bpp)
    {
        case(1):
            pixelColor = *p;
            break;
        case(2):
            pixelColor = *(Uint16*)p;
            break;
        case(3):
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
                pixelColor = p[0] << 16 | p[1] << 8 | p[2];
            else
                pixelColor = p[0] | p[1] << 8 | p[2] << 16;
            break;
        case(4):
            pixelColor = *(Uint32*)p;
            break;
    }
     
    Uint8 red, green, blue, alpha;
    SDL_GetRGBA(pixelColor, surface->format, &red, &green, &blue, &alpha);
 
    return alpha > 200;
}

//todo
int hage_per_pixel_collision(HageUnit *tempAO1,HageUnit *tempAO2) 
{ 	
	if(hage_collision(tempAO1,tempAO2))
	{
		AOHaedP *A01obj=tempAO1->parentobject;
		AOHaedP *A02obj=tempAO2->parentobject;
	
		SDL_Surface *A01surface=A01obj->images[tempAO1->animation];
		SDL_Surface *A02surface=A02obj->images[tempAO2->animation];
		
		SDL_Rect *A01frame=A01obj->rect[tempAO1->animation][tempAO1->animation_frame];
		SDL_Rect *A02frame=A02obj->rect[tempAO2->animation][tempAO2->animation_frame];
	
		for(int x01 = 0; x01 < A01frame->w; x01++) 
		{
			for(int y01 = 0; y01 < A01frame->h; y01++) 
			{
				for(int x02 = 0; x02 < A02frame->w; x02++) 
				{
					for(int y02 = 0; y02 < A02frame->h; y02++) 
					{
						if(	!hage_get_alpha_from_pos(A01surface,A01surface->x+x01,A01surface->y+y01) && !hage_get_alpha_from_pos(A01surface,A01surface->x+x01,A01surface->y+y01) &&
								
						)
					}
				}
			}
		}
	}
	
	//If neither set of collision boxes touched 
	return 0; 
}
*/
/*
//snott från hedgewars
bool active_objects_in_box(collisionbox obj1,int x,int y,int w,int h)
{
	if (obj1.x+obj1.cboffsetx+obj1.cbwidth >= x && obj1.y+obj1.cboffsety+obj1.cbheight >= y && obj1.x+obj1.cboffsetx <= x + w && obj1.y+obj1.cboffsety <= y + h)
	{
		return true;
	}
	return false;
}
//snott från hedgewars
uint8_t active_objects_in_circle(Object obj1,Object obj2,int r,uint8_t useRadius)
{
	if(useRadius)
	{
		r = r + obj1.width;
	}
	if(pow(r,2) >= pow((obj2.x - obj1.x),2) + pow((obj2.y - obj1.y),2))
	{
		return 1;
	}
	return 0;
}
*/
void hage_stop(HageUnit *tempAO)
{
	if(!tempAO)
		return;
	
	AOHaedP *parentobject=tempAO->type;
	
	for(int xyz=0;xyz<DIMENSION;xyz++)
	{
		if((tempAO->collisionvect[xyz])!=0 && (tempAO->fcollisionvect[xyz])!=0)
		{
			tempAO->coord[xyz]-= tempAO->velocity[xyz];
		}
	}
}

//testing it
void hage_bounce(HageUnit *tempAO)
{
	if(!tempAO)
		return;
	
	for(int xyz=0;xyz<DIMENSION;xyz++)
	{
		if(tempAO->velocity[xyz]!=0)
		{
			tempAO->velocity[xyz] = -tempAO->velocity[xyz];
		}
	}
}

void hage_set_x(HageUnit *tempAO, int position)
{
	if(!tempAO)
		return;
	
	tempAO->coord[0] = position;
}

void hage_set_y(HageUnit *tempAO, int position)
{
	if(!tempAO)
		return;
	
	tempAO->coord[1] = position;
}
