/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "includes.h"

//the timer
TimerHaedP *fps;
long ANIMATION_ROLLER=0;

//The surfaces
SDL_Window *window = NULL;
SDL_Surface *screen = NULL;
//SDL_Renderer* renderer;

//toplevel unit
HageUnit **TOPLEVEL_UNIT;
void (*CURRENT_LEVEL)(void);
uint8_t LEVEL_START=1;

//quit program flag
int QUIT_PROGRAM_FLAG=0;

//screen depth
const int SCREEN_BPP = 32;

//32 �r den b�sta
const int FRAMES_PER_SECOND = 64;

//special game options
const float GRAVITY=0.4;

//The event structure
SDL_Event event;

int init()
{
	//Initialize all SDL subsystems
	if( SDL_Init( SDL_INIT_VIDEO ) == -1 )
	{
		fprintf(stderr,"Could not initiate SDL\n");
		return 0;
	}
	
	//Set up the screen
	//screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
	window = SDL_CreateWindow(PROGRAM_NAME,SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,SCREEN_WIDTH, SCREEN_HEIGHT,SDL_SWSURFACE);
	//renderer = SDL_CreateRenderer(window, -1, 0);
	screen = SDL_GetWindowSurface(window);
	
	//If there was an error in setting up the screen
	if( screen == NULL )
	{
		fprintf(stderr,"Could not initiate SDL-screen\n");
		return 0;
	}
	
	if(TTF_Init() != 0)
	{
		fprintf(stderr,"Could not initiate SDL-TTF\n");
		return 0;
	}
	
	if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
	{
		fprintf(stderr,"Could not initiate SDL-mixer\n");
		return 0;
	}

	//Set the window caption
	//SDL_WM_SetCaption( PROGRAM_NAME, NULL );
	
	//If everything initialized fine
	return 1;
}

void clean_up()
{
	Mix_CloseAudio();
	TTF_Quit();
	SDL_Quit();
}

int main(void)
{
	CURRENT_LEVEL=FIRST_LEVEL;
	TOPLEVEL_UNIT=calloc(1,sizeof(void*));
	
	fps=InitTimer();
	
	//Initialize
	if( init() == 0 )
	{
		fprintf(stderr,"Could not initiate the pgd program!\n");
		return 1;
	}
	
	//Load the files
	INIT_PROGRAM();
	
	//While the user hasn't quit
	while(QUIT_PROGRAM_FLAG==0)
	{
		//Start the frame timer
		StartTimer(fps);
		
		//While there's events to handle
		while( SDL_PollEvent( &event ) )
		{
			INPUT_PROGRAM(&event);

			//If the user has Xed out the window
			if( event.type == SDL_QUIT )
			{
				//Quit the program
				QUIT_PROGRAM_FLAG=1;
			}
		}
		
		//all the actions from program.c
		CURRENT_LEVEL();
		
		//display stuff from program.c
		hage_units_show(TOPLEVEL_UNIT);
		
		//Update the screen
		SDL_UpdateWindowSurface(window);


		//Cap the frame rate
		if( GetTicksTimer(fps) < 1000 / FRAMES_PER_SECOND )
		{
			SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - GetTicksTimer(fps) );
		}
		
		//tidtagaruret f�r animationer
		ANIMATION_ROLLER++;
	}
	//Clean up
	clean_up();

	return 0;
}
