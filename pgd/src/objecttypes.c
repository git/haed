/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "includes.h"

/**
	Create the animation vector used for all animations

	@param animationvect
		animation vector for which we will return everything into.
	@param str
		first animation name
	@param ...
		the list, it will look something like this:
		"stopped","ALL_MIRROR",1,&(SDL_Rect){0,0,64,64}
		or
		<animation>,<direction>,<number of frames>,<information about the frames>
	@returns
		pointer to everything
*/
SDL_Rect ***create_rect_vector(animationvect *outlist,const char *str, ...)
{
	SDL_Rect ***returnrect=NULL;
			
	animationvect list;
	list.animvect=NULL;
	list.length=0;
	
	va_list ap;
	int counter=0;
	SDL_Rect *i=NULL;
	const char *s=NULL;
	int l;
	int left=0;
	
	int gy=0;
     
	va_start (ap, str);
     
	//Determine how much space we need. 
	
	s = str; 
	
	while(s!=NULL)
	{
		if(counter==0)
		{
			//printf("OK S %d\n",list.length);
			for(int y=0;y<list.length;y++)
			{
				//printf("listanim:%s %d\n",list.animvect[y].animation,y);
				if(strcmp(list.animvect[y].animation,s)==0)
				{
					//printf("OK\n");
					gy=y;
					goto endlist;
				}
			}
			
			gy=list.length;
			list.length++;
			//printf("ERP3 %d %d\n",gy,list.length);
			list.animvect=realloc(list.animvect,sizeof(animationvect)*list.length);
			list.animvect[gy].animation=g_strndup(s,strlen(s));
			list.animvect[gy].animlength=0;
			//printf("ERP2 %s\n",list.animvect[gy].animation);
			returnrect=realloc(returnrect,sizeof(SDL_Rect**)*list.length);
			//printf("ERP2\n");
			returnrect[gy]=NULL;
			
			//returnrect[gy][list.animvect[gy].animlength-1]=calloc(1,sizeof(SDL_Rect));
		}
		else if(counter==2)
		{
			l=va_arg (ap, int);
			left=counter+l;
			//printf("UID %d %d\n",l,gy);
			list.animvect[gy].animlength=l;
			returnrect[gy]=realloc(returnrect[gy],sizeof(SDL_Rect*)*l);
			for(int y=0;y<l;y++)
			{
			returnrect[gy][y]=calloc(1,sizeof(SDL_Rect));
			}
		}
		else if(counter>=3)
		{
			if(counter<left+1)
			{
				i = va_arg (ap, SDL_Rect*);
				//printf("i: %d\n",i->w);
				returnrect[gy][l-(left-counter+1)][0]=(*i);
			}
			else if(counter>=left)
			{
				//printf("FAIL!\n");
				counter=-1;
				left=0;
			}
		}
		endlist:
		
		counter++;
		//printf("BLERP %d %d!\n",counter,left);
		
		if(counter<2)
		{
			s = va_arg (ap, const char*);
			//printf("s: %s\n",s);
		}
	}
	//printf("ENDED!\n");
	
	va_end (ap);
	
	(*outlist)=list;
     
	return returnrect;
}

/*anim vector is to define the width and height of the object
offsetsize is the ammount of the object at the beginning (it can increase)
animvector={1level=animation{numberofanimationframes,width,height}}
*/
//(SDL_Rect){.x=,.y=,width=,.height},(SDL_Rect){.x=,.y=,width=,.height},(SDL_Rect){.x=,.y=,width=,.height}
//{{{(SDL_Rect){.x=0,.y=0,.w=32,.h=32}},{(SDL_Rect){.x=0,.y=32,.w=32,.h=32}},{(SDL_Rect){.x=0,.y=64,.w=32,.h=32}}},{{(SDL_Rect){.x=0,.y=0,.w=32,.h=32}},{(SDL_Rect){.x=0,.y=32,.w=32,.h=32}},{(SDL_Rect){.x=0,.y=64,.w=32,.h=32}}},{{(SDL_Rect){.x=0,.y=0,.w=32,.h=32}},{(SDL_Rect){.x=0,.y=32,.w=32,.h=32}},{(SDL_Rect){.x=0,.y=64,.w=32,.h=32}}}}

/**
	Init the object structure
	
	@param name
		the name of the object
	@param rect
		the input sdl-rect (see ↑)
	@param inlist
		
*/

/** @todo rename to hage_object_new() */
AOHaedP *init_ActiveObject(const char *name, SDL_Rect ***rect, animationvect inlist)
{
	//do it safe
	AOHaedP *tempAO=calloc(1,sizeof(AOHaedP));
	
	//int angle=0;
	
	tempAO->name=g_strdup(name);
	
	tempAO->firstunit=NULL;

	tempAO->speed=malloc(sizeof(double)*(DIMENSION));
	
	//FIX THIS IF NOT PLATFORM (+1 is for gravity & jump-strength)
	tempAO->accel=malloc(sizeof(double)*(DIMENSION+1));
	tempAO->deaccel=malloc(sizeof(double)*(DIMENSION+1));
	
	tempAO->images=malloc(sizeof(SDL_Surface*)*inlist.length);
	
	tempAO->maxFrames=calloc(PGD_NUMBER_OF_ANIMATIONS,sizeof(int));
	tempAO->rect=malloc(sizeof(SDL_Rect**)*inlist.length);
	
	for(int i=0;i<inlist.length;i++)
	{
		char *tmpstr=g_strconcat("./img/",name,"/",inlist.animvect[i].animation,".png",NULL);
		//ROTATE NEEDED!
		tempAO->images[i]=loadimagefile(tmpstr);
		free(tmpstr);

		tempAO->maxFrames[i]=inlist.animvect[i].animlength;
	}
	tempAO->rect=rect;
	
	return tempAO;
}

/**
	check if animation exists
	
	@param this
		input object
	@param animation
		animation type
*/
int hage_check_animation(AOHaedP *this, PgdAnimation animation)
{
	return this->maxFrames[animation];
}


/**
	Get the amount of objects from an object type.
	
	@param thisobject
		object to get amount from.
	@returns
		amount
*/
int hage_object_get_unit_amount(AOHaedP *thisobject)
{
	int amount=0;
	HageUnit *firstunit=thisobject->firstunit;
	
	if(!firstunit)
		return 0;
	
	HageUnit *loopunit=firstunit;
	
	do
	{
		amount++;
		loopunit=loopunit->nextSUnit;
	}while(loopunit!=firstunit);
	
	return amount;
}

HageUnit *hage_object_get_unit_from_position(AOHaedP *thisobject,int pos)
{
	int thispos=pos;
	
	if(thispos>=0)
	{
		HageUnit *firstunit=thisobject->firstunit;
		
		if(!firstunit)
		{
			b_error_message("No objects found!");
			return NULL;
		}
		
		HageUnit *loopunit=firstunit;
		
		do
		{
			if(thispos==0)
				return loopunit;
			
			thispos--;
			loopunit=loopunit->nextSUnit;
		}while(loopunit!=firstunit);
		
		b_error_message("Your pos is too high! %d",pos);
		return NULL;
	}
	else
	{
		b_error_message("negative position is not supported yet!");
		return NULL;
	}
}

double GetPositive(float f)
{
	if(f<0)
	{
		return -f;
	}
	return f;
}

void SetInputPlayer(int *PlayerKeys,SDL_Keycode *PlayerBindings,SDL_Event *tempEvent)
{
	//If a key was pressed
	if( tempEvent->type == SDL_KEYDOWN)
	{
		//229(E5)=Ã¥
		//228(E4)=Ã€
		//246(F6)=Ã¶
		//printf("EV TYPE=%d\n",tempEvent->key.keysym.sym);
		for(int i=0;i<6;i++)
		{
			if(tempEvent->key.keysym.sym == PlayerBindings[i])
			{
				PlayerKeys[i]=1;
			}
		}
	}
	//If a key was released
	else if( tempEvent->type == SDL_KEYUP)
	{
		for(int i=0;i<6;i++)
		{
			if(tempEvent->key.keysym.sym == PlayerBindings[i])
			{
				PlayerKeys[i]=0;
			}
		}
	}
}

void hage_move_objects(AOHaedP *thisobject, int *PlayerKeys)
{
	HageUnit *firstunit=thisobject->firstunit;
	
	if(!firstunit)
		return;
	
	HageUnit *loopunit=firstunit;
	
	do
	{
		AOHaedP *parentobject=loopunit->type;
	
		for(int xyz=0;xyz<DIMENSION;xyz++)
		{
			if(PlayerKeys[((xyz&1)^1)<<1] && !PlayerKeys[(((xyz&1)^1)<<1)|1])
			{
				if(loopunit->velocity[xyz]<-parentobject->speed[xyz])
				{
					loopunit->velocity[xyz] = -parentobject->speed[xyz];
				}
				else
				{
					loopunit->velocity[xyz] += -(GetPositive(loopunit->velocity[xyz])+1)*parentobject->accel[xyz];
				}
				//printf("upp\n");
			}
			else if(!PlayerKeys[((xyz&1)^1)<<1] && PlayerKeys[(((xyz&1)^1)<<1)|1])
			{
				if(loopunit->velocity[xyz]>parentobject->speed[xyz])
				{
					loopunit->velocity[xyz] = parentobject->speed[xyz];
				}
				else
				{
					loopunit->velocity[xyz] += (GetPositive(loopunit->velocity[xyz])+1)*parentobject->accel[xyz];
				}
				//printf("dwn\n");
			}
			else if(loopunit->velocity[xyz]!=0)
			{
				if(loopunit->velocity[xyz] < -0.01 || loopunit->velocity[xyz] > 0.01)
				{
					loopunit->velocity[xyz] += -loopunit->velocity[xyz]*parentobject->deaccel[xyz];
					//printf("mov\n");
				}
				else if(loopunit->velocity[xyz] > -0.01 && loopunit->velocity[xyz] < 0.01)
				{
					loopunit->velocity[xyz] =0;
					//printf("stop\n");
				}
			}
			
			//tempAO->coord[object][xyz] += tempAO->velocity[object][xyz];//*(32/(float)(FRAMES_PER_SECOND));
		}
		loopunit=loopunit->nextSUnit;
	}while(loopunit!=firstunit);
}

/**
	Moves all objects relating to thisobject (sets position, however this might change before final destination)
	
	@param thisobject
		object/objects to move
*/
void hage_apply_movement(AOHaedP *thisobject)
{
	HageUnit *firstunit=thisobject->firstunit;
	
	if(!firstunit)
		return;
	
	HageUnit *loopunit=firstunit;
	do
	{
		for(int xyz=0;xyz<DIMENSION;xyz++)
		{
			loopunit->coord[xyz] += loopunit->velocity[xyz];//*(32/(float)(FRAMES_PER_SECOND));
		}
		loopunit=loopunit->nextSUnit;
	}while(loopunit!=firstunit);
}
