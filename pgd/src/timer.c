/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/
#include "includes.h"

TimerHaedP *InitTimer()
{
	TimerHaedP *tempTimer=malloc(sizeof(TimerHaedP));
	//Initialize the variables
	
	tempTimer->startTicks=0;
	tempTimer->pausedTicks=0;
	tempTimer->values=0;
	
	return tempTimer;
}

void StartTimer(TimerHaedP *tempTimer)
{
	//Start the timer
	tempTimer->values=1;

	//Get the current clock time
	tempTimer->startTicks = SDL_GetTicks();
}

void StopTimer(TimerHaedP *tempTimer)
{
	tempTimer->values=0;
}

void PauseTimer(TimerHaedP *tempTimer)
{
	//If the timer is running and isn't already paused
	if(tempTimer->values==1)
	{
		//Pause the timer
		tempTimer->values=3;
	
		//Calculate the paused ticks
		tempTimer->pausedTicks=SDL_GetTicks()-tempTimer->startTicks;
	}
}

void UnpauseTimer(TimerHaedP *tempTimer)
{
	//If the timer is paused
	if((tempTimer->values&2)==2)
	{
		//Unpause the timer
		tempTimer->values^=2;
	
		//Reset the starting ticks
		tempTimer->startTicks=SDL_GetTicks()-tempTimer->pausedTicks;
	
		//Reset the paused ticks
		tempTimer->pausedTicks=0;
	}
}

int GetTicksTimer(TimerHaedP *tempTimer)
{
	//If the timer is running
	if((tempTimer->values&1)==1)
	{
		//If the timer is paused
		if((tempTimer->values&2)==2)
		{
			//Return the number of ticks when the timer was paused
			return tempTimer->pausedTicks;
		}
		else
		{
			//Return the current time minus the start time
			return SDL_GetTicks()-tempTimer->startTicks;
		}
	}

	//If the timer isn't running
	return 0;
}

int GetStartedTimer(TimerHaedP *tempTimer)
{
	return tempTimer->values&1;
}

int GetPausedTimer(TimerHaedP *tempTimer)
{
	return (tempTimer->values&2)>>1;
}
