/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/

#pragma once

/// borde vara hage_collision(a,b,0) för hastighet
#define COLLISION(a,b) hage_collision(a,b,1)
/// Use this with stop eg...
#define COLLISION_TRACK(a,b) hage_collision(a,b,1)
#define STOP(a) hage_stop(a)
#define COUNT(a) hage_count(a)
#define SET_X(a,b) hage_set_x(a,b)
#define SET_Y(a,b) hage_set_y(a,b)
#define DESTROY_OBJECT(obj) obj=hage_unit_destroy_give_next_s(TOPLEVEL_UNIT,obj);obj=(obj?obj->prevSUnit:NULL);
#define DESTROY(obj) DESTROY_OBJECT(obj)
#define CREATE_OBJECT(obj,coordx,coordy) hage_unit_add_unit(TOPLEVEL_UNIT,obj,coordx,coordy)
#define CREATE_INSTANT_OBJECT(obj,coordx,coordy) hage_unit_add_unit(TOPLEVEL_UNIT,obj##_init,coordx,coordy)

//level related
#ifdef RUN_LEVEL
	#define SET_LEVEL(levelname) QUIT_PROGRAM_FLAG=1
	#define RESTART_PROGRAM() QUIT_PROGRAM_FLAG=1
	#define NEXT_LEVEL() QUIT_PROGRAM_FLAG=1
#else
	#define SET_LEVEL(levelname) CURRENT_LEVEL=levelname;LEVEL_START=1;ANIMATION_ROLLER=0
	#define RESTART_PROGRAM() CURRENT_LEVEL=FIRST_LEVEL;LEVEL_START=1;ANIMATION_ROLLER=0
	#define NEXT_LEVEL() do{const int maxLevelSize=sizeof(LEVEL_VECTOR)/sizeof(LEVEL_VECTOR[0]); \
								for(int i=0;i<maxLevelSize;i++) \
								{ \
									if(LEVEL_VECTOR[i]==CURRENT_LEVEL && i!=(maxLevelSize-1)) \
									{ \
										CURRENT_LEVEL=LEVEL_VECTOR[i+1]; \
										LEVEL_START=1; \
										ANIMATION_ROLLER=0; \
										break; \
									} \
								}}while(0)
#endif

#define RESTART_LEVEL() LEVEL_START=1;ANIMATION_ROLLER=0

//other
#define FOR_EACH_OBJECT(obj) while(obj && (obj=obj->nextSUnit)!=obj ## _init->firstunit)
#define FOR_EACH_START(obj) if((obj=obj ## _init->firstunit))
