/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/

#pragma once
#ifndef PGD_UNIT
#define PGD_UNIT

void hage_unit_add_unit(HageUnit **firstUnitP,void *unitparent,int x,int y);
void hage_unit_destroy_unit(HageUnit **firstUnit,HageUnit *unittodestroy);
void destroy_all(HageUnit **firstUnit);
HageUnit *hage_unit_destroy_give_next(HageUnit **firstUnit,HageUnit *unittodestroy);
HageUnit *hage_unit_destroy_give_next_s(HageUnit **firstUnit,HageUnit *unittodestroy);
int hage_count(HageUnit *firstunit);
void hage_units_show(HageUnit **firstUnit);
int hage_collision(HageUnit *tempAO1,HageUnit *tempAO2,int tracking);
void hage_stop(HageUnit *tempAO);
void hage_bounce(HageUnit *tempAO);
void hage_set_x(HageUnit *tempAO,int position);
void hage_set_y(HageUnit *tempAO,int position);

#endif
