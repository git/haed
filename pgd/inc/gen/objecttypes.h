/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/

#pragma once
#ifndef PGD_OBJECTTYPES
#define PGD_OBJECTTYPES

#define b_critical_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;37m\e[41mCRITICAL\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)
#define b_error_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;31mERROR\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)
#define b_warning_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;33mWARNING\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)
#define b_debug_message(msg, ...) printf("\e[0m\e[1m" __FILE__ ":%d: \e[1;36mDEBUG\e[0m\e[1m: (%s)\n\e[32m→\e[0m\e[1m " msg "\e[0m\n\n",__LINE__,__func__, ##__VA_ARGS__)

//2d, should be implemented for 3d as well
#define DIMENSION 2

typedef enum PgdAnimation
{
	PGD_ANIMATION_STOPPED,
	PGD_ANIMATION_RUNNING,
	PGD_ANIMATION_KILLED,
	
	PGD_NUMBER_OF_ANIMATIONS
}PgdAnimation;

//const char *ANIMATION_NAMES[]={"stopped","running","disappearing"};

typedef struct _HageUnit HageUnit;

//Active Object
typedef struct AOHaedP
{
	//ammount=number of objects defined in init
	//ON-variable[object]-->>1bit(1)=controllable,2bit(2)=showable,3bit(4)=on
	//if AnimationArg=Pi/4 (45*) then it will allow animations to be angled in that direction.
	//animation frame-->>of object
	//animation-->>of object
	//animation-->>of all objects
	int *maxFrames,*animationSpeed,type;
	//coord-->of object-->>{x,y,(z)}
	//direction = arg in radians max=2Pi
	//velocity[unit][coord] each object have their own velocity, accel & deaccel is in percentage. increase dimension of accel & deaccel if its platform-mode and the object wants jump strength and gravity, should be calculated after common gravity?.
	//speed is the maximum velocity (x,y(,z))
	double *speed,*accel,*deaccel;
	const char *name;
	//defined in haedgui and will be in the generated file
	void ***vars;
	
	//movement type
	//accel & deaccel are defined here
	//void *MOV_AllDirections(AOHaedP *tempAO,int object,SDL_Event *tempEvent);
	
	SDL_Surface **images;
	SDL_Rect ***rect;
	
	HageUnit *firstunit;
} AOHaedP;

/**
	Hage unit type, this refers to an object
	
	@todo
		split so that it only contains basic information such as position and next object and such
		move code for velocity to HageUnitMovable,
		object only containing text should have such like HageUnitText
*/
struct _HageUnit
{
	//control-information
	
	unsigned int on:1;
	unsigned int showable:1;
	unsigned int controllable:1;

	int animation_frame, animation, collisionvect[DIMENSION], fcollisionvect[DIMENSION];
	
	double coord[DIMENSION], velocity[DIMENSION], direction;
	
	void *type; ///< the object it is refering to.
	
	HageUnit *nextUnit,*prevUnit; //< the next unit seen from the list, but necessarily of the same type
	//of the same type
	HageUnit *nextSUnit,*prevSUnit; //< the next unit but from the same type
};

struct animationvecti
{
	char *animation;
	int animlength;
};

typedef struct
{
	struct animationvecti *animvect;
	int length;
}animationvect;


SDL_Rect ***create_rect_vector(animationvect *outlist,const char *str,...);
AOHaedP *init_ActiveObject(const char *name,SDL_Rect ***rect,animationvect inlist);
int hage_check_animation(AOHaedP *this,PgdAnimation animation);
int hage_object_get_unit_amount(AOHaedP *thisobject);
HageUnit *hage_object_get_unit_from_position(AOHaedP *thisobject,int pos);
double GetPositive(float f);
void SetInputPlayer(int *PlayerKeys,SDL_Keycode *PlayerBindings,SDL_Event *tempEvent);
void hage_move_objects(AOHaedP *thisobject,int *PlayerKeys);
void hage_apply_movement(AOHaedP *thisobject);

#endif
