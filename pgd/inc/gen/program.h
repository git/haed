/**
	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 
*/

#pragma once
#ifndef PGD_PROGRAM
#define PGD_PROGRAM

extern const char *PROGRAM_NAME;
extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;
extern AOHaedP *hattis_init;
extern AOHaedP *hinder_init;
extern AOHaedP *mal_init;
extern AOHaedP *ao1431_init;
extern AOHaedP *ao14358_init;
extern AOHaedP *ao_15_6_27_11_6_11_init;
extern SDL_Keycode player1Keys[];
extern int Player1Input[6];
extern SDL_Keycode player2Keys[];
extern int Player2Input[6];
void INIT_PROGRAM(void);
void INPUT_PROGRAM(SDL_Event *tempEvent);
void map1(void);
void map1(void);
void New_level(void);
void New_level(void);
void New_level_3(void);
void New_level_3(void);
extern void *LEVEL_VECTOR[];
extern void *FIRST_LEVEL;

#endif
