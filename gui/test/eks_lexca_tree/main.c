/**
	TODO make get object more predictable
	get_object_from_relative_position
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAIN_FILE
#include "includes.h"

int main(int argc, char ** argv)
{
	const char *ref_s="COLLISION(__OBJECT,__OBJECT)";
/*	const char *ref_s="COUNT(__OBJECT) __EQUIV_CALCULATION";*/

	g_autoptr(Pgd_trigger_lexca_buffer) test=pgd_trigger_lexca_buffer_create("HELLO && COLLISION(OBJECT_RANDOM(obj1,12),obj2) && COUNT(obj1) >= 100");
/*	g_autoptr(GPtrArray) ref=pgd_trigger_lexca_buffer_create(ref_s);*/

	printf("CREATED:: %p\n",test);

	pgd_trigger_lexca_buffer_print(test);
/*	pgd_trigger_lexca_buffer_print(ref);*/

/*	int res=pgd_trigger_lexca_buffer_compare(test,ref);*/
/*	*/
/*	printf("RES:: %d %s\n",res,res>=0?"found":"not found");*/

	return 0;
}
