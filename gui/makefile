#######################################################
#
# Makefile for the Haed project
#
# @license GPLv3
# Haed a free program maker and generator
# Copyright (C) 2018  Florian Evaldsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# @author Florian Evaldsson 
#
#######################################################

NAME = haed

#######################################################
#STD OPTIONS
#######################################################

CC := gcc
DEBUG_TOOL := ddd #nemiver

#######################################################
#COMPILER DIRS
#######################################################

OUTDIR := ./.build/

INCLUDES := ./inc/
INCLUDES += ./inc/dialog/
INCLUDES += ./inc/dialog/draw/
INCLUDES += ./inc/pgd/
INCLUDES += ./inc/level_editor/
INCLUDES += ./inc/trigger_editor/
INCLUDES += ./inc/storyboard_editor/
INCLUDES += ./inc/config/

INCLUDES += ./../include/sacm/
INCLUDES += ./../include/gtk_menu_list/

INCLUDES += ./../include/lexca/src/
INCLUDES += ./../include/blibc/src/
INCLUDES += ./../include/gtk_menu_list

#### SRC ####

SRCS := $(wildcard ./src/*.c)
SRCS += $(wildcard ./src/dialog/*.c)
SRCS += $(wildcard ./src/dialog/draw/*.c)
SRCS += $(wildcard ./src/pgd/*.c)
SRCS += $(wildcard ./src/level_editor/*.c)
SRCS += $(wildcard ./src/trigger_editor/*.c)
SRCS += $(wildcard ./src/storyboard_editor/*.c)
SRCS += $(wildcard ./src/config/*.c)

#######################################################
#COMPILER OPTIONS
#######################################################

CFLAGS = -std=gnu11 -g -fPIC -Wall -D_GNU_SOURCE
CFLAGS += $(addprefix -I,$(INCLUDES))
CFLAGS += $(shell pkg-config --cflags gtk+-3.0)

LDFALGS = -L../include/lexca/src -L../include/gtk_menu_list -L../include/blibc/src
LDFLAGS += -lm -lzip -leksparent
LDFLAGS += $(shell pkg-config --libs gtk+-3.0)
LDFLAGS += -Wl,-Bstatic -L../include/lexca/src -L../include/gtk_menu_list -L../include/blibc/src -lblibc -lGtkMenuList -llexca -Wl,-Bdynamic

CFLAGS += $(if $(NO_ASAN),,-fsanitize=address)
LDFLAGS += $(if $(NO_ASAN),,-fsanitize=address)

#CFLAGS += -I/path/to/eksparent
#LDFLAGS += -L/path/to/eksparent

QUICKTEST:= 0

ifeq ($(QUICKTEST),1)
EXTRAS:=-DQUICKTEST
endif

#######################################################
#OUT GLOBALS
#######################################################

OBJS := $(addprefix $(OUTDIR),$(patsubst %.s,%.o,$(patsubst %.c,%.o,$(SRCS))))

OBJ_DIRS := $(sort $(dir $(OBJS)))

#######################################################
#COMPILING SELECTIONS
#######################################################

$(info )

all: $(NAME)

$(OUTDIR) $(OBJ_DIRS):
	mkdir -p $(@D)

$(NAME): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)
	
$(OUTDIR)%.o: %.c | $(OUTDIR) $(OBJ_DIRS)
	$(CC) $(CFLAGS) -c -o $@ $< $(EXTRAS)
	#;echo "compiling ... $<"

frun:
	$(MAKE) run -j

run: all
	./$(NAME)
	
clean:
	rm -f $(OBJS)

debugc: $(OBJS)
	$(CC) -o $(NAME) $(PKG_CONF) $(LIBS) $^ $(OTHER)

debug: all
	$(DEBUG_TOOL) ./$(NAME) -x debug_profile

gdb: all
	gdb --args ./$(NAME)
	
valgrind:
	valgrind  --track-origins=yes --tool=memcheck --leak-check=full ./$(NAME)
	
libs:
	make -C ./../include/minilang/src

###WINDOWS INFO, compiling from GNU/Linux!
#downloaded wingw32
#downloaded from: http://www.tarnyko.net/dl/ (3.6)
#changed the first value of each file in /lib/pkgconfig to current path eg[prefix= $$HOME"/.wine/drive_c/Program Files/GTK+-Bundle-3.6.0"] and removed -lintl from Libs in glib

#Windows related
#PKG_CONFIG_PATH_WIN = $${HOME}"/.wine/drive_c/Program Files/GTK+-Bundle-3.6.0/lib/pkgconfig"
#CFLAGS_WIN := $(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH_WIN) pkg-config --cflags gtk+-win32-3.0)
#LIBS_WIN := $(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH_WIN) pkg-config --libs gtk+-win32-3.0)

windows:
	@echo $@ windows
	i586-mingw32msvc-g++ -mwindows $(MAIN_FILE) -o Haed.exe -DWINDOWS $(CFLAGS_WIN) $< $(LIBS_WIN)
	wine ./GlobalismStore.exe

#have no clue on how to fix this...
macosx:
	@echo $@
	$(CC) $(MAIN_FILE) -o Haed -DOSX $(PKG_CONF)
	./GlobalismStore.exe
