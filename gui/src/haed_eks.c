/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

/**
	Get file contents from zipfile.
	
	@todo 
		move this function elsewhere?
		
	@param filename
		
*/
char *zip_get_file_contents(char *filename,struct zip *z,struct zip_stat *st,int *contentsLength) 
{
	struct zip_file *f;
	char *contents;
	
	if(zip_stat(z, filename, 0, st)==-1)
	{
		b_error_message("Could not initzialize the file length!");
		return NULL;
	}
	
	//Alloc memory for its uncompressed contents
	contents = calloc(st->size+1,sizeof(char));
	
	//Read the compressed file
	if(!(f = zip_fopen(z, filename, 0)))
	{
		b_error_message("Could not open the inner file!");
		return NULL;
	}
	
	//läs in filinehållet i strängen.
	zip_fread(f, contents, st->size);
	
	if(contentsLength)
		*contentsLength=st->size;
	
	zip_fclose(f);
	
	return contents;
}

/**
	Set all units from a eksparent structure, used in loadgame
	on a save-file the aoparent should be the one with Active-object as the name

	@param rootParent
		the input parent object wich will set all objects after its structure.
*/
void haed_eks_set_objects_from_parent(EksParent *rootParent)
{
	EksParent *objectsParent=eks_parent_get_child_from_name(rootParent,"objects");
	EksParent *AOEksParent=eks_parent_get_child_from_name(objectsParent,"active");
	EksParent *levelsParent=eks_parent_get_child_from_name(rootParent,"levels");
	EksParent *currLevelParent=eks_parent_get_child_from_type(levelsParent,0,EKS_PARENT_TYPE_VALUE);
	
	haed_programinfo_set_current_level(rootParent,currLevelParent);
	
	TriggerHandler *thandler=calloc(1,sizeof(TriggerHandler));
	eks_parent_custom_set(currLevelParent,HAED_LEVEL_TRIGGHANDLER,thandler);

	//set linkage for all position objects
	eks_parent_foreach_child_start(levelsParent,thisLevel)
	{
		if(eks_parent_compare_type(thisLevel,EKS_PARENT_TYPE_VALUE))
		{
			EksParent *positionsParent=eks_parent_get_child_from_name(thisLevel,"positions");
		
			eks_parent_foreach_child_start(positionsParent,loopUnit)
			{
				if(eks_parent_compare_type(loopUnit,EKS_PARENT_TYPE_VALUE))
				{
					eks_parent_foreach_child_start(AOEksParent,loopObject)
					{
						if(eks_parent_compare_type(loopObject,EKS_PARENT_TYPE_VALUE))
						{
							b_tm("obj1 %s obj2 %s",loopUnit->name,loopObject->name);
				
							if(strcmp(loopUnit->name,loopObject->name)==0)
							{
								eks_parent_custom_set(loopUnit,HAED_POS_OBJECT_DECLARE,loopObject);
								eks_parent_custom_set(loopUnit,HAED_POS_OBJECT_MARKED,HAED_UNIT_IS_UNMARKED);
							}
						}
					}eks_parent_foreach_child_end(AOEksParent,loopObject);
				}
			}eks_parent_foreach_child_end(positionsParent,loopUnit);
		}
	}eks_parent_foreach_child_end(levelsParent,thisLevel);
	
	printf("TEEEST1:: %s\n",eks_parent_dump_text(rootParent));
	
	//fix active objects
	eks_parent_foreach_child_start(AOEksParent,this_active)
	{
		if(eks_parent_compare_type(this_active,EKS_PARENT_TYPE_VALUE))
		{
			EksParent *mov_type=eks_parent_get_child_from_name(this_active,"mov");
			
			if(!mov_type)
			{
				EksParent *new_mov_type=eks_parent_add_child(this_active,"mov",EKS_PARENT_TYPE_VALUE,NULL);
				eks_parent_add_child(new_mov_type,"none",EKS_PARENT_TYPE_VALUE,NULL);
			}
		}
	}eks_parent_foreach_child_end(AOEksParent,this_active);
	
	printf("TEEEST2:: %s\n",eks_parent_dump_text(rootParent));
}

/**
	Load a program from a hft file aka zip-file.
	
	@param filename
		the path to the hft file.
	@returns
		1 if the function exited correctly.
*/
int haed_open_filetype_hft(char *filename)
{
	//Open the ZIP archive
	int err = 0;
	const char *name;
	struct zip *z;
	struct zip_stat *st;
	struct zip_file *f;
	char *contents;
	char *contents2;
	
	EksParent *gameEksParent=NULL;
	
/*	haed_triggers_visual_reset();*/
	
	z = zip_open(filename, 0, &err);
	
	//om du lnyckades att öppna filen.
	if(z!=NULL)
	{
		//Search for the file of given nam
		st=calloc(1,sizeof(struct zip_stat));
		
		zip_stat_init(st);
		
		if(zip_stat(z, "program.eks", 0, st)==-1)
		{
			b_error_message("Could not find the file: program.eks!");
			return 0;
		}
		
		contents = calloc(st->size+1,sizeof(char));
		
		f = zip_fopen(z, "program.eks", 0);
		
		zip_fread(f, contents, st->size);
		
		gameEksParent=eks_parent_parse_text(contents,"Program");
		GLOBAL_HAED_INFORMATION.firstProgram->program=gameEksParent;
		
		haed_eks_set_objects_from_parent(gameEksParent);
		
		b_tm("%s",eks_parent_dump_text(gameEksParent));
		
		free(contents);
		
		//read other
		
		int numberofzipentries=zip_get_num_entries(z,ZIP_FL_UNCHANGED);
		
		//för varje fil i zipfilen
		for(int i=0;i<numberofzipentries;i++)
		{
			//namnet av filen
			name= zip_get_name(z, i, err);
			
			b_tm("filename: %s\n",name);
			
			//Är en fil && har mapp-namnet "customXml" ||  "docProps"
			if(name[strlen(name)-1]!='/')
			{
				int explode_ln=0;
				char **explodedName=b_explode((char*)name,&explode_ln,"/");
				
				if(explode_ln==3 && strcmp(explodedName[0],"img")==0 && strcmp("inf.eks",explodedName[2])==0)
				{
					//=====image file======
					b_tm("filename: %s",name);

					//======eks file====
					char *eksfilename=concat(explodedName[0],"/",explodedName[1],"/inf.eks",NULL);

					contents2=zip_get_file_contents(eksfilename,z,st,NULL);
					
					//printf("EKSFILENAME: %s\n%d\n",contents2,imageBufferLen);

					EksParent *bufholderParent=eks_parent_parse_text(contents2,"inf");
					
					//get all image files and read them into the pix buffer
					for(int iii=0;iii<numberofzipentries;iii++)
					{
						const char *name2= zip_get_name(z, iii, err);
						
						if(name2[strlen(name2)-1]!='/')
						{
							int explode_ln2=0;
							char **explodedName2=b_explode((char*)name2,&explode_ln2,"/");
							
							const char *imagefilename=NULL;
							
							for(int ii=0;ii<GLOBAL_ANIMATION_VECTOR_LEN;ii++)
							{
								char *tempname=concat(GLOBAL_ANIMATION_VECTOR[ii],".png",NULL);
								
								if(explode_ln2 ==3 && strcmp(explodedName[1],explodedName2[1])==0 && strcmp(explodedName2[2],tempname)==0)
								{
									imagefilename=GLOBAL_ANIMATION_VECTOR[ii];
									
									b_tm("ANIMATION: %s\n",imagefilename);
									
									int imageBufferLen;
									//Alloc memory for its uncompressed contents
									contents = zip_get_file_contents((char*)name2,z,st,&imageBufferLen);
									
									GdkPixbufLoader *testLoader=gdk_pixbuf_loader_new_with_type("png",NULL);
									gdk_pixbuf_loader_write(testLoader,(const unsigned char *)contents,imageBufferLen,NULL);
									
									haed_pixbufholder_set_animation_from_pixbuf(bufholderParent,(char*)imagefilename,gdk_pixbuf_loader_get_pixbuf(testLoader));
									
									free(contents);
								}
								free(tempname);
							}
						}
					}
					
					haed_pixbufholder_fix_object(bufholderParent,explodedName[1],gameEksParent);
					
					printf("RIGHTY DONE\n");
					//GdkPixbufLoader *testLoader=gdk_pixbuf_loader_new_with_type("png",NULL);
					//gdk_pixbuf_loader_write(testLoader,contents,imageBufferLen,NULL);
					
					//haed_pixbufholder_add_animation_from_pixbuf(bufholderParent,(char*)imagefilename,gdk_pixbuf_loader_get_pixbuf(testLoader));
					
					//haed_object_add_eks_parent_to_gui(explodedName[1],bufholderParent,GLOBAL_ACTIVE_OBJECT_S);
					
					free(contents2);
				}
			}
		}
		
/*		haed_eks_set_triggers_from_parent(gameEksParent);*/
		
		//b_tm("→→→%s\n%d\n%d",contents,strlen(contents),st->size);
		
		zip_fclose(f);
		
		//And close the archive
		zip_close(z);
	}
	
	haed_storyboard_visual_reset(gameEksParent);
	haed_level_editor_update();
	haed_triggers_visual_update();
	
	b_tm("DONE LOADING!");
	
	return 0;
}

/**
	Cleans everything

	@param widget NO_FREE
		The widget you pressed
	@param input NO_FREE
		the input data
*/
void haed_clean_everything(void)
{
	eks_parent_destroy(GLOBAL_HAED_INFORMATION.firstProgram->program,TRUE);
	GLOBAL_HAED_INFORMATION.firstProgram->program=haed_programinfo_init("untitled");
	haed_storyboard_visual_reset(GLOBAL_HAED_INFORMATION.firstProgram->program);
	haed_triggers_visual_update();
	haed_object_list_visual_reset();
}

void haed_new_program(GtkWidget *widget,gpointer data)
{
	b_tm("New program\n");
	haed_clean_everything();
}

/**
	If someone presses the load game button.

	@param widget NO_FREE
		The widget you pressed
	@param input NO_FREE
		the input data
*/
void haed_load_program(GtkWidget *widget,gpointer data)
{
	GtkWidget *dialog;

	dialog = gtk_file_chooser_dialog_new (_("Open Project"),GTK_WINDOW(GLOBAL_MAIN_WINDOW),GTK_FILE_CHOOSER_ACTION_OPEN,_("_Cancel"), GTK_RESPONSE_CANCEL,_("_Open"), GTK_RESPONSE_ACCEPT,NULL);

	if(gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		g_autofree char *tmp_filename=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		haed_global_filename_set(tmp_filename);
		
		g_autofree char *extension=get_file_type(GLOBAL_CURRENT_FILE_NAME);
		
		b_tm("FileExtension=%s\n",extension);

		if(g_strcmp0(extension,"hft")==0)
		{
			haed_open_filetype_hft(GLOBAL_CURRENT_FILE_NAME);
		}
	}

	gtk_widget_destroy (dialog);

}

/**
	Save the program to a hft file. Saving to eks structure isnt supported yet.
	
	@todo
		change function name to like, haed_save_program_hft or haed_save_program(const char *file,HaedFileType filetype)?
	
	@param fileName
		the path to the file to be saved to
*/
void haed_save_program(char *fileName)
{
	char *textHaedTextFile=NULL;
	EksParent *GameEksParent;
	
	printf("Will now save the game...\n");
	
	//do not compress, want all unit info :P
	GameEksParent=GLOBAL_HAED_INFORMATION.firstProgram->program;
	
	printf("GOT TO THE END\n");
	
	textHaedTextFile=eks_parent_dump_text(GameEksParent);
	
	printf("%s\n",textHaedTextFile);
	
	b_tm("WILL NOW SAVE THE FILE!\n");
	
	struct zip *z;
	struct zip_source *s; 
	//const char buf="teststring"; 
	 
	z=zip_open(fileName,ZIP_CREATE,NULL);
	
	if((s=zip_source_buffer(z, textHaedTextFile, strlen(textHaedTextFile), 0)) == NULL || zip_add(z, "program.eks", s) < 0) 
	{ 
		zip_source_free(s); 
		b_error_message("Could not add file: %s\n", zip_strerror(z)); 
	}
	
	char *buffer=NULL;
	gsize buflength=0;
	
	EksParent *objectP=eks_parent_get_child_from_name(GameEksParent,"objects");
	EksParent *activeP=eks_parent_get_child_from_name(objectP,"active");
	
	eks_parent_foreach_child_start(activeP,object)
	{
		EksParent *objbuffholder=eks_parent_custom_get(object,HAED_OBJECT_BUFFHOLDER);
		
		eks_parent_foreach_child_start(objbuffholder,curBuf)
		{
			char *animationstr=eks_parent_get_string(curBuf);
		
			if(eks_parent_compare_type(curBuf,EKS_PARENT_TYPE_VALUE) && haed_animations_is_animation(animationstr))
			{
				gdk_pixbuf_save_to_buffer(eks_parent_custom_get(curBuf,0),&buffer,&buflength,"png",NULL,NULL);
				char *pngfilename=concat("img/",object->name,"/",animationstr,".png",NULL);
			
				if((s=zip_source_buffer(z, buffer, buflength, 0)) == NULL || zip_add(z, pngfilename, s) < 0) 
				{ 
					zip_source_free(s); 
					b_error_message("Could not add file: %s\n", zip_strerror(z)); 
				}
			
				free(pngfilename);
			}
			
			free(animationstr);
		}eks_parent_foreach_child_end(objbuffholder,curBuf);
		
		char *buffer2=eks_parent_dump_text(objbuffholder);
		
		b_tm("buffer2 %s\n",buffer2);
		
		char *inffilename=concat("img/",object->name,"/inf.eks",NULL);
		
		if((s=zip_source_buffer(z, buffer2, strlen(buffer2), 0)) == NULL || zip_add(z, inffilename, s) < 0) 
		{ 
			zip_source_free(s); 
			b_error_message("Could not add file: %s\n", zip_strerror(z)); 
		}
		
		free(inffilename);
	}eks_parent_foreach_child_end(activeP,object);
	
	//b_tm("%s\n",buffer);
	
	zip_close(z);

	b_tm("DONE!");
	/*
	if(textHaedTextFile!=NULL)
	{
		save_file("./../hattis/program.haed",textHaedTextFile);
	}
	else
	{
		printf("ERROR: Failed to save file!");
	}
	*/
}

/**
	Save as dialog window.
	
	@todo
		move this to dialog functions?
		
	@param widget
		widget from
	@param data
		some input data
*/
void haed_gui_save_as(GtkWidget *widget,gpointer data)
{
	GtkWidget *dialog;

	dialog = gtk_file_chooser_dialog_new (_("Save as"),GTK_WINDOW(GLOBAL_MAIN_WINDOW),GTK_FILE_CHOOSER_ACTION_SAVE,_("_Cancel"), GTK_RESPONSE_CANCEL,_("_Save"), GTK_RESPONSE_ACCEPT,NULL);

	if(gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		g_autofree char *tmp_filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		
		g_autofree char *extension=get_file_type(tmp_filename);
		
		g_autofree char *filename=NULL;
		
		if(g_strcmp0(extension,"hft")==0)
		{
			filename=g_steal_pointer(&tmp_filename);
		}
		else
		{
			asprintf(&filename,"%s.hft",tmp_filename);
		}
		
		haed_global_filename_set(filename);
		
		b_tm("WILL SAVE TO: %s\n",GLOBAL_CURRENT_FILE_NAME);
		
		haed_save_program(GLOBAL_CURRENT_FILE_NAME);
	}

	gtk_widget_destroy (dialog);
}

/**
	only save from the current instance

	@param widget
		widget from
	@param data
		some input data
*/
void haed_gui_save(GtkWidget *widget,gpointer data)
{
	if(GLOBAL_CURRENT_FILE_NAME)
	{
		//haed_save_program(GLOBAL_CURRENT_FILE_NAME);
	}
	else
	{
		haed_gui_save_as(widget,data);
	}
}

/**
	If someone presses the run level button
	
	@param widget
		the input widget
	@param data
		the input data
		probably nothing
*/
void haed_run(GtkWidget *widget,gpointer data)
{
	g_autofree char *makecommand=NULL;
	EksParent *GameEksParent;
	
	b_debug_message("Will now generate the game...\n");
	
	GameEksParent=GLOBAL_HAED_INFORMATION.firstProgram->program;//haed_eks_generate_program_parent(0);
	
	printf("GOT TO THE END \n===\n%s\n===\n",eks_parent_dump_text(GameEksParent));
	
	EksParent *runProgramOrLevel;
	
	if(data==NULL)
	{
		runProgramOrLevel=NULL;
		if(GLOBAL_HAED_BUILD_POLICY==GLOBAL_HAED_BUILD_POLICY_PGD_PATH)
		{
			asprintf(&makecommand,"make run -C %s",GLOBAL_HAED_PGD_PATH);
		}
		else if(GLOBAL_HAED_BUILD_POLICY==GLOBAL_HAED_BUILD_POLICY_USER_PATH)
		{
			asprintf(&makecommand,"mkdir -p %s/game && cp -ruf %s/* %s/game && make run -C %s/game",
			         GLOBAL_HAED_USER_PATH,GLOBAL_HAED_PGD_PATH,GLOBAL_HAED_USER_PATH,GLOBAL_HAED_USER_PATH);
		}
		else
		{
			asprintf(&makecommand,"mkdir -p %s/%s && cp -ruf %s/* %s/%s && make run -C %s/%s",
			         GLOBAL_HAED_USER_PATH,"test",GLOBAL_HAED_PGD_PATH,GLOBAL_HAED_USER_PATH,"test",GLOBAL_HAED_USER_PATH,"test");
		}
	}
	else
	{
		runProgramOrLevel=haed_programinfo_get_current_level(GameEksParent);
		
		if(GLOBAL_HAED_BUILD_POLICY==GLOBAL_HAED_BUILD_POLICY_PGD_PATH)
		{
			asprintf(&makecommand,"make run -C %s RUN_LEVEL=-DRUN_LEVEL",GLOBAL_HAED_PGD_PATH);
		}
		else if(GLOBAL_HAED_BUILD_POLICY==GLOBAL_HAED_BUILD_POLICY_USER_PATH)
		{
			asprintf(&makecommand,"mkdir -p %s/game && cp -ruf %s/* %s/game && make run -C %s/game RUN_LEVEL=-DRUN_LEVEL",
			         GLOBAL_HAED_USER_PATH,GLOBAL_HAED_PGD_PATH,GLOBAL_HAED_USER_PATH,GLOBAL_HAED_USER_PATH);
		}
		else
		{
			asprintf(&makecommand,"mkdir -p %s/%s && cp -ruf %s/* %s/%s && make run -C %s/%s RUN_LEVEL=-DRUN_LEVEL",
			         GLOBAL_HAED_USER_PATH,"test",GLOBAL_HAED_PGD_PATH,GLOBAL_HAED_USER_PATH,"test",GLOBAL_HAED_USER_PATH,"test");
		}
	}
	
	char *GeneratedFile=haed_generate_c_program_file(GameEksParent,runProgramOrLevel);
	
	if(GeneratedFile!=NULL)
	{
		EksParent *objectP=eks_parent_get_child_from_name(GameEksParent,"objects");
		EksParent *activeP=eks_parent_get_child_from_name(objectP,"active");
		
		eks_parent_foreach_child_start(activeP,object)
		{
			if(eks_parent_compare_type(object,EKS_PARENT_TYPE_VALUE))
			{
				EksParent *objbuffholder=eks_parent_custom_get(object,HAED_OBJECT_BUFFHOLDER);
			
				eks_parent_foreach_child_start(objbuffholder,animationParent)
				{
					char *animationstr=eks_parent_get_string(animationParent);
					
					if(eks_parent_compare_type(animationParent,EKS_PARENT_TYPE_VALUE) && haed_animations_is_animation(animationstr))
					{
						g_autofree char *buffer=NULL;
						gsize buflength=0;

						b_tm("RUNLEVEL GENERATE IMG");
				
						b_tm("ANIMATIONSTR %s",animationstr);
						
						gdk_pixbuf_save_to_buffer(eks_parent_custom_get(animationParent,0),&buffer,&buflength,"png",NULL,NULL);
				
						g_autofree char *mkdir;
						asprintf(&mkdir,"%s/img/%s",GLOBAL_HAED_PGD_PATH,object->name);
						
						g_mkdir_with_parents(mkdir,0777);
						
						g_autofree char *imgfilename;
						asprintf(&imgfilename,"%s/img/%s/%s.png",GLOBAL_HAED_PGD_PATH,object->name,animationstr);
						
						b_tm("mkdir:: %s, filename:: %s",mkdir,imgfilename);
						
						save_file_tl(imgfilename,buffer,buflength);
					}
					
					free(animationstr);
				}eks_parent_foreach_child_end(objbuffholder,animationParent);
			}
		}eks_parent_foreach_child_end(activeP,object);
		
		g_autofree char *generated_filename=NULL;
		
		asprintf(&generated_filename,"%s/program.c",GLOBAL_HAED_PGD_PATH);
		
		save_file(generated_filename,GeneratedFile);
		
		system(makecommand);
	}
	else
	{
		b_error_message("Failed to create file!");
	}
	//printf("%s\n",GeneratedFile);
}
