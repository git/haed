/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/**
	Clean it up!
*/

#include "includes.h"

static int GLOBAL_DRAG_BOX_POS[4]={-1,-1,-1,-1};
static int MOV_OBJECT[2]={-1,-1};
static int JUST_MARKED=0;

void haed_set_storyboard_mode(GtkWidget *widget,gpointer   data)
{
	printf("set to storyboard\n");
	gtk_notebook_set_current_page(GTK_NOTEBOOK(GLOBAL_MAIN_NOTEBOOK),0);
}

void haed_set_level_editor_mode(GtkWidget *widget,gpointer   data)
{
	printf("set to level editor\n");
	gtk_notebook_set_current_page(GTK_NOTEBOOK(GLOBAL_MAIN_NOTEBOOK),1);
}

void haed_set_event_editor_mode(GtkWidget *widget,gpointer   data)
{
	printf("set to event editor\n");
	gtk_notebook_set_current_page(GTK_NOTEBOOK(GLOBAL_MAIN_NOTEBOOK),2);
}

void haed_update_event_editor_grid(GtkWidget *widget,gpointer data)
{
	printf("update event editor\n");
}

int create_popup_menu(GtkWidget *menu,GdkEvent *event)
{
	if(event->type == GDK_BUTTON_PRESS && event->button.button == GDK_BUTTON_SECONDARY)
	{
		gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, event->button.button, event->button.time);
		return 1;
	}
	return 0;
}

/**
	Add all objects visually to the different areas in the current ''level''?
	
	@todo
		move this elsewhere?

	@param programParent
		programparent to read from
*/
void haed_level_editor_update(void)
{
	if(GLOBAL_HAED_INFORMATION.firstProgram && GLOBAL_HAED_INFORMATION.firstProgram->program)
	{
		EksParent *objectsParent=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"objects");
		EksParent *activeParent=eks_parent_get_child_from_name(objectsParent,"active");
		
		eks_parent_foreach_child_start(activeParent,loopUnit)
		{
			if(eks_parent_compare_type(loopUnit,EKS_PARENT_TYPE_VALUE))
			{
				haed_object_set_level_editor_icons(loopUnit);
			}
		}eks_parent_foreach_child_end(activeParent,loopUnit);
	}
}

//===================EDIT ACTIVE OBJECT MENU=====================
//SELECT MOVEMENT WINDOW
void haed_select_movement_active_object_button(GtkWidget *widget,gpointer data)
{
	EksParent *thisObj=data;
	
	int movementType;
	
	movementType=haed_dialog_select_movement_active_object();
	//printf("%d \n",movementType);
	
	//thisObj->movType=movementType;
	
	switch(movementType)
	{
		//STOPPED
		case 0:
			printf("Movement set to stopped\n");
			haed_object_set_string(thisObj,"mov","none",0);
			//objectInnerSettings->player=0;
			//free(objectInnerSettings->speed);
			//free(objectInnerSettings->acceleration);
			//free(objectInnerSettings->deacceleration);
			//objectInnerSettings->speed=NULL;
			//objectInnerSettings->acceleration=NULL;
			//objectInnerSettings->deacceleration=NULL;
			
			haed_object_set_int(thisObj,"player",0,1);
			haed_object_set_double(thisObj,"speed",0,1);
			haed_object_set_double(thisObj,"acceleration",0,1);
			haed_object_set_double(thisObj,"deacceleration",0,1);
			
		break;
		//ALL DIRECTIONS
		case 1:
			haed_object_set_string(thisObj,"mov","all_directions",0);
			haed_object_set_int(thisObj,"player",1,0);
			haed_dialog_edit_movement_all_directions(thisObj);
		break;
	}
	
	//GLOBAL_HAED_ACTION_INF=NULL;
}

void haed_object_modify_movement(GtkWidget *widget,gpointer data)
{
	EksParent *thisObj=data;

	haed_dialog_edit_movement_all_directions(thisObj);
}

void haed_select_player_active_object_button(GtkWidget *widget,gpointer data)
{
	intptr_t player=(intptr_t)data;

	EksParent *thisObj=g_object_get_data(G_OBJECT(widget),"haed_object");
	
	if(thisObj==NULL)
	{
		thisObj=eks_parent_custom_get(GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits,0);
	}

	//EksParent *objectInnerSettings;
	//objectInnerSettings=(EksParent*)(thisObj->objectTypeStruct);
	
	//objectInnerSettings->player=(int)(long)data;
	haed_object_set_int(thisObj,"player",player,0);
	
	printf("Player set to: %ld\n",player);

	//GLOBAL_HAED_ACTION_INF=NULL;
}

//REMOVE OBJECT FROM LEVEL EDITOR
void haed_level_editor_active_object_remove_from_draw_area(GtkWidget *widget,gpointer data)
{
	EksParent *obj=data;
	
	b_tm("remove obj: %p",obj);
	
	if(obj)
	{
		EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
		EksParent *firstUnit=haed_unitlist_get_positions(curLevel)->firstChild;//curLevel->firstUnit->firstChild;
		haed_unitlist_remove(firstUnit,obj);
		
		gtk_widget_queue_draw(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);
	}
}

/**
	The popupmenu which will appear if you click on a object in the field or at the sidebar.

	@param widget NO_FREE
		The parent widget
	@param event
		the event
	@param data NO_FREE
		???
	@return
		void
*/
///VARNING KÖRS ÄVEN I OBJECTSTRUCTURE!!!!
void haed_level_editor_active_object_popup_menu(GtkWidget *widget,GdkEvent *event, gpointer data)
{
	EksParent *thisObj=g_object_get_data(G_OBJECT(widget),"haed_object");
	int buttonOrOnField=1;
	
	
	if(thisObj==NULL)
	{
		thisObj=eks_parent_custom_get(GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits,0);
		buttonOrOnField=0;
	}
	
	b_tm("object %s %d",thisObj->name,buttonOrOnField);

	GtkMenuListInner placedObject;
	GtkMenuListInner playerType;

	if(GLOBAL_MAIN_ACTIVE_OBJECT_MENU!=NULL)
	{
		gtk_widget_destroy(GLOBAL_MAIN_ACTIVE_OBJECT_MENU);
		GLOBAL_MAIN_ACTIVE_OBJECT_MENU=NULL;
	}
	
	//the side widget is NOT selected
	if(!buttonOrOnField)
	{
		placedObject=(GtkMenuListInner){"Remove",haed_level_editor_active_object_remove_from_draw_area,GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits};
	}
	else
	{
		placedObject=(GtkMenuListInner){NULL,NULL,NULL};
	}
	
	char *compstring=haed_object_get_string(thisObj,"mov");
	
	if(compstring && strcmp(compstring,"none")==0)
	{
		playerType=(GtkMenuListInner){"Select Player",NULL,NULL,(GtkMenuListInner[])
		{
			{"1",haed_select_player_active_object_button,(void*)1},
			{"2",haed_select_player_active_object_button,(void*)2},
			{"3",haed_select_player_active_object_button,(void*)3},
			{"4",haed_select_player_active_object_button,(void*)4}
		},4};
	}
	else
	{
		playerType=(GtkMenuListInner){NULL,NULL,NULL};
	}
	
	//general stuff
	GtkMenuListInner GLOBAL_MAIN_ACTIVE_OBJECT_MENUList[]=
	{
		{"Movement",NULL,NULL,
		(GtkMenuListInner[]){
			{"Select Movement",haed_select_movement_active_object_button,thisObj},
			{"Edit Movement",haed_object_modify_movement,thisObj},
			playerType,
		},3},
		{"View",NULL,NULL},
		{"Edit animations",haed_draw_active_object_frame_editor,eks_parent_custom_get(thisObj,HAED_OBJECT_BUFFHOLDER)},
		{"Object Preferences",haed_object_settings_window,thisObj}, //add edit name and icon here
		{"New Object",NULL,NULL},
		{"Resize",NULL,NULL},
		{"Align",NULL,NULL},
		placedObject
	};

	GLOBAL_MAIN_ACTIVE_OBJECT_MENU=gtk_menu_new_from_list(GLOBAL_MAIN_ACTIVE_OBJECT_MENUList,sizeof(GLOBAL_MAIN_ACTIVE_OBJECT_MENUList)/sizeof(GLOBAL_MAIN_ACTIVE_OBJECT_MENUList[0]));
	gtk_widget_show_all(GLOBAL_MAIN_ACTIVE_OBJECT_MENU);
	
	create_popup_menu(GLOBAL_MAIN_ACTIVE_OBJECT_MENU,event);
}
//==============================================

/**
	What happens when you click on the button to "draw" out an object in the main layot

	@param widget NO_FREE
		The button widget (the one you clicked on)
	@param data NO_FREE
		???
	@return
		void
*/
void haed_set_current_layout_object_pointer(GtkWidget *widget,gpointer data)
{
	b_debug_message("set layout pointer");

	if(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget!=NULL && widget!=GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget)
	{
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget),0);
		GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget=NULL;
	}
	GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget=widget;
	EksParent *temp=g_object_get_data(G_OBJECT(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget),"haed_object");
	
	b_debug_message("set pointer to:%s",temp->name);
}

static void haed_level_get_dimensions(int pos_a_x, int pos_a_y, int pos_b_x, int pos_b_y, int *pos_top_left_x, int *pos_top_left_y, int *width, int *height)
{
	if(GLOBAL_DRAG_BOX_POS[2]-GLOBAL_DRAG_BOX_POS[0]<0 && GLOBAL_DRAG_BOX_POS[3]-GLOBAL_DRAG_BOX_POS[1]<0)
	{
		*pos_top_left_x=GLOBAL_DRAG_BOX_POS[2];
		*pos_top_left_y=GLOBAL_DRAG_BOX_POS[3];
		*width=GLOBAL_DRAG_BOX_POS[0]-GLOBAL_DRAG_BOX_POS[2];
		*height=GLOBAL_DRAG_BOX_POS[1]-GLOBAL_DRAG_BOX_POS[3];
	}
	else if(GLOBAL_DRAG_BOX_POS[2]-GLOBAL_DRAG_BOX_POS[0]>0 && GLOBAL_DRAG_BOX_POS[3]-GLOBAL_DRAG_BOX_POS[1]>0)
	{
		*pos_top_left_x=GLOBAL_DRAG_BOX_POS[0];
		*pos_top_left_y=GLOBAL_DRAG_BOX_POS[1];
		*width=GLOBAL_DRAG_BOX_POS[2]-GLOBAL_DRAG_BOX_POS[0];
		*height=GLOBAL_DRAG_BOX_POS[3]-GLOBAL_DRAG_BOX_POS[1];
	}
	else if(GLOBAL_DRAG_BOX_POS[2]-GLOBAL_DRAG_BOX_POS[0]<0 && GLOBAL_DRAG_BOX_POS[3]-GLOBAL_DRAG_BOX_POS[1]>0)
	{
		*pos_top_left_x=GLOBAL_DRAG_BOX_POS[2];
		*pos_top_left_y=GLOBAL_DRAG_BOX_POS[1];
		*width=GLOBAL_DRAG_BOX_POS[0]-GLOBAL_DRAG_BOX_POS[2];
		*height=GLOBAL_DRAG_BOX_POS[3]-GLOBAL_DRAG_BOX_POS[1];
	}
	else if(GLOBAL_DRAG_BOX_POS[2]-GLOBAL_DRAG_BOX_POS[0]>0 && GLOBAL_DRAG_BOX_POS[3]-GLOBAL_DRAG_BOX_POS[1]<0)
	{
		*pos_top_left_x=GLOBAL_DRAG_BOX_POS[0];
		*pos_top_left_y=GLOBAL_DRAG_BOX_POS[3];
		*width=GLOBAL_DRAG_BOX_POS[2]-GLOBAL_DRAG_BOX_POS[0];
		*height=GLOBAL_DRAG_BOX_POS[1]-GLOBAL_DRAG_BOX_POS[3];
	}
	else
	{
		*pos_top_left_x=0;
		*pos_top_left_y=0;
		*width=0;
		*height=0;
	}
}

/**
	Drawing events

	@param widget
		input widget
	@param cr
		cairo drawing 
	@param data
		user data
	
	@return boolean
		
*/
gboolean haed_level_editor_signal_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
	int widgetWidth;
	int widgetHeight;

	EksParent *curLevel;

	if(data==NULL)
	{
		widgetWidth=gtk_widget_get_allocated_width(widget);
		widgetHeight=gtk_widget_get_allocated_height(widget);
		
		curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
	}
	else
	{
		widgetWidth=GLOBAL_SCREEN_WIDTH*1.3;
		widgetHeight=GLOBAL_SCREEN_HEIGHT*1.3;
		
		cairo_scale(cr, (double)gtk_widget_get_allocated_width(widget)/widgetWidth, (double)gtk_widget_get_allocated_height(widget)/widgetHeight);
		
		curLevel=data;
	}
	
	GLOBAL_MARGIN_SCREEN_WIDTH=widgetWidth-GLOBAL_SCREEN_WIDTH>0?(widgetWidth-GLOBAL_SCREEN_WIDTH)/2:GLOBAL_MARGIN_SCREEN_WIDTH_MIN;
	GLOBAL_MARGIN_SCREEN_HEIGHT=widgetHeight-GLOBAL_SCREEN_HEIGHT>0?(widgetHeight-GLOBAL_SCREEN_HEIGHT)/2:GLOBAL_MARGIN_SCREEN_HEIGHT_MIN;
	
	cairo_rectangle(cr,0,0,widgetWidth,widgetHeight);
	
	int bRed,bGreen,bBlue;
			
	if(eks_parent_get_child_from_name(curLevel,"background-color"))
	{
		bRed=haed_get_int_from_pos(curLevel,"background-color",0);
		bGreen=haed_get_int_from_pos(curLevel,"background-color",1);
		bBlue=haed_get_int_from_pos(curLevel,"background-color",2);
	}
	else
	{
		bRed=255;
		bGreen=255;
		bBlue=255;
	}
	
	cairo_set_source_rgba (cr, 0.9, 0.9, 0.9, 1.0);
	cairo_fill (cr);
	//draw the area that will be shown
	cairo_rectangle(cr,GLOBAL_MARGIN_SCREEN_WIDTH-GLOBAL_PLACER_SCREEN_RANGE_X,GLOBAL_MARGIN_SCREEN_HEIGHT-GLOBAL_PLACER_SCREEN_RANGE_Y,GLOBAL_SCREEN_WIDTH,GLOBAL_SCREEN_HEIGHT);
	cairo_set_source_rgba (cr, (double)bRed/255, (double)bGreen/255, (double)bBlue/255, 1.0);
	cairo_fill (cr);
	
	//b_tm("GOT_HERE! %p %d",GLOBAL_HAED_INFORMATION.firstProgram->program,haed_trigghandler_count_objects(GLOBAL_ACTIVE_OBJECT_S));
	
	EksParent *firstUnit=haed_unitlist_get_positions(curLevel);

	eks_parent_foreach_child_start(firstUnit,loopUnit)
	{
		EksParent *unitObject=eks_parent_custom_get(loopUnit,0);
		GdkPixbuf *obj_pixbuff=eks_parent_custom_get(unitObject,HAED_OBJECT_IMAGEBUFF);
		
		int obj_x=haed_unit_get_coord_x(loopUnit);
		int obj_y=haed_unit_get_coord_y(loopUnit);
		int frame_width=gdk_pixbuf_get_width(obj_pixbuff);
		int frame_height=gdk_pixbuf_get_height(obj_pixbuff);
		
		void *is_marked=eks_parent_custom_get(loopUnit,HAED_POS_OBJECT_MARKED);
		
		if(is_marked==HAED_UNIT_IS_MARKED)
		{
			cairo_rectangle(cr,obj_x-GLOBAL_PLACER_SCREEN_RANGE_X+GLOBAL_MARGIN_SCREEN_WIDTH-2, obj_y-GLOBAL_PLACER_SCREEN_RANGE_Y+GLOBAL_MARGIN_SCREEN_HEIGHT-2,
				            frame_width+4, frame_height+4);
			cairo_set_source_rgba (cr, 0, 0, 0,0.5);
			cairo_fill (cr);
		}
		
		gdk_cairo_set_source_pixbuf(cr,obj_pixbuff,obj_x-GLOBAL_PLACER_SCREEN_RANGE_X+GLOBAL_MARGIN_SCREEN_WIDTH,obj_y-GLOBAL_PLACER_SCREEN_RANGE_Y+GLOBAL_MARGIN_SCREEN_HEIGHT);
	
		cairo_paint (cr);
		cairo_fill (cr);
		
	}eks_parent_foreach_child_end(firstUnit,loopUnit);
	
	if(GLOBAL_DRAG_BOX_POS[0]!=-1)
	{
		int box_pos_top_left_x=0;
		int box_pos_top_left_y=0;
		int box_width=0;
		int box_height=0;
		haed_level_get_dimensions(GLOBAL_DRAG_BOX_POS[0],GLOBAL_DRAG_BOX_POS[1],GLOBAL_DRAG_BOX_POS[2],GLOBAL_DRAG_BOX_POS[3],&box_pos_top_left_x,&box_pos_top_left_y,&box_width,&box_height);
	
		cairo_rectangle(cr,box_pos_top_left_x,box_pos_top_left_y,box_width,box_height);
		
		//b_tm("HIT!");
		
		cairo_set_source_rgba (cr, 0, 0, 0,0.5);
		
		cairo_fill (cr);
	}
	
	return 1;
}

int haed_level_editor_move_marked_relative_shift(int shift_x,int shift_y)
{
	EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
	EksParent *positions=haed_unitlist_get_positions(curLevel);
	
	eks_parent_foreach_child_start(positions,loopUnit)
	{
		void *is_marked=eks_parent_custom_get(loopUnit,HAED_POS_OBJECT_MARKED);
		
		if(is_marked==HAED_UNIT_IS_MARKED)
		{
			EksParent *x_parent=eks_parent_get_child_from_type(loopUnit,0,EKS_PARENT_TYPE_VALUE);
			EksParent *y_parent=eks_parent_get_child_from_type(loopUnit,1,EKS_PARENT_TYPE_VALUE);
			
			x_parent->iname+=shift_x;
			y_parent->iname+=shift_y;
		}
	}eks_parent_foreach_child_end(positions,loopUnit);
	
	return 0;
}

int haed_level_editor_move_marked_relative_object(EksParent *object,int pos_x, int pos_y)
{
	EksParent *x_parent=eks_parent_get_child_from_type(object,0,EKS_PARENT_TYPE_VALUE);
	EksParent *y_parent=eks_parent_get_child_from_type(object,1,EKS_PARENT_TYPE_VALUE);

	int obj_now_x=x_parent->iname;
	int obj_now_y=y_parent->iname;
	
	int shift_x=pos_x-obj_now_x;
	int shift_y=pos_y-obj_now_y;

	haed_level_editor_move_marked_relative_shift(shift_x,shift_y);
}

int haed_level_editor_reset_all_marked(void)
{
	if(JUST_MARKED==0)
	{
		EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
		EksParent *positions=haed_unitlist_get_positions(curLevel);
		
		eks_parent_foreach_child_start(positions,loopUnit)
		{
			eks_parent_custom_set(loopUnit,HAED_POS_OBJECT_MARKED,HAED_UNIT_IS_UNMARKED);
		}eks_parent_foreach_child_end(positions,loopUnit);
	}
	return 0;
}

///TODO
int haed_level_editor_mark_objects(GdkEvent *event, int box_top_x, int box_top_y, int box_width, int box_height)
{
	haed_level_editor_reset_all_marked();

	b_tm("mark release3 :: %d %d -> %d %d",box_top_x,box_top_y,box_width,box_height);
	
	EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
	EksParent *positions=haed_unitlist_get_positions(curLevel);
	
	eks_parent_foreach_child_start(positions,loopUnit)
	{
		GdkPixbuf *objimagebuff=eks_parent_custom_get((EksParent*)eks_parent_custom_get(loopUnit,0),HAED_OBJECT_IMAGEBUFF);
/*		EksParent *holder=eks_parent_custom_get(thisObject,HAED_OBJECT_BUFFHOLDER);*/
/*		EksParent *buf_settings=eks_parent_get_child(eks_parent_get_child_from_name(eks_parent_get_child_from_name(eks_parent_get_child_from_name(holder,GLOBAL_ANIMATION_STOPPED),"ALL_MIRROR"),"FRAMES"),0);*/
	
		int curr_x=haed_unit_get_coord_x(loopUnit);
		int curr_y=haed_unit_get_coord_y(loopUnit);
/*		int frame_width=eks_parent_get_int(eks_parent_get_child(buf_settings,2));*/
/*		int frame_height=eks_parent_get_int(eks_parent_get_child(buf_settings,3));*/
		int frame_width=gdk_pixbuf_get_width(objimagebuff);
		int frame_height=gdk_pixbuf_get_height(objimagebuff);
		
		if((double)(curr_x-GLOBAL_PLACER_SCREEN_RANGE_X+GLOBAL_MARGIN_SCREEN_WIDTH)>=box_top_x && (double)(curr_x-GLOBAL_PLACER_SCREEN_RANGE_X+GLOBAL_MARGIN_SCREEN_WIDTH+frame_width)<=box_top_x+box_width && 
			(double)(curr_y-GLOBAL_PLACER_SCREEN_RANGE_Y+GLOBAL_MARGIN_SCREEN_HEIGHT)>=box_top_y && (double)(curr_y-GLOBAL_PLACER_SCREEN_RANGE_Y+GLOBAL_MARGIN_SCREEN_HEIGHT+frame_height)<=box_top_y+box_height)
		{
			printf("LOOP_OBJ:: %p marked\n",loopUnit);
			eks_parent_custom_set(loopUnit,HAED_POS_OBJECT_MARKED,HAED_UNIT_IS_MARKED);
			GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits=loopUnit;
		}
	}eks_parent_foreach_child_end(positions,loopUnit);
	
	return 0;
}

/**
	Event handling for the level editor

	@param widget
		input widget
	@param event
		input event (with all signals)
	@param data
		user data
	
	@return void
*/
void haed_level_editor_signal_event(GtkWidget *widget,GdkEvent *event,gpointer data)
{
	//b_tm("event");
	EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
	EksParent *positions=haed_unitlist_get_positions(curLevel);

	if(event->button.type==GDK_BUTTON_PRESS)
	{
		eks_parent_foreach_child_start(positions,loopUnit)
		{
			int currX=haed_unit_get_coord_x(loopUnit);
			int currY=haed_unit_get_coord_y(loopUnit);
			
			b_tm("TESTEST: mx=%g my=%g",event->motion.x,event->motion.y);
			
			GdkPixbuf *objimagebuff=eks_parent_custom_get((EksParent*)eks_parent_custom_get(loopUnit,0),HAED_OBJECT_IMAGEBUFF);
			
			if((double)(currX-GLOBAL_PLACER_SCREEN_RANGE_X+GLOBAL_MARGIN_SCREEN_WIDTH)<=event->motion.x && (double)(currX-GLOBAL_PLACER_SCREEN_RANGE_X+GLOBAL_MARGIN_SCREEN_WIDTH+gdk_pixbuf_get_width(objimagebuff))>=event->motion.x && 
				(double)(currY-GLOBAL_PLACER_SCREEN_RANGE_Y+GLOBAL_MARGIN_SCREEN_HEIGHT)<=event->motion.y && (double)(currY-GLOBAL_PLACER_SCREEN_RANGE_Y+GLOBAL_MARGIN_SCREEN_HEIGHT+gdk_pixbuf_get_height(objimagebuff))>=event->motion.y)
			{
/*				gtk_widget_grab_focus(widget);*/
				eks_parent_custom_set(loopUnit,HAED_POS_OBJECT_MARKED,HAED_UNIT_IS_MARKED);
				JUST_MARKED=1;
				
				//temp solution, should support multiple objects
				GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits=loopUnit;
			
				if(event->button.button==3)
				{
					haed_level_editor_active_object_popup_menu(NULL,event,NULL);
					MOV_OBJECT[0]=-1;
/*						b_tm("mark release1 :: %d %d -> %d %d",GLOBAL_DRAG_BOX_POS[0],GLOBAL_DRAG_BOX_POS[1],GLOBAL_DRAG_BOX_POS[2],GLOBAL_DRAG_BOX_POS[3]);*/
					GLOBAL_DRAG_BOX_POS[0]=-1;
					return;
				}
				else if(event->button.button==1)
				{
					MOV_OBJECT[0]=event->motion.x-currX;
					MOV_OBJECT[1]=event->motion.y-currY;
				}
			}
		}eks_parent_foreach_child_end(positions,loopUnit);
	}
	
	
	if(GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits && event->type==GDK_KEY_PRESS)
	{
		int px=0,py=0;
		
		if(event->key.keyval==GDK_KEY_Up)
		{
			py=-1;
		}
		if(event->key.keyval==GDK_KEY_Down)
		{
			py=1;
		}
		if(event->key.keyval==GDK_KEY_Left)
		{
			px=-1;
		}
		if(event->key.keyval==GDK_KEY_Right)
		{
			px=1;
		}
		
		EksParent *currUnit=GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits;

/*		EksParent *xParent=eks_parent_get_child_from_type(currUnit,0,EKS_PARENT_TYPE_VALUE);*/
/*		EksParent *yParent=eks_parent_get_child_from_type(currUnit,1,EKS_PARENT_TYPE_VALUE);*/
/*		*/
/*		xParent->iname+=px;*/
/*		yParent->iname+=py;*/
		haed_level_editor_move_marked_relative_shift(px,py);
		
		gtk_widget_queue_draw(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);
	}
	
	if(event->button.type==GDK_MOTION_NOTIFY)
	{
		if(MOV_OBJECT[0]!=-1)
		{
			EksParent *currUnit=GLOBAL_HAED_INFORMATION.levelEditorSelectedUnits;
			
/*			EksParent *xParent=eks_parent_get_child_from_type(currUnit,0,EKS_PARENT_TYPE_VALUE);*/
/*			EksParent *yParent=eks_parent_get_child_from_type(currUnit,1,EKS_PARENT_TYPE_VALUE);*/
/*			*/
/*			xParent->iname=event->motion.x-MOV_OBJECT[0];*/
/*			yParent->iname=event->motion.y-MOV_OBJECT[1];*/
/*			*/
			haed_level_editor_move_marked_relative_object(currUnit,event->motion.x-MOV_OBJECT[0],event->motion.y-MOV_OBJECT[1]);
			//MOV_OBJECT[0]=-1;
/*			b_tm("mark release2 :: %d %d -> %d %d",GLOBAL_DRAG_BOX_POS[0],GLOBAL_DRAG_BOX_POS[1],GLOBAL_DRAG_BOX_POS[2],GLOBAL_DRAG_BOX_POS[3]);*/
			GLOBAL_DRAG_BOX_POS[0]=-1;
		}
		else
		{
			GLOBAL_DRAG_BOX_POS[2]=event->motion.x;
			GLOBAL_DRAG_BOX_POS[3]=event->motion.y;
		}
		
		gtk_widget_queue_draw(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);
	}
	
	if(event->button.type==GDK_BUTTON_RELEASE)
	{
		int box_pos_top_left_x=0;
		int box_pos_top_left_y=0;
		int box_width=0;
		int box_height=0;
		haed_level_get_dimensions(GLOBAL_DRAG_BOX_POS[0],GLOBAL_DRAG_BOX_POS[1],GLOBAL_DRAG_BOX_POS[2],GLOBAL_DRAG_BOX_POS[3],&box_pos_top_left_x,&box_pos_top_left_y,&box_width,&box_height);
	
		haed_level_editor_mark_objects(event, box_pos_top_left_x,box_pos_top_left_y,box_width,box_height);
		GLOBAL_DRAG_BOX_POS[0]=-1;
		MOV_OBJECT[0]=-1;
		gtk_widget_queue_draw(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);
		JUST_MARKED=0;
	}
	
	if(event->button.type==GDK_BUTTON_PRESS)
	{
		//gtk_widget_hide(GLOBAL_DRAG_BOX_WIDGET);
		if(GLOBAL_DRAG_BOX_POS[0]==-1 && MOV_OBJECT[0]==-1)
		{
			GLOBAL_DRAG_BOX_POS[0]=event->motion.x;
			GLOBAL_DRAG_BOX_POS[1]=event->motion.y;
		}
		
		//for the output of objects
		if(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget!=NULL && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget)))
		{
			
			if(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget)
				haed_unitlist_add(positions,(EksParent*)g_object_get_data(G_OBJECT(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget),"haed_object"),(int)event->motion.x-GLOBAL_MARGIN_SCREEN_WIDTH+GLOBAL_PLACER_SCREEN_RANGE_X,(int)event->motion.y-GLOBAL_MARGIN_SCREEN_HEIGHT+GLOBAL_PLACER_SCREEN_RANGE_Y);
			
			if(event->button.button==1)
			{
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget),0);
				GLOBAL_HAED_INFORMATION.levelEditorSelectedSideBarWidget=NULL;
			}
			
			gtk_widget_queue_draw(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);
		}
	}
	//unfocus?
	//gtk_widget_grab_focus(GLOBAL_MAIN_WINDOW);
	if(!gtk_widget_is_focus(widget))
		gtk_widget_grab_focus(widget);
}

void haed_update_event_editor_placer(GtkRange *range,gpointer  user_data)
{
	gtk_widget_queue_draw(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);
	
	GLOBAL_PLACER_SCREEN_RANGE_X=((gtk_range_get_value(GTK_RANGE(GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[1]))*abs(GLOBAL_MARGIN_SCREEN_WIDTH*2+GLOBAL_SCREEN_WIDTH-gtk_widget_get_allocated_width(GLOBAL_LEVEL_EDITOR_DRAWING_AREA)))/100);
	GLOBAL_PLACER_SCREEN_RANGE_Y=((gtk_range_get_value(GTK_RANGE(GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[0]))*abs(GLOBAL_MARGIN_SCREEN_HEIGHT*2+GLOBAL_SCREEN_HEIGHT-gtk_widget_get_allocated_height(GLOBAL_LEVEL_EDITOR_DRAWING_AREA)))/100);
	
	//printf("%f %f\n",GLOBAL_PLACER_SCREEN_RANGE_X,GLOBAL_PLACER_SCREEN_RANGE_Y);
}
