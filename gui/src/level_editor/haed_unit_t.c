/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

EksParent *haed_unitlist_get_unit_from_position(EksParent *firstUnit, int pos)
{
	return eks_parent_get_child(firstUnit,pos);
}

void haed_unitlist_add(EksParent *firstUnit, EksParent *object, int x,int y)
{
	b_debug_message("ADDING: %s",object->name);

	EksParent *unit=eks_parent_add_child(firstUnit,object->name,EKS_PARENT_TYPE_VALUE,NULL);
	
	eks_parent_custom_set(unit,HAED_POS_OBJECT_DECLARE,object);
	eks_parent_custom_set(unit,HAED_POS_OBJECT_MARKED,HAED_UNIT_IS_UNMARKED);
	
	eks_parent_add_child(unit,x,EKS_PARENT_TYPE_VALUE,NULL);
	eks_parent_add_child(unit,y,EKS_PARENT_TYPE_VALUE,NULL);
}

void haed_unitlist_remove(EksParent *firstUnit, EksParent *remobj)
{
	eks_parent_destroy(remobj,EKS_TRUE);
}

void haed_unitlist_remove_from_marked(EksParent *firstUnit)
{
	/*
	EksParent *rFirstUnit=firstUnit->firstChild;

	if(!firstUnit)
	{
		b_error_message("No first child?");
		return;
	}
		
	EksParent *loopUnit=rFirstUnit;
	EksParent *tempLoop;

	do
	{
		tempLoop=loopUnit->nextUnit;
		
		if(haed_unit_is_marked(loopUnit))
		{
			b_tm("REMOVING HIT!");
			
			if(loopUnit==rFirstUnit)
			{
				if(loopUnit->nextUnit==loopUnit && loopUnit->prevUnit==loopUnit)
				{
					(*firstUnit)=NULL;
					b_tm("exited mass deletion!");
					return;
				}
				else
				{
					//for human logics
					(*firstUnit)=loopUnit->nextUnit;
					//so it can delete
					rFirstUnit=loopUnit->prevUnit;
				}
			}
			
			loopUnit->nextUnit->prevUnit=loopUnit->prevUnit;
			loopUnit->prevUnit->nextUnit=loopUnit->nextUnit;
			
			free(loopUnit);
		}
		
		loopUnit=tempLoop;
	}while(loopUnit!=rFirstUnit);
	*/
	return;
}

size_t haed_unitlist_count(EksParent *firstUnit)
{
	return eks_parent_get_amount_from_type(firstUnit,EKS_PARENT_TYPE_VALUE);
}

size_t haed_unitlist_object_count(EksParent *firstUnit,EksParent *object)
{
	EksParent *fUnit=firstUnit->firstChild;

	size_t returnSize=0;

	if(!fUnit)
	{
		b_error_message("No first child?");
		return 0;
	}
		
	EksParent *loopUnit=fUnit;

	do
	{
		if(eks_parent_custom_get(loopUnit,0)==object)
			returnSize++;
		
		loopUnit=loopUnit->nextChild;
	}while(loopUnit!=fUnit);
	
	return returnSize;
}

char *haed_unit_get_obj_name(EksParent *unit)
{
	return unit->name;
}

intptr_t haed_unit_get_coord_x(EksParent *unit)
{
	EksParent *thisParent=eks_parent_get_child_from_type(unit,0,EKS_PARENT_TYPE_VALUE);
	return eks_parent_get_int(thisParent);
}

intptr_t haed_unit_get_coord_y(EksParent *unit)
{
	EksParent *thisParent=eks_parent_get_child_from_type(unit,1,EKS_PARENT_TYPE_VALUE);
	return eks_parent_get_int(thisParent);
}

EksParent *haed_unitlist_get_positions(EksParent *level)
{
	EksParent *positions=eks_parent_get_child_from_name(level,"positions");

	if(!positions)
		positions=eks_parent_add_child(level,"positions",EKS_PARENT_TYPE_VALUE,NULL);
		
	return positions;
}

