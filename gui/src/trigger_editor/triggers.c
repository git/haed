/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//==============ACTIONS===============

/**
	Creates the popup menu to select conditon

	@param widget NO_FREE
		the widget the user hovers
	@param event NO_FREE
		what happens
	@param data → (EksParent*) NO_FREE
		some inf to send
	@return
		void
*/
void trigger_conditions_on_click(GtkWidget *widget,GdkEvent *event, gpointer user_data)
{
	EksParent *conf_trigger_parent=eks_parent_get_child_from_name(GLOBAL_HAED_CONFIGURATION->data,"triggers");
	EksParent *conf_conditions_parent=eks_parent_get_child_from_name(conf_trigger_parent,"conditions");
	EksParent *conf_object_parent=eks_parent_get_child_from_name(conf_conditions_parent,user_data);

	GLOBAL_HAED_TEMP_HAEDOBJECT=(EksParent*)g_object_get_data(G_OBJECT(widget),"object_struct");

	haed_triggers_create_popup_from_eks_description(conf_object_parent,NULL,event,0,haed_trigger_conditions_add_internal);
}

/**
	Remove the current action and show it

	@param widget
		irrelevant
	@param data
		also irrelevant
*/
void haed_triggers_remove_action(GtkWidget *widget,gpointer data)
{
	b_debug_message("REMOVING ACTION");
	
	EksParent *conditionEksParent=g_object_get_data(G_OBJECT(GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton),"haed_condition");;
	GtkWidget *currentWidget=GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton;
	
	if(conditionEksParent==NULL)
	{
		b_error_message("ERROR ON REMOVING ACTION!");
	}
	else
	{
		b_tm("GOT HERE %s",gtk_widget_get_tooltip_text(currentWidget));
	
		size_t numberOfChildsAfterRemoval=0;
		
		char **explodedString=g_strsplit(gtk_widget_get_tooltip_text(currentWidget),"\n",0);

		int explodelen=0,doNexUnit=0;
		
		if(explodedString!=NULL)
		{
			while(explodedString[explodelen]!=NULL){
				b_tm("EXPLODED STRING %d %s",explodelen,explodedString[explodelen]);
				explodelen++;
			}
		}

		char *infotext=NULL;
		
		int destroyed=0;
		
		EksParent *firstUnit=conditionEksParent->firstChild;

		if(!firstUnit)
			return;
	
		b_tm("GOT HERE %d",explodelen);
	
		EksParent *loopUnit=firstUnit;
		EksParent *sloopUnit;

		do
		{
			doNexUnit=0;
			sloopUnit=loopUnit->nextChild;
			
			char *curChildName=eks_parent_get_string(loopUnit);
			
			for(int j=0;j<explodelen;j++)
			{
				b_tm("Match: %s,%s",curChildName,explodedString[j]);
		
				if(strcmp(curChildName,explodedString[j])==0)
				{
					if(destroyed==0 && j==0)
					{
						b_tm("REMOVING: %s",loopUnit->name);
						eks_parent_destroy(loopUnit,EKS_TRUE);
						numberOfChildsAfterRemoval=explodelen-1;
						
						b_tm("NUM: %ld",numberOfChildsAfterRemoval);
						
						if(numberOfChildsAfterRemoval<=0)
						{
							gtk_button_set_label(GTK_BUTTON(currentWidget)," ");
							gtk_widget_set_tooltip_text(currentWidget,NULL);
							
							return;
						}
						
						destroyed++;
						
						if(conditionEksParent->firstChild!=NULL)
						{
							firstUnit=conditionEksParent->firstChild->prevChild;
							loopUnit=conditionEksParent->firstChild;
						}
						else
							return;
						
						doNexUnit=1;
						
						goto do_next_object;
					}
					else
					{
						size_t textlen=strlen(curChildName);
					
						size_t itextlen=infotext?strlen(infotext):0;
	
						int isNotLast=(destroyed-1<(numberOfChildsAfterRemoval-1));
	
						if(infotext)
							infotext=realloc(infotext,textlen+itextlen+1+isNotLast);
						else
							infotext=malloc(textlen+itextlen+1+isNotLast);
		
						memcpy(infotext+itextlen,curChildName,textlen);
		
						if(isNotLast)
						{
							infotext[textlen+itextlen]='\n';
							infotext[textlen+itextlen+1]='\0';
						}
						else
							infotext[textlen+itextlen]='\0';
		
						b_tm("mess=%s %s",infotext,curChildName);
						
						destroyed++;
					}
				}
			}
			
			do_next_object:

			loopUnit=sloopUnit;
		}while(loopUnit!=firstUnit || doNexUnit==1);
		
		gtk_widget_set_tooltip_text(currentWidget,infotext);
	}
}

/**
	Creates the popup menu to select action

	@param widget NO_FREE
		the widget the user hovers
	@param event NO_FREE
		what happens
	@param data
	@return
		void
*/
void haed_trigger_action_on_click(GtkWidget *widget,GdkEvent *event, gpointer user_data)
{
	int status=0;
	
	EksParent *conf_trigger_parent=eks_parent_get_child_from_name(GLOBAL_HAED_CONFIGURATION->data,"triggers");
	EksParent *conf_conditions_parent=eks_parent_get_child_from_name(conf_trigger_parent,"actions");
	EksParent *conf_object_parent=eks_parent_get_child_from_name(conf_conditions_parent,user_data);
	
	GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton=widget;
	
	if(strcmp("X",gtk_button_get_label(GTK_BUTTON(widget)))==0)
	{
		status=1;
	}
	
	haed_triggers_create_popup_from_eks_description(conf_object_parent,NULL,event,status,haed_trigger_actions_add_internal);	
}
