/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

static void haed_object_set_trigger_icons(EksParent *thisObject)
{
	GdkPixbuf *imagebuff=eks_parent_custom_get(thisObject,HAED_OBJECT_IMAGEBUFF);
	//set the icon
	GdkPixbuf *iconbuff=gdk_pixbuf_scale_simple(imagebuff,32,32,GDK_INTERP_NEAREST);
	
	GtkWidget *triggerMenuWidget=gtk_toggle_button_new();
	GtkWidget *triggerButtonWidget=gtk_button_new();
	GtkWidget *triggerTopWidget=gtk_toggle_button_new();

	g_signal_connect(triggerButtonWidget,"clicked",G_CALLBACK(haed_get_object_from_selection_object_select),(void*)thisObject);
	g_object_set_data(G_OBJECT(triggerMenuWidget),"object_struct",thisObject);
	g_signal_connect(triggerMenuWidget, "button_press_event",G_CALLBACK(trigger_conditions_on_click),"STD_OBJECT");

	gtk_button_set_image(GTK_BUTTON(triggerButtonWidget),gtk_image_new_from_pixbuf(iconbuff));
	gtk_button_set_image(GTK_BUTTON(triggerMenuWidget),gtk_image_new_from_pixbuf(iconbuff));
	gtk_button_set_image(GTK_BUTTON(triggerTopWidget),gtk_image_new_from_pixbuf(iconbuff));
	
	gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_GET_OBJECT_BOX),triggerButtonWidget,0,0,0);
	
	gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_CONDITIONS_BOX),triggerMenuWidget,0,0,0);

	int curpos=haed_triggerhandler_get_pos_from_object(thisObject);

	b_tm("curpos %d",curpos);
	//for the top action objects
	gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),triggerTopWidget,curpos+3,1,1,1);
	gtk_widget_show_all(triggerTopWidget);
	
	eks_parent_custom_set(thisObject,HAED_OBJECT_TRIGGER_MENU_WIDGET,triggerMenuWidget);
	eks_parent_custom_set(thisObject,HAED_OBJECT_TRIGGER_BUTTON_WIDGET,triggerButtonWidget);
	eks_parent_custom_set(thisObject,HAED_OBJECT_TRIGGER_TOP_WIDGET,triggerTopWidget);
	g_object_unref(iconbuff);
}

/**
	Adding trigger object
*/
static void haed_object_add_trigger_std(const char *name,EksParent *holder)
{
	EksParent *thisObject;
	GdkPixbuf *iconbuff=NULL;
	
	b_tm("ADDING OBJECT %s",name);
	
	thisObject=eks_parent_new(name,EKS_PARENT_TYPE_VALUE,NULL,NULL);
	
	eks_parent_custom_set(thisObject,HAED_OBJECT_BUFFHOLDER,holder);
	
	haed_trigghandler_add_object(thisObject);
	
	//only create the widgets to display it along triggers and to make a condition from it
	GtkWidget *triggerMenuWidget=gtk_toggle_button_new();
	GtkWidget *triggerTopWidget=gtk_toggle_button_new();
	
	g_object_set_data(G_OBJECT(triggerMenuWidget),"object_struct",thisObject);
	
	if(g_strcmp0(name,GLOBAL_TRIGGERS_SPECIAL_S)==0)
	{
		iconbuff=gdk_pixbuf_new_from_file("./images/event_editor/special_conditions.png",NULL);
		g_signal_connect(triggerMenuWidget, "button_press_event",G_CALLBACK(trigger_conditions_on_click),"STD_SPECIAL");
	}
	else if(g_strcmp0(name,GLOBAL_TRIGGERS_SOUND_S)==0)
	{
		iconbuff=gdk_pixbuf_new_from_file("./images/event_editor/sound.png",NULL);
		g_signal_connect(triggerMenuWidget, "button_press_event",G_CALLBACK(trigger_conditions_on_click),"STD_SOUND");
	}
	else if(g_strcmp0(name,GLOBAL_TRIGGERS_STORY_S)==0)
	{
		iconbuff=gdk_pixbuf_new_from_file("./images/event_editor/storyboard_controls.png",NULL);
		g_signal_connect(triggerMenuWidget, "button_press_event",G_CALLBACK(trigger_conditions_on_click),"STD_STORYBOARD");
	}
	else if(g_strcmp0(name,GLOBAL_TRIGGERS_INPUT_S)==0)
	{
		iconbuff=gdk_pixbuf_new_from_file("./images/event_editor/input.png",NULL);
		g_signal_connect(triggerMenuWidget, "button_press_event",G_CALLBACK(trigger_conditions_on_click),"STD_INPUT");
	}

	gtk_button_set_image(GTK_BUTTON(triggerMenuWidget),gtk_image_new_from_pixbuf(iconbuff));
	gtk_button_set_image(GTK_BUTTON(triggerTopWidget),gtk_image_new_from_pixbuf(iconbuff));
	
	gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_CONDITIONS_BOX),triggerMenuWidget,0,0,0);

	int curpos=haed_triggerhandler_get_pos_from_object(thisObject);
	b_tm("curpos %d",curpos);
	//for the top action objects
	gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),triggerTopWidget,curpos+3,1,1,1);
	
	gtk_widget_show_all(triggerTopWidget);
}

/**
	Used by lexca to extract a list of objects found
	
	@param str
		used from function
	@param type
		type returned
	@param inparam
		a structure list, see below
*/
static int haed_eks_lexca_get_unit_data(const char *str,LexcaStrType type, size_t index,void *user_data)
{
	void **inparam=user_data;
	intptr_t pos=(intptr_t)inparam[0];

	if(type==LEXCA_TYPE_IDENTIFIER)
	{
		inparam[pos]=g_strdup(str);
		
		inparam[0]=(void*)(pos+1);
	}
	
	return 0;
}

/**
	Load triggers from a parent structure. This will load all triggers, and add them to the global structure.

	@param program
		the parent structure to load from
*/
static void haed_eks_set_triggers_from_parent(EksParent *program)
{
	b_tm("%s",eks_parent_dump_text(program));
	EksParent *curLevel=haed_programinfo_get_current_level(program);

	EksParent *mapConditions=eks_parent_get_child_from_name(curLevel,"triggers");

	TriggerHandler *thishandler=GLOBAL_HAED_TRIGGER_HANDLER;

	if(!(mapConditions && thishandler))
	{
		b_error_message("no map conditions or triggers to modify!");
		return;
	}
	
	b_tm("GETTING TRIGGERS! %ld %s",eks_parent_get_child_amount(mapConditions),eks_parent_dump_text(curLevel));

	for(int i=0;i<eks_parent_get_child_amount(mapConditions);i++)
	{
		EksParent *conditionEksParent=eks_parent_get_child(mapConditions,i);
		
		b_tm("thisname: %s\n",conditionEksParent->name);

		EksParent *condition=conditionEksParent;
		haed_triggers_insert_row(GLOBAL_HAED_TRIGGER_GRID,condition);

		//for setting the buttons correctly
		//go through all condition actions
		for(int j=0;j<eks_parent_get_child_amount(conditionEksParent);j++)
		{
			char *actionString=eks_parent_get_string(eks_parent_get_child(conditionEksParent,j));
			
			b_tm("actionString: %s",actionString);
			
			//
			void *inout[]={(void*)(intptr_t)1,NULL,NULL};
			
			lexca_for_each(actionString,haed_eks_lexca_get_unit_data,inout);
			
			//should get a list of objects
			char *firstFunctionName=inout[1];
			char *unitListString=inout[2];

			//to find the object
			for(int k=0;k<thishandler->objects->len;k++)
			{
				EksParent *theObject=g_ptr_array_index(thishandler->objects,k);
				
				if(theObject)
				{
					UITriggRow *last_row=g_ptr_array_index(thishandler->rows,thishandler->rows->len-1);
					
					GtkWidget *last_action_button=g_ptr_array_index(last_row->action_buttons,k);
					
					if(strcmp(theObject->upperEksParent->name,GLOBAL_ACTIVE_OBJECT_S)==0)
					{
						//if it found the object
						if(strcmp(theObject->name,unitListString)==0)
						{
							haed_triggers_add_visual_for_action(last_action_button,actionString);
						}
					}
					else if(strcmp(theObject->name,GLOBAL_TRIGGERS_STORY_S)==0 && strcmp(firstFunctionName,"SET_LEVEL")==0)
					{
						haed_triggers_add_visual_for_action(last_action_button,actionString);
					}
				}
			}
		}
	}
}

/**
	This function is supposed to initiate the entire GLOBAL_HAED_TRIGGER_GRID.
*/
static void haed_triggers_visual_init(void)
{
	//Define the box for the conditionswindow
	GLOBAL_MAIN_CONDITIONS_BOX=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);
	g_object_ref(GLOBAL_MAIN_CONDITIONS_BOX);
	//define for select object window
	GLOBAL_MAIN_GET_OBJECT_BOX=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);
	g_object_ref(GLOBAL_MAIN_GET_OBJECT_BOX);

	GLOBAL_HAED_TRIGGER_GRID=gtk_grid_new();
					
		GLOBAL_MAIN_TOP_LEFT_INFO=gtk_label_new("All the events");
		gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),GLOBAL_MAIN_TOP_LEFT_INFO,1,1,2,1);

		GLOBAL_LIST_FUNCTIONS[0]=gtk_button_new();
			g_signal_connect(GLOBAL_LIST_FUNCTIONS[0], "clicked",G_CALLBACK (haed_dialog_new_condition), 0);
			gtk_button_set_label(GTK_BUTTON(GLOBAL_LIST_FUNCTIONS[0]),"1");
		gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),GLOBAL_LIST_FUNCTIONS[0],1,2,1,1);

		GLOBAL_LIST_FUNCTIONS[1]=gtk_label_new("⚫ New condition");
		gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),GLOBAL_LIST_FUNCTIONS[1],2,2,1,1);
	gtk_box_pack_start(GTK_BOX(GLOBAL_HAED_TRIGGER_BOX),GLOBAL_HAED_TRIGGER_GRID,0,0,0);

	haed_object_add_trigger_std(GLOBAL_TRIGGERS_SPECIAL_S,NULL);
	haed_object_add_trigger_std(GLOBAL_TRIGGERS_SOUND_S,NULL);
	haed_object_add_trigger_std(GLOBAL_TRIGGERS_STORY_S,NULL);
	haed_object_add_trigger_std(GLOBAL_TRIGGERS_INPUT_S,NULL);
	
	///UPDATE OBJECTS
	if(GLOBAL_HAED_INFORMATION.firstProgram && GLOBAL_HAED_INFORMATION.firstProgram->program)
	{
		printf("got first program:: %p\n",GLOBAL_HAED_INFORMATION.firstProgram);
	
		EksParent *objectsParent=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"objects");
		EksParent *activeParent=eks_parent_get_child_from_name(objectsParent,"active");
		
		printf("got first program2:: %p\n",activeParent);
		
		eks_parent_foreach_child_start(activeParent,loopUnit)
		{
			if(eks_parent_compare_type(loopUnit,EKS_PARENT_TYPE_VALUE))
			{
				printf("got first program3:: %s\n",loopUnit->name);
				haed_trigghandler_add_object(loopUnit);
				haed_object_set_trigger_icons(loopUnit);
			}
		}eks_parent_foreach_child_end(activeParent,loopUnit);
		
		haed_eks_set_triggers_from_parent(GLOBAL_HAED_INFORMATION.firstProgram->program);
	}
}

static void haed_triggers_visual_destroy(void)
{
	//Define the box for the conditionswindow
	if(GLOBAL_MAIN_CONDITIONS_BOX)
		gtk_widget_destroy(GLOBAL_MAIN_CONDITIONS_BOX);
	
	if(GLOBAL_MAIN_GET_OBJECT_BOX)
		gtk_widget_destroy(GLOBAL_MAIN_GET_OBJECT_BOX);

	if(GLOBAL_HAED_TRIGGER_GRID)
		gtk_widget_destroy(GLOBAL_HAED_TRIGGER_GRID);
	//maybe something more?
}

/**
	Needs to be run before the eksparent is read
*/
void haed_triggers_visual_update(void)
{
	haed_trigghandler_clean();
	b_tm("free ok");
	haed_triggers_visual_destroy();
	b_tm("visual destroy ok");
	
/*	GLOBAL_HAED_TRIGGER_HANDLER=calloc(1,sizeof(TriggerHandler));*/
	
	haed_triggers_visual_init();
	b_tm("add objects to triggers");
	
	gtk_widget_show_all(GLOBAL_HAED_TRIGGER_GRID);
}

/*//never tested*/
/*void haed_triggers_visual_reload(void)*/
/*{*/
/*	haed_eks_set_triggers_from_parent(GLOBAL_HAED_INFORMATION.firstProgram->program);*/
/*	*/
/*	haed_triggers_visual_reset();*/
/*}*/

void haed_triggers_remove_condition_press(GtkWidget *widget, gpointer data)
{
	//GtkWidget *tempWidget=GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton;
	//EksParent *tempObject=g_object_get_data(G_OBJECT(GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton),"haed_object");
	
	//EksParent *tempCondition=g_object_get_data(G_OBJECT(GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton),"haed_condition");
	UITriggRow *thisRow=data;
	
	EksParent *tempCondition=thisRow->to_condition;

	printf("%s\n",tempCondition->name);

	printf("%s\n",eks_parent_dump_text(tempCondition));

	haed_trigghandler_remove_condition(GLOBAL_HAED_TRIGGER_GRID,tempCondition);
}

/**
	When the user clicks on the num button, close to the condition

	@param widget NO_FREE
		the button widget
	@param event NO_FREE
		in event
	@param data
	@return 
		void

	@param globals GLOBAL_MAIN_ACTIVE_OBJECT_MENU GLOBAL_MAIN_ACTIVE_OBJECT_MENUList
*/
void haed_triggers_insert_button_press(GtkWidget *widget,GdkEvent *event, gpointer data)
{
	if(GLOBAL_MAIN_ACTIVE_OBJECT_MENU!=NULL)
	{
		gtk_widget_destroy(GLOBAL_MAIN_ACTIVE_OBJECT_MENU);
		GLOBAL_MAIN_ACTIVE_OBJECT_MENU=NULL;
	}

	//general stuff
	GtkMenuListInner GLOBAL_MAIN_ACTIVE_OBJECT_MENUList[]=
	{
		{"Insert",NULL,NULL},
		{"Remove",haed_triggers_remove_condition_press,data}
	};

	GLOBAL_MAIN_ACTIVE_OBJECT_MENU=gtk_menu_new_from_list(GLOBAL_MAIN_ACTIVE_OBJECT_MENUList,sizeof(GLOBAL_MAIN_ACTIVE_OBJECT_MENUList)/sizeof(GLOBAL_MAIN_ACTIVE_OBJECT_MENUList[0]));
	gtk_widget_show_all(GLOBAL_MAIN_ACTIVE_OBJECT_MENU);

	create_popup_menu(GLOBAL_MAIN_ACTIVE_OBJECT_MENU,event);
}

static void haed_triggers_change_object(GtkWidget *widget, gpointer data)
{
	intptr_t index=(intptr_t)g_object_get_data(G_OBJECT(widget),"button_index");
	EksParent *condition_in=g_object_get_data(G_OBJECT(widget),"condition_in");

	EksParent *new_selected_object=haed_dialog_get_object_from_selection();
	g_autofree char *obj_name=eks_parent_get_string(new_selected_object);
	
	char *new_condition_str=NULL;
	size_t new_condition_str_len=0;
	
	printf("changed condition to:: [%s] [%s]\n",condition_in->name,obj_name);
	
	lexca_replace_at_pos(condition_in->name,index,obj_name,&new_condition_str_len,&new_condition_str);
	
	eks_parent_set_string(condition_in,new_condition_str);
	
	printf("changed condition to:: %s\n",new_condition_str);
	
	haed_triggers_visual_update();
}

static void haed_triggers_change_object_destroy(GtkWidget *widget, gpointer data)
{
	free(data);
}

static int haed_triggers_draw_trigger(const char *str_in, LexcaStrType type, size_t index, void *user_data)
{
	void **data_in=user_data;
	EksParent *condition_in=data_in[0];
	UITriggRow *this_row=data_in[1];
	
	printf("STR IN %s\n",str_in);
	
	EksParent *objects_parent=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"objects");
	
	EksParent *found_parent=NULL;
	
	eks_parent_foreach_child_start(objects_parent,loopobject)
	{
		eks_parent_foreach_child_start(loopobject,loopobjtype)
		{
			g_autofree char *loopobjtype_name=eks_parent_get_string(loopobjtype);
			
			printf("COMPARE:: %s %s\n",loopobjtype_name,str_in);
			
			if(g_strcmp0(loopobjtype_name,str_in)==0)
			{
				found_parent=loopobjtype;
				goto match_found;
			}
			
		}eks_parent_foreach_child_end(loopobject,loopobjtype);
		
	}eks_parent_foreach_child_end(objects_parent,loopobject);
	
match_found: ;
	
	GtkWidget *widget=NULL;
	
	if(found_parent)
	{
		GdkPixbuf *objimagebuff=eks_parent_custom_get(found_parent,HAED_OBJECT_IMAGEBUFF);
		
		//should save the forloop spot if changing object
		GtkWidget *image=gtk_image_new();
		widget=gtk_button_new();
		gtk_image_set_from_pixbuf(GTK_IMAGE(image),gdk_pixbuf_scale_simple(objimagebuff,32,32,GDK_INTERP_NEAREST));
		gtk_button_set_image(GTK_BUTTON(widget), image);
		
		g_object_set_data(G_OBJECT(widget),"button_index",(void*)(intptr_t)index);
		g_object_set_data(G_OBJECT(widget),"condition_in",condition_in);
		g_signal_connect(widget, "clicked",G_CALLBACK(haed_triggers_change_object),NULL);
	}
	else
	{
		widget=gtk_label_new(str_in);
	}
	
	gtk_box_pack_start(GTK_BOX(this_row->condition_info),widget,0,0,0);
	
	return 0;
}

/**
	Insert the condition into the graphical layout.

	@param handler INNER_REALLOC
		the handler for the trigger settings ( the one you see in trigger editor )
	@param triggerGrid NO_FREE
		the grid to insert it into
		- REMOVE? this should be integrated so it will be drawn into it with another function.
	@param conditionIn
		A bottom level parent containing the condition.
	@return
		void
*/
void haed_triggers_insert_row(GtkWidget *triggerGrid,EksParent *conditionIn)
{
	TriggerHandler *handler=GLOBAL_HAED_TRIGGER_HANDLER;

	int currentRow;
	
	currentRow=handler->rows->len;
	
	//first row is for the objects thereby +2
	gtk_grid_insert_row(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),currentRow+2);
	
	UITriggRow *thisRow=calloc(1,sizeof(UITriggRow));
	g_ptr_array_add(handler->rows,thisRow);
	
	//STANDARD DEFINITIONS FOR ROW
	//thisRow->number=currentRow;
	thisRow->to_condition=conditionIn;
	
	
	//thisRow->action_buttons=malloc(sizeof(GtkWidget*)*(handler->columnAmount));
	thisRow->action_buttons=g_ptr_array_sized_new(100);
	
	///@TODO
/*	thisRow->condition_info=gtk_label_new(conditionIn->name);*/
	thisRow->condition_info=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
	void *compdata[]={conditionIn,thisRow};
/*	printf("TTSTR IN %s\n",conditionIn->name);*/
	lexca_for_each(conditionIn->name,haed_triggers_draw_trigger,compdata);
	//gtk_label_set_justify(GTK_LABEL(thisRow->condition_info), GTK_JUSTIFY_LEFT);
	gtk_grid_attach(GTK_GRID(triggerGrid),thisRow->condition_info,2,2+currentRow,1,1);
	gtk_widget_show_all(thisRow->condition_info);
	
	thisRow->number_info=gtk_button_new_with_label(b_int_to_string(handler->rows->len));
	gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),thisRow->number_info,1,2+currentRow,1,1);
	//void i = send custom
	g_signal_connect(thisRow->number_info, "button_press_event",G_CALLBACK(haed_triggers_insert_button_press),thisRow);
	gtk_widget_show(thisRow->number_info);
	
	b_tm("columnAmount: %d",handler->objects->len);	
	
	for(int i=0;i<handler->objects->len;i++)
	{
		EksParent *current_object=g_ptr_array_index(handler->objects,i);
	
		b_tm("TESTIS:: %s\n",current_object->name);
	
		GtkWidget *current_action_button=gtk_button_new_with_label(" ");
		gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),current_action_button,3+i,2+currentRow,1,1);
		gtk_widget_show(current_action_button);
		
		//4=engine units
		if(current_object)
		{
			haed_triggers_bind_object_to_button(current_action_button,conditionIn,current_object);
		}
		
		g_ptr_array_add(thisRow->action_buttons,current_action_button);
	}
	
	gtk_button_set_label(GTK_BUTTON(GLOBAL_LIST_FUNCTIONS[0]),b_int_to_string(handler->rows->len+1));
}

/*
=============================GRAPHICAL===========================
*/
//3 HANDLE FUINCTIONS

/**
	Adds a condition to a trigger-based parent structure

	@param level .
		level to add it to. 0=1 map (will use eks_parent_get_child)
	@param conditionString NO_FREE
		the conditionString
	@return
		the pointer to the EksParent to the trigger. (not the top-level one)

	@param globals GLOBAL_TRIGGER_PARENT
*/
EksParent *haed_triggerparent_add_condition(int level, char *conditionString)
{
	EksParent *theTrigger=NULL;
	
	theTrigger=eks_parent_add_child(GLOBAL_TRIGGER_PARENT,conditionString,EKS_PARENT_TYPE_VALUE,NULL);

	b_tm("%s",eks_parent_dump_text(GLOBAL_TRIGGER_PARENT));

	return theTrigger;
}

//==============ACTIONS===============
/**
	Will just add the actionString into the global trigger parent
*/
char *haed_triggerparent_add_action(EksParent *conditionEksParent,char *actionString)
{
	if(conditionEksParent==NULL)
	{
		b_error_message("ERROR ON ADDING ACTION!");
		return NULL;
	}
	else
	{
		printf("Number of children: %ld Upper parent: %s\n",eks_parent_get_child_amount(conditionEksParent),conditionEksParent->upperEksParent->name);
		//if it exists
		for(int i=0;i<eks_parent_get_child_amount(conditionEksParent);i++)
		{
			EksParent *child=eks_parent_get_child(conditionEksParent,i);
		
			if(eks_parent_compare_type(child,EKS_PARENT_TYPE_VALUE) && strcmp(actionString,child->name)==0)
				return 0;
		}
		
		printf("ERRR!\n");
		
		EksParent *theTrigger=eks_parent_add_child(conditionEksParent,actionString,EKS_PARENT_TYPE_VALUE,NULL);
		
		return theTrigger->name;
	}
}

/**
	Add cross to button, and add info text about it.

	@param widget
		button
	@param actionText
		text to add to it
*/
void haed_triggers_add_visual_for_action(GtkWidget *widget,char *actionText)
{
	//get hover string
	char *buttonActionList=gtk_widget_get_tooltip_text(widget);
	
	//set as action set
	gtk_button_set_label(GTK_BUTTON(widget),"X");
	
	//update hover string
	if(buttonActionList==NULL)
	{
		buttonActionList=g_strndup(actionText,strlen(actionText));
	}
	else
	{
		buttonActionList=concat(buttonActionList,"\n",actionText,NULL);
	}
	
	gtk_widget_set_tooltip_text(widget,buttonActionList);
}

///@DEPRICATED
void haed_triggers_create_popup_from_menu_list(GtkMenuListInner *theList,size_t listSize,GdkEvent *event)
{
	GLOBAL_MAIN_ACTIVE_OBJECT_MENU_TRIGGERS[0]=gtk_menu_new_from_list(theList,listSize);
	gtk_widget_show_all(GLOBAL_MAIN_ACTIVE_OBJECT_MENU_TRIGGERS[0]);
	
	//show the menu
	create_popup_menu(GLOBAL_MAIN_ACTIVE_OBJECT_MENU_TRIGGERS[0],event);
}

static int haed_triggers_populate_code_internal(const char *input, LexcaStrType type, size_t index, void *user_data)
{
	void **real_user_data=user_data;
	
	FILE *dump_fp=real_user_data[0];
	intptr_t refers=(intptr_t)real_user_data[1];
	EksParent *refered_object=real_user_data[2];
	int *ref_counter=real_user_data[3];

	if(g_str_has_prefix(input,"__OBJECT"))
	{
		if(*ref_counter==refers)
		{
			g_autofree char *obj_name=eks_parent_get_string(refered_object);
			printf("OBJ_NAME:: %s %d\n",obj_name,*ref_counter);
			
			if(obj_name)
			{
				fprintf(dump_fp,"%s",obj_name);
			}
			else
			{
				return 1;
			}
		}
		else
		{
			EksParent *other_object=haed_dialog_get_object_from_selection();
			g_autofree char *obj_name=eks_parent_get_string(other_object);
			
			if(obj_name)
			{
				fprintf(dump_fp,"%s",obj_name);
			}
			else
			{
				return 1;
			}
		}
		
		(*ref_counter)++;
	}
	else if(strcmp(input,"__EQUIV_CALCULATION")==0)
	{
		char *operator=NULL;
	
		char *other_string=haed_window_compare_field(&operator);
		
		b_tm("recieved:: %s %s",operator,other_string);
		
		if(operator && other_string)
		{
			fprintf(dump_fp,"%s %s",operator,other_string);
		}
		else
		{
			return 1;
		}
	}
	else if(strcmp(input,"__COORD")==0)
	{
		char *other_string1=haed_dialog_enter_code();
		char *other_string2=haed_dialog_enter_code();
		
		if(other_string1 && other_string2)
		{
			fprintf(dump_fp,"%s,%s",other_string1,other_string2);
		}
		else
		{
			return 1;
		}
	}
	else if(strcmp(input,"__COORD_X")==0 || strcmp(input,"__COORD_Y")==0)
	{
		char *other_string=haed_dialog_enter_code();
		
		if(other_string)
		{
			fprintf(dump_fp,"%s",other_string);
		}
		else
		{
			return 1;
		}
	}
	///TODO needs better
	else if(strcmp(input,"__AREA")==0)
	{
		char *other_string1=haed_dialog_enter_code();
		char *other_string2=haed_dialog_enter_code();
		char *other_string3=haed_dialog_enter_code();
		char *other_string4=haed_dialog_enter_code();
		
		if(other_string1 && other_string2 && other_string3 && other_string4)
		{
			fprintf(dump_fp,"%s,%s,%s,%s",other_string1,other_string2,other_string3,other_string4);
		}
		else
		{
			return 1;
		}
	}
	else if(strcmp(input,"__LEVEL")==0)
	{
		char *other_string=haed_pick_level_dialog();
		
		if(other_string)
		{
			fprintf(dump_fp,"%s",other_string);
		}
		else
		{
			return 1;
		}
	}
	///TODO needs better
	else if(strcmp(input,"__DIRECTION")==0)
	{
		char *other_string=haed_dialog_enter_code();
		
		if(other_string)
		{
			fprintf(dump_fp,"%s",other_string);
		}
		else
		{
			return 1;
		}
	}
	///TODO needs better
	else if(strcmp(input,"__EDGE_AREA")==0)
	{
		char *other_string=haed_dialog_enter_code();
		
		if(other_string)
		{
			fprintf(dump_fp,"%s",other_string);
		}
		else
		{
			return 1;
		}
	}
	///TODO needs better
	else if(strcmp(input,"__CALCULATION")==0)
	{
		char *other_string=haed_dialog_enter_code();
		
		if(other_string)
		{
			fprintf(dump_fp,"%s",other_string);
		}
		else
		{
			return 1;
		}
	}
	else
	{
		fprintf(dump_fp,"%s",input);
	}
	
	return 0;
}

int haed_triggers_populate_code(const char *input, const char *desc, intptr_t refers, EksParent *refered_object, size_t *result_len, char **result)
{
	printf("input::: %s\n",input);

	if(input)
	{
		int ref_counter=1;
	
		char *dump=NULL;
		size_t dump_len=0;
		
		FILE *dump_fp=open_memstream(&dump,&dump_len);
		
		void *data[]={dump_fp,(void*)refers,refered_object,&ref_counter};

		lexca_for_each(input, haed_triggers_populate_code_internal,data);
		
		fflush(dump_fp);
		fclose(dump_fp);
		
		printf("DUMP_FP:: %s\n",dump);
		
		*result=dump;
		*result_len=dump_len;
	}
	return 0;
}

void haed_trigger_conditions_add_internal(GtkWidget *widget,gpointer user_data)
{
	char *returned_condition=NULL;
	size_t returned_condition_len=0;

	void **recv_data=user_data;

	EksParent *temp_object=GLOBAL_HAED_TEMP_HAEDOBJECT;
	
	haed_triggers_populate_code(recv_data[1],recv_data[0],(intptr_t)recv_data[2],temp_object,&returned_condition_len,&returned_condition);
	
	EksParent *condition=haed_triggerparent_add_condition(0,returned_condition);
	//haed_dialog_new_conditionAdd(condition);
	
	haed_triggers_insert_row(GLOBAL_HAED_TRIGGER_GRID,condition);
	//see popup windows
	gtk_widget_destroy(GLOBAL_CONDITIONS_WINDOW);
}

void haed_trigger_actions_add_internal(GtkWidget *widget,gpointer user_data)
{
	char *returned_action=NULL;
	size_t returned_action_len=0;

	void **recv_data=user_data;
	
	char *action_text;
	
	GtkWidget *temp_widget=GLOBAL_HAED_INFORMATION.triggerEditorCurrentButton;
	EksParent *temp_object=g_object_get_data(G_OBJECT(temp_widget),"haed_object");
	EksParent *temp_condition=g_object_get_data(G_OBJECT(temp_widget),"haed_condition");
	
	haed_triggers_populate_code(recv_data[1],recv_data[0],(intptr_t)recv_data[2],temp_object,&returned_action_len,&returned_action);
	
	//åäöpyfgcrl
	if((action_text=haed_triggerparent_add_action(temp_condition,returned_action)))
	{
		haed_triggers_add_visual_for_action(temp_widget,action_text);
	}
}

/**
	Create a popup menu from an eks configuration menu item
	
	@param object 
		an object like STD_OBJECT
	@param parent
		used std with NULL
	@param event
		Event used
	@returns
		0 on success
*/
GtkWidget *haed_triggers_create_popup_from_eks_description(EksParent *object, GtkWidget *parent, GdkEvent *event, int status, void (*funct)(GtkWidget *widget,gpointer user_data))
{
	GtkWidget *popup_menu=gtk_menu_new();
	
	eks_parent_foreach_child_start(object,conf_child)
	{
		if(eks_parent_compare_type(conf_child,EKS_PARENT_TYPE_VALUE))
		{
			int cannot_spawn_more=0;
			g_autofree char *desc=NULL;
			g_autofree char *code=NULL;
			intptr_t refers=-1;
			GtkWidget *menu_item=gtk_menu_item_new_with_label(conf_child->name);
			//printf("NAMI %s\n",conf_child->name);
			
			eks_parent_foreach_child_start(conf_child,conf_child_search)
			{
				if(eks_parent_compare_type(conf_child_search,EKS_PARENT_TYPE_VALUE))
				{
					if(conf_child_search->name && strcmp(conf_child_search->name,"_DESC")==0)
					{
						cannot_spawn_more=1;
						EksParent *desc_child=eks_parent_get_child(conf_child_search,0);
						desc=(desc_child?eks_parent_get_string(desc_child):NULL);
					}
					else if(conf_child_search->name && strcmp(conf_child_search->name,"_CODE")==0)
					{
						cannot_spawn_more=1;
						EksParent *code_child=eks_parent_get_child(conf_child_search,0);
						code=(code_child?eks_parent_get_string(code_child):NULL);
					}
					else if(conf_child_search->name && strcmp(conf_child_search->name,"_REFERS")==0)
					{
						cannot_spawn_more=1;
						EksParent *refers_child=eks_parent_get_child(conf_child_search,0);
						refers=(refers_child?eks_parent_get_int(refers_child):-1);
		/*				g_signal_connect(items[i], "activate",G_CALLBACK (list[i].function),list[i].data);*/
					}
				}
			}eks_parent_foreach_child_end(conf_child,conf_child_search);
			
			printf("FFOUND:: %s %s %ld\n",desc,code,refers);
			
			if(cannot_spawn_more)
			{
				if(code)
				{
					void **send_data=malloc(sizeof(void*)*3);
					send_data[0]=g_steal_pointer(&desc);
					send_data[1]=g_steal_pointer(&code);
					send_data[2]=(void*)refers;
					g_signal_connect(menu_item, "activate",G_CALLBACK(funct),send_data);
					g_signal_connect(menu_item, "destroy",G_CALLBACK(haed_triggers_change_object_destroy),send_data);
				}
			}
			else if(conf_child->firstChild)
			{
				GtkWidget *submenu=haed_triggers_create_popup_from_eks_description(conf_child,popup_menu,event,status,funct);
				printf("FFOUND2:: %p\n",submenu);
				gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item),submenu);
			}
			
			gtk_menu_shell_append(GTK_MENU_SHELL(popup_menu), menu_item);
		}
	}eks_parent_foreach_child_end(object,conf_child);

	if(parent==NULL)
	{
		if(status==1)
		{
			//delete
			GtkWidget *menu_item=gtk_menu_item_new_with_label("Remove action");
			g_signal_connect(menu_item, "activate",G_CALLBACK(haed_triggers_remove_action),NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(popup_menu), menu_item);
		}
	
		gtk_widget_show_all(popup_menu);
		
		//show the menu
		create_popup_menu(popup_menu,event);
	}
	
	return popup_menu;
}
