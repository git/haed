/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

TriggerHandler *haed_trigghandler_begin(void)
{
	if(GLOBAL_HAED_TRIGGER_HANDLER==NULL)
	{
		GLOBAL_HAED_TRIGGER_HANDLER=calloc(1,sizeof(TriggerHandler));
		
		GLOBAL_HAED_TRIGGER_HANDLER->rows=g_ptr_array_sized_new(100);
/*		g_ptr_array_set_free_func(GLOBAL_HAED_TRIGGER_HANDLER->rows,NULL);*/
		GLOBAL_HAED_TRIGGER_HANDLER->objects=g_ptr_array_sized_new(100);
/*		g_ptr_array_set_free_func(GLOBAL_HAED_TRIGGER_HANDLER->objects,);*/

		b_tm("GOT:: %p %p",GLOBAL_HAED_TRIGGER_HANDLER->rows,GLOBAL_HAED_TRIGGER_HANDLER->objects);
	}
	
	return GLOBAL_HAED_TRIGGER_HANDLER;
}

//TODO
void haed_trigghandler_end(void)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;

	if(triggHandler)
	{
		//BETTER CLEANUP
		for(int i=0;i<triggHandler->rows->len;i++)
		{
			UITriggRow *thisRow=g_ptr_array_index(triggHandler->rows,i);
		
			free(thisRow->action_buttons);
			free(thisRow);
		}
		
/*		g_ptr_array_free(triggHandler->rows,FALSE);*/
/*		g_ptr_array_free(triggHandler->objects,FALSE);*/
/*		*/
/*		memset(triggHandler,0,sizeof(TriggerHandler));*/
	}
}

/**
	Free the triggerhandler struct, used in new-program for example, or if destroying a map.
	Wont free any internal widgets, only freeing the pointers to them
	
	@param triggHandler
		the trigghandler to free
*/
void haed_trigghandler_clean(void)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;

	if(triggHandler)
	{
		//BETTER CLEANUP
		for(int i=0;i<triggHandler->rows->len;i++)
		{
			UITriggRow *thisRow=g_ptr_array_index(triggHandler->rows,i);
		
			free(thisRow->action_buttons);
			free(thisRow);
		}
		
		g_ptr_array_set_size(triggHandler->rows,0);
		g_ptr_array_set_size(triggHandler->objects,0);
/*		*/
/*		memset(triggHandler,0,sizeof(TriggerHandler));*/
	}
}

/**
	Add the action stuff to the new button depending on object

	@param button
		button to change
	@param condition
		the condition for later usage
	@param object
		the object, with its type
*/
void haed_triggers_bind_object_to_button(GtkWidget *button,EksParent *condition,EksParent *object)
{
	if(g_strcmp0(object->upperEksParent->name,GLOBAL_ACTIVE_OBJECT_S)==0)
	{
		g_signal_connect(button, "button_press_event",G_CALLBACK(haed_trigger_action_on_click),"STD_OBJECT");
	}
	else if(g_strcmp0(object->upperEksParent->name,GLOBAL_TRIGGERS_SPECIAL_S)==0)
	{
		g_signal_connect(button, "button_press_event",G_CALLBACK(haed_trigger_action_on_click),"STD_SPECIAL");
	}
	else if(g_strcmp0(object->upperEksParent->name,GLOBAL_TRIGGERS_SOUND_S)==0)
	{
		g_signal_connect(button, "button_press_event",G_CALLBACK(haed_trigger_action_on_click),"STD_SOUND");
	}
	else if(g_strcmp0(object->upperEksParent->name,GLOBAL_TRIGGERS_STORY_S)==0)
	{
		g_signal_connect(button, "button_press_event",G_CALLBACK(haed_trigger_action_on_click),"STD_STORYBOARD");
	}
	else if(g_strcmp0(object->upperEksParent->name,GLOBAL_TRIGGERS_INPUT_S)==0)
	{
		g_signal_connect(button, "button_press_event",G_CALLBACK(haed_trigger_action_on_click),"STD_INPUT");
	}
	
	g_object_set_data(G_OBJECT(button),"haed_object",(void*)object);
	g_object_set_data(G_OBJECT(button),"haed_condition",(void*)condition);
}

/**
	Adds an object to the Trigger Handler.
	Also adds buttons for the new object into the trigger fields. otherwise, it would be impossible to set triggers for new objects.

	@param object NO_FREE
		the object to add.
	@param objectType NO_FREE
		the string of the object type.
	@return .
		void
*/
void haed_trigghandler_add_object(EksParent *object)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;

	if(triggHandler->rows)
	{
		for(int i=0;i<triggHandler->rows->len;i++)
		{
			UITriggRow *thisRow=g_ptr_array_index(triggHandler->rows,i);
			
			GtkWidget *new_action_button=gtk_button_new_with_label(" ");
			gtk_grid_attach(GTK_GRID(GLOBAL_HAED_TRIGGER_GRID),new_action_button,3+triggHandler->objects->len,2+i,1,1);
			gtk_widget_show(new_action_button);
	 
			haed_triggers_bind_object_to_button(new_action_button,thisRow->to_condition,object);
			
			g_ptr_array_add(thisRow->action_buttons,new_action_button);
		}
		
/*		b_tm("ADDED OBJECT:: %s\n",object->name);*/
		
		//resize the object structure
		g_ptr_array_add(triggHandler->objects,object);
	}
}

/**
	Counts the number of objects in that area.

	@param triggHandler NO_FREE
		The trigger handler to search in
	@param objectType NO_FREE
		name of the type.
	@return .
		returns the amount.
*/
int haed_trigghandler_count_objects(const char *objectType)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;
	//b_tm("ERP? %d",triggHandler->columnAmount);
	int amount=0;
	
	for(int i=0;i<triggHandler->objects->len;i++)
	{
		EksParent *this_parent=g_ptr_array_index(triggHandler->objects,i);
		//b_tm("TRIGG MATCH %s %s %s",triggHandler->objects[i]->upperEksParent->name,triggHandler->objects[i]->name,objectType);
		
		if(this_parent && strcmp(this_parent->upperEksParent->name,objectType)==0)
		{
			amount++;
		}
	}
	
	return amount;
}

/**
	Get an object from a position.
	
	@param triggHandler NO_FREE
		The trigger handler to search in
	@param objectType NO_FREE
		name of the type.
	@param pos .
		position of the object
	@return POINTER
		the object
		or NULL if it failed.
*/

EksParent *haed_trigghandler_get_object(const char *objectType,int pos)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;

	int amount=0;

	for(int i=0;i<triggHandler->objects->len;i++)
	{
		EksParent *this_parent=g_ptr_array_index(triggHandler->objects,i);
	
		if(this_parent && strcmp(this_parent->upperEksParent->name,objectType)==0)
		{
			if(amount==pos)
			{
				return this_parent;
			}
			amount++;
		}
	}
	
	//get nothing
	return NULL;
}

EksParent *haed_triggerhandler_get_object_by_name(char *name)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;

	for(int i=0;i<triggHandler->objects->len;i++)
	{
		EksParent *this_parent=g_ptr_array_index(triggHandler->objects,i);
		
		if(this_parent && strcmp(this_parent->name,name)==0)
		{
			return this_parent;
		}
	}

	return NULL;
}

/**
	Remove condition.
	

*/
void haed_trigghandler_remove_condition(GtkWidget *triggerGrid,EksParent *conditionIn)
{
	TriggerHandler *handler=GLOBAL_HAED_TRIGGER_HANDLER;
	
	/*	EksParent *returnObject=NULL;*/
	/*	*/
	/*	GList *firstContain=gtk_container_get_children(GTK_CONTAINER(gtk_container_get_children(GTK_CONTAINER(GLOBAL_HAED_TRIGGER_GRID))->data));*/
	/*	GList *firstChild=firstContain;//gtk_container_get_children(GTK_CONTAINER(GLOBAL_HAED_TRIGGER_GRID));*/
	/*	GList *currentChild=firstChild;*/
	/*	*/
	/*	do*/
	/*	{*/
	/*		EksParent *tempObject=g_object_get_data(G_OBJECT(currentChild->data),"haed_object");*/

	/*		if(tempObject && strcmp(tempObject->type,objectType)==0)*/
	/*		{*/
	/*			if(amount==pos)*/
	/*			{*/
	/*				returnObject=tempObject;*/
	/*			}*/
	/*			amount++;*/
	/*		}*/
	/*		*/
	/*		currentChild=currentChild->next;*/
	/*	*/
	/*	}while(currentChild!=firstChild && currentChild!=NULL);*/

/*	return returnObject;*/
	
	for(int i=0;i<handler->rows->len;i++)
	{
		UITriggRow *currentRow=g_ptr_array_index(handler->rows,i);
		
		if(currentRow->to_condition==conditionIn)
		{
			for(int ii=0;ii<(currentRow->action_buttons->len);ii++)
			{
				GtkWidget *action_button=g_ptr_array_index(currentRow->action_buttons,ii);
			
				gtk_widget_destroy(action_button);
			}
			
			gtk_widget_destroy(currentRow->number_info);
			gtk_widget_destroy(currentRow->condition_info);
			
			eks_parent_destroy(conditionIn,EKS_TRUE);
			
			g_ptr_array_free(currentRow->action_buttons,FALSE);
			
			g_ptr_array_remove_index(handler->rows,i);
			free(currentRow);
		}
	}
}

int haed_triggerhandler_get_pos_from_object(EksParent *object)
{
	TriggerHandler *triggHandler=GLOBAL_HAED_TRIGGER_HANDLER;

	for(int i=0;i<triggHandler->objects->len;i++)
	{
		EksParent *curr_object=g_ptr_array_index(triggHandler->objects,i);
	
		if(curr_object==object)
		{
			return i;
		}
	}
	
	return 0;
}

