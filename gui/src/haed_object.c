/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

/**
	Add a new object, eg Active object, backdrop ...
	
	@param tempobject INNER_REALLOC
		the top-level object-fond to add the object to
	@param name 
*/
void haed_object_add_eks_parent_to_gui(char *name,EksParent *holder,const char *type)
{
	EksParent *thisObject;
	
	b_tm("ADDING OBJECT %s",name);
	
	EksParent *objects=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"objects");
	
	EksParent *activeObjects=eks_parent_get_child_from_name(objects,"active");
	
	thisObject=eks_parent_add_child(activeObjects,name,EKS_PARENT_TYPE_VALUE,NULL);
	
	eks_parent_custom_set(thisObject,HAED_OBJECT_BUFFHOLDER,holder);
	
	haed_trigghandler_add_object(thisObject);
	
	//Read the eks file for dimensions
	if(holder)
	{
		haed_object_set_imagebuff_from_bufholder(thisObject);

		haed_object_set_level_editor_icons(thisObject);
	}
	//active object s = constant
/*	if(type==GLOBAL_ACTIVE_OBJECT_S)*/
/*	{*/
/*		haed_object_set_trigger_icons(thisObject);*/
/*	}*/
	haed_triggers_visual_update();
}

void haed_object_set_level_editor_icons(EksParent *thisObject)
{
	GdkPixbuf *imagebuff=eks_parent_custom_get(thisObject,HAED_OBJECT_IMAGEBUFF);
	//set the icon
	GdkPixbuf *iconbuff=gdk_pixbuf_scale_simple(imagebuff,32,32,GDK_INTERP_NEAREST);
	
	//set the buttons images
	GtkWidget *drawMenuWidget=gtk_toggle_button_new();
	
	gtk_button_set_image(GTK_BUTTON(drawMenuWidget),gtk_image_new_from_pixbuf(iconbuff));
		g_signal_connect(drawMenuWidget, "clicked",G_CALLBACK(haed_set_current_layout_object_pointer), NULL);
		g_signal_connect(drawMenuWidget, "button_press_event",G_CALLBACK(haed_level_editor_active_object_popup_menu),NULL);
		g_object_set_data(G_OBJECT(drawMenuWidget),"haed_object",(void*)(thisObject));
	gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS),drawMenuWidget,0,0,0);
	gtk_widget_show_all(drawMenuWidget);
	
	eks_parent_custom_set(thisObject,HAED_OBJECT_DRAW_MENU_WIDGET,drawMenuWidget);
	
	g_object_unref(iconbuff);
}

void haed_object_set_imagebuff_from_bufholder(EksParent *thisObject)
{
	EksParent *holder=eks_parent_custom_get(thisObject,HAED_OBJECT_BUFFHOLDER);

	GdkPixbuf *testPixbuf=haed_pixbufholder_get_pixbuf_from_name(holder,GLOBAL_ANIMATION_STOPPED);
	
	EksParent *bufSettings=eks_parent_get_child(eks_parent_get_child_from_name(eks_parent_get_child_from_name(eks_parent_get_child_from_name(holder,GLOBAL_ANIMATION_STOPPED),"ALL_MIRROR"),"FRAMES"),0);

	int framex=eks_parent_get_int(eks_parent_get_child(bufSettings,0));
	int framey=eks_parent_get_int(eks_parent_get_child(bufSettings,1));
	int framewidth=eks_parent_get_int(eks_parent_get_child(bufSettings,2));
	int frameheight=eks_parent_get_int(eks_parent_get_child(bufSettings,3));
	//int framehs=eks_parent_get_int(eks_parent_get_child(bufSettings,4));
	//int framehs=eks_parent_get_int(eks_parent_get_child(bufSettings,5));
	
	b_tm("x %d,y %d,w %d,h %d\n",framex,framey,framewidth,frameheight);

	//get only the corner for the icon
	GdkPixbuf *imagebuff=gdk_pixbuf_new(GDK_COLORSPACE_RGB,1,8,framewidth,frameheight);
	gdk_pixbuf_copy_area(testPixbuf,framex,framey,framewidth,frameheight,imagebuff,0,0);
	eks_parent_custom_set(thisObject,HAED_OBJECT_IMAGEBUFF,imagebuff);
}


char *haed_object_get_string(EksParent *object, const char *assignparam)
{
	return haed_get_string_from_pos(object,assignparam,0);
}

void haed_object_set_string(EksParent *object, const char *assignparam,const char *setparam, int delete)
{
	haed_set_string_from_pos(object,assignparam,setparam,delete,0);
}

intptr_t haed_object_get_int(EksParent *object, const char *assignparam)
{
	return haed_get_int_from_pos(object,assignparam,0);
}

void haed_object_set_int(EksParent *object, const char *assignparam,int setparam, int delete)
{
	haed_set_int_from_pos(object,assignparam,setparam,delete,0);
}

double haed_object_get_double(EksParent *object, const char *assignparam)
{
	return haed_get_double_from_pos(object,assignparam,0);
}

void haed_object_set_double(EksParent *object, const char *assignparam,double setparam, int delete)
{
	haed_set_double_from_pos(object,assignparam,setparam,delete,0);
}

/**
	Initiates the left-side bar in the level editor
	
	
	-
*/
void haed_object_list_visual_init(void)
{
	GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS=gtk_box_new(GTK_ORIENTATION_VERTICAL,2);
						
		//This is just definitions for the objects shown in the left row
		GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS_ICONS_IMAGE[0]=gtk_image_new_from_file("./images/level_editor/active_objects.png");
		gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS),GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS_ICONS_IMAGE[0],0,0,0);

	gtk_box_pack_start(GTK_BOX(GLOBAL_HAED_LIST_OBJECTS),GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS,0,1,0);
	
	gtk_widget_show_all(GLOBAL_HAED_LIST_OBJECTS);
}

/**
	resets the left-side bar in the level editor
	
	-
*/
void haed_object_list_visual_reset(void)
{
	if(GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS)
		gtk_widget_destroy(GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS);
	
	haed_object_list_visual_init();
}
