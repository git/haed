/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "includes.h"

/**
	Constructor for Pgd_trigger_lexca_buffer
	
	@param self
		reset or initialize the Pgd_trigger_lexca_buffer struct.
	@returns
		initialized Pgd_trigger_lexca_buffer struct.
*/
Pgd_trigger_lexca_buffer_node *pgd_trigger_lexca_buffer_node_init(Pgd_trigger_lexca_buffer_node *self, const char *obj, LexcaStrType type, size_t index)
{
	memset(self,'\0',sizeof(Pgd_trigger_lexca_buffer_node));
	
	self->obj_type=PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_STD;
	self->obj_str=g_strdup(obj);
	self->type=type;
	self->index=index;

	return self;
}

/**
	Constructor and allocator for the Pgd_trigger_lexca_buffer_node struct
	
	@returns
		newly allocated Pgd_trigger_lexca_buffer_node struct.
*/
Pgd_trigger_lexca_buffer_node *pgd_trigger_lexca_buffer_node_new(const char *obj, LexcaStrType type, size_t index)
{
	Pgd_trigger_lexca_buffer_node *self=malloc(sizeof(Pgd_trigger_lexca_buffer_node));
	if(!self)
	{
		return NULL;
	}

	Pgd_trigger_lexca_buffer_node *result=pgd_trigger_lexca_buffer_node_init(self,obj,type,index);
	
	if(result==NULL)
	{
		free(self);
	}
	
	return result;
}

/**
	De-initialize the struct but does not free the input.
	
	@param self
		Struct to handle
*/
void pgd_trigger_lexca_buffer_node_finalize(Pgd_trigger_lexca_buffer_node *self)
{
	if(self)
	{
		g_free(self->obj_str);
	}
}

/**
	Destructor of the struct, will call pgd_trigger_lexca_buffer_node_finalize and free. 
	
	@param self
		Struct to handle. Note that self will be freed inside this function
*/
void pgd_trigger_lexca_buffer_node_free(Pgd_trigger_lexca_buffer_node *self)
{
	pgd_trigger_lexca_buffer_node_finalize(self);

	free(self);
}

void pgd_trigger_lexca_buffer_nestled_node_free(Pgd_trigger_lexca_buffer_node *self)
{
	if(self->obj_type==PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_FUNCTION)
	{
		g_ptr_array_free(self->obj_children,TRUE);
		free(self);
	}
}

/////////////////////////////

/**
	Constructor for Pgd_trigger_lexca_buffer
	
	@param self
		reset or initialize the Pgd_trigger_lexca_buffer struct.
	@returns
		initialized Pgd_trigger_lexca_buffer struct.
*/
Pgd_trigger_lexca_buffer *pgd_trigger_lexca_buffer_init(Pgd_trigger_lexca_buffer *self)
{
	memset(self,'\0',sizeof(Pgd_trigger_lexca_buffer));
	
	self->a=g_ptr_array_sized_new(10);
	g_ptr_array_set_free_func(self->a,(GDestroyNotify)pgd_trigger_lexca_buffer_node_free);
	
	self->n=g_ptr_array_sized_new(10);
	g_ptr_array_set_free_func(self->n,(GDestroyNotify)pgd_trigger_lexca_buffer_nestled_node_free);
	
	return self;
}

/**
	Constructor and allocator for the Pgd_trigger_lexca_buffer struct
	
	@returns
		newly allocated Pgd_trigger_lexca_buffer struct.
*/
Pgd_trigger_lexca_buffer *pgd_trigger_lexca_buffer_new(void)
{
	Pgd_trigger_lexca_buffer *self=malloc(sizeof(Pgd_trigger_lexca_buffer));
	if(!self)
	{
		return NULL;
	}

	Pgd_trigger_lexca_buffer *result=pgd_trigger_lexca_buffer_init(self);
	
	if(result==NULL)
	{
		free(self);
	}
	
	return result;
}

/**
	De-initialize the struct but does not free the input.
	
	@param self
		Struct to handle
*/
void pgd_trigger_lexca_buffer_finalize(Pgd_trigger_lexca_buffer *self)
{
	if(self)
	{
		g_ptr_array_free(self->a,TRUE);
	}
}

/**
	Destructor of the struct, will call pgd_trigger_lexca_buffer_finalize and free. 
	
	@param self
		Struct to handle. Note that self will be freed inside this function
*/
void pgd_trigger_lexca_buffer_free(Pgd_trigger_lexca_buffer *self)
{
	pgd_trigger_lexca_buffer_finalize(self);

	free(self);
}


/////////////////////////////

static int _pgd_trigger_lexca_buffer_create_internal(const char *obj,LexcaStrType type, size_t index, void *user_data)
{
	Pgd_trigger_lexca_buffer *tree_buffer=user_data;

	GPtrArray *buffer=tree_buffer->a;
	
	Pgd_trigger_lexca_buffer_node *node=pgd_trigger_lexca_buffer_node_new(obj,type,index);
	g_ptr_array_add(buffer,node);
	
	return 0;
}

static int _pgd_trigger_lexca_buffer_create_nestling(Pgd_trigger_lexca_buffer *self)
{
	int nestling=0;

	GPtrArray *obj_arr=self->a;
	
	GPtrArray *current=self->n;
	Pgd_trigger_lexca_buffer_node *current_node=NULL;
	
	for(int i=0;i<obj_arr->len;i++)
	{
		Pgd_trigger_lexca_buffer_node *next=NULL;
		
		if((i+1)<(obj_arr->len))
		{
			next=g_ptr_array_index(obj_arr,i+1);
		}
		Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(obj_arr,i);
		
		if(next && next->type=='(' && node->type==LEXCA_TYPE_IDENTIFIER)
		{
			Pgd_trigger_lexca_buffer_node *new_node=calloc(1,sizeof(Pgd_trigger_lexca_buffer_node));
			new_node->obj_type=PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_FUNCTION;
			new_node->obj_children=g_ptr_array_sized_new(10);
			g_ptr_array_set_free_func(new_node->obj_children,(GDestroyNotify)pgd_trigger_lexca_buffer_nestled_node_free);
			new_node->type=0;
			new_node->index=i;
			new_node->parent=current_node;
			
			g_ptr_array_add(current,new_node);
			
			current=new_node->obj_children;
			current_node=new_node;
		
			nestling++;
		}
		
		g_ptr_array_add(current,node);
		
		if(node->type==')' && nestling>0)
		{
			current_node=current_node->parent;
			if(current_node)
			{
				current=current_node->obj_children;
			}
			else
			{
				current=self->n;
			}
		
			nestling--;
		}
	}
	
	return 0;
}

Pgd_trigger_lexca_buffer *pgd_trigger_lexca_buffer_create(const char *c_code)
{
	Pgd_trigger_lexca_buffer *buffer=pgd_trigger_lexca_buffer_new();
	if(buffer)
	{
		lexca_for_each(c_code,_pgd_trigger_lexca_buffer_create_internal,buffer);
	}
	
	_pgd_trigger_lexca_buffer_create_nestling(buffer);
	
	return buffer;
}

static int pgd_trigger_lexca_buffer_nestled_print(GPtrArray *nes_arr,int num)
{
	for(size_t i=0;i<nes_arr->len;i++)
	{
		Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(nes_arr,i);
		
		switch(node->obj_type)
		{
			case PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_STD:
			for (int j = 0; j < num; j++)
			{
				putchar('\t');
			}
			
			printf("FOUND:: [%s] {%d}\n",node->obj_str,node->type);
			break;
			case PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_FUNCTION:
			pgd_trigger_lexca_buffer_nestled_print(node->obj_children,num+1);
			break;
			default:
			printf("UNDEFINED\n");
			break;
		}
	}
	return 0;
}

int pgd_trigger_lexca_buffer_print(Pgd_trigger_lexca_buffer *obj)
{
	GPtrArray *obj_arr=obj->a;
	GPtrArray *nes_arr=obj->n;

	printf("STANDARD ARR::\n");

	pgd_trigger_lexca_buffer_nestled_print(obj_arr,0);
	
	printf("NESTLED ARR::\n");

	pgd_trigger_lexca_buffer_nestled_print(nes_arr,0);
	return 0;
}

/////////////////////////////

static int type_is_equals(int type)
{
	if(type==LEXCA_TYPE_LE_OP || type==LEXCA_TYPE_GE_OP || type==LEXCA_TYPE_EQ_OP || type==LEXCA_TYPE_NE_OP || type=='<' || type=='>')
	{
		return 1;
	}
	
	return 0;
}

static int contains_variable_letters(const char *string)
{
	char *tmp=(char *)string;
	
	while(*tmp)
	{
		char c=*tmp;
		
		if(!((c>='A' && c<='Z') || (c>='a' && c<='z') || (c>='0' && c<='9') || c=='_'))
		{
			return 0;
		}
		
		tmp++;
	}
	
	return 1;
}

int pgd_trigger_lexca_buffer_get_object_from_pos(Pgd_trigger_lexca_buffer *obj, int object_pos)
{
	int res=-1;
	
	int obj_pos=0;
	int curr_obj_obj=1;
	
	GPtrArray *obj_arr=obj->a;
	
	for(size_t i=0;i<obj_arr->len;i++)
	{
		Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(obj_arr,i);
		
		if(node->obj_str && strcmp(node->obj_str,"__OBJECT")==0)
		{
			if(curr_obj_obj>=object_pos)
			{
				obj_pos=i;
			}
		
			curr_obj_obj++;
		}
	}
	
	return res;
}

int pgd_trigger_lexca_buffer_get_next_nestling(Pgd_trigger_lexca_buffer *obj, int curr)
{
	int nestling=0;
	int ret=curr;
	
	GPtrArray *obj_arr=obj->a;
	
	for(int i=curr;i<obj_arr->len;i++)
	{
		Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(obj_arr,i);
		
		printf("SEARCH %s %d %d (%d)\n",node->obj_str,node->type,i,nestling);
		
		if(node->type=='(')
		{
			nestling++;
		}
		else if((node->type==',' || node->type==')') && nestling==0)
		{
			printf("NORMAL FOUND %d\n",i);
			return i;
		}
		else if(node->type==')' && nestling>0)
		{
			nestling--;
		}
	}
	
	return -2;
}

int pgd_trigger_lexca_buffer_get_pos_relative(Pgd_trigger_lexca_buffer *obj1, Pgd_trigger_lexca_buffer *obj2, int obj2_pos, int index)
{
	GPtrArray *obj1_arr=obj1->a;
	GPtrArray *obj2_arr=obj2->a;

	if(obj1_arr->len>=obj2_arr->len)
	{
		for(size_t i=0;i<obj1_arr->len;i++)
		{
		
		}
	}
	
	return 0;
}

/**
	Compare code with a object,
	
	@TODO fix if something like COLLISION(RAND(__OBJECT1),__OBJECT2) ... etc
	
	@param obj1
		is a code_conversion
	@param obj2
		is a parameter file reference, may have __OBJECT for example
*/
int pgd_trigger_lexca_buffer_compare(Pgd_trigger_lexca_buffer *obj1, Pgd_trigger_lexca_buffer *obj2)
{
	int res=0;
	int node2_i=0;
	int node2_i_start=0;
	int hit=0;
	int inside_function=0;
	
	GPtrArray *obj1_arr=obj1->a;
	GPtrArray *obj2_arr=obj2->a;
	
	if(obj1_arr->len>=obj2_arr->len)
	{
		for(size_t i=0;i<obj1_arr->len;i++)
		{
			Pgd_trigger_lexca_buffer_node *node1=g_ptr_array_index(obj1_arr,i);
			Pgd_trigger_lexca_buffer_node *node2=g_ptr_array_index(obj2_arr,node2_i);
			
			hit=0;
			
			printf("COMPARE:: [%s][%s]\n",node1->obj_str,node2->obj_str);
			
			if(node1->type=='(')
			{
				inside_function++;
			}
			else if(node1->type==')')
			{
				inside_function--;
			}
			
			if(strcmp(node1->obj_str,node2->obj_str)==0 && node2->type==node1->type)
			{
				hit=1;
				goto do_hit;
			}
			
			if((type_is_equals(node1->type) && (type_is_equals(node2->type))) || strcmp(node2->obj_str,"__EQUIV")==0 || strcmp(node2->obj_str,"__EQUIV_CALCULATION")==0)
			{
				hit=2;
				goto do_hit;
			}
			
			if(contains_variable_letters(node1->obj_str) && (g_str_has_prefix(node2->obj_str,"__OBJECT") || strcmp(node2->obj_str,"__LEVEL")==0) && node1->type == LEXCA_TYPE_IDENTIFIER)
			{
				hit=3;
				goto do_hit;
			}
			
			if((strcmp(node2->obj_str,"__COORD")==0 || strcmp(node2->obj_str,"__COORD_X")==0 || strcmp(node2->obj_str,"__COORD_Y")==0 || strcmp(node2->obj_str,"__LENGTH")==0
			   || strcmp(node2->obj_str,"__DIRECTION")==0)
			   && (node1->type == LEXCA_TYPE_I_CONSTANT || node1->type == LEXCA_TYPE_F_CONSTANT))
			{
				hit=4;
				goto do_hit;
			}
			
			if(inside_function>0 && (obj2_arr->len-node2_i)>1)
			{
				int found=pgd_trigger_lexca_buffer_get_next_nestling(obj1,i);
				
				printf("FOUND-- %d (%d %d)\n",found,obj2_arr->len,node2_i);
				
				if(found>=0 || found==-2)
				{
					hit=5;
					i=found;
					goto do_hit;
				}
			}
			
		do_hit:
			
			if(hit>0)
			{
				printf("HIT::%d %d\n",hit,i);
				
				if(node2_i==0)
				{
					node2_i_start=i;
				}
				
				node2_i++;
				
				if(node2_i<obj2_arr->len)
				{
					continue;
				}
				else
				{
					res=node2_i_start;
					break;
				}
			}
			
			res=-1;
			node2_i=0;
/*			break;*/
		}
	}
	else
	{
		res=-1;
	}
	
	return res;
}

