/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//IT SHOULD MALLOC MAX_AO_ON FIELD if no create object function is defined, otherwise it should malloc as the ammount of objects it is from the start.
//const int MAX_AO_ON_FIELD=10;

//const int DIMENSION=2;

static char pgd_unwierdify_char(char c,int numok)
{
	while(!((c>='A' && c<='Z') || (c>='a' && c<='z') || (c=='_') || (numok && (c>='0' && c<='9'))))
	{
		if(c==' ')
		{
			c='_';
			break;
		}
	
		c+=100;
	}
	
	return c;
}

static char *pgd_string_to_valstr(char *str)
{
	char *fstr=g_strdup(str);
	char *fstrb=fstr;
	int numok=0;
	
	while(*fstrb)
	{
		*fstrb=pgd_unwierdify_char(*fstrb,numok);
		
		if(!numok)
			numok++;
		
		fstrb++;
	}
	
	return fstr;
}

/**
	This function will generate the C program file.

	@param gameOrLevel
		if not null then only generate for 1 specific level (for run level)
		if NULL then generate the entire game.

	@only the triggerparent,name and the tempparent are needed. remove/ all the else
*/
char* haed_generate_c_program_file(EksParent *tempEksParent,EksParent *gameOrLevel)
{
	GString *output=g_string_sized_new(2000);
	//char *output;
	
	EksParent *objects=eks_parent_get_child_from_name(tempEksParent,"objects");
	EksParent *ActiveObjects=eks_parent_get_child_from_name(objects,"active");
	
	EksParent *screenParent=eks_parent_get_child_from_name(tempEksParent,"screen");
	EksParent *sizeParent=eks_parent_get_child_from_name(screenParent,"size");
	
	intptr_t screenWidth=sizeParent?eks_parent_get_int(eks_parent_get_child_from_type(sizeParent,0,EKS_PARENT_TYPE_VALUE)):GLOBAL_SCREEN_WIDTH;
	intptr_t screenHeight=sizeParent?eks_parent_get_int(eks_parent_get_child_from_type(sizeParent,1,EKS_PARENT_TYPE_VALUE)):GLOBAL_SCREEN_HEIGHT;
	
	b_tm("getting screen!");
	
	//important definitions for user
	g_string_append_printf(output,"#include \"includes.h\"\n//Haed program!");
	//generate name
	g_string_append_printf(output,"//Program name\nconst char* PROGRAM_NAME=\"%s\";\n\n",tempEksParent->name);
	
	//generate screen width
	char *screenWidthStr=b_int_to_string(screenWidth);

	g_string_append_printf(output,"//Program screen width\nconst int SCREEN_WIDTH=%s;\n",screenWidthStr);
	free(screenWidthStr);
	
	//generate screen width
	char *screenHeightStr=b_int_to_string(screenHeight);
	
	g_string_append_printf(output,"//Program screen height\nconst int SCREEN_HEIGHT=%s;\n\n",screenHeightStr);
	free(screenHeightStr);
	
	//define all active objects
	eks_parent_foreach_child_start(ActiveObjects,curObject)
	{
		if(eks_parent_compare_type(curObject,EKS_PARENT_TYPE_VALUE))
		{
			g_string_append_printf(output,"AOHaedP *%s_init;\n",curObject->name);
		}
	}eks_parent_foreach_child_end(ActiveObjects,curObject);
	
	//define other here
	
	//input definitions
	g_string_append_printf(output,"\n//INPUT DEFINITIONS\nSDL_Keycode player1Keys[]={SDLK_UP,SDLK_DOWN,SDLK_LEFT,SDLK_RIGHT,SDLK_RSHIFT,SDLK_z};\nint Player1Input[6];\nSDL_Keycode player2Keys[]={0xe4,SDLK_o,SDLK_a,SDLK_e,SDLK_2,SDLK_1};\nint Player2Input[6];\n");
	
	//FUNCTIONS
	
	//init all active objects
	g_string_append_printf(output,"\n//INIT RULES\nvoid INIT_PROGRAM(void)\n{\n");
	
	for(int i=0;i<eks_parent_get_amount_from_type(ActiveObjects,EKS_PARENT_TYPE_VALUE);i++)
	{
		EksParent *thisActiveObject=eks_parent_get_child_from_type(ActiveObjects,i,EKS_PARENT_TYPE_VALUE);
		//define "unit number"
		
		//define animationssettings
		EksParent *animsettings=eks_parent_custom_get(haed_triggerhandler_get_object_by_name(thisActiveObject->name),HAED_OBJECT_BUFFHOLDER);
		g_string_append_printf(output,"\tanimationvect %s_AnimationVect;\n",thisActiveObject->name);
		g_string_append_printf(output,"\tSDL_Rect ***%s_AnimationSettings=create_rect_vector(&%s_AnimationVect",thisActiveObject->name,thisActiveObject->name);
		
		for(int ii=0;ii<eks_parent_get_amount_from_type(animsettings,EKS_PARENT_TYPE_VALUE);ii++)
		{
			EksParent *animationtype=eks_parent_get_child_from_type(animsettings,ii,EKS_PARENT_TYPE_VALUE);
			
			if(strcmp(animationtype->name,"objectinf")!=0)
			{
				g_string_append_printf(output,",\"%s\"",animationtype->name);
				
				for(int iii=0;iii<eks_parent_get_amount_from_type(animationtype,EKS_PARENT_TYPE_VALUE);iii++)
				{
					//all-directions
					EksParent *animationdirection=eks_parent_get_child_from_type(animationtype,iii,EKS_PARENT_TYPE_VALUE);
					
					//get_frames
					EksParent *getframes=eks_parent_get_child_from_name(animationdirection,"FRAMES");
					
					int framesamount=eks_parent_get_amount_from_type(getframes,EKS_PARENT_TYPE_VALUE);
					
					g_string_append_printf(output,",\"%s\",%s",animationdirection->name,b_int_to_string(framesamount));
					
					//1=animation speed
					for(int ii2=0;ii2<framesamount;ii2++)
					{
						EksParent *framepos=eks_parent_get_child_from_type(getframes,ii2,EKS_PARENT_TYPE_VALUE);
					
						g_string_append_printf(output,",&(SDL_Rect){");
						
						for(int ii3=0;ii3<4;ii3++)
						{
							char *frameval=eks_parent_get_string(eks_parent_get_child_from_type(framepos,ii3,EKS_PARENT_TYPE_VALUE));
							
							if(ii3==0)
							{
								g_string_append_printf(output,"%s",frameval);
							}
							else
							{
								g_string_append_printf(output,",%s",frameval);
							}
						}
						
						g_string_append_printf(output,"}");
						
						//b_int_to_string(eks_parent_get_child_from_name(eks_parent_get_child_from_name(animsettings,"stopped"),"ALL_MIRROR")->amount-1),",32,32},{",b_int_to_string(eks_parent_get_child_from_name(eks_parent_get_child_from_name(animsettings,"running"),"ALL_MIRROR")->amount-1),",32,32},{",b_int_to_string(eks_parent_get_child_from_name(eks_parent_get_child_from_name(animsettings,"disappearing"),"ALL_MIRROR")->amount-1),",32,32}});\n",NULL);
					}
				}
			}
		}
		
		g_string_append_printf(output,",NULL);\n");
		
		//init object function
		g_string_append_printf(output,"\t%s_init=init_ActiveObject(\"%s\",%s_AnimationSettings,%s_AnimationVect);\n\n",thisActiveObject->name,thisActiveObject->name,thisActiveObject->name,thisActiveObject->name);
		
		EksParent *movparent=eks_parent_get_child_from_name(thisActiveObject,"mov");
		
		if(movparent)
		{
			if(strcmp(eks_parent_get_string(eks_parent_get_child_from_type(eks_parent_get_child_from_name(thisActiveObject,"mov"),0,EKS_PARENT_TYPE_VALUE)),"none")!=0 && eks_parent_get_child(eks_parent_get_child_from_name(thisActiveObject,"mov"),0)!=NULL)
			{
				//speed
				double speedstr=haed_object_get_double(thisActiveObject,"speed");
				
				for(int ii=0;ii<DIMENSION;ii++)
				{
					char *iistr=b_int_to_string(ii);
				
					g_string_append_printf(output,"\t%s_init->speed[%s]=%s;\n",thisActiveObject->name,iistr,b_float_to_string((speedstr*5)/100));
					
					free(iistr);
				}
				
				//accel
				double accelerationstr=haed_object_get_double(thisActiveObject,"acceleration");
				int accelerationstr2=haed_object_get_int(thisActiveObject,"acceleration");
				
				b_tm("STRTEST= %g %d",accelerationstr,accelerationstr2);
				
				for(int ii=0;ii<DIMENSION;ii++)
				{
					char *iistr=b_int_to_string(ii);
				
					g_string_append_printf(output,"\t%s_init->accel[%s]=%s;\n",thisActiveObject->name,iistr,b_float_to_string(accelerationstr/100));
					
					free(iistr);
				}
				
				double deaccelerationstr=haed_object_get_double(thisActiveObject,"deacceleration");
				//deaccel
				
				for(int ii=0;ii<DIMENSION;ii++)
				{
					char *iistr=b_int_to_string(ii);
				
					g_string_append_printf(output,"\t%s_init->deaccel[%s]=%s;\n",thisActiveObject->name,iistr,b_float_to_string(deaccelerationstr/100));
					
					free(iistr);
				}
			}
		}
		
		g_string_append_printf(output,"\t\n");
	}
	
	//INPUT
	g_string_append_printf(output,"}\n\n//HAPPENS ON INPUT\nvoid INPUT_PROGRAM(SDL_Event *tempEvent)\n{\n");
	
	g_string_append_printf(output,"\tSetInputPlayer(Player1Input,player1Keys,tempEvent);\n");
	g_string_append_printf(output,"\tSetInputPlayer(Player2Input,player2Keys,tempEvent);\n}\n\n");
	
	
	
	EksParent *levelsparent=eks_parent_get_child_from_name(tempEksParent,"levels");
	
	char *firstLevelName=NULL;
	char *levelvector=concat("void *LEVEL_VECTOR[] = {",NULL);
	
	eks_parent_foreach_child_start(levelsparent,curLevelParent1)
	{
		if(eks_parent_compare_type(curLevelParent1,EKS_PARENT_TYPE_VALUE) && (gameOrLevel==NULL || gameOrLevel==curLevelParent1))
		{
			char *curLevelName=curLevelParent1->name;
	
			if(!firstLevelName)
				firstLevelName=curLevelName;
			
			g_string_append_printf(output,"void %s(void);\n",curLevelName);
			levelvector=concat(levelvector,curLevelName,",",NULL);
		}
	}eks_parent_foreach_child_end(levelsparent,curLevelParent1);
	
	levelvector[strlen(levelvector)-1]='}';
	levelvector=concat(levelvector,";\n",NULL);
	
	g_string_append_printf(output,"%s",levelvector);
	
	eks_parent_foreach_child_start(levelsparent,curLevelParent)
	{
		if(eks_parent_compare_type(curLevelParent,EKS_PARENT_TYPE_VALUE) && (gameOrLevel==NULL || gameOrLevel==curLevelParent))
		{
			char *curLevelName=curLevelParent->name;
	
			//ACTIONS
			g_string_append_printf(output,"//WHERE ALL THE FUN STUFF HAPPENS...\nvoid %s(void)\n{\n",curLevelName);
	
			//temp for triggers
			for(int i=0;i<eks_parent_get_amount_from_type(ActiveObjects,EKS_PARENT_TYPE_VALUE);i++)
			{
				g_string_append_printf(output,"\tHageUnit *%s;\n",eks_parent_get_child_from_type(ActiveObjects,i,EKS_PARENT_TYPE_VALUE)->name);
			}
	
			//if any coordinates are definded
			EksParent *positionsParent=eks_parent_get_child_from_name(curLevelParent,"positions");
	
			int numberOfplacedObjects=eks_parent_get_amount_from_type(positionsParent,EKS_PARENT_TYPE_VALUE);

			g_string_append_printf(output,"\tif(LEVEL_START)\n\t{\n\t\tdestroy_all(TOPLEVEL_UNIT);\n");

			b_tm("Placing objects, number of objects: %d",numberOfplacedObjects);

			for(int ii=0;ii<numberOfplacedObjects;ii++)
			{
				//get "UNIT"
				EksParent *unit=eks_parent_get_child_from_type(positionsParent,ii,EKS_PARENT_TYPE_VALUE);		
		
				g_string_append_printf(output,"\t\tCREATE_OBJECT(%s_init,%s,%s);\n",haed_unit_get_obj_name(unit),b_int_to_string(haed_unit_get_coord_x(unit)),b_int_to_string(haed_unit_get_coord_y(unit)));
			}
		
			g_string_append_printf(output,"\t\tLEVEL_START=0;\n\t}\n");
			
			int bRed,bGreen,bBlue;
			
			if(eks_parent_get_child_from_name(curLevelParent,"background-color"))
			{
				bRed=haed_get_int_from_pos(curLevelParent,"background-color",0);
				bGreen=haed_get_int_from_pos(curLevelParent,"background-color",1);
				bBlue=haed_get_int_from_pos(curLevelParent,"background-color",2);
			}
			else
			{
				bRed=255;
				bGreen=255;
				bBlue=255;
			}
			
			char *sRed=b_int_to_string(bRed);
			char *sGreen=b_int_to_string(bGreen);
			char *sBlue=b_int_to_string(bBlue);
			
			g_string_append_printf(output,"\tSDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format,%s,%s,%s));",sRed,sGreen,sBlue);
			
			free(sRed);
			free(sGreen);
			free(sBlue);
	
			//set the velocity
			for(int i=0;i<eks_parent_get_amount_from_type(ActiveObjects,EKS_PARENT_TYPE_VALUE);i++)
			{
				EksParent *thisActiveObject=eks_parent_get_child_from_type(ActiveObjects,i,EKS_PARENT_TYPE_VALUE);
		
				EksParent *movparent=eks_parent_get_child_from_name(thisActiveObject,"mov");
				
				EksParent *movtypeparent=eks_parent_get_child_from_type(movparent,0,EKS_PARENT_TYPE_VALUE);
				
				if(movparent && movtypeparent && strcmp(eks_parent_get_string(movtypeparent),"none")!=0)
				{
					g_string_append_printf(output,"\n\thage_move_objects(%s_init,Player%sInput);\n",thisActiveObject->name,eks_parent_get_string(eks_parent_get_child_from_type(eks_parent_get_child_from_name(thisActiveObject,"player"),0,EKS_PARENT_TYPE_VALUE)));
					g_string_append_printf(output,"\n\thage_apply_movement(%s_init);\n",thisActiveObject->name);
				}
			}
			
			//apply the velocity
/*			for(int i=0;i<eks_parent_get_amount_from_type(ActiveObjects,EKS_PARENT_TYPE_VALUE);i++)*/
/*			{*/
/*				EksParent *thisActiveObject=eks_parent_get_child_from_type(ActiveObjects,i,EKS_PARENT_TYPE_VALUE);*/
/*		*/
/*				EksParent *movparent=eks_parent_get_child_from_name(thisActiveObject,"mov");*/
/*		*/
/*				if(movparent && eks_parent_get_child_from_type(movparent,0,EKS_PARENT_TYPE_VALUE) && strcmp(eks_parent_get_string(eks_parent_get_child_from_type(movparent,0,EKS_PARENT_TYPE_VALUE)),"none")!=0)*/
/*				{*/
/*					*/
/*				}*/
/*			}*/
			
			b_tm("Doing triggers...");
			
			EksParent *triggerparent=eks_parent_get_child_from_name(curLevelParent,"triggers");
	
			if(triggerparent)
			{
				//READ TRIGGERS
				for(int i=0;i<eks_parent_get_amount_from_type(triggerparent,EKS_PARENT_TYPE_VALUE);i++)
				{
					g_string_append_printf(output,"%s",pgd_trigger_as_string(eks_parent_get_child_from_type(triggerparent,i,EKS_PARENT_TYPE_VALUE),tempEksParent));
				}
			}
	
			g_string_append_printf(output,"}\n");
		}
	}eks_parent_foreach_child_end(levelsparent,curLevelParent);
	
	g_string_append_printf(output,"\n\nvoid *FIRST_LEVEL=%s;\n",firstLevelName);

	return g_string_free(output,FALSE);
}
