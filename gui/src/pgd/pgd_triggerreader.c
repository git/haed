/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

/*
E_ (= engine) 0
Active-object 1
Backdrops 2

=== CONDITION (if) ===
object collision object
E_ timer 9:35
E_ startup
((object collision object) && (E_ timer 9:35)) || E_ startup

=== ACTIONS (what happens,see actionreader.c) ===
object stop

=== How it does this ===


=== TODO ===
avancerade conditions ex: cond1 && cond 2 
alla objekt funna i actions men inte i conditions ska loopas fram inne i if-satsen

*/

/**


*/
void pgd_infvect_trigg_obj_add(PgdTriggObjInfVect *objInfVect,const char *object,int numberOfLoops,bool writeEnable)
{
	//check if it already exists
	for(int i=0;i<objInfVect->amount;i++)
	{
		if(strcmp(object,objInfVect->objInf[i].object)==0)
		{
			if(writeEnable)
			{
 
				objInfVect->objInf[i].writeEnable=(writeEnable!=0);
			}
			return;
		}
	}
	
	//else
	objInfVect->objInf=(PgdTriggObjInf*)realloc(objInfVect->objInf,sizeof(PgdTriggObjInfVect)*(objInfVect->amount+1));
	objInfVect->objInf[objInfVect->amount].object=g_strdup(object);
	objInfVect->objInf[objInfVect->amount].numberOfLoops=numberOfLoops;
	objInfVect->objInf[objInfVect->amount].writeEnable=(writeEnable!=0);
	//objInfVect->objInf[objInfVect->amount].value=g_strndup(value,strlen(value));
	objInfVect->amount++;
	
	return;
}

/**
	Loop through the parameters of the trigger
*/
int pgd_infvect_trigg_obj_add_from_trigger_handle(const char* valStr, LexcaStrType valType, size_t index, void* user_data)
{
	void** paramin=user_data;
	EksParent *aoEksParent=paramin[0];
	PgdTriggObjInfVect *objInfVect=paramin[1];
	intptr_t *state=(intptr_t*)&(paramin[2]);
	char **tmpstr=(char**)&(paramin[3]);

	if(valType == LEXCA_TYPE_IDENTIFIER && strcmp(valStr,"COUNT")==0)
	{
		*state=1;
		return 0;
	}
	else if(valType == LEXCA_TYPE_IDENTIFIER && *state==1)
	{
		*tmpstr=g_strdup(valStr);
		*state=2;
		return 0;
	}
	else if(valType == LEXCA_TYPE_EQ_OP && *state==2)
	{
		*state=3;
		return 0;
	}

	eks_parent_foreach_child_start(aoEksParent,loopObject)
	{
		//dont add it!!!
		if(strcmp(valStr,"0")==0 && *state==3)
		{
			b_tm("Found count function resulting into 0, dont add.");
			
			pgd_infvect_trigg_obj_add(objInfVect,*tmpstr,0,FALSE);
			g_free(*tmpstr);
			*tmpstr=NULL;
			return 0;
		}
		else if(strcmp(valStr,loopObject->name)==0 && eks_parent_compare_type(loopObject,EKS_PARENT_TYPE_VALUE) && valType==LEXCA_TYPE_IDENTIFIER)
		{
			pgd_infvect_trigg_obj_add(objInfVect,valStr,-1,TRUE);
			return 0;
		}
	}eks_parent_foreach_child_end(aoEksParent,loopObject);
	
	return 0;
}

static void pgd_infvect_trigg_obj_add_from_trigger_foreach(PgdTriggObjInfVect *objInfVect,const char *trigger,EksParent *topEksParent, Pgd_trigger_lexca_buffer *triggerab, const char *codestr, EksParent *loop_object)
{
	g_autoptr(Pgd_trigger_lexca_buffer) refab=pgd_trigger_lexca_buffer_create(codestr);
	
	GPtrArray *refa=refab->a;
	GPtrArray *triggera=triggerab->a;
	
	int res=pgd_trigger_lexca_buffer_compare(triggerab,refab);
	
	printf("RES:: %d %s -> %s\n",res,trigger,codestr);
	
	if(res>=0)
	{
		int added=0;
	
		for(size_t i=res;i<refa->len;i++)
		{
			Pgd_trigger_lexca_buffer_node *refa_node=g_ptr_array_index(refa,i);
		
			if(g_strcmp0(refa_node->obj_str,"__OBJECT")==0)
			{
				if(i<triggera->len)
				{
					Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(triggera,i);
					
					b_tm("REAL ADDING OBJ:: %s [%s] {%s}",node->obj_str,trigger,codestr);
					
/*					pgd_infvect_trigg_obj_add(objInfVect,node->obj,0,FALSE);*/
					pgd_infvect_trigg_obj_add(objInfVect,node->obj_str,-1,TRUE);
					
					added++;
				}
			}
			else if(g_strcmp0(refa_node->obj_str,"__OBJECT__NOITR")==0)
			{
				if(i<triggera->len)
				{
					Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(triggera,i);
					
					b_tm("ITRREAL ADDING OBJ:: %s [%s] {%s}",node->obj_str,trigger,codestr);
					
					pgd_infvect_trigg_obj_add(objInfVect,node->obj_str,0,FALSE);
/*					pgd_infvect_trigg_obj_add(objInfVect,node->obj,-1,TRUE);*/
					
					added++;
				}
			}
		}
		
		if(added>0)
		{
			return;
		}
	}
}

/**
	Make a treatable structure which can be used to create the do{}while loops.
*/
void pgd_infvect_trigg_obj_add_from_trigger(PgdTriggObjInfVect *objInfVect,const char *trigger,EksParent *topEksParent)
{
	EksParent *objectsParent=eks_parent_get_child_from_name(topEksParent,"objects");
	
	EksParent *aoEksParent=eks_parent_get_child_from_name(objectsParent,"active");

/*	HamsFunction *objects=hams_parse_text(trigger);*/
/*	*/
/*	for(int i=0;i<objects->amount;i++)*/
/*	{*/
/*		eks_parent_foreach_child_start(aoEksParent,loopObject)*/
/*		{*/
/*			b_tm("OBJ NAME: %s OBJ TYPE: %d",objects->parameters[i].name,objects->parameters[i].type);*/
/*			//dont add it!!!*/
/*			if(objects->parameters[i].type == HAMS_FUNCTION && strcmp(objects->parameters[i].name,"COUNT")==0 && strcmp(objects->parameters[i+3].name,"0")==0)*/
/*			{*/
/*				b_tm("GOT IN HERE! ARRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");*/
/*				pgd_infvect_trigg_obj_add(objInfVect,objects->parameters[i+1].name,0,FALSE);*/
/*			}*/
/*			else if(strcmp(objects->parameters[i].name,loopObject->name)==0 && eks_parent_compare_type(loopObject,EKS_PARENT_TYPE_VALUE))*/
/*			{*/
/*				pgd_infvect_trigg_obj_add(objInfVect,objects->parameters[i].name,-1,TRUE);*/
/*			}*/
/*		}eks_parent_foreach_child_end(aoEksParent,loopObject);*/
/*		*/
/*	}*/
/*	*/
/*	hams_destroy(objects);*/
/*	*/


/*	
	//old
	void *params[]={aoEksParent,objInfVect,0,NULL};
	lexca_for_each(trigger,pgd_infvect_trigg_obj_add_from_trigger_handle,params);
*/

	g_autoptr(Pgd_trigger_lexca_buffer) triggerab=pgd_trigger_lexca_buffer_create(trigger);
	
	GPtrArray *triggera=triggerab->a;

	EksParent *conf_trigger_parent=eks_parent_get_child_from_name(GLOBAL_HAED_CONFIGURATION->data,"triggers");
	EksParent *conf_conditions_parent=eks_parent_get_child_from_name(conf_trigger_parent,"conditions");
	EksParent *conf_object_parent=eks_parent_get_child_from_name(conf_conditions_parent,"STD_OBJECT");
	
	eks_parent_foreach_child_start(conf_object_parent,loop_object)
	{
		EksParent *trigger_check_parent=eks_parent_get_child_from_name(loop_object,"_CODE");
		g_autofree char *check_str=eks_parent_get_string(eks_parent_get_child(trigger_check_parent,0));
		
		if(check_str)
		{
			pgd_infvect_trigg_obj_add_from_trigger_foreach(objInfVect,trigger,topEksParent,triggerab,check_str,loop_object);
		}
		else
		{
			eks_parent_foreach_child_start(loop_object,loop_object2)
			{
				EksParent *trigger_check_parent2=eks_parent_get_child_from_name(loop_object2,"_CODE");
				g_autofree char *check_str2=eks_parent_get_string(eks_parent_get_child(trigger_check_parent2,0));
				
				if(check_str2)
				{
					pgd_infvect_trigg_obj_add_from_trigger_foreach(objInfVect,trigger,topEksParent,triggerab,check_str2,loop_object2);
				}
			}eks_parent_foreach_child_end(loop_object,loop_object2);
		}
	}eks_parent_foreach_child_end(conf_object_parent,loop_object);
	
	eks_parent_foreach_child_start(aoEksParent,loopObject)
	{
		for(size_t i=0;i<triggera->len;i++)
		{
			Pgd_trigger_lexca_buffer_node *node=g_ptr_array_index(triggera,i);
			
			if(node->obj_str && loopObject->name && strcmp(node->obj_str,loopObject->name)==0 && eks_parent_compare_type(loopObject,EKS_PARENT_TYPE_VALUE) && 
			   node->type==LEXCA_TYPE_IDENTIFIER)
			{
				b_tm("ADDING OBJ:: %s [%s]",node->obj_str,trigger);
				
				pgd_infvect_trigg_obj_add(objInfVect,node->obj_str,-1,TRUE);
			}
		}
	}eks_parent_foreach_child_end(aoEksParent,loopObject);
}


/**
	Converts a EksParent trigger object into something that is "compile"-able

	@triggerEksParent NO_FREE
		the parent contaninig the condition and a trigger vector.
	@topEksParent NO_FREE
		The top-level parent. Which means the one containig the active objects and stuff
	@return NEW
		Returns the pointer to the compileable string.
	

	if(unit1=hattis->firstunit){if(unit2=mal->firstunit){do{do{
		if(hage_collision(unit1,unit2))
		{
			hage_stop(unit1);
			DESTROY_OBJECT(unit2);
		}
	}TRY_THIS_OBJECT_OUT(unit2,mal);}TRY_THIS_OBJECT_OUT(unit1,hattis);}}
*/
char *pgd_trigger_as_string(EksParent *triggerEksParent,EksParent *topEksParent)
{
	PgdTriggObjInfVect *AllTriggerObjects=calloc(1,sizeof(PgdTriggObjInfVect));
	
	GString *objectForLoopsString=g_string_sized_new(200);
	GString *endBrackets=g_string_sized_new(200);
	GString *actionsString=g_string_sized_new(200);
	char *conditionsString;
	
	//if there is some sort of implementation on fixing bad syntax, then put it here.
	conditionsString=eks_parent_get_string(triggerEksParent);
	
	pgd_infvect_trigg_obj_add_from_trigger(AllTriggerObjects,conditionsString,topEksParent);
	
	//GENERATE FINAL
	/* ADD THE ACTIONS eg DESTROY, STOP etc..*/
	eks_parent_foreach_child_start(triggerEksParent,triggObject)
	{
		//covert this to a more modern
		char *thetrigger=eks_parent_get_string(triggObject);
		
		g_string_append_printf(actionsString,"\t\t\t%s;\n",thetrigger);
		
		pgd_infvect_trigg_obj_add_from_trigger(AllTriggerObjects,thetrigger,topEksParent);
		
		free(thetrigger);
	}eks_parent_foreach_child_end(triggerEksParent,triggObject);
	
	for(int i=0;i<AllTriggerObjects->amount;i++)
	{
		if(AllTriggerObjects->objInf[i].numberOfLoops!=0)
		{
			g_string_append_printf(objectForLoopsString,"FOR_EACH_START(%s){",AllTriggerObjects->objInf[i].object);
		
			g_string_append_printf(endBrackets,"}FOR_EACH_OBJECT(%s);",AllTriggerObjects->objInf[i].object);
		}
	}
	for(int i=0;i<AllTriggerObjects->amount;i++)
	{
		if(AllTriggerObjects->objInf[i].numberOfLoops!=0)
		{
			g_string_append_printf(objectForLoopsString,"do{");

			g_string_append_printf(endBrackets,"}");
		}
	}
	
	char *tmpobjectForLoopsString=g_string_free(objectForLoopsString,FALSE);
	char *tmpendBrackets=g_string_free(endBrackets,FALSE);
	char *tmpactionsString=g_string_free(actionsString,FALSE);
	
	char *retstring=concat("\t",tmpobjectForLoopsString,"\n\t\tif(",conditionsString,")\n\t\t{\n",tmpactionsString,"\n\t\t}\n\t",tmpendBrackets,"\n",NULL);
	
	free(tmpobjectForLoopsString);
	free(tmpactionsString);
	free(tmpendBrackets);
	
	free(conditionsString);
	
	return retstring;
}
