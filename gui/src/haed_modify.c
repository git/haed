/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

char *haed_get_string_from_pos(EksParent *object, const char *assignparam, int pos)
{
	EksParent *objParent=eks_parent_get_child_from_name(object,assignparam);

	if(!objParent)
		return NULL;

	return eks_parent_get_string(eks_parent_get_child(objParent,pos));
}

void haed_set_string_from_pos(EksParent *object, const char *assignparam,const char *setparam, int delete, int pos)
{
	EksParent *mov=eks_parent_get_child_from_name(object,assignparam);
	
	if(delete && mov)
	{
		eks_parent_destroy(mov,EKS_TRUE);
		
		return;
	}
	else
	{
		if(!mov)
			mov=eks_parent_add_child(object,assignparam,EKS_PARENT_TYPE_VALUE,NULL);

		EksParent *theValue=eks_parent_get_child(mov,pos);
	
		if(!theValue)
			theValue=eks_parent_add_child_base(mov,EKS_PARENT_TYPE_VALUE,NULL);
		
		eks_parent_set(theValue,setparam);
	}
}

intptr_t haed_get_int_from_pos(EksParent *object, const char *assignparam, int pos)
{
	EksParent *objParent=eks_parent_get_child_from_name(object,assignparam);

	if(!objParent)
		return 0;

	return eks_parent_get_int(eks_parent_get_child(objParent,pos));
}

void haed_set_int_from_pos(EksParent *object, const char *assignparam,int setparam, int delete, int pos)
{
	EksParent *mov=eks_parent_get_child_from_name(object,assignparam);
	
	if(delete && mov)
	{
		eks_parent_destroy(mov,EKS_TRUE);
		
		return;
	}
	else
	{
		if(!mov)
			mov=eks_parent_add_child(object,assignparam,EKS_PARENT_TYPE_VALUE,NULL);

		EksParent *theValue=eks_parent_get_child(mov,pos);
	
		if(!theValue)
			theValue=eks_parent_add_child_base(mov,EKS_PARENT_TYPE_VALUE,NULL);
		
		eks_parent_set(theValue,setparam);
	}
}

double haed_get_double_from_pos(EksParent *object, const char *assignparam, int pos)
{
	EksParent *objParent=eks_parent_get_child_from_name(object,assignparam);

	if(!objParent)
		return 0;

	return eks_parent_get_double(eks_parent_get_child(objParent,pos));
}

void haed_set_double_from_pos(EksParent *object, const char *assignparam,double setparam, int delete, int pos)
{
	EksParent *mov=eks_parent_get_child_from_name(object,assignparam);
	
	if(delete && mov)
	{
		eks_parent_destroy(mov,EKS_TRUE);
		
		return;
	}
	else
	{
		if(!mov)
			mov=eks_parent_add_child(object,assignparam,EKS_PARENT_TYPE_VALUE,NULL);

		EksParent *theValue=eks_parent_get_child(mov,pos);
	
		if(!theValue)
			theValue=eks_parent_add_child_base(mov,EKS_PARENT_TYPE_VALUE,NULL);
		
		eks_parent_set(theValue,setparam);
	}
}
