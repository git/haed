/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

void haed_top_menu_init(void)
{

	//MENU
	GLOBAL_MAIN_MENU=gtk_menu_bar_new();
		GtkWidget *haedFileMenu = gtk_menu_new();
		//FILE AND SUBMENUS
		GtkWidget *fileMenuItem = gtk_menu_item_new_with_mnemonic("_File");
	
			//GtkToolButton *item = gtk_tool_button_new (NULL, “_New”);

			//gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (item), “new”);
			
			GtkWidget *documentNewMenuItem = gtk_menu_item_new();
			//gtk_menu_set_reserve_toggle_size (GTK_MENU(documentNewMenuItem),0);
			/*g_object_new (GTK_TYPE_MENU_ITEM,
			"use-underline", TRUE,
			"label", "_New",
			NULL);*/
			GtkWidget *hbox = gtk_box_new( GTK_ORIENTATION_HORIZONTAL, 0 );
	
			
			//gtk_container_remove(documentNewMenuItem,(GtkWidget*)(gtk_container_get_children(GTK_CONTAINER(documentNewMenuItem))->data));
			gtk_container_add( GTK_CONTAINER(documentNewMenuItem), hbox );
	
			GtkWidget *image = gtk_image_new_from_icon_name( "document-new",GTK_ICON_SIZE_MENU);
			//Fill image here
			gtk_box_pack_start( GTK_BOX( hbox ), image, FALSE, FALSE, 2 );
			
			GtkWidget *label = gtk_label_new_with_mnemonic(_("_New"));
			//gtk_entry_set_icon_from_icon_name (GTK_ENTRY(label),GTK_ENTRY_ICON_PRIMARY,"document-new");
			g_signal_connect (documentNewMenuItem, "activate",G_CALLBACK (haed_new_program), NULL);
			gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 2 );
			
			/*
			gtk_widget_set_hexpand(hbox,1);
			gtk_widget_set_hexpand(gtk_widget_get_parent (documentNewMenuItem),0);
			gtk_widget_set_hexpand(gtk_widget_get_parent (hbox),0);
			gtk_widget_set_margin_left(hbox,0);
			gtk_widget_set_margin_left(documentNewMenuItem,0);
			*/
			//gtk_grid_attach( GTK_BOX( (GtkWidget*)(gtk_container_get_children(GTK_CONTAINER(documentNewMenuItem))->data) ), label, FALSE, FALSE, 2 );
			
			//GtkWidget *cimage=gtk_image_new_from_pixbuf(gdk_pixbuf_new_from_file("gtk-new",NULL));
			//gtk_container_add((GtkContainer*)documentNewMenuItem,cimage);
			//gtk_widget_show(cimage);
			
			
			//GtkWidget *child = gtk_bin_get_child (GTK_BIN (documentNewMenuItem));
			//gtk_label_set_markup (GTK_LABEL (child), "<img src=\"open\"/><i>new label</i> with <b>markup</b>");
			//gtk_misc_set_padding(GTK_MISC(child),0,0);
			//gtk_misc_set_alignment(GTK_MISC(child),0,0);
			
			//gtk_image_set_from_icon_name(GTK_IMAGE(child), "document-new",GTK_ICON_SIZE_MENU);
			//gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_1, 0);
			
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), documentNewMenuItem);
				
			GtkWidget *openMenuItem = gtk_menu_item_new_with_mnemonic(_("_Open"));
			//gdk_window_set_icon_name(gtk_widget_get_window (gtk_bin_get_child(GTK_BIN(openMenuItem))),"document-new");
			g_signal_connect (openMenuItem, "activate",G_CALLBACK (haed_load_program), NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), openMenuItem);
			
			//gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), gtk_separator_menu_item_new());
	
			GtkWidget *saveMenuItem = gtk_menu_item_new_with_mnemonic(_("_Save"));
			g_signal_connect (saveMenuItem, "activate",G_CALLBACK (haed_gui_save), NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), saveMenuItem);
				
			GtkWidget *saveAsMenuItem = gtk_menu_item_new_with_mnemonic("Save _As");
			g_signal_connect (saveAsMenuItem, "activate",G_CALLBACK (haed_gui_save_as), NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), saveAsMenuItem);
			
			GtkWidget *produceMenuItem = gtk_menu_item_new_with_label("Produce project");
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), produceMenuItem);
				
			GtkWidget *preferencesMenuItem = gtk_menu_item_new_with_mnemonic("Preferences");
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), preferencesMenuItem);
			
			GtkWidget *quitMenuItem = gtk_menu_item_new_with_mnemonic("_Quit");
			gtk_menu_shell_append(GTK_MENU_SHELL(haedFileMenu), quitMenuItem);
				
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(fileMenuItem), haedFileMenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(GLOBAL_MAIN_MENU), fileMenuItem);
		
		GtkWidget *editMenu = gtk_menu_new();
		GtkWidget *editMenuItem = gtk_menu_item_new_with_label("Edit");
				
			GtkWidget *undoMenuItem = gtk_menu_item_new_with_mnemonic("_Undo");
			gtk_menu_shell_append(GTK_MENU_SHELL(editMenu), undoMenuItem);
				
			GtkWidget *redoMenuItem = gtk_menu_item_new_with_mnemonic("_Redo");
			gtk_menu_shell_append(GTK_MENU_SHELL(editMenu), redoMenuItem);
			
			GtkWidget *cutMenuItem = gtk_menu_item_new_with_mnemonic("_Cut");
			gtk_menu_shell_append(GTK_MENU_SHELL(editMenu), cutMenuItem);
				
			GtkWidget *copyMenuItem = gtk_menu_item_new_with_mnemonic("C_opy");
			gtk_menu_shell_append(GTK_MENU_SHELL(editMenu), copyMenuItem);
				
			GtkWidget *pasteMenuItem = gtk_menu_item_new_with_mnemonic("_Paste");
			gtk_menu_shell_append(GTK_MENU_SHELL(editMenu), pasteMenuItem);
			
			GtkWidget *selectAllMenuItem = gtk_menu_item_new_with_mnemonic("_Select All");
			gtk_menu_shell_append(GTK_MENU_SHELL(editMenu), selectAllMenuItem);
				
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(editMenuItem), editMenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(GLOBAL_MAIN_MENU), editMenuItem);
		
		GtkWidget *objectsMenu = gtk_menu_new();
		GtkWidget *objectsMenuItem = gtk_menu_item_new_with_label("Objects");
				
			GtkWidget *showObjectMenuItem = gtk_menu_item_new_with_label("Show objects");
			gtk_menu_shell_append(GTK_MENU_SHELL(objectsMenu), showObjectMenuItem);
				
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(objectsMenuItem), objectsMenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(GLOBAL_MAIN_MENU), objectsMenuItem);
		
		GtkWidget *windowMenu = gtk_menu_new();
		GtkWidget *windowMenuItem = gtk_menu_item_new_with_label("Window");
				
			GtkWidget *cascadeMenuItem = gtk_menu_item_new_with_label("Cascade");
			gtk_menu_shell_append(GTK_MENU_SHELL(windowMenu), cascadeMenuItem);
				
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(windowMenuItem), windowMenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(GLOBAL_MAIN_MENU), windowMenuItem);
		
		GtkWidget *runMenu = gtk_menu_new();
		GtkWidget *runMenuItem = gtk_menu_item_new_with_label("Run");
				
			GtkWidget *runGameMenuItem = gtk_menu_item_new_with_label("Run game");
			gtk_menu_shell_append(GTK_MENU_SHELL(runMenu), runGameMenuItem);
			
			GtkWidget *runLevelMenuItem = gtk_menu_item_new_with_label("Run level");
			gtk_menu_shell_append(GTK_MENU_SHELL(runMenu), runLevelMenuItem);
				
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(runMenuItem), runMenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(GLOBAL_MAIN_MENU), runMenuItem);
		
		GtkWidget *helpMenu = gtk_menu_new();
		GtkWidget *helpMenuItem = gtk_menu_item_new_with_label("Help");
				
			GtkWidget *aboutMenuItem = gtk_menu_item_new_with_mnemonic("_About");
			gtk_menu_shell_append(GTK_MENU_SHELL(helpMenu), aboutMenuItem);
				
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(helpMenuItem), helpMenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(GLOBAL_MAIN_MENU), helpMenuItem);

	gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_BOX),GLOBAL_MAIN_MENU,0,1,0);
}
