/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "haed_config.h"

Haed_config *GLOBAL_HAED_CONFIGURATION=NULL;

/**
	Constructor for Haed_config
	
	@param self
		reset or initialize the Haed_config struct.
	@returns
		initialized Haed_config struct.
*/
Haed_config *haed_config_init(Haed_config *self, const char *main_config_file)
{
	memset(self,'\0',sizeof(Haed_config));

	self->main_config_file=strdup(main_config_file);
	self->data=eks_parent_parse_file(main_config_file);

	return self;
}

/**
	Constructor and allocator for the Haed_config struct
	
	@returns
		newly allocated Haed_config struct.
*/
Haed_config *haed_config_new(const char *main_config_file)
{
	Haed_config *self=malloc(sizeof(Haed_config));
	if(!self)
	{
		return NULL;
	}

	Haed_config *result=haed_config_init(self,main_config_file);
	
	if(result==NULL)
	{
		free(self);
	}
	
	return result;
}

/**
	De-initialize the struct but does not free the input.
	
	@param self
		Struct to handle
*/
void haed_config_finalize(Haed_config *self)
{
	free(self->main_config_file);
}

/**
	Destructor of the struct, will call haed_config_finalize and free. 
	
	@param self
		Struct to handle. Note that self will be freed inside this function
*/
void haed_config_free(Haed_config *self)
{
	haed_config_finalize(self);

	free(self);
}

/////////////////////////////
