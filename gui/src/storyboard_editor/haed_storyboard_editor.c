/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

#define STORYBOARD_SCALER 3

void haed_storyboard_change_level(GtkWidget *widget, gpointer data)
{
	EksParent *selectedLevel=data;

	b_tm("Selected level %p",selectedLevel);

	haed_programinfo_set_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program,selectedLevel);
	
	//we dont need to reset the storyboard
	haed_object_list_visual_reset();
	haed_level_editor_update();
	
	haed_triggers_visual_update();
	
	haed_set_level_editor_mode(NULL,NULL);
}

void haed_storyboard_init_visual(void)
{
	GLOBAL_MAIN_STORYBOARD=gtk_grid_new();
				
		//most of these definitions are temporary and should be generated.
		GLOBAL_MAIN_STORYBOARD_INFO[0]=gtk_label_new("No.");
		gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),GLOBAL_MAIN_STORYBOARD_INFO[0],1,1,1,1);

		GLOBAL_MAIN_STORYBOARD_INFO[1]=gtk_label_new("Thumbnail");
		gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),GLOBAL_MAIN_STORYBOARD_INFO[1],2,1,1,1);

		GLOBAL_MAIN_STORYBOARD_INFO[2]=gtk_label_new("Comments");
		gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),GLOBAL_MAIN_STORYBOARD_INFO[2],3,1,4,1);
		
		GtkWidget *newLevelButton=gtk_button_new_with_label("⚫ New level");
			g_signal_connect(newLevelButton, "clicked",G_CALLBACK (haed_storyboard_add_button), 0);
		gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),newLevelButton,1,5,1,1);
	
	gtk_box_pack_start(GTK_BOX(GLOBAL_HAED_STORY_BOARD),GLOBAL_MAIN_STORYBOARD,0,0,0);
}

static int haed_storyboard_get_position(EksParent *game,EksParent *inlevel)
{
	int pos=0;

	EksParent *levels=eks_parent_get_child_from_name(game,"levels");

	eks_parent_foreach_child_start(levels,level)
	{
		if(eks_parent_compare_type(level,EKS_PARENT_TYPE_VALUE))
		{
			if(inlevel==level)
				return pos;
			
			pos++;
		}
	}eks_parent_foreach_child_end(levels,level);

	return -1;
}

void haed_storyboard_add_visual(EksParent *level)
{
	size_t pos=haed_storyboard_get_position(GLOBAL_HAED_INFORMATION.firstProgram->program,level);

	gtk_grid_insert_row(GTK_GRID(GLOBAL_MAIN_STORYBOARD),5+STORYBOARD_SCALER*pos);
	gtk_grid_insert_row(GTK_GRID(GLOBAL_MAIN_STORYBOARD),5+STORYBOARD_SCALER*pos);
	gtk_grid_insert_row(GTK_GRID(GLOBAL_MAIN_STORYBOARD),5+STORYBOARD_SCALER*pos);
	
	GtkWidget *goToLevel=gtk_button_new_with_label(b_int_to_string(pos+1));
	g_signal_connect (goToLevel, "clicked",G_CALLBACK (haed_storyboard_change_level), level);
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),goToLevel,1,2+STORYBOARD_SCALER*pos,1,3);
	
/*	gtk_image_new_from_pixbuf(gdk_pixbuf_get_from_window(gtk_widget_get_window(GLOBAL_LEVEL_EDITOR_DRAWING_AREA),0,0,gtk_widget_get_allocated_width (GLOBAL_LEVEL_EDITOR_DRAWING_AREA),gtk_widget_get_allocated_height(GLOBAL_LEVEL_EDITOR_DRAWING_AREA)));*/
	
/*	gtk_widget_show(GLOBAL_LEVEL_EDITOR_DRAWING_AREA);*/
	
	GtkWidget *imageOfTheLevel=gtk_drawing_area_new();//gtk_image_new_from_pixbuf(gdk_pixbuf_get_from_window(gtk_widget_get_window(GLOBAL_LEVEL_EDITOR_DRAWING_AREA),0,0,gtk_widget_get_allocated_width (GLOBAL_LEVEL_EDITOR_DRAWING_AREA),gtk_widget_get_allocated_height(GLOBAL_LEVEL_EDITOR_DRAWING_AREA)));//gtk_image_new_from_file("./images/storyboard/prev_temp.png");
		gtk_widget_set_size_request(imageOfTheLevel,100,-1);
		g_signal_connect (imageOfTheLevel, "draw",G_CALLBACK(haed_level_editor_signal_draw), level);
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),imageOfTheLevel,2,2+STORYBOARD_SCALER*pos,1,3);
	
	GtkWidget *levelLabel=gtk_label_new("Title:");
		gtk_label_set_justify(GTK_LABEL(levelLabel),GTK_JUSTIFY_RIGHT);
		gtk_widget_set_halign(levelLabel, GTK_ALIGN_END);
		gtk_widget_set_margin_end(levelLabel, 6);
		gtk_widget_set_margin_start(levelLabel, 6);
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),levelLabel,3,2+STORYBOARD_SCALER*pos,2,1);
	
	GtkWidget *levelname=gtk_entry_new();
		gtk_entry_set_text(GTK_ENTRY(levelname),level->name);
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),levelname,5,2+STORYBOARD_SCALER*pos,2,1);
	
	GtkWidget *titleSave=gtk_button_new_with_label("Save");
		g_object_set_data(G_OBJECT(titleSave),"title_entry",(void*)levelname);
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),titleSave,7,2+STORYBOARD_SCALER*pos,1,1);
	
	GtkWidget *levelPassword=gtk_label_new("Password:");
		gtk_label_set_justify(GTK_LABEL(levelPassword),GTK_JUSTIFY_RIGHT);
		gtk_widget_set_halign(levelPassword, GTK_ALIGN_END);
		gtk_widget_set_margin_end(levelPassword, 6);
		gtk_widget_set_margin_start(levelPassword, 6);
		gtk_widget_set_tooltip_text(levelPassword,"Set the password for this level!");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),levelPassword,3,3+STORYBOARD_SCALER*pos,2,1);
	
	GtkWidget *passwordentry=gtk_entry_new();
		gtk_entry_set_text(GTK_ENTRY(passwordentry),"");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),passwordentry,5,3+STORYBOARD_SCALER*pos,2,1);
	
	GtkWidget *passwordSave=gtk_button_new_with_label("Save");
		g_object_set_data(G_OBJECT(titleSave),"password_entry",(void*)passwordentry);
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),passwordSave,7,3+STORYBOARD_SCALER*pos,1,1);
	
	GtkWidget *levelType=gtk_image_new_from_file("./images/storyboard/multimedia_level.png");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),levelType,3,4+STORYBOARD_SCALER*pos,1,1);
	
	GtkWidget *fadeIn=gtk_image_new_from_file("./images/storyboard/fade_in.png");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),fadeIn,4,4+STORYBOARD_SCALER*pos,1,1);
	
	GtkWidget *fadeOut=gtk_image_new_from_file("./images/storyboard/fade_out.png");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),fadeOut,5,4+STORYBOARD_SCALER*pos,1,1);
	
	GtkWidget *levelSize=gtk_image_new_from_file("./images/storyboard/playfield_size.png");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),levelSize,6,4+STORYBOARD_SCALER*pos,1,1);
	
	GtkWidget *sizeText=gtk_label_new("600 by 400");
	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),sizeText,7,4+STORYBOARD_SCALER*pos,1,1);
	
	gtk_widget_show_all(GLOBAL_MAIN_STORYBOARD);
}

/**
	Needs to be run after the eksparent is read
*/
void haed_storyboard_visual_reset(EksParent *gameParent)
{
	if(GLOBAL_MAIN_STORYBOARD)
		gtk_widget_destroy(GLOBAL_MAIN_STORYBOARD);
	
	haed_storyboard_init_visual();
	
	EksParent *levels=eks_parent_get_child_from_name(gameParent,"levels");
	
	eks_parent_foreach_child_start(levels,level)
	{
		if(eks_parent_compare_type(level,EKS_PARENT_TYPE_VALUE))
			haed_storyboard_add_visual(level);
	}eks_parent_foreach_child_end(levels,level);
}

void haed_storyboard_add_button(GtkWidget *widget, gpointer data)
{
	EksParent *lastlevel=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"levels");
	
	char *levelName=g_strdup_printf("New_level_%d",eks_parent_get_amount_from_type(lastlevel,EKS_PARENT_TYPE_VALUE)+1);

	EksParent *level=haed_programinfo_add_level(GLOBAL_HAED_INFORMATION.firstProgram->program,levelName);
	
	//b_tm("%s",eks_parent_dump_text(GLOBAL_HAED_INFORMATION.firstProgram->program));
	
	//haed_programinfo_set_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program,level);

	
	b_tm("ADDING NEW LEVEL!");
	
	haed_storyboard_add_visual(level);
	
	free(levelName);
}
