/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define MAIN_FILE
#include "includes.h"
#include <getopt.h>

/**
	THE MAIN FUNCTION!
	
	@param argc .
		Number of input arguments
	@param argv NO_FREE
		A char vector with the input args
	@return
		0=on success
		other=fail
*/
int main(int argc, char *argv[])
{	
	int c;
	int digit_optind = 0;

	char *tmp_pgd=NULL;
	char *tmp_user_path=NULL;
	char *tmp_build_policy=NULL;
	
	g_autofree char *build_policy_str=NULL;
	int build_policy=0;
	
	g_autofree char *tmp_config=NULL;

	while (1)
	{
		int this_option_optind = optind ? optind : 1;
		int option_index = 0;
		static struct option long_options[] = {
			{"pgd-path", required_argument, 0,  'p' },
			{"user-path", required_argument, 0,  'u' },
			{"build-policy", required_argument, 0,  'b' },
			{"config",   required_argument, 0,  'c' },
			{"help",     no_argument,       0,  'h' },
			{0,          0,                 0,  0 }
		};

		c = getopt_long(argc, argv, "p:h", long_options, &option_index);
		if (c == -1)
			break;

		switch (c)
		{
		case 0:
			printf("option %s", long_options[option_index].name);
			if (optarg)
				printf(" with arg %s", optarg);
			printf("\n");
			break;
			
		case 'h':
			printf("Haed GUI\n");
			printf("Usage:\n");
			printf("%-3s %-20s %s\n","-h","--help","Print this information");
			printf("%-3s %-20s %s\n","-p","--pgd-path","Path to pgd, default \"./../pgd\"");
			printf("%-3s %-20s %s\n","-u","--user-path","Path to user-path, default \"~/.haed\"");
			printf("%-3s %-20s %s\n","-b","--build-policy","Build policy to use. Pick between: PROJECT_NAME, USER_PATH, PGD_PATH, default \"PROJECT_NAME\"");
			printf("%-3s %-20s %s\n","-c","--config","Configuration file to use, default \"./cfg/haed.eks\"\n");
			return 0;
			
		case 'p':
			tmp_pgd=strdup(optarg);
			break;
			
		case 'u':
			tmp_user_path=strdup(optarg);
			break;
			
		case 'b':
			tmp_build_policy=strdup(optarg);
			break;
			
		case 'c':
			tmp_config=strdup(optarg);
			break;

		case '?':
			break;

		default:
			printf("?? getopt returned character code 0%o ??\n", c);
		}
	}

	if (optind < argc)
	{
		printf("non-option ARGV-elements: ");
		while (optind < argc)
		{
			printf("%s ", argv[optind++]);
		}
		printf("\n");
	}
	
	/////////
	
	if(tmp_config)
	{
		GLOBAL_HAED_CONFIGURATION=haed_config_new(tmp_config);
	}
	else
	{
		GLOBAL_HAED_CONFIGURATION=haed_config_new("./cfg/haed.eks");
	}
	
	EksParent *conf_general_parent=eks_parent_get_child_from_name(GLOBAL_HAED_CONFIGURATION->data,"general");
	g_autofree char *conf_pgd=eks_parent_get_string(eks_parent_get_child_from_type(eks_parent_get_child_from_name(conf_general_parent,"pgd_dir"),0,EKS_PARENT_TYPE_VALUE));
	g_autofree char *conf_user_path=eks_parent_get_string(eks_parent_get_child_from_type(eks_parent_get_child_from_name(conf_general_parent,"haed_user_dir"),0,EKS_PARENT_TYPE_VALUE));
	g_autofree char *conf_build_policy=eks_parent_get_string(eks_parent_get_child_from_type(eks_parent_get_child_from_name(conf_general_parent,"haed_build_policy"),0,EKS_PARENT_TYPE_VALUE));
	
	if(tmp_pgd)
	{
		GLOBAL_HAED_PGD_PATH=tmp_pgd;
	}
	else if(conf_pgd)
	{
		GLOBAL_HAED_PGD_PATH=g_steal_pointer(&conf_pgd);
	}
	else
	{
		GLOBAL_HAED_PGD_PATH=strdup("./../pgd");
	}
	
	if(tmp_user_path)
	{
		GLOBAL_HAED_USER_PATH=tmp_user_path;
	}
	else if(conf_user_path)
	{
		GLOBAL_HAED_USER_PATH=g_steal_pointer(&conf_user_path);
	}
	else
	{
		GLOBAL_HAED_USER_PATH=strdup("~/.haed");
	}
	
	if(tmp_build_policy)
	{
		build_policy_str=tmp_build_policy;
	}
	else if(conf_build_policy)
	{
		build_policy_str=g_steal_pointer(&conf_build_policy);
	}
	else
	{
		build_policy_str=strdup("PROJECT_NAME");
	}
	
	if(g_strcmp0(build_policy_str,"USER_PATH")==0)
	{
		GLOBAL_HAED_BUILD_POLICY=GLOBAL_HAED_BUILD_POLICY_USER_PATH;
	}
	else if(g_strcmp0(build_policy_str,"PGD_PATH")==0)
	{
		GLOBAL_HAED_BUILD_POLICY=GLOBAL_HAED_BUILD_POLICY_PGD_PATH;
	}
	//if(g_strcmp0(build_policy_str,"PROJECT_NAME")==0)
	else
	{
		GLOBAL_HAED_BUILD_POLICY=GLOBAL_HAED_BUILD_POLICY_PROJECT_NAME;
	}
	
	//init the global information
	haed_globalinfo_add_program(&GLOBAL_HAED_INFORMATION,"untitled");
	
	//icons for defining which type of oject you are trying to place
	GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS_ICONS_IMAGE=(GtkWidget**)malloc(sizeof(GtkWidget*)*(1+1));
	
	/////////////////////////////////
	
	gtk_init(&argc, &argv);

	GLOBAL_MAIN_WINDOW = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(GLOBAL_MAIN_WINDOW), 600, 400);
	//gtk_window_set_title(GTK_WINDOW(GLOBAL_MAIN_WINDOW), "Haed");
	gtk_window_set_icon(GTK_WINDOW(GLOBAL_MAIN_WINDOW), gdk_pixbuf_new_from_file("./images/icon.ico",NULL));
	g_signal_connect(GLOBAL_MAIN_WINDOW, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	
	GtkWidget *titlebar=gtk_header_bar_new();
	gtk_header_bar_set_title(GTK_HEADER_BAR(titlebar),"Haed");
	gtk_header_bar_set_subtitle(GTK_HEADER_BAR(titlebar),"New file");
	gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(titlebar),TRUE);
	gtk_window_set_titlebar(GTK_WINDOW(GLOBAL_MAIN_WINDOW),titlebar);
	
	//DEFINE ALL ELEMENTS
	GLOBAL_MAIN_BOX = gtk_box_new(GTK_ORIENTATION_VERTICAL,4);
	
		//Define and place the menu
		haed_top_menu_init();
		
		//Define and place the menubuttons (toolbar)
		haed_titlebar_init();
		
		//THE NOTEBOOK FOR SWITCHING AROUND BETWEEN LEVEL_EDITOR STORYBOARD TRIGGER_EDITOR
		GLOBAL_MAIN_NOTEBOOK=gtk_notebook_new();
			GLOBAL_HAED_STORY_BOARD=gtk_box_new(GTK_ORIENTATION_VERTICAL,1);
			//STORYBOARD
			
			GtkWidget *STORYBOARD_NB_LABEL_BOX=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,4);

				GdkPixbuf *STORYBOARD_PIXBUF=gdk_pixbuf_new_from_file_at_scale("./images/main/storyboard.png",16,16,1,NULL);
				gtk_box_pack_start(GTK_BOX(STORYBOARD_NB_LABEL_BOX),gtk_image_new_from_pixbuf(STORYBOARD_PIXBUF),0,1,0);
				gtk_box_pack_start(GTK_BOX(STORYBOARD_NB_LABEL_BOX),gtk_label_new("StoryBoard"),0,1,0);
				
				GtkWidget *separator=gtk_separator_new(GTK_ORIENTATION_VERTICAL);
				gtk_box_pack_start(GTK_BOX(STORYBOARD_NB_LABEL_BOX),separator,0,0,0);
				
				GtkWidget *button=gtk_button_new();
					gtk_button_set_image(GTK_BUTTON(button),gtk_image_new_from_file("./images/main/sound_manager2.png"));
					g_signal_connect (button, "clicked",G_CALLBACK (haed_dialog_sound_manager), 0);
				gtk_box_pack_start(GTK_BOX(STORYBOARD_NB_LABEL_BOX),button,0,0,0);
				
				gtk_widget_show_all(STORYBOARD_NB_LABEL_BOX);
			
			gtk_notebook_insert_page(GTK_NOTEBOOK(GLOBAL_MAIN_NOTEBOOK),GLOBAL_HAED_STORY_BOARD,STORYBOARD_NB_LABEL_BOX,0);
			
			//LEVEL EDITOR
			//head
			GLOBAL_MAIN_LAYOUT_AX=gtk_adjustment_new(0,0,600,2,2,1);
			GLOBAL_MAIN_LAYOUT_AY=gtk_adjustment_new(0,0,400,2,2,1);
			
			//main
			GLOBAL_MAIN_LEVEL_EDITOR=gtk_box_new(GTK_ORIENTATION_VERTICAL,2);
				GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,2);
				gtk_widget_set_vexpand(GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX,1);
					
					GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[2]=gtk_scrollbar_new(GTK_ORIENTATION_VERTICAL,NULL);
					gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX),GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[2],0,1,0);
					
					GLOBAL_HAED_LIST_OBJECTS=gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
					gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX),GLOBAL_HAED_LIST_OBJECTS,0,1,0);
					
					//LAYOUT
					GLOBAL_LEVEL_EDITOR_DRAWING_AREA = gtk_drawing_area_new ();
					gtk_widget_set_hexpand(GLOBAL_LEVEL_EDITOR_DRAWING_AREA,TRUE);
					gtk_widget_set_can_focus(GLOBAL_LEVEL_EDITOR_DRAWING_AREA,TRUE);
					gtk_widget_add_events (GLOBAL_LEVEL_EDITOR_DRAWING_AREA, GDK_ALL_EVENTS_MASK);
					g_signal_connect (GLOBAL_LEVEL_EDITOR_DRAWING_AREA, "draw",G_CALLBACK(haed_level_editor_signal_draw), NULL);
					g_signal_connect (GLOBAL_LEVEL_EDITOR_DRAWING_AREA, "event", G_CALLBACK (haed_level_editor_signal_event), 0);
					gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX),GLOBAL_LEVEL_EDITOR_DRAWING_AREA,0,1,0);
						
					GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[0]=gtk_scrollbar_new(GTK_ORIENTATION_VERTICAL,GLOBAL_MAIN_LAYOUT_AY);
					gtk_range_set_range(GTK_RANGE(GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[0]),0,100);
					g_signal_connect (GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[0], "value-changed", G_CALLBACK (haed_update_event_editor_placer), 0);
					gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX),GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[0],0,1,0);
					
				gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR),GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX,0,1,0);
				
				GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[1]=gtk_scrollbar_new(GTK_ORIENTATION_HORIZONTAL,GLOBAL_MAIN_LAYOUT_AX);
				gtk_range_set_range(GTK_RANGE(GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[1]),0,100);
				g_signal_connect (GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[1], "value-changed", G_CALLBACK (haed_update_event_editor_placer), 0);
				gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_LEVEL_EDITOR),GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[1],0,1,0);
				
				GtkWidget *LEVEL_EDITOR_NB_LABEL_BOX=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,4);

					GdkPixbuf *LEVEL_EDITOR_PIXBUF=gdk_pixbuf_new_from_file_at_scale("./images/main/level_editor.png",16,16,1,NULL);
					gtk_box_pack_start(GTK_BOX(LEVEL_EDITOR_NB_LABEL_BOX),gtk_image_new_from_pixbuf(LEVEL_EDITOR_PIXBUF),0,1,0);
					gtk_box_pack_start(GTK_BOX(LEVEL_EDITOR_NB_LABEL_BOX),gtk_label_new("Level Editor"),0,1,0);
					
					GtkWidget *testtt=gtk_separator_new(GTK_ORIENTATION_VERTICAL);
					gtk_box_pack_start(GTK_BOX(LEVEL_EDITOR_NB_LABEL_BOX),testtt,0,0,0);
					
					testtt=gtk_button_new();
						gtk_button_set_image(GTK_BUTTON(testtt),gtk_image_new_from_file("./images/main/new_object.png"));
						g_signal_connect (testtt, "clicked",G_CALLBACK (haed_dialog_level_editor_create), 0);
					gtk_box_pack_start(GTK_BOX(LEVEL_EDITOR_NB_LABEL_BOX),testtt,0,0,0);
					
					testtt=gtk_button_new();
						gtk_button_set_image(GTK_BUTTON(testtt),gtk_image_new_from_file("./images/main/pref.png"));
						g_signal_connect (testtt, "clicked",G_CALLBACK (haed_dialog_level_settings), 0);
					gtk_box_pack_start(GTK_BOX(LEVEL_EDITOR_NB_LABEL_BOX),testtt,0,0,1);
					
					gtk_widget_show_all(LEVEL_EDITOR_NB_LABEL_BOX);
			
			gtk_notebook_insert_page(GTK_NOTEBOOK(GLOBAL_MAIN_NOTEBOOK),GLOBAL_MAIN_LEVEL_EDITOR,LEVEL_EDITOR_NB_LABEL_BOX,1);
			
			//EVENT EDITOR
			//misc general definitions for event editor
			GLOBAL_HAED_TRIGGER_BOX=gtk_box_new(GTK_ORIENTATION_VERTICAL,1);

				GtkWidget *EVENT_EDITOR_NB_LABEL_BOX=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,4);

					GdkPixbuf *EVENT_EDITOR_PIXBUF=gdk_pixbuf_new_from_file_at_scale("./images/main/event_editor.png",16,16,1,NULL);
					gtk_box_pack_start(GTK_BOX(EVENT_EDITOR_NB_LABEL_BOX),gtk_image_new_from_pixbuf(EVENT_EDITOR_PIXBUF),0,1,0);
					gtk_box_pack_start(GTK_BOX(EVENT_EDITOR_NB_LABEL_BOX),gtk_label_new("Event Editor"),0,1,0);
					gtk_widget_show_all(EVENT_EDITOR_NB_LABEL_BOX);
				
			gtk_notebook_insert_page(GTK_NOTEBOOK(GLOBAL_MAIN_NOTEBOOK),GLOBAL_HAED_TRIGGER_BOX,EVENT_EDITOR_NB_LABEL_BOX,2);
			
		gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_BOX),GLOBAL_MAIN_NOTEBOOK,0,1,0);
		
	gtk_container_add(GTK_CONTAINER(GLOBAL_MAIN_WINDOW),GLOBAL_MAIN_BOX);

	haed_storyboard_visual_reset(GLOBAL_HAED_INFORMATION.firstProgram->program);
	haed_triggers_visual_update();
	haed_object_list_visual_reset();
	
	gtk_widget_show_all(GLOBAL_MAIN_WINDOW);
	
	//MAIN LOOP
	gtk_main();
	
	return 0;
}
