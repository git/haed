/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

EksParent *haed_pixbufholder_new(void)
{
	EksParent *holder=eks_parent_new("toplevel",EKS_PARENT_TYPE_VALUE,NULL,NULL);
	return holder;
}

size_t haed_pixbufholder_count_amount(EksParent *holder)
{
	size_t returnamount=0;
	
	eks_parent_foreach_child_start(holder,loopUnit)
	{
		char *holderstr=eks_parent_get_string(loopUnit);
		
		if(eks_parent_compare_type(loopUnit,EKS_PARENT_TYPE_VALUE))
		{
			for(int i=0;i<GLOBAL_ANIMATION_VECTOR_LEN;i++)
			{
				if(strcmp(holderstr,GLOBAL_ANIMATION_VECTOR[i])==0)
				{
					returnamount++;
				}
			}
		}
		
		free(holderstr);

	}eks_parent_foreach_child_end(holder,loopUnit);
	
	return returnamount;
}

/**
	Get a pixbuf from a pixbufholder from its name

	@param holder
		the pixbufholder
	@param name
		name to get
	
	@return 
		the pointer to the pixbufholder
*/
GdkPixbuf *haed_pixbufholder_get_pixbuf_from_name(EksParent *holder,char *name)
{
	EksParent *thisParent=eks_parent_get_child_from_name(holder,name);
	
	if(thisParent)
	{
		GdkPixbuf *outbuf=eks_parent_custom_get(thisParent,0);
	
		b_tm("customBuf: %s %p %p",name,thisParent,outbuf);
		
		return outbuf;
	
	}
	else
	{
		b_error_message("Could not get animation!");
		
		return NULL;
	}
}

/**
	Add an pixbuf to the holder
	@param todo fix so the  holder support several directions

	@param holder NO_FREE
		the pixbufholder to add it into
	@param animationname NO_FREE
		the name of the animation
	@param inbuf NO_FREE
		the input pixbuf

	@return .
		void
*/

EksParent *haed_pixbufholder_add_animation(EksParent *holder,const char *animationname,GdkPixbuf *inbuf)
{
	b_tm("ADDING %s ANIMATION:\n",animationname);
	//add stopped
	EksParent *parentanimation=eks_parent_add_child(holder,animationname,EKS_PARENT_TYPE_VALUE,NULL);

	b_tm("ADDING %s ANIMATION: DONE!\n",animationname);

	GdkPixbuf *customBuf=gdk_pixbuf_copy(inbuf);

	eks_parent_custom_set(parentanimation,HAED_OBJECT_BUFFHOLDER,customBuf);
	
	b_tm("customBuf: %s %p %p",animationname,parentanimation,customBuf);
	
	return parentanimation;
}

void haed_pixbufholder_set_animation_from_pixbuf(EksParent *holder,char *animationname,GdkPixbuf *inbuf)
{
	GdkPixbuf *customBuf;	
	
	EksParent *thisParent=eks_parent_get_child_from_name(holder,animationname);
	
	if(thisParent->custom)
	{
		customBuf=eks_parent_custom_get(thisParent,0);
	
		g_object_unref(customBuf);
	}
	
	customBuf=gdk_pixbuf_copy(inbuf);
	
	eks_parent_custom_set(thisParent,HAED_OBJECT_BUFFHOLDER,customBuf);
}

void haed_pixbufholder_fix_object(EksParent *holder, const char *holderObjName, EksParent *programParent)
{
	EksParent *objectsParent=eks_parent_get_child_from_name(programParent,"objects");
	EksParent *activeParent=eks_parent_get_child_from_name(objectsParent,"active");
	
	b_tm("GOTTA FIX IT! %s",holderObjName);
	
	eks_parent_foreach_child_start(activeParent,loopUnit)
	{
		if(eks_parent_compare_type(loopUnit,EKS_PARENT_TYPE_VALUE))
		{
			if(strcmp(loopUnit->name,holderObjName)==0)
			{
				eks_parent_custom_set(loopUnit,HAED_OBJECT_BUFFHOLDER,holder);
				haed_object_set_imagebuff_from_bufholder(loopUnit);
			}
		}
	}eks_parent_foreach_child_end(activeParent,loopUnit);
}

