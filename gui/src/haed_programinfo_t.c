/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//add functions such as load, save, new game, game in tab? 
#include "includes.h"

/**
	Init programinfo
	
	@param programname
		name of your program
*/
EksParent *haed_programinfo_init(const char *programname)
{
	//lägg till screen-size och level-size
	
	EksParent *programParent=eks_parent_new(programname,EKS_PARENT_TYPE_VALUE,NULL,NULL);
	
	EksParent *screen=eks_parent_add_child(programParent,"screen",EKS_PARENT_TYPE_VALUE,NULL);
	EksParent *size=eks_parent_add_child(screen,"size",EKS_PARENT_TYPE_VALUE,NULL);
	eks_parent_add_child(size,GLOBAL_SCREEN_WIDTH,EKS_PARENT_TYPE_VALUE,NULL);
	eks_parent_add_child(size,GLOBAL_SCREEN_HEIGHT,EKS_PARENT_TYPE_VALUE,NULL);
	
	EksParent *objects=eks_parent_add_child(programParent,"objects",EKS_PARENT_TYPE_VALUE,NULL);
	
	eks_parent_add_child(objects,"active",EKS_PARENT_TYPE_VALUE,NULL);
	
	haed_programinfo_add_level(programParent,"New_level_1");
	
	return programParent;
}

/**
	Set current level from eksstruct

	@param programParent
		the parent to modify
	@param id
		the new current level
*/
void haed_programinfo_set_current_level(EksParent *programParent,EksParent *id)
{
	//EksParent *levels=eks_parent_get_child_from_name(programParent,"levels");
	eks_parent_custom_set(programParent,HAED_PROGRAM_CURRENT_LEVEL,(void*)id);
}

/**
	Get current level from eksstruct

	@param programParent
		the parent to modify
*/
EksParent *haed_programinfo_get_current_level(EksParent *programParent)
{
	//EksParent *levels=eks_parent_get_child_from_name(programParent,"levels");
	return eks_parent_custom_get(programParent,HAED_PROGRAM_CURRENT_LEVEL);
}

/**
	Adds a level to the program structure
	
	@param thisProgram
		the input program to add a level into
	@param levelname
		the name of the added level
*/

EksParent *haed_programinfo_add_level(EksParent *thisProgram,const char *levelname)
{
	EksParent *levels=eks_parent_get_child_from_name(thisProgram,"levels");
	
	EksParent *newLevels=levels;
	
	if(!levels)
	{
		newLevels=eks_parent_add_child(thisProgram,"levels",EKS_PARENT_TYPE_VALUE,NULL);
	}
	
	EksParent *thislevel=eks_parent_add_child(newLevels,levelname,EKS_PARENT_TYPE_VALUE,NULL);
	
	//set current level (only for the first level)
	if(!levels)
		haed_programinfo_set_current_level(thisProgram,thislevel);
	
	eks_parent_add_child(thislevel,"triggers",EKS_PARENT_TYPE_VALUE,NULL);
	eks_parent_add_child(thislevel,"positions",EKS_PARENT_TYPE_VALUE,NULL);
	
	return thislevel;
}
/**
	Counts the amount of levels in a program
	
	@param thisProgram
		the program to count the level amount from
	@return
		the amount
*/

size_t haed_programinfo_count_levels(EksParent *thisProgram)
{
	EksParent *levels=eks_parent_get_child_from_name(thisProgram,"levels");

	return eks_parent_get_amount_from_type(levels,EKS_PARENT_TYPE_VALUE);
}

/**
	Change the current level from a position
	
	@param thisProgram
		the program in which we can find the level from
	@param pos
		the position of the level
*/
void haed_programinfo_select_level_from_pos(HaedProgramInfo *thisProgram,size_t pos)
{
	EksParent *levels=eks_parent_get_child_from_name(thisProgram->program,"levels");

	haed_programinfo_set_current_level(thisProgram->program,eks_parent_get_child(levels,pos));
}
