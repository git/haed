/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

static GtkWidget *HaedSoundManagerGrid=NULL;

/*haed_dialog_sound_manager_add_sound(GtkWidget *widget,gpointer data)*/
/*{*/
/*	*/
/*}*/


/**
	Updates the sound manager window
	
	@param dialogBox
		the dialogbox for the sound manager (eg the window)
*/
static void haed_dialog_sound_manager_update_display(GtkWidget *dialogBox)
{
	GtkWidget *widget;
	
	if(HaedSoundManagerGrid!=NULL)
		g_object_unref(HaedSoundManagerGrid);
	
	HaedSoundManagerGrid=gtk_grid_new();
	
	//containments
	widget=gtk_label_new("Sound manager");
	gtk_grid_attach(GTK_GRID(HaedSoundManagerGrid),widget,1,1,1,1);
	
	widget=gtk_button_new_with_label("Add sound");
	gtk_grid_attach(GTK_GRID(HaedSoundManagerGrid),widget,1,2,1,1);
	
	gtk_box_pack_start(GTK_BOX(dialogBox),HaedSoundManagerGrid,0,0,0);

	gtk_widget_show_all(HaedSoundManagerGrid);
}

/**
	Select object to create
	
	@param widget 
		the parent button on which the user clicked on to get here
	@param data
		NULL
*/
void haed_dialog_sound_manager(GtkWidget *widget,gpointer data)
{
	GtkWidget *newobjectdialog;
	
	GtkWidget *dialogBox;
	GtkWidget *button;
	int dialogresponce;
	
	//define the window
	newobjectdialog=gtk_dialog_new();
	g_signal_connect (newobjectdialog, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),_("_Cancel"),GTK_RESPONSE_NO);
	
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(newobjectdialog));
	
	haed_dialog_sound_manager_update_display(dialogBox);
	
	//show the dialog
	gtk_widget_show_all(newobjectdialog);
	
	dialogresponce=gtk_dialog_run(GTK_DIALOG(newobjectdialog));
	gtk_widget_destroy(newobjectdialog);
	
	if(dialogresponce==1)
	{
		b_tm("Not made yet!");
	}
	else
	{
		b_tm("Not made yet!");
	}
}
