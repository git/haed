/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//==========================Get Number=======================

char *haed_pick_level_dialog(void)
{
	GtkWidget *pickLevelWindow;
	GtkWidget *pickLevelGrid;
	
	GtkWidget *pickLevelInfo;
	GtkWidget *levelDropDown;
	
	
	pickLevelWindow = gtk_dialog_new();
	g_signal_connect (GTK_WIDGET (pickLevelWindow), "destroy",G_CALLBACK (gtk_widget_destroy), NULL);
	
	gtk_dialog_add_button(GTK_DIALOG(pickLevelWindow),_("Ok"),GTK_RESPONSE_YES);
	gtk_dialog_add_button(GTK_DIALOG(pickLevelWindow),_("Cancel"),GTK_RESPONSE_NO);
		pickLevelGrid=gtk_dialog_get_content_area(GTK_DIALOG(pickLevelWindow));
			
			//SET SPEED
			pickLevelInfo=gtk_label_new("Select a level from the list!");
			gtk_box_pack_start(GTK_BOX(pickLevelGrid),pickLevelInfo,0,0,0);
	
			levelDropDown=gtk_combo_box_text_new();
				
				EksParent *levelsparent=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"levels");
				
				eks_parent_foreach_child_start(levelsparent,looplevel)
				{
					char *thestring=eks_parent_get_string(looplevel);
					
					gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(levelDropDown),NULL,thestring);
					
					free(thestring);
				
				}eks_parent_foreach_child_end(levelsparent,looplevel);
		
				gtk_combo_box_set_active(GTK_COMBO_BOX(levelDropDown),0);
			gtk_box_pack_start(GTK_BOX(pickLevelGrid),levelDropDown,0,0,0);

	gtk_widget_show_all(pickLevelWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(pickLevelWindow))==GTK_RESPONSE_YES)
	{
		char *outstr=gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(levelDropDown));
	
		gtk_widget_destroy(pickLevelWindow);
	
		return outstr;
	}
	else
	{
		gtk_widget_destroy(pickLevelWindow);
		b_error_message("Could not get level!\n");
		return NULL;
	}
}
