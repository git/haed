/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

/////////////////////////////////////////////////////////////////////////

static GtkWidget *LEVEL_SETTINGS_WINDOW;

static void haed_draw_set_background_color(GtkWidget *widget,gpointer data)
{
	GdkRGBA color;

	gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(widget),&color);

	EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
	
	haed_set_double_from_pos(curLevel,"background-color",color.red*255,0,0);
	haed_set_double_from_pos(curLevel,"background-color",color.green*255,0,1);
	haed_set_double_from_pos(curLevel,"background-color",color.blue*255,0,2);
}

void haed_dialog_level_settings(GtkWidget *widget,gpointer data)
{
	GtkWidget *thisNotebook;
	
	GtkWidget *mainSettings;
	GtkWidget *otherSettings;
	
	EksParent *curLevel=haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program);
	
	LEVEL_SETTINGS_WINDOW = gtk_dialog_new();
	
	gtk_dialog_add_button(GTK_DIALOG(LEVEL_SETTINGS_WINDOW),_("Ok"),GTK_RESPONSE_YES);
	gtk_dialog_add_button(GTK_DIALOG(LEVEL_SETTINGS_WINDOW),_("Cancel"),GTK_RESPONSE_NO);
	
	g_signal_connect (GTK_WIDGET (LEVEL_SETTINGS_WINDOW), "destroy",G_CALLBACK (gtk_widget_destroy), NULL);
	
	thisNotebook=gtk_notebook_new();
		mainSettings=gtk_box_new(GTK_ORIENTATION_VERTICAL,1);
		
			GtkWidget *infoLabelSetName=gtk_label_new("Set backround color:");
			
			gtk_box_pack_start(GTK_BOX(mainSettings),infoLabelSetName,0,0,0);
			
			GtkWidget *colorbutton = gtk_color_button_new();
				gtk_color_chooser_set_use_alpha (GTK_COLOR_CHOOSER(colorbutton),TRUE);
				g_signal_connect (colorbutton, "color-set",G_CALLBACK (haed_draw_set_background_color), 0);
				
				int bRed,bGreen,bBlue;
			
				if(eks_parent_get_child_from_name(curLevel,"background-color"))
				{
					bRed=haed_get_int_from_pos(curLevel,"background-color",0);
					bGreen=haed_get_int_from_pos(curLevel,"background-color",1);
					bBlue=haed_get_int_from_pos(curLevel,"background-color",2);
				}
				else
				{
					bRed=255;
					bGreen=255;
					bBlue=255;
				}
				
				gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorbutton),&(GdkRGBA){(double)bRed/255, (double)bGreen/255, (double)bBlue/255,1});
			gtk_box_pack_start(GTK_BOX(mainSettings),colorbutton,0,0,0);
			
			
		gtk_notebook_insert_page(GTK_NOTEBOOK(thisNotebook),mainSettings,gtk_label_new("Main settings"),-1);
	
		otherSettings=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);
			
		gtk_notebook_insert_page(GTK_NOTEBOOK(thisNotebook),otherSettings,gtk_label_new("Other settings"),-1);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(LEVEL_SETTINGS_WINDOW))),thisNotebook);
	
	gtk_widget_show_all(LEVEL_SETTINGS_WINDOW);
	
	if(gtk_dialog_run(GTK_DIALOG(LEVEL_SETTINGS_WINDOW))==GTK_RESPONSE_YES)
	{
	
		gtk_widget_destroy(LEVEL_SETTINGS_WINDOW);
		return;
	}
	else
	{
		gtk_widget_destroy(LEVEL_SETTINGS_WINDOW);
		b_error_message("You pressed NO!\n");
		return;
	}
}
