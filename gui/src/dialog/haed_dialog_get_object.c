/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//==========================GET OBJECT FROM SELECTION WINDOW=======================
static EksParent *haed_dialog_get_object_from_selection_Object;
static GtkWidget *haed_dialog_get_object_from_selection_Window;

//when clicked on an object
void haed_get_object_from_selection_object_select(GtkWidget *widget,gpointer data)
{
	if(((EksParent*)data)->name!=NULL)
	{
		gtk_dialog_response(GTK_DIALOG(haed_dialog_get_object_from_selection_Window),1);
		
		haed_dialog_get_object_from_selection_Object=data;
	}
	else
	{
		printf("ERROR (haed_get_object_from_selection_object_select)! Could not retrieve object!\n");
	}
}

//the window
EksParent *haed_dialog_get_object_from_selection(void)
{
	//reset haed_dialog_get_object_from_selection_Object
	haed_dialog_get_object_from_selection_Object=NULL;
	
	//define the window
	haed_dialog_get_object_from_selection_Window=gtk_dialog_new();
	g_signal_connect (GTK_WIDGET (gtk_dialog_get_content_area(GTK_DIALOG(haed_dialog_get_object_from_selection_Window))), "destroy",G_CALLBACK (haed_destroy_window_only), GLOBAL_MAIN_GET_OBJECT_BOX); //will destroy when its box is destroyed
	gtk_dialog_add_button(GTK_DIALOG(haed_dialog_get_object_from_selection_Window),_("Cancel"),GTK_RESPONSE_NO);
	
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(haed_dialog_get_object_from_selection_Window))),GLOBAL_MAIN_GET_OBJECT_BOX);
	
	//show the dialog
	gtk_widget_show_all(haed_dialog_get_object_from_selection_Window);
	
	if(gtk_dialog_run(GTK_DIALOG(haed_dialog_get_object_from_selection_Window))==1)
	{
		gtk_widget_destroy(haed_dialog_get_object_from_selection_Window);
		return haed_dialog_get_object_from_selection_Object;
	}
	else
	{
		printf("ERROR (haed_dialog_get_object_from_selection): Could not get object!\n");
		return NULL;
	}
}
