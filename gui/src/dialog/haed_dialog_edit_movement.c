/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//==========================SELECT MOVEMENT SPEED ALL DIRECTIONS (MAYBE OTHER AS WELL)=======================
static GtkWidget *AllDirectionsWindow;
static EksParent *tempAOGUI_SEND;

static void ModifySpeed(GtkWidget *widget,gpointer data)
{
	float objectGetValue=gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
	
	/*
	0=speed
	1=accel
	2=deaccel
	*/
	switch((long int)data)
	{
		case 0:
		/*
		if(tempAOGUI_SEND->speed==NULL)
		{
			tempAOGUI_SEND->speed=(float*)malloc(2*sizeof(float));
		}
		tempAOGUI_SEND->speed[0]=objectGetValue;
		tempAOGUI_SEND->speed[1]=objectGetValue;
		*/
		
		haed_object_set_double(tempAOGUI_SEND,"speed",objectGetValue,0);
		
		//printf("s %f\n",tempAOGUI->speed[0]);
		
		break;
		case 1:
		/*
		if(tempAOGUI_SEND->acceleration==NULL)
		{
			tempAOGUI_SEND->acceleration=(float*)malloc(2*sizeof(float));
		}
		tempAOGUI_SEND->acceleration[0]=objectGetValue;
		tempAOGUI_SEND->acceleration[1]=objectGetValue;
		*/
		
		haed_object_set_double(tempAOGUI_SEND,"acceleration",objectGetValue,0);
		
		//printf("a %f\n",tempAOGUI->acceleration[0]);
		
		break;
		case 2:
		/*
		if(tempAOGUI_SEND->deacceleration==NULL)
		{
			tempAOGUI_SEND->deacceleration=(float*)malloc(2*sizeof(float));
		}
		tempAOGUI_SEND->deacceleration[0]=objectGetValue;
		tempAOGUI_SEND->deacceleration[1]=objectGetValue;
		*/
		
		haed_object_set_double(tempAOGUI_SEND,"deacceleration",objectGetValue,0);
		
		//printf("d %f\n",tempAOGUI_SEND->deacceleration[0]);
		
		break;
	}
}

void haed_dialog_edit_movement_all_directions(EksParent *tempAOGUI)
{
	tempAOGUI_SEND=tempAOGUI;
	
	GtkWidget *AllDirectionsNotebook;
	GtkWidget *SetDirectionsGird;
	GtkWidget *SetSpeedGird;
	
	GtkWidget *SetSpeedSpinButton;
	GtkWidget *SetSpeedInfo;
	
	GtkWidget *SetAccelSpinButton;
	GtkWidget *SetAccelInfo;
	
	GtkWidget *SetDeaccelSpinButton;
	GtkWidget *SetDeaccelInfo;
	
	//1=active object
	//tempAOGUI->movType=1;
	
	AllDirectionsWindow = gtk_dialog_new();
	
	gtk_dialog_add_button(GTK_DIALOG(AllDirectionsWindow),_("Ok"),GTK_RESPONSE_YES);
	gtk_dialog_add_button(GTK_DIALOG(AllDirectionsWindow),_("Cancel"),GTK_RESPONSE_NO);
	
	//gtk_window_set_title(GTK_WINDOW(AllDirectionsWindow), "Edit Movement (All Directions)");
	//gtk_window_set_default_size(GTK_WINDOW(AllDirectionsWindow), 300, 200);
	g_signal_connect (GTK_WIDGET (AllDirectionsWindow), "destroy",G_CALLBACK (gtk_widget_destroy), NULL);
	
	AllDirectionsNotebook=gtk_notebook_new();
		SetSpeedGird=gtk_box_new(GTK_ORIENTATION_VERTICAL,1);
			
			//SET SPEED
			SetSpeedInfo=gtk_label_new("Speed");
			
			gtk_box_pack_start(GTK_BOX(SetSpeedGird),SetSpeedInfo,0,0,0);
	
			SetSpeedSpinButton = gtk_spin_button_new_with_range(0,100,1);
				g_signal_connect (SetSpeedSpinButton, "value-changed", G_CALLBACK (ModifySpeed), (void*)0);
				
				haed_object_get_double(tempAOGUI,"speed")==0?gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetSpeedSpinButton),50):gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetSpeedSpinButton),haed_object_get_double(tempAOGUI,"speed"));
				
			gtk_box_pack_start(GTK_BOX(SetSpeedGird),SetSpeedSpinButton,0,0,0);
			
			//SET ACCELERATION
			SetAccelInfo=gtk_label_new("Acceleration");
			gtk_box_pack_start(GTK_BOX(SetSpeedGird),SetAccelInfo,0,0,0);
	
			SetAccelSpinButton = gtk_spin_button_new_with_range(0,100,1);
				g_signal_connect (SetAccelSpinButton, "value-changed", G_CALLBACK (ModifySpeed), (void*)1);
				
				
				haed_object_get_double(tempAOGUI,"acceleration")==0?gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetAccelSpinButton),50):gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetAccelSpinButton),haed_object_get_double(tempAOGUI,"acceleration"));
				
			gtk_box_pack_start(GTK_BOX(SetSpeedGird),SetAccelSpinButton,0,0,0);
			
			//SET DEACCELERATION
			SetDeaccelInfo=gtk_label_new("Deacceleration");
			gtk_box_pack_start(GTK_BOX(SetSpeedGird),SetDeaccelInfo,0,0,0);
	
			SetDeaccelSpinButton = gtk_spin_button_new_with_range(0,100,1);
				g_signal_connect (SetDeaccelSpinButton, "value-changed", G_CALLBACK (ModifySpeed), (void*)2);
				
				haed_object_get_double(tempAOGUI,"deacceleration")==0?gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetDeaccelSpinButton),50):gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetDeaccelSpinButton),haed_object_get_double(tempAOGUI,"deacceleration"));
				
			gtk_box_pack_start(GTK_BOX(SetSpeedGird),SetDeaccelSpinButton,0,0,0);

		gtk_notebook_insert_page(GTK_NOTEBOOK(AllDirectionsNotebook),SetSpeedGird,gtk_label_new("Set Speed"),-1);
	
		SetDirectionsGird=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);

		gtk_notebook_insert_page(GTK_NOTEBOOK(AllDirectionsNotebook),SetDirectionsGird,gtk_label_new("Set Directions"),-1);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(AllDirectionsWindow))),AllDirectionsNotebook);
	
	gtk_widget_show_all(AllDirectionsWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(AllDirectionsWindow))==GTK_RESPONSE_YES)
	{
		gtk_widget_destroy(AllDirectionsWindow);
		tempAOGUI_SEND=NULL;
		return;
		//return SelectMovementReturn;
	}
	else
	{
		gtk_widget_destroy(AllDirectionsWindow);
		b_error_message("Could not get object!\n");
		tempAOGUI_SEND=NULL;
		return;
		//return NULL;
	}
}
