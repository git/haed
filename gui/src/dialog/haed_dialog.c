/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//VARIOUS FUNCTIONS
void haed_destroy_window_only(GtkWidget *widget, gpointer data)
{
	gtk_container_remove(GTK_CONTAINER(widget),(GtkWidget*)data);
	gtk_widget_destroy(widget);
}

/**
	changes behaviour of the signal in the dialog to give what object was selected
	
	@param widget 
		the parent button on which the user clicked on to get here
	@param data
		NULL
*/
void gtk_dialog_set_responce_widget_apply(GtkWidget *widget,gpointer data)
{
	void **object;
	//object=g_object_get_data((GObject*)widget,"dialog-widget-signal");
	object=data;
	
	/*
	object&&data?
		gtk_dialog_response(GTK_DIALOG(data),(intptr_t)object):
		b_error_message("Could not set responce to dialog!");
	*/
	if(object)
	{
		if(object[0]&&object[1])
			gtk_dialog_response(GTK_DIALOG(object[0]),(intptr_t)object[1]);
		
		free(object);
	}
	else
		b_error_message("Could not set responce to dialog!");
	
}

void gtk_dialog_set_responce_widget(GtkDialog *dialog,GtkWidget *widget,gpointer data)
{
	void **object=malloc(sizeof(void*)*2);
	object[0]=dialog;
	object[1]=data;
	
	//g_object_set_data((GObject*)widget,"dialog-widget-signal",data);
	//g_signal_connect (widget, "clicked",G_CALLBACK (gtk_dialog_set_responce_widget_apply), dialog); //active object
	g_signal_connect (widget, "clicked",G_CALLBACK (gtk_dialog_set_responce_widget_apply), (void*)object); //active object
}
