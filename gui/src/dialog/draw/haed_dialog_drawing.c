/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "includes.h"

enum
{
	HAED_BRUSH_ROUND,
	HAED_BRUSH_SQUARED
};

static int BRUSH_SIZE=1;
//static int ALPHA_LEVEL=0;
//1=eraser
static int BRUSH_TYPE=0;
static GdkRGBA BRUSH_COLOR={0.0,0.0,0.0,1.0};

static GdkPixbuf *PIXBUF_DRAWBACK=NULL;

//if you are painting
static int BRUSH_DRAW=0;

static GtkWidget *entryXSet;
static GtkWidget *entryYSet;

static int SIZE_OF_DRAW_AREA_X=64;
static int SIZE_OF_DRAW_AREA_Y=64;

static double DRAW_ZOOM_X=1.0;
static double DRAW_ZOOM_Y=1.0;

static GtkWidget *da;

static GtkWidget *alpha_button;

/**
	Drawback from the DA. Shows the pixbuf drawback
	
	@param widget
		the parent button on which the user clicked on to get here (DA)
	@param event
		the event from the DA
	@param data
		NULL
*/
static gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer data)
{
	if(PIXBUF_DRAWBACK)
	{
		cairo_scale(cr, (double)gtk_widget_get_allocated_width(widget)/gdk_pixbuf_get_width(PIXBUF_DRAWBACK), (double)gtk_widget_get_allocated_height(widget)/gdk_pixbuf_get_height(PIXBUF_DRAWBACK));
		
		//get the main pixbuf drawback, wont change anything to it
		gdk_cairo_set_source_pixbuf(cr,PIXBUF_DRAWBACK,0,0);
		
		cairo_set_antialias (cr, CAIRO_ANTIALIAS_NONE);
		cairo_pattern_set_filter(cairo_get_source(cr), CAIRO_FILTER_FAST);
		
		cairo_paint (cr);
		cairo_fill (cr);
	}
	
	return 0;
}

/**
	Callback from the main drawing area from the drawing window
	
	@param widget
		the parent button on which the user clicked on to get here (DA)
	@param event
		the event from the DA
	@param data
		NULL
*/
static void haed_draw_main_function(GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
	b_tm("draw area: \nX:%f\nY:%f\n%d\n%d\n%f\n",event->motion.x,event->motion.y,event->button.button,event->button.type,BRUSH_COLOR.red);
	
	double currdrawX=((double)gdk_pixbuf_get_width(PIXBUF_DRAWBACK)*(event->motion.x-1))/gtk_widget_get_allocated_width(widget);
	double currdrawY=((double)gdk_pixbuf_get_height(PIXBUF_DRAWBACK)*(event->motion.y-1))/gtk_widget_get_allocated_height(widget);
	
	
	if(event->button.type==GDK_BUTTON_PRESS && event->button.button==1)
	{
		BRUSH_DRAW=1;
	}
	else if(event->button.type==GDK_BUTTON_RELEASE)
	{
		BRUSH_DRAW=0;
	}
	
	if(!PIXBUF_DRAWBACK)
	{
		PIXBUF_DRAWBACK=gdk_pixbuf_new(GDK_COLORSPACE_RGB,1,8,SIZE_OF_DRAW_AREA_X,SIZE_OF_DRAW_AREA_Y);
		gdk_pixbuf_fill(PIXBUF_DRAWBACK,0x00000000);
		//gtk_widget_set_size_request (da, gdk_pixbuf_get_width(PIXBUF_DRAWBACK),gdk_pixbuf_get_height(PIXBUF_DRAWBACK));
	}
		
	
	if(BRUSH_DRAW==1 && (event->button.type==GDK_MOTION_NOTIFY || event->button.type==GDK_BUTTON_PRESS))
	{
		//main drawing
		//create new pixbuf if the old does not exist
		
		cairo_t *cr=cairo_create(cairo_image_surface_create_for_data(gdk_pixbuf_get_pixels(PIXBUF_DRAWBACK),CAIRO_FORMAT_ARGB32,gdk_pixbuf_get_width(PIXBUF_DRAWBACK),gdk_pixbuf_get_height(PIXBUF_DRAWBACK),gdk_pixbuf_get_rowstride(PIXBUF_DRAWBACK)));
		
		gdk_cairo_set_source_pixbuf(cr,PIXBUF_DRAWBACK,0,0);
		
		cairo_arc(cr,currdrawX, currdrawY,BRUSH_SIZE,0, 2 * G_PI);
		
		//gtk_style_context_get_color (gtk_widget_get_style_context (widget),0,&BRUSH_COLOR);
		printf("%f %f %f %f %d",BRUSH_COLOR.red,BRUSH_COLOR.green,BRUSH_COLOR.blue,BRUSH_COLOR.alpha,BRUSH_TYPE);
		
		//för att sudda
		if(BRUSH_TYPE&1)
		{
			cairo_set_operator(cr,CAIRO_OPERATOR_SOURCE);
		}
		else
		{
			cairo_set_operator(cr,CAIRO_OPERATOR_OVER);
		}
		gdk_cairo_set_source_rgba (cr, &BRUSH_COLOR);

		//cairo_paint(cr);
		cairo_fill (cr);
		
		cairo_destroy(cr);

		//redraw to the drawing area
		//gtk_widget_queue_draw_area(widget,0,0,gdk_pixbuf_get_width(PIXBUF_DRAWBACK),gdk_pixbuf_get_height(PIXBUF_DRAWBACK));
		gtk_widget_queue_draw_area(widget,0,0,gtk_widget_get_allocated_width(widget),gtk_widget_get_allocated_height(widget));
	}
}

/**
	Set brush size
	
	@param widget
		the parent button on which the user clicked on to get here
	@param data
		NULL
*/
static void haed_draw_set_brush_size(GtkWidget *widget,gpointer data)
{
	BRUSH_SIZE=(int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
}

static void haed_draw_set_alpha_level(GtkWidget *widget,gpointer data)
{
	if(BRUSH_TYPE==0)
	{
		BRUSH_COLOR.alpha=gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
	}
}

static void haed_draw_set_eraser(GtkWidget *widget,gpointer data)
{
	BRUSH_TYPE^=1;
	
/*	gdouble cur_value=gtk_spin_button_get_value(GTK_SPIN_BUTTON(alpha_button));*/
/*	gtk_spin_button_set_value(GTK_SPIN_BUTTON(alpha_button),1.0-cur_value);*/

	if(BRUSH_TYPE==1)
	{
		BRUSH_COLOR.alpha=0.0;
	}
	else
	{
		BRUSH_COLOR.alpha=gtk_spin_button_get_value(GTK_SPIN_BUTTON(alpha_button));
	}
}

/**
	Set brush color
	
	@param widget
		the parent button on which the user clicked on to get here
	@param data
		NULL (currently)
*/
static void haed_draw_set_brush_color(GtkWidget *widget,gpointer data)
{
	gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(widget),&BRUSH_COLOR);
	//for cairo
	gdouble tempred=BRUSH_COLOR.blue;
	BRUSH_COLOR.blue=BRUSH_COLOR.red;
	BRUSH_COLOR.red=tempred;
}

/**
	Set screen size of the drawing area, (resize the image)
	
	@param widget
		the parent button on which the user clicked on to get here
	@param data
		NULL (currently)
*/
static void haed_draw_set_screen_size(GtkWidget *widget,gpointer data)
{
	void *freemem;
	uint8_t increase=0;
	
	int oldsizex;
	int oldsizey;
	
	oldsizex=SIZE_OF_DRAW_AREA_X;
	oldsizey=SIZE_OF_DRAW_AREA_Y;
	
	//x
	if(!(intptr_t)data)
	{
		SIZE_OF_DRAW_AREA_X=(int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(entryXSet));
		
		if(oldsizex<SIZE_OF_DRAW_AREA_X)
			increase=1;
	}
	//y
	else
	{
		SIZE_OF_DRAW_AREA_Y=(int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(entryYSet));
		
		if(oldsizey<SIZE_OF_DRAW_AREA_Y)
			increase=1;
	}
	
	//gtk_widget_set_size_request(da, SIZE_OF_DRAW_AREA_X, SIZE_OF_DRAW_AREA_Y);
	
	freemem=PIXBUF_DRAWBACK;
	
	PIXBUF_DRAWBACK=gdk_pixbuf_new(GDK_COLORSPACE_RGB,1,8,SIZE_OF_DRAW_AREA_X,SIZE_OF_DRAW_AREA_Y);
	gdk_pixbuf_fill(PIXBUF_DRAWBACK,0x00000000);
	
	if(increase)
		gdk_pixbuf_copy_area(freemem,0,0,oldsizex,oldsizey,PIXBUF_DRAWBACK,0,0);
	else
		gdk_pixbuf_copy_area(freemem,0,0,SIZE_OF_DRAW_AREA_X,SIZE_OF_DRAW_AREA_Y,PIXBUF_DRAWBACK,0,0);
	
	g_object_unref(freemem);
	
	b_tm("new_size: %d %d %d:%d:%d",SIZE_OF_DRAW_AREA_X,SIZE_OF_DRAW_AREA_Y,oldsizex,oldsizey,increase);
}

/**
	Main drawing area, you will draw-edit your objects here.
	
	@param widget 
		the parent button on which the user clicked on to get here
	@param data
		frame
*/
int haed_dialog_drawing(GdkPixbuf **editingObject,GtkWidget *optionalDrawbackImage)
{
	GtkWidget *dialogBox;
	GtkWidget *innerDialogBox;
	GtkWidget *button,*label;
	
	if(*editingObject)
	{
		PIXBUF_DRAWBACK=(*editingObject);
		
		SIZE_OF_DRAW_AREA_X=gdk_pixbuf_get_width(PIXBUF_DRAWBACK);
		SIZE_OF_DRAW_AREA_Y=gdk_pixbuf_get_height(PIXBUF_DRAWBACK);
	}
	
	//define the window
	GtkWidget *newobjectdialog=gtk_dialog_new();
	g_signal_connect (newobjectdialog, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),"_Ok",GTK_RESPONSE_OK);
	gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),"_Cancel",GTK_RESPONSE_NO);
	//gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),"_Help",GTK_RESPONSE_HELP);
	
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(newobjectdialog));
	
	//containments
	innerDialogBox=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,2);

	//=======Color setting space=======
	
	button = gtk_color_button_new();
	gtk_color_chooser_set_use_alpha (GTK_COLOR_CHOOSER(button),TRUE);
	g_signal_connect (button, "color-set",G_CALLBACK (haed_draw_set_brush_color), 0);
	haed_draw_set_brush_color(button,0);
	gtk_box_pack_start(GTK_BOX(innerDialogBox),button,0,0,0);
	
	//=======DRAWING SPACE=======
	da = gtk_drawing_area_new ();
	//gtk_widget_set_size_request (da, SIZE_OF_DRAW_AREA_X, SIZE_OF_DRAW_AREA_Y);
	gtk_widget_add_events (da, GDK_ALL_EVENTS_MASK);
	g_signal_connect (G_OBJECT (da), "draw",G_CALLBACK (draw_callback), NULL);
	gtk_widget_set_size_request(da, 500, 500);
	//gtk_window_set_resizable(gtk_widget_get_window(da),0);
	g_signal_connect (da, "event", G_CALLBACK (haed_draw_main_function), 0);
	//gtk_container_add (GTK_CONTAINER (innerDialogBox), da);
	gtk_box_pack_start(GTK_BOX(innerDialogBox),da,0,0,0);

	//=======EDITING SPACE=======

	GtkWidget *editingBox=gtk_grid_new();

		label=gtk_label_new("brush size");
		gtk_grid_attach(GTK_GRID(editingBox),label,1,1,1,1);

		button = gtk_spin_button_new_with_range(1,5000,1);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(button),BRUSH_SIZE);
			g_signal_connect (button, "value-changed",G_CALLBACK (haed_draw_set_brush_size),NULL);
		gtk_grid_attach(GTK_GRID(editingBox),button,2,1,1,1);
		
		label=gtk_label_new("set alpha");
		gtk_grid_attach(GTK_GRID(editingBox),label,1,2,1,1);
	
		alpha_button = gtk_spin_button_new_with_range(0,1,0.01);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(alpha_button),BRUSH_COLOR.alpha);
			g_signal_connect (alpha_button, "value-changed",G_CALLBACK (haed_draw_set_alpha_level),NULL);
		gtk_grid_attach(GTK_GRID(editingBox),alpha_button,2,2,1,1);
	
		button = gtk_toggle_button_new_with_label("ERASE");
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),BRUSH_TYPE&1);
			g_signal_connect (button, "clicked",G_CALLBACK (haed_draw_set_eraser),NULL);
		gtk_grid_attach(GTK_GRID(editingBox),button,2,3,1,1);
	
		entryXSet=gtk_spin_button_new_with_range(1,5000,1);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(entryXSet),SIZE_OF_DRAW_AREA_X);
			g_signal_connect (entryXSet, "value-changed",G_CALLBACK (haed_draw_set_screen_size),(void*)0);
		gtk_grid_attach(GTK_GRID(editingBox),entryXSet,2,4,1,1);
	
		entryYSet=gtk_spin_button_new_with_range(1,5000,1);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(entryYSet),SIZE_OF_DRAW_AREA_Y);
			g_signal_connect (entryYSet, "value-changed",G_CALLBACK (haed_draw_set_screen_size),(void*)1);
		gtk_grid_attach(GTK_GRID(editingBox),entryYSet,2,5,1,1);
		
	gtk_box_pack_start(GTK_BOX(innerDialogBox),editingBox,0,0,0);
	
	gtk_box_pack_start(GTK_BOX(dialogBox),innerDialogBox,0,0,0);
	
	//show the dialog
	gtk_widget_show_all(newobjectdialog);
	
	if(gtk_dialog_run(GTK_DIALOG(newobjectdialog))==GTK_RESPONSE_OK)
	{
		//(*editingObject)=gdk_pixbuf_get_from_window(gtk_widget_get_window(da),0,0,gtk_widget_get_allocated_width (da),gtk_widget_get_allocated_height(da));
		(*editingObject)=PIXBUF_DRAWBACK;
		PIXBUF_DRAWBACK=NULL;
		
		GtkWidget *tempImage=gtk_image_new();
		gtk_image_set_from_pixbuf(GTK_IMAGE(tempImage),gdk_pixbuf_scale_simple((*editingObject),100,100,GDK_INTERP_NEAREST));
		
		if(optionalDrawbackImage)
			gtk_button_set_image(GTK_BUTTON(optionalDrawbackImage),tempImage);
			
		gtk_widget_destroy(newobjectdialog);
		
		return 1;
	}
	else
	{
		b_warning_message("No changes were made!\n");
		
		gtk_widget_destroy(newobjectdialog);
		
		return 0;
	}
}
