/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "includes.h"

/*static GtkWidget *PubllicTestImage;*/
static GdkPixbuf *HAED_DRAW_PIXBUF[8][8];
static int HAED_DRAW_PIXBUF_ANIMATION=0;

static GtkAdjustment *HAED_ADJ_POS=NULL;
static GtkWidget *animationDropDown;
static GtkWidget *frameButtons[8];

void haed_object_drawing_edit_image(GtkWidget *widget,gpointer user_data)
{
	int inputWindow=(int)(intptr_t)user_data;
	
	int a=0;
	while(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][a]!=NULL)
		a++;
	
	//if you pressed a frame way beyod the next.
	if(inputWindow>a)
		inputWindow=a;
		
	b_tm("editing frame: %d\n",inputWindow);
		
	haed_dialog_drawing(&HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][inputWindow],frameButtons[inputWindow]);
}

static GdkPixbuf *copy_paste_pixbuf=NULL;

void haed_object_drawing_image_options_copy(GtkWidget *widget,gpointer user_data)
{
	guint current_image=(guint)(intptr_t)user_data;
	
	printf("COPY:: %u\n",current_image);
	
	if(copy_paste_pixbuf)
	{
		 g_object_unref(copy_paste_pixbuf);
	}
	
	copy_paste_pixbuf=gdk_pixbuf_copy(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][current_image]);
}

void haed_object_drawing_image_options_paste(GtkWidget *widget,gpointer user_data)
{
	if(copy_paste_pixbuf)
	{
		guint current_image=(guint)(intptr_t)user_data;
		
		printf("PASTE:: -> %u\n",current_image);
	
		int a=0;
		while(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][a]!=NULL)
			a++;
		
		//if you pressed a frame way beyod the next.
		if(current_image>a)
		{
			current_image=a;
		}
		
		GtkWidget *tempImage=gtk_image_new();
		gtk_image_set_from_pixbuf(GTK_IMAGE(tempImage),gdk_pixbuf_scale_simple(copy_paste_pixbuf,100,100,GDK_INTERP_NEAREST));
		
		if(frameButtons[current_image])
		{
			if(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][current_image])
			{
				 g_object_unref(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][current_image]);
			}
		
			gtk_button_set_image(GTK_BUTTON(frameButtons[current_image]),tempImage);
			HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][current_image]=gdk_pixbuf_copy(copy_paste_pixbuf);
		}
		
	}
}

gboolean haed_object_drawing_image_options(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
	if (event->type == GDK_BUTTON_PRESS && event->button == 3)
	{
		GtkMenuListInner image_options_arr[]=
		{
			{"Cut",NULL,NULL},
			{"Copy",haed_object_drawing_image_options_copy,user_data},
			{"Paste",haed_object_drawing_image_options_paste,user_data},
			{"Import image",NULL,NULL}
		};
		
		GtkWidget *image_options=gtk_menu_new_from_list(image_options_arr,sizeof(image_options_arr)/sizeof(image_options_arr[0]));
		
/*		gtk_menu_popup_at_widget(GTK_MENU(menu), widget, GDK_GRAVITY_CENTER, GDK_GRAVITY_CENTER, NULL);*/
		gtk_menu_popup(GTK_MENU(image_options), NULL, NULL, NULL, NULL, event->button, event->time);
/*		gtk_menu_popup_at_pointer(GTK_MENU(image_options),(GdkEvent*)event);*/
		gtk_widget_show_all(image_options);
	}
	else if (event->type == GDK_BUTTON_PRESS && event->button == 1)
	{
		
	}
	
	return false;
}

/**
	Action when changing movement type in drawing area
	
	@param widget 
		the parent button on which the user clicked on to get here
	@param data
		NULL
*/
void haed_draw_change_move_type(GtkWidget *widget,gpointer data)
{
	GtkWidget *tempImage;
	int prevanimation=HAED_DRAW_PIXBUF_ANIMATION;
	
	char *animationtext=gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widget));
	b_tm("changed %s",animationtext);
	HAED_DRAW_PIXBUF_ANIMATION=haed_animations_get_pos(animationtext);
	
	gdouble slider_animation_pos=gtk_adjustment_get_value (HAED_ADJ_POS);
	
	b_tm("POS:: %f\n",slider_animation_pos);
	
	for(int i=0;i<8;i++)
	{
		if(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][i])
		{
			tempImage=gtk_image_new();
			gtk_image_set_from_pixbuf(GTK_IMAGE(tempImage),gdk_pixbuf_scale_simple(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][i],100,100,GDK_INTERP_NEAREST));
			gtk_button_set_image(GTK_BUTTON(frameButtons[i]),tempImage);
		}
		else if(HAED_DRAW_PIXBUF[prevanimation][i])
		{
			GdkPixbuf *tempbuf=gdk_pixbuf_new(GDK_COLORSPACE_RGB,1,8,100,100);
			gdk_pixbuf_fill(tempbuf,0x0);
			
			tempImage=gtk_image_new();
			gtk_image_set_from_pixbuf(GTK_IMAGE(tempImage),tempbuf);
			gtk_button_set_image(GTK_BUTTON(frameButtons[i]),tempImage);
		}
	}
}

/**
	adds animation to parent structure
	hai=haed animation inf

	@param tempEksParent INNER_ALLOC
		the parent to save the animation into
	@param animationtype SAVED
		Animation type
	@param animationinfo SAVED
		all vectors carrying the information
	@param animationinfolen
		length of @param animationinfo
	
	@return void
*/
void hai_add_animation(EksParent *tempEksParent,char *animationtype,int animationspeed,char **animationinfo,int animationinfolen)
{
	b_tm("ADDING %s ANIMATION:\n",animationtype);
	//add stopped
	EksParent *parentstopped=eks_parent_add_child(tempEksParent,animationtype,EKS_PARENT_TYPE_VALUE,NULL);
	
	//eks_parent_add_children(parentstopped,1);
	
	//set the stopped animations
	EksParent *parentmirror=eks_parent_add_child(parentstopped,"ALL_MIRROR",EKS_PARENT_TYPE_VALUE,NULL);
	
	//animationspeed
	//eks_parent_add_children(parentmirror,1+animationinfolen);
	eks_parent_add_child(parentmirror,b_int_to_string(animationspeed),EKS_PARENT_TYPE_VALUE,NULL);

	//offset_x,offset_y,width,height,offset_actionspot_x,offset_actionspot_y
	
	//char *infostr=concat("0,0,",b_int_to_string(gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[0][0])),",",b_int_to_string(gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[0][0])),",0,0",NULL);

	for(int i=0;i<animationinfolen;i++)
	{
		//parentmirror->children[1+i]=(char*)g_strndup(animationinfo[i],strlen(animationinfo[i]));
		//eks_parent_set(eks_parent_get_child(parentmirror,1+i),g_strdup(animationinfo[i])),EKS_PARENT_TYPE_TEXT);
		eks_parent_add_child(parentmirror,g_strdup(animationinfo[i]),EKS_PARENT_TYPE_VALUE,NULL);
	}
	b_tm("ADDING %s ANIMATION: DONE!\n",animationtype);
}


void haed_draw_move_slider(GtkRange *range, GtkScrollType step, gpointer user_data)
{
	printf("Moved slider...\n");
	haed_draw_change_move_type(animationDropDown,NULL);
}


/*static void drag_motion(GtkWidget *widget, GdkDragContext *context, gint x, gint y, guint time, gpointer user_data)*/
/*{*/
/*	printf("GOT DND DATA!\n");*/
/*	return;*/
/*}*/

void drag_data_received (GtkWidget *widget, GdkDragContext *context, gint x, gint y, GtkSelectionData *data, guint info, guint time, gpointer user_data)
{
	guint *tdata=(guint*)gtk_selection_data_get_data(data);
	
	printf("GOT DND DATA! %u\n",*tdata);
	
	guint output_window=(guint)(intptr_t)user_data;
	
	int a=0;
	while(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][a]!=NULL)
		a++;
	
	//if you pressed a frame way beyod the next.
	if(output_window>a)
	{
		output_window=a;
	}
	
	guint inputWindow=*tdata;
	
	GdkPixbuf *move_pixbuf=HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][inputWindow];
	
	GtkWidget *tempImage=gtk_image_new();
	gtk_image_set_from_pixbuf(GTK_IMAGE(tempImage),gdk_pixbuf_scale_simple(move_pixbuf,100,100,GDK_INTERP_NEAREST));
	
	if(frameButtons[inputWindow])
	{
		if(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][output_window])
		{
			 g_object_unref(HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][output_window]);
		}
	
		gtk_button_set_image(GTK_BUTTON(frameButtons[output_window]),tempImage);
		HAED_DRAW_PIXBUF[HAED_DRAW_PIXBUF_ANIMATION][output_window]=gdk_pixbuf_copy(move_pixbuf);
	}
	
	return;
}

void on_drag_data_get(GtkWidget *widget, GdkDragContext *drag_context, GtkSelectionData *sdata, guint info, guint time, gpointer user_data)
{
	printf("SEND DND DATA! %d\n",(int)(intptr_t)user_data);
	guint cur_index=(guint)(intptr_t)user_data;
	guint *tdata=&cur_index;
	gtk_selection_data_set(sdata,GDK_SELECTION_TYPE_INTEGER,sizeof (guint32)*8,(void*)tdata,sizeof (guint32));
	return;
}

/**
	Lists all frames to paint into
	
	@param widget 
		the parent button on which the user clicked on to get here
	@param data
		input pixbufholder.
*/
void haed_draw_active_object_frame_editor(GtkWidget *widget,gpointer data)
{
	GtkWidget *dialogBox;
	GtkWidget *innerDialogBox;
	
	EksParent *pbholder=data;
	
	for(int i=0;i<8;i++)
	{
		for(int ii=0;ii<8;ii++)
		{
			if(HAED_DRAW_PIXBUF[i][ii])
				g_object_unref(HAED_DRAW_PIXBUF[i][ii]);
			
			HAED_DRAW_PIXBUF[i][ii]=NULL;
		}
	}
	
	//define the window
	GtkWidget *newobjectdialog=gtk_dialog_new();
/*	printf("TOPLEVEL:: %p %p\n",GTK_WINDOW(gtk_widget_get_toplevel(newobjectdialog)),GTK_WINDOW(gtk_widget_get_toplevel(widget)));*/
	gtk_window_set_transient_for(GTK_WINDOW(gtk_widget_get_toplevel(newobjectdialog)),GTK_WINDOW(gtk_widget_get_toplevel(widget)));
	g_signal_connect (newobjectdialog, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),"_Ok",GTK_RESPONSE_OK);
	gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),"_Cancel",GTK_RESPONSE_NO);
	//gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),"_Help",GTK_RESPONSE_HELP);
	
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(newobjectdialog));
	
	//containments
	innerDialogBox=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,2);
	
	GtkTargetEntry *dnd_entry=gtk_target_entry_new("STRING",GTK_TARGET_SAME_APP,0);
	
	for(int i=0;i<8;i++)
	{
		frameButtons[i]=gtk_button_new();
		g_signal_connect (frameButtons[i], "clicked",G_CALLBACK (haed_object_drawing_edit_image), (void*)(intptr_t)i);
		g_signal_connect (frameButtons[i], "button-press-event", G_CALLBACK (haed_object_drawing_image_options), (void*)(intptr_t)i);
		gtk_widget_set_size_request (frameButtons[i],100,100);
		
		gtk_drag_dest_set(frameButtons[i],GTK_DEST_DEFAULT_ALL,dnd_entry,1,GDK_ACTION_COPY);
		gtk_drag_source_set(frameButtons[i],GDK_BUTTON1_MASK,dnd_entry,1,GDK_ACTION_COPY);
		g_signal_connect (frameButtons[i], "drag-data-received",G_CALLBACK (drag_data_received), (void*)(intptr_t)i);
		g_signal_connect (frameButtons[i], "drag-data-get",G_CALLBACK (on_drag_data_get), (void*)(intptr_t)i);
		
		gtk_box_pack_start(GTK_BOX(innerDialogBox),frameButtons[i],0,0,0);
	}
		
	gtk_box_pack_start(GTK_BOX(dialogBox),innerDialogBox,0,0,0);
	
	HAED_ADJ_POS=gtk_adjustment_new(0,0,100,1,1,1);
	GtkWidget *scrollArea=gtk_scrollbar_new(GTK_ORIENTATION_HORIZONTAL,HAED_ADJ_POS);
		g_signal_connect (scrollArea, "value-changed",G_CALLBACK (haed_draw_move_slider), NULL);
	
	gtk_box_pack_start(GTK_BOX(dialogBox),scrollArea,0,0,0);
	
	animationDropDown=gtk_combo_box_text_new();
	
		for(int i=0;i<GLOBAL_ANIMATION_VECTOR_LEN;i++)
		{
			gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(animationDropDown),NULL,GLOBAL_ANIMATION_VECTOR[i]);
		}
	
		HAED_DRAW_PIXBUF_ANIMATION=0;
		gtk_combo_box_set_active(GTK_COMBO_BOX(animationDropDown),0);
		//gtk_combo_box_get_model(GTK_COMBO_BOX(animationDropDown))
		g_signal_connect (animationDropDown, "changed",G_CALLBACK (haed_draw_change_move_type), NULL);
	
	gtk_box_pack_start(GTK_BOX(dialogBox),animationDropDown,0,0,0);
	
	//show the dialog
	gtk_widget_show_all(newobjectdialog);
	
	//edit existing
	if(pbholder)
	{
		int numberOfAnimations=haed_pixbufholder_count_amount(pbholder);
	
		for(int i=0;i<numberOfAnimations;i++)
		{
			b_tm("%s",eks_parent_dump_text(pbholder));
			
			EksParent *animationp=eks_parent_get_child_from_type(pbholder,i,EKS_PARENT_TYPE_VALUE);
			
			g_autofree char *animationstr=eks_parent_get_string(animationp);
			
			int bufpos=haed_animations_get_pos(animationstr);
			
			//ALL_MIRROR
			EksParent *animationparent=eks_parent_get_child_from_type(animationp,0,EKS_PARENT_TYPE_VALUE);
			
			EksParent *framesparent=eks_parent_get_child_from_name(animationparent,"FRAMES");
			
			b_tm("%s",eks_parent_dump_text(framesparent));
			
			int numberOfFrames=eks_parent_get_amount_from_type(framesparent,EKS_PARENT_TYPE_VALUE);
			
			//skip speed
			for(int ii=0;ii<numberOfFrames;ii++)
			{
				EksParent *frame=eks_parent_get_child_from_type(framesparent,ii,EKS_PARENT_TYPE_VALUE);
				
				int framex=eks_parent_get_int(eks_parent_get_child(frame,0));
				int framey=eks_parent_get_int(eks_parent_get_child(frame,1));
				int framewidth=eks_parent_get_int(eks_parent_get_child(frame,2));
				int frameheight=eks_parent_get_int(eks_parent_get_child(frame,3));
				//hotspots...
				
				b_tm("%d %d %d %d",framex,framey,framewidth,frameheight);
				
				HAED_DRAW_PIXBUF[bufpos][ii]=gdk_pixbuf_new(GDK_COLORSPACE_RGB,1,8,framewidth,frameheight);
				
				gdk_pixbuf_copy_area(eks_parent_custom_get(animationp,0),framex,framey,framewidth,frameheight,HAED_DRAW_PIXBUF[bufpos][ii],0,0);
			}
		}
		
		haed_draw_change_move_type(animationDropDown,NULL);
	}
	
	//run the dialog
	if(gtk_dialog_run(GTK_DIALOG(newobjectdialog))==GTK_RESPONSE_OK)
	{
		//for a new active object
		
		//define the parent
		EksParent *bufholder=haed_pixbufholder_new();
		//calloc(1,sizeof(EksParent));
		//eks_parent_set(toplevelparent,"toplevel",EKS_PARENT_TYPE_VALUE);
		
		//tempstr[0]=concat("0,0,",b_int_to_string(gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[0][0])),",",b_int_to_string(gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[0][0])),",0,0",NULL);
		
		//hai_add_animation(toplevelparent,GLOBAL_ANIMATION_STOPPED,10,tempstr,1);
		
		int totalwidth=0;
		int totalheight=0;
		int frame=0;
		
		//the image that will be generated for each animation/direction
		GdkPixbuf *animationsp[8];
		
		//if running exists
		for(int a=0;a<GLOBAL_ANIMATION_VECTOR_LEN;a++)
		{
			if(HAED_DRAW_PIXBUF[a][0])
			{
				b_tm("anim %d",a);
				totalheight=0;
				totalwidth=0;
				
				frame=0;
				
				EksParent *directioninfo=eks_parent_new("ALL_MIRROR",EKS_PARENT_TYPE_VALUE,NULL,NULL);
				
				//info regarding the animation speed
				EksParent *animspeed=eks_parent_add_child(directioninfo,"ANIMATION_SPEED",EKS_PARENT_TYPE_VALUE,NULL);
				eks_parent_add_child(animspeed,10,EKS_PARENT_TYPE_VALUE,NULL);
				
				EksParent *frames=eks_parent_add_child(directioninfo,"FRAMES",EKS_PARENT_TYPE_VALUE,NULL);
				
				//loop through all frames
				while(HAED_DRAW_PIXBUF[a][frame]!=NULL)
				{
					//info regarding the frame positions
					EksParent *animvect=eks_parent_add_child(frames,"FRAME_POSITIONS",EKS_PARENT_TYPE_VALUE,NULL);
					eks_parent_add_child(animvect,0,EKS_PARENT_TYPE_VALUE,NULL);
					eks_parent_add_child(animvect,totalheight,EKS_PARENT_TYPE_VALUE,NULL);
					eks_parent_add_child(animvect,gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[a][frame]),EKS_PARENT_TYPE_VALUE,NULL);
					eks_parent_add_child(animvect,gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[a][frame]),EKS_PARENT_TYPE_VALUE,NULL);
					//info regarding the hot spot
					eks_parent_add_child(animvect,0,EKS_PARENT_TYPE_VALUE,NULL);
					eks_parent_add_child(animvect,0,EKS_PARENT_TYPE_VALUE,NULL);
					
					//tempstr[frame]=concat("0,",b_int_to_string(totalheight),",",b_int_to_string(gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[a][frame])),",",b_int_to_string(gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[a][frame])),",0,0",NULL);
					
					//b_tm("tempstr: %s",tempstr[frame]);
					
					totalheight+=gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[a][frame]);
					if(totalwidth<gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[a][frame]))
					{
						totalwidth=gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[a][frame]);
					}
					frame++;
				}
				b_tm("frame: %d",frame);
				//add the string into the eks structure
				//hai_add_animation(toplevelparent,(char*)GLOBAL_ANIMATION_VECTOR[a],10,tempstr,animations);
				
				//make new pixbuf that holds all images
				animationsp[a]=gdk_pixbuf_new(GDK_COLORSPACE_RGB,1,8,totalwidth,totalheight);
				
				//gdk_pixbuf_fill(animations[a],0x00000000);
				totalheight=0;
				
				//loop through all created frames
				for(int i=0;i<frame;i++)
				{
					gdk_pixbuf_copy_area(HAED_DRAW_PIXBUF[a][i],0,0,gdk_pixbuf_get_width(HAED_DRAW_PIXBUF[a][i]),gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[a][i]),animationsp[a],0,totalheight);
					totalheight+=gdk_pixbuf_get_height(HAED_DRAW_PIXBUF[a][i]);
				}
				
				EksParent *animparent=haed_pixbufholder_add_animation(bufholder,(char*)GLOBAL_ANIMATION_VECTOR[a],animationsp[a]);
				
				eks_parent_insert(animparent,directioninfo);
				
				b_tm("struct:\n %s \n\n",eks_parent_dump_text(bufholder));
			}
		}

		//edit existing holder
		if(data)
		{
			memcpy(pbholder,bufholder,sizeof(EksParent));
		}
		else
		{
			//time seed on active object
			time_t current_time;
		 
			//Obtain current time as seconds elapsed since the Epoch.
			current_time = time(NULL);
		 
			if (current_time == ((time_t)-1))
			{
				fprintf(stderr, "Failure to compute the current time.");
			}
		 
			//Convert to local time format. 
			struct tm *theTime = gmtime(&current_time);
			
			char *objectName=g_strdup_printf("ao_%d_%d_%d_%d_%d_%d",theTime->tm_year-100,theTime->tm_mon,theTime->tm_mday,theTime->tm_hour,theTime->tm_min,theTime->tm_sec);
			
			haed_object_add_eks_parent_to_gui(objectName,bufholder,GLOBAL_ACTIVE_OBJECT_S);
			
			printf("%s WAS SUCCESFULLY CREATED!\n",objectName);
			
			free(objectName);
		}
	}
	else
	{
		b_warning_message("Could not get object!\n");
	}
	
	gtk_widget_destroy(newobjectdialog);
}
