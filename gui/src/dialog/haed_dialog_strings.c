/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

typedef struct Compare_dialog
{
	const char *use;
	const char *description;
}Compare_dialog;

static const Compare_dialog GLOBAL_COMPARE_FUNCTIONS[]=
{
	{"==","== (equals)"},
	{"!=","!= (not equals)"},
	{"<","< (greater)"},
	{">","> (smaller)"},
	{"<=","<= (greater or equals)"},
	{">=",">= (smaller or equals)"}
};

#define GLOBAL_COMPARE_FUNCTIONS_LEN (sizeof(GLOBAL_COMPARE_FUNCTIONS)/sizeof(GLOBAL_COMPARE_FUNCTIONS[0]))

//==========================Get text====================

char *haed_dialog_enter_code(void)
{
	char *ReturnString=NULL;
	//tempAOGUI_SEND=tempAOGUI;
	
	//GtkWidget *AllDirectionsNotebook;
	//GtkWidget *SetDirectionsGird;
	GtkWidget *TypeWindow;
	GtkWidget *dialog_grid;
	
	GtkWidget *EntryField;
	GtkWidget *SetSpeedInfo;
	
	//1=active object
	//tempAOGUI->movType=1;
	
	TypeWindow = gtk_dialog_new();
	//gtk_window_set_title(GTK_WINDOW(TypeWindow), "Edit Movement (All Directions)");
	//gtk_window_set_default_size(GTK_WINDOW(TypeWindow), 300, 200);
	g_signal_connect (GTK_WIDGET (TypeWindow), "destroy",G_CALLBACK (gtk_widget_destroy), NULL);
	
	gtk_dialog_add_button(GTK_DIALOG(TypeWindow),_("Ok"),GTK_RESPONSE_YES);
	gtk_dialog_add_button(GTK_DIALOG(TypeWindow),_("Cancel"),GTK_RESPONSE_NO);
	//AllDirectionsNotebook=gtk_notebook_new();
		dialog_grid=gtk_dialog_get_content_area(GTK_DIALOG(TypeWindow));
			
			//SET SPEED
			SetSpeedInfo=gtk_label_new("Type something");
			
			gtk_box_pack_start(GTK_BOX(dialog_grid),SetSpeedInfo,0,0,0);
	
			EntryField = gtk_entry_new();
				//g_signal_connect (SetSpeedSpinButton, "value-changed", G_CALLBACK (ModifySpeed), (void*)0);
				
				//tempAOGUI->speed==NULL?gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetSpeedSpinButton),50):gtk_spin_button_set_value(GTK_SPIN_BUTTON(SetSpeedSpinButton),tempAOGUI->speed[0]);
				
			gtk_box_pack_start(GTK_BOX(dialog_grid),EntryField,0,0,0);

		//gtk_notebook_insert_page(GTK_NOTEBOOK(AllDirectionsNotebook),dialog_grid,gtk_label_new("Set Speed"),-1);
	
		//SetDirectionsGird=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);

		//gtk_notebook_insert_page(GTK_NOTEBOOK(AllDirectionsNotebook),SetDirectionsGird,gtk_label_new("Set Directions"),-1);
	//gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(TypeWindow))),EntryField);
	
	gtk_widget_show_all(TypeWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(TypeWindow))==GTK_RESPONSE_YES)
	{
		ReturnString=g_strndup((char*)gtk_entry_get_text(GTK_ENTRY(EntryField)),strlen((char*)gtk_entry_get_text(GTK_ENTRY(EntryField))));
		gtk_widget_destroy(TypeWindow);
		//tempAOGUI_SEND=NULL;
		return ReturnString;
		//return SelectMovementReturn;
	}
	else
	{
		gtk_widget_destroy(TypeWindow);
		b_error_message("Could not get object!\n");
		//tempAOGUI_SEND=NULL;
		return NULL;
		//return NULL;
	}
}

char *haed_window_compare_field(char **operator)
{
	char *ReturnString=NULL;

	GtkWidget *TypeWindow;
	GtkWidget *dialog_grid;
	
	GtkWidget *EntryField;
	GtkWidget *SetSpeedInfo;
	
	GtkWidget *tempWidget;
	GtkWidget *operation_selection_dropdown;
	
	//1=active object
	//tempAOGUI->movType=1;
	
	TypeWindow = gtk_dialog_new();
	g_signal_connect (GTK_WIDGET (TypeWindow), "destroy",G_CALLBACK (gtk_widget_destroy), NULL);
	
	gtk_dialog_add_button(GTK_DIALOG(TypeWindow),_("Ok"),GTK_RESPONSE_YES);
	gtk_dialog_add_button(GTK_DIALOG(TypeWindow),_("Cancel"),GTK_RESPONSE_NO);
	//AllDirectionsNotebook=gtk_notebook_new();
	dialog_grid=gtk_dialog_get_content_area(GTK_DIALOG(TypeWindow));
			
		//SET SPEED
		SetSpeedInfo=gtk_label_new("Type something");
		gtk_box_pack_start(GTK_BOX(dialog_grid),SetSpeedInfo,0,0,0);

		EntryField = gtk_entry_new();

		gtk_box_pack_start(GTK_BOX(dialog_grid),EntryField,0,0,0);
		
		operation_selection_dropdown=gtk_combo_box_text_new();
			for(int i=0;i<GLOBAL_COMPARE_FUNCTIONS_LEN;i++)
			{
				gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(operation_selection_dropdown),NULL,GLOBAL_COMPARE_FUNCTIONS[i].description);
			}
		
			gtk_combo_box_set_active(GTK_COMBO_BOX(operation_selection_dropdown),0);
			//g_signal_connect (operation_selection_dropdown, "changed",G_CALLBACK (haed_draw_change_move_type), NULL);
		gtk_box_pack_start(GTK_BOX(dialog_grid),operation_selection_dropdown,0,0,0);
		
		///@seethis
		if(GLOBAL_HAED_INFORMATION.firstProgram && GLOBAL_HAED_INFORMATION.firstProgram->program)
		{
			EksParent *objectsParent=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"objects");
			EksParent *activeParent=eks_parent_get_child_from_name(objectsParent,"active");
			
			eks_parent_foreach_child_start(activeParent,loopUnit)
			{
				if(eks_parent_compare_type(loopUnit,EKS_PARENT_TYPE_VALUE))
				{
					GtkWidget *object_push_button=gtk_button_new();
					
					GdkPixbuf *imagebuff=eks_parent_custom_get(loopUnit,HAED_OBJECT_IMAGEBUFF);
					//set the icon
					GdkPixbuf *iconbuff=gdk_pixbuf_scale_simple(imagebuff,32,32,GDK_INTERP_NEAREST);
					gtk_button_set_image(GTK_BUTTON(object_push_button),gtk_image_new_from_pixbuf(iconbuff));
					gtk_box_pack_start(GTK_BOX(dialog_grid),object_push_button,0,0,0);
				}
			}eks_parent_foreach_child_end(activeParent,loopUnit);
		}
	
	gtk_widget_show_all(TypeWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(TypeWindow))==GTK_RESPONSE_YES)
	{
		ReturnString=g_strdup(gtk_entry_get_text(GTK_ENTRY(EntryField)));
		//tempAOGUI_SEND=NULL;
		
		if(operator)
		{
			char *comptext=gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(operation_selection_dropdown));
			
			for(int i=0;i<GLOBAL_COMPARE_FUNCTIONS_LEN;i++)
			{
				if(g_strcmp0(comptext,GLOBAL_COMPARE_FUNCTIONS[i].description)==0)
				{
					(*operator) = g_strdup(GLOBAL_COMPARE_FUNCTIONS[i].use);
					break;
				}
			}
		}
		
		gtk_widget_destroy(TypeWindow);
		return ReturnString;
		//return SelectMovementReturn;
	}
	else
	{
		gtk_widget_destroy(TypeWindow);
		b_error_message("Could not get object!\n");
		//tempAOGUI_SEND=NULL;
		return NULL;
		//return NULL;
	}
}
