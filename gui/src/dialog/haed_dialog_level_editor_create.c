/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

/**
	Select object to create
	
	@param widget 
		the parent button on which the user clicked on to get here
	@param data
		NULL
*/
void haed_dialog_level_editor_create(GtkWidget *widget,gpointer data)
{
	GtkWidget *dialogBox;
	GtkWidget *grid;
	GtkWidget *button;
	int dialogresponce;
	
	//define the window
	GtkWidget *newobjectdialog=gtk_dialog_new();
/*	printf("TOPLEVEL:: %p %p\n",GTK_WINDOW(gtk_widget_get_toplevel(newobjectdialog)),GTK_WINDOW(gtk_widget_get_toplevel(widget)));*/
	gtk_window_set_transient_for(GTK_WINDOW(gtk_widget_get_toplevel(newobjectdialog)),GTK_WINDOW(gtk_widget_get_toplevel(widget)));
	
	g_signal_connect (newobjectdialog, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	gtk_dialog_add_button(GTK_DIALOG(newobjectdialog),_("_Cancel"),GTK_RESPONSE_NO);
	
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(newobjectdialog));
	
	grid=gtk_grid_new();
	
	//containments
	button=gtk_button_new();
		gtk_dialog_set_responce_widget(GTK_DIALOG(newobjectdialog),button,(void*)(intptr_t)1); //active object
		gtk_button_set_image(GTK_BUTTON(button),gtk_image_new_from_file("./images/level_editor/active_objects.png"));
	gtk_box_pack_start(GTK_BOX(dialogBox),button,0,0,0);
	
	button=gtk_button_new();
		gtk_dialog_set_responce_widget(GTK_DIALOG(newobjectdialog),button,(void*)(intptr_t)2); //backdrop
		gtk_button_set_image(GTK_BUTTON(button),gtk_image_new_from_file("./images/level_editor/backdrop.png"));
	gtk_box_pack_start(GTK_BOX(dialogBox),button,0,0,0);
	
	button=gtk_button_new();
		gtk_dialog_set_responce_widget(GTK_DIALOG(newobjectdialog),button,(void*)(intptr_t)3); //text
		gtk_button_set_image(GTK_BUTTON(button),gtk_image_new_from_file("./images/level_editor/text.png"));
	gtk_box_pack_start(GTK_BOX(dialogBox),button,0,0,0);
/*	gtk_grid_attach(GTK_GRID(GLOBAL_MAIN_STORYBOARD),button,1,5,1,1);*/
	
	gtk_box_pack_start(GTK_BOX(dialogBox),grid,0,0,0);
	
	//show the dialog
	gtk_widget_show_all(newobjectdialog);
	
	dialogresponce=gtk_dialog_run(GTK_DIALOG(newobjectdialog));
	
	if(dialogresponce==1)
	{
		haed_draw_active_object_frame_editor(newobjectdialog,NULL);
	}
	else if(dialogresponce==2)
	{
		b_tm("Not made yet!");
	}
	else
	{
		b_warning_message("Could not get object!\n");
	}
	
	gtk_widget_destroy(newobjectdialog);
}
