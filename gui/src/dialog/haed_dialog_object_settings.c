/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

/////////////////////////////////////////////////////////////////////////

static GtkWidget *STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW;

static void haed_object_settings_edit_object_image(GtkWidget *widget,gpointer data)
{
	EksParent *thisObj=data;
	
	GdkPixbuf *objimagebuff=eks_parent_custom_get(thisObj,HAED_OBJECT_IMAGEBUFF);
	
	haed_dialog_drawing(&objimagebuff,widget);
	
	//set the side-image too
	GdkPixbuf *tempImage=gdk_pixbuf_scale_simple(objimagebuff,32,32,GDK_INTERP_NEAREST);
	
	gtk_button_set_image(GTK_BUTTON(eks_parent_custom_get(thisObj,HAED_OBJECT_TRIGGER_BUTTON_WIDGET)),gtk_image_new_from_pixbuf(tempImage));
	gtk_button_set_image(GTK_BUTTON(eks_parent_custom_get(thisObj,HAED_OBJECT_TRIGGER_MENU_WIDGET)),gtk_image_new_from_pixbuf(tempImage));
	gtk_button_set_image(GTK_BUTTON(eks_parent_custom_get(thisObj,HAED_OBJECT_TRIGGER_TOP_WIDGET)),gtk_image_new_from_pixbuf(tempImage));
	gtk_button_set_image(GTK_BUTTON(eks_parent_custom_get(thisObj,HAED_OBJECT_DRAW_MENU_WIDGET)),gtk_image_new_from_pixbuf(tempImage));
}

static void haed_object_change_in_name_entry(GtkWidget *widget, gpointer user_data)
{
	EksParent *this_obj=user_data;
	char *old_value=eks_parent_get_string(this_obj);
	const char *new_value=gtk_entry_get_text(GTK_ENTRY(widget));
	
	if(new_value && strlen(new_value))
	{
		printf("Pressed key %s -> %s\n%p\n",old_value,new_value,GLOBAL_HAED_INFORMATION.firstProgram->program);
		
		EksParent *levelsparent=eks_parent_get_child_from_name(GLOBAL_HAED_INFORMATION.firstProgram->program,"levels");
		
		eks_parent_foreach_child_start(levelsparent,looplevel)
		{
			EksParent *triggersparent=eks_parent_get_child_from_name(looplevel,"triggers");
			
			eks_parent_foreach_child_start(triggersparent,looptriggers)
			{
				g_autofree char *triggerstring=eks_parent_get_string(looptriggers);
				g_autofree char *new_triggerstring=lexca_replace_value(triggerstring,old_value,new_value);
				
				printf("LOOPTRIGGERS:: %s -> %s\n",triggerstring,new_triggerstring);
				
				eks_parent_set_string(looptriggers,new_triggerstring);
				
				eks_parent_foreach_child_start(looptriggers,loopactions)
				{
					g_autofree char *actionstring=eks_parent_get_string(loopactions);
					g_autofree char *new_actionstring=lexca_replace_value(actionstring,old_value,new_value);
					
					printf("LOOPACTION:: %s -> %s\n",actionstring,new_actionstring);
					
					eks_parent_set_string(loopactions,new_actionstring);
				}eks_parent_foreach_child_end(looptriggers,loopactions);
				
			}eks_parent_foreach_child_end(triggersparent,looptriggers);
			
		}eks_parent_foreach_child_end(levelsparent,looplevel);
		
		eks_parent_set_string(this_obj,new_value);
	/*	printf("GAME:: %s\n",eks_parent_dump_text(GLOBAL_HAED_INFORMATION.firstProgram->program));*/
	}
	return;
}

/*static void haed_object_change_in_name_entry_test(GtkEntry *entry, gchar *preedit, gpointer user_data)*/
/*{*/
/*	printf("Pressed akey %s\n",preedit);*/
/*}*/

void haed_object_settings_window(GtkWidget *widget,gpointer data)
{
	GtkWidget *thisNotebook;
	
	GtkWidget *setImageAndName;
	GtkWidget *setObjectInformation;
	
	EksParent *thisObj=data;
	
	GdkPixbuf *objimagebuff=eks_parent_custom_get(thisObj,HAED_OBJECT_IMAGEBUFF);
	
	STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW = gtk_dialog_new();
	
	gtk_dialog_add_button(GTK_DIALOG(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW),_("Ok"),GTK_RESPONSE_YES);
	gtk_dialog_add_button(GTK_DIALOG(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW),_("Cancel"),GTK_RESPONSE_NO);
	
	g_signal_connect (GTK_WIDGET (STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW), "destroy",G_CALLBACK (gtk_widget_destroy), NULL);
	
	thisNotebook=gtk_notebook_new();
		setImageAndName=gtk_box_new(GTK_ORIENTATION_VERTICAL,1);
		
			GtkWidget *infoLabelSetName=gtk_label_new("Name of the object");
			
			gtk_box_pack_start(GTK_BOX(setImageAndName),infoLabelSetName,0,0,0);
	
			GtkWidget *setNameEntryField = gtk_entry_new();
				//gtk_entry_set_max_length(GTK_ENTRY(setNameEntryField),50);
				g_signal_connect (setNameEntryField, "changed", G_CALLBACK (haed_object_change_in_name_entry), data);
/*				g_signal_connect (setNameEntryField, "preedit-changed", G_CALLBACK (haed_object_change_in_name_entry_test), data);*/
				gtk_entry_set_text(GTK_ENTRY(setNameEntryField),thisObj->name);
			
			gtk_box_pack_start(GTK_BOX(setImageAndName),setNameEntryField,0,0,0);
			
			GtkWidget *setImageButton=gtk_button_new();
				g_signal_connect (setImageButton, "clicked",G_CALLBACK(haed_object_settings_edit_object_image), data); //1=all directions
				
				GtkWidget *tempImage=gtk_image_new();
				gtk_image_set_from_pixbuf(GTK_IMAGE(tempImage),gdk_pixbuf_scale_simple(objimagebuff,100,100,GDK_INTERP_NEAREST));
				gtk_button_set_image(GTK_BUTTON(setImageButton),tempImage);
			gtk_box_pack_start(GTK_BOX(setImageAndName),setImageButton,0,0,0);
			
		gtk_notebook_insert_page(GTK_NOTEBOOK(thisNotebook),setImageAndName,gtk_label_new("Set name and image"),-1);
	
		setObjectInformation=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);
			
		gtk_notebook_insert_page(GTK_NOTEBOOK(thisNotebook),setObjectInformation,gtk_label_new("Set object information"),-1);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW))),thisNotebook);
	
	gtk_widget_show_all(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW);
	
	if(gtk_dialog_run(GTK_DIALOG(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW))==GTK_RESPONSE_YES)
	{
		const char *nameEntryText=gtk_entry_get_text(GTK_ENTRY(setNameEntryField));
	
		if(strcmp(thisObj->name,nameEntryText)!=0)
		{
			if(thisObj->name)
				free(thisObj->name);
				
			thisObj->name=g_strdup(nameEntryText);
		}
		
		b_tm("The entry str: %s",nameEntryText);
	
		gtk_widget_destroy(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW);
		return;
	}
	else
	{
		gtk_widget_destroy(STATIC_GLOBAL_ACTIVE_OBJECT_SETTINGS_WINDOW);
		b_error_message("You pressed NO!\n");
		return;
	}
}
