/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//==========================SELECT MOVEMENT TYPE=======================
static GtkWidget *SelectMovementDialogWindow;
static int SelectMovementReturn;

static void SelectMovementActiveOBject_SELECT(GtkWidget *widget,gpointer data)
{
	//if(data!=NULL)
	//{
		gtk_dialog_response(GTK_DIALOG(SelectMovementDialogWindow),1);
		
		SelectMovementReturn=(long int)data;
	/*}
	else
	{
		printf("ERROR (SelectMovementActiveOBject_SELECT)! Could not retrieve object!\n");
	}*/
}

int haed_dialog_select_movement_active_object(void)
{
	GtkWidget *dialogBox;
	GtkWidget *MovAllButton;
	
	GtkWidget *StopButton;
	
	//define the window
	SelectMovementDialogWindow=gtk_dialog_new();
	g_signal_connect (SelectMovementDialogWindow, "destroy",G_CALLBACK (gtk_widget_destroy), NULL); //will destroy when its box is destroyed
	gtk_dialog_add_button(GTK_DIALOG(SelectMovementDialogWindow),_("Cancel"),GTK_RESPONSE_NO);
	
	dialogBox=gtk_dialog_get_content_area(GTK_DIALOG(SelectMovementDialogWindow));
	
	//containments
	MovAllButton=gtk_button_new();
		g_signal_connect (MovAllButton, "clicked",G_CALLBACK (SelectMovementActiveOBject_SELECT), (void*)1); //1=all directions
		gtk_button_set_image(GTK_BUTTON(MovAllButton),gtk_image_new_from_file("./images/level_editor/all_directions.png"));
	gtk_box_pack_start(GTK_BOX(dialogBox),MovAllButton,0,0,0);
	
	StopButton=gtk_button_new();
		g_signal_connect (StopButton, "clicked",G_CALLBACK (SelectMovementActiveOBject_SELECT), (void*)0); //0=stop
		gtk_button_set_image(GTK_BUTTON(StopButton),gtk_image_new_from_file("./images/level_editor/stop.png"));
	gtk_box_pack_start(GTK_BOX(dialogBox),StopButton,0,0,0);
	
	//show the dialog
	gtk_widget_show_all(SelectMovementDialogWindow);
	
	if(gtk_dialog_run(GTK_DIALOG(SelectMovementDialogWindow))==1)
	{
		gtk_widget_destroy(SelectMovementDialogWindow);
		return SelectMovementReturn;
	}
	else
	{
		printf("ERROR (SelectMovementActiveOBject): Could not get object!\n");
		gtk_widget_destroy(SelectMovementDialogWindow);
		return 0;
	}
}
