/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//NEW CONDITION WINDOW
void haed_dialog_new_condition(GtkWidget *widget,gpointer data)
{
	GLOBAL_CONDITIONS_WINDOW = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(GLOBAL_CONDITIONS_WINDOW), "New Condition");
	gtk_window_set_default_size(GTK_WINDOW(GLOBAL_CONDITIONS_WINDOW), 300, 200);
	g_signal_connect (GTK_WIDGET (GLOBAL_CONDITIONS_WINDOW), "destroy",G_CALLBACK (haed_destroy_window_only), GLOBAL_MAIN_CONDITIONS_BOX);

	gtk_container_add(GTK_CONTAINER(GLOBAL_CONDITIONS_WINDOW),GLOBAL_MAIN_CONDITIONS_BOX);
	
	gtk_widget_show_all(GLOBAL_CONDITIONS_WINDOW);
}
