/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//MISC BUTTONS SECOND ROW
void haed_titlebar_init(void)
{
	GtkWidget *titlebar=gtk_window_get_titlebar(GTK_WINDOW(GLOBAL_MAIN_WINDOW));

/*	MAIN_MidSectWidgets=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,1);*/
			//QUICK SAVE & LOAD & NEW - BUTTONS
			GtkWidget *newFileButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(newFileButton),gtk_image_new_from_icon_name("document-new",GTK_ICON_SIZE_SMALL_TOOLBAR));//("./images/main/new_file.png"));
				g_signal_connect (newFileButton, "clicked",G_CALLBACK (haed_clean_everything), NULL);
			gtk_header_bar_pack_start(GTK_HEADER_BAR(titlebar),newFileButton);
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),newFileButton,0,0,0);*/
			
			GtkWidget *loadFileButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(loadFileButton),gtk_image_new_from_icon_name("document-open",GTK_ICON_SIZE_SMALL_TOOLBAR));//gtk_image_new_from_file("./images/main/load_file.png"));
				g_signal_connect (loadFileButton, "clicked",G_CALLBACK (haed_load_program), NULL);
			gtk_header_bar_pack_start(GTK_HEADER_BAR(titlebar),loadFileButton);
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),loadFileButton,0,0,0);*/
			
			GtkWidget *saveFileButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(saveFileButton),gtk_image_new_from_icon_name("document-save",GTK_ICON_SIZE_SMALL_TOOLBAR));//gtk_image_new_from_file("./images/main/save_file.png"));
				g_signal_connect (saveFileButton, "clicked",G_CALLBACK (haed_gui_save), NULL);
			gtk_header_bar_pack_start(GTK_HEADER_BAR(titlebar),saveFileButton);
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),saveFileButton,0,0,0);*/
			
			//--------
/*			GtkWidget *separator=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),separator,0,0,3);*/
			/*
			//EDIT MODES
			MAIN_EditModes[0]=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(MAIN_EditModes[0]),gtk_image_new_from_file("./images/main/storyboard.png"));
				g_signal_connect (MAIN_EditModes[0], "clicked",G_CALLBACK (haed_set_storyboard_mode), 0);
			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),MAIN_EditModes[0],0,0,0);
			
			MAIN_EditModes[1]=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(MAIN_EditModes[1]),gtk_image_new_from_file("./images/main/level_editor.png"));
				g_signal_connect (MAIN_EditModes[1], "clicked",G_CALLBACK (haed_set_level_editor_mode), 0);
			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),MAIN_EditModes[1],0,0,0);
			
			MAIN_EditModes[2]=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(MAIN_EditModes[2]),gtk_image_new_from_file("./images/main/event_editor.png"));
				g_signal_connect (MAIN_EditModes[2], "clicked",G_CALLBACK (haed_set_event_editor_mode), 0);
			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),MAIN_EditModes[2],0,0,0);
			
			//--------
			separator=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),separator,0,0,3);
			
			//MODE SETTINGS
			MAIN_ModeSettings[0]=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(MAIN_ModeSettings[0]),gtk_image_new_from_file("./images/main/new_object.png"));
				g_signal_connect (MAIN_ModeSettings[0], "clicked",G_CALLBACK (haed_draw_main_dialog), 0);
			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),MAIN_ModeSettings[0],0,0,0);
			
			copyButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(copyButton),gtk_image_new_from_file("./images/main/pref.png"));
			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),copyButton,0,0,0);
			
			*/
			
			//--------
/*			separator=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),separator,0,0,3);*/
			
			//COPY PASTE ETC
/*			GtkWidget *cutButton=gtk_button_new();*/
/*				gtk_button_set_image(GTK_BUTTON(cutButton),gtk_image_new_from_file("./images/main/cut.png"));*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),cutButton,0,0,0);*/
/*			*/
/*			GtkWidget *copyButton=gtk_button_new();*/
/*				gtk_button_set_image(GTK_BUTTON(copyButton),gtk_image_new_from_file("./images/main/copy.png"));*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),copyButton,0,0,0);*/
/*			*/
/*			GtkWidget *pasteButton=gtk_button_new();*/
/*				gtk_button_set_image(GTK_BUTTON(pasteButton),gtk_image_new_from_file("./images/main/paste.png"));*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),pasteButton,0,0,0);*/
			
			//--------
/*			separator=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),separator,0,0,3);*/
			
			//UNDO REDO
/*			GtkWidget *undoButton=gtk_button_new();*/
/*				gtk_button_set_image(GTK_BUTTON(undoButton),gtk_image_new_from_file("./images/main/undo.png"));*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),undoButton,0,0,0);*/
/*			*/
/*			GtkWidget *redoButton=gtk_button_new();*/
/*				gtk_button_set_image(GTK_BUTTON(redoButton),gtk_image_new_from_file("./images/main/redo.png"));*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),redoButton,0,0,0);*/
			
			//--------
/*			separator=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);*/
/*			gtk_box_pack_start(GTK_BOX(MAIN_MidSectWidgets),separator,1,0,0);*/
			
			GtkWidget *settingsButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(settingsButton),gtk_image_new_from_icon_name("open-menu-symbolic",GTK_ICON_SIZE_SMALL_TOOLBAR));
				//g_signal_connect (saveFileButton, "clicked",G_CALLBACK (haed_gui_save), NULL);
			gtk_header_bar_pack_end(GTK_HEADER_BAR(titlebar),settingsButton);
			
			//RUN GAME BUTTONS
			GtkWidget *runProgramButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(runProgramButton),gtk_image_new_from_icon_name("system-run",GTK_ICON_SIZE_SMALL_TOOLBAR));//gtk_image_new_from_file("./images/main/run_game.png"));
				g_signal_connect (runProgramButton, "clicked",G_CALLBACK (haed_run), (void*)0);
			gtk_header_bar_pack_end(GTK_HEADER_BAR(titlebar),runProgramButton);
			
			GtkWidget *runLevelButton=gtk_button_new();
				gtk_button_set_image(GTK_BUTTON(runLevelButton),gtk_image_new_from_icon_name("media-playback-start",GTK_ICON_SIZE_SMALL_TOOLBAR));//gtk_image_new_from_file("./images/main/run_level.png"));
				g_signal_connect (runLevelButton, "clicked",G_CALLBACK (haed_run), (void*)1);
			gtk_header_bar_pack_end(GTK_HEADER_BAR(titlebar),runLevelButton);
			
			
/*		gtk_box_pack_start(GTK_BOX(GLOBAL_MAIN_BOX),MAIN_MidSectWidgets,0,1,0);*/
}
