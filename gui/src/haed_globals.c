/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "includes.h"

//==========FOR ANIMATIONS (GLOBAL_ANIMATION_VECTOR)==========

/**
	Get coordiantion from animation name in the GLOBAL_ANIMATION_VECTOR.
	
	@param animationname
		the input animation name
	@returns
		the position
*/
int haed_animations_get_pos(char *animationname)
{
	for(int i=0;i<GLOBAL_ANIMATION_VECTOR_LEN;i++)
	{
		if(strcmp(animationname,GLOBAL_ANIMATION_VECTOR[i])==0)
		{
			return i;
		}
	}
	
	b_warning_message("Could not find the animation: %s!",animationname);
	
	return -1;
}

/**
	Check if an animation is an animation, eg if disappearing is an animation, should in this case return true (1).
	
	@param animationname
		the input animation name.
	@returns
		1 = if it is in the GLOBAL_ANIMATION_VECTOR, 0 = if its not.
*/
uint8_t haed_animations_is_animation(char *animationname)
{
	for(int i=0;i<GLOBAL_ANIMATION_VECTOR_LEN;i++)
	{
		if(strcmp(animationname,GLOBAL_ANIMATION_VECTOR[i])==0)
		{
			return 1;
		}
	}
	
	//is not an animation
	return 0;
}

//==========FOR GLOBALINFO==========

void haed_globalinfo_add_program(HaedGlobalinfo *thisInfo,const char *programname)
{
	HaedProgramInfo *newProgram=calloc(1,sizeof(HaedProgramInfo));

	newProgram->program=haed_programinfo_init(programname);

	haed_trigghandler_begin();

	if(thisInfo->firstProgram==NULL)
	{
		newProgram->prevProgram=newProgram;
		newProgram->nextProgram=newProgram;
		
		thisInfo->firstProgram=newProgram;
	}
	else
	{
		HaedProgramInfo *newFirstProgram=thisInfo->firstProgram;
	
		newProgram->nextProgram=newFirstProgram;
		newProgram->prevProgram=newFirstProgram->prevProgram;
		
		newFirstProgram->prevProgram->nextProgram=newProgram;
		newFirstProgram->prevProgram=newProgram;
	}
}

HaedProgramInfo *haed_globalinfo_get_program_from_position(HaedGlobalinfo *thisInfo,int pos)
{
	HaedProgramInfo *theFirstProgram=thisInfo->firstProgram;

	if(!theFirstProgram)
	{
		b_error_message("No first child?");
		return NULL;
	}
		
	HaedProgramInfo *loopProgram=theFirstProgram;

	do
	{
		if(pos<=0)
		{
			return loopProgram;
		}
		
		pos--;
		
		loopProgram=loopProgram->nextProgram;
	}while(loopProgram!=theFirstProgram);
	
	b_error_message("Your position is probably wrong!");
	
	return NULL;
}

HaedProgramInfo *haed_globalinfo_get_program_from_name(HaedGlobalinfo *thisInfo,char *findname)
{
	HaedProgramInfo *theFirstProgram=thisInfo->firstProgram;

	if(!theFirstProgram)
	{
		b_error_message("No first child?");
		return NULL;
	}
		
	HaedProgramInfo *loopProgram=theFirstProgram;

	do
	{
		if(strcmp(findname,loopProgram->program->name))
		{
			return loopProgram;
		}
		
		loopProgram=loopProgram->nextProgram;
	}while(loopProgram!=theFirstProgram);
	
	b_error_message("Could not find the child!");
	
	return NULL;
}

////

int haed_global_filename_set(const char *filename)
{
	if(GLOBAL_CURRENT_FILE_NAME)
	{
		free(GLOBAL_CURRENT_FILE_NAME);
	}
	
	GLOBAL_CURRENT_FILE_NAME=strdup(filename);
	
	return 0;
}
