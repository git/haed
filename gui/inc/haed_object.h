/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#ifndef HAED_OBJECT_SOURCE
#define HAED_OBJECT_SOURCE

#include "includes.h"

//UiActiveObject
/*
typedef struct _EksParent
{
	//type=object type ex active object
	//int amount, **coord;
	
	bool triggerCreation;
	//name of object
	const char *type;
	char *name,*imagePath;
	
	//image=the image of the object (gtk_image)
	//drawableWidgets=the objects you can draw with on the level (field) (gtk_?????) probably (gtk_image)
	//drawMenuWidget=the widget in the sidemenu (gtk toggle)
	//triggerMenuWidget=the widget for the conditions, with dropdown-menu (gtk_toggle)
	//triggerButtonWidget=the widget that will appear if you selected something that needs confirmation in 
	//triggerTopWidget=the widget shown at the top (gtk_toggle)
	//conditionWidgets=the widgets shown in conditions (gtk_button in tgf) should maybe switch to toggle.
	//EksParent *animationinf;
	
	EksParent *pixbuffholder;
	
	GdkPixbuf *imagebuff;
	
	GtkWidget *drawMenuWidget,*triggerMenuWidget,*triggerButtonWidget,*triggerTopWidget,**conditionWidgets;

	//for other information... points to a different structure
	void *objectTypeStruct;
}EksParent;

typedef struct _EksParent
{
	//mov types:
	//0=none
	//1=all directions
	//....
	
	int player, movType, graphicsFrameInf, directions, triggCreated;
	float *acceleration,*deacceleration, *speed;
}EksParent;
*/

typedef enum HaedObjectCustomData{
	HAED_OBJECT_BUFFHOLDER,
	HAED_OBJECT_IMAGEBUFF,
	HAED_OBJECT_DRAW_MENU_WIDGET,
	HAED_OBJECT_TRIGGER_MENU_WIDGET,
	HAED_OBJECT_TRIGGER_BUTTON_WIDGET,
	HAED_OBJECT_TRIGGER_TOP_WIDGET,
	HAED_OBJECT_CONDITIONS_WIDGET
}HaedObjectCustomData;

void haed_object_add_eks_parent_to_gui(char *name,EksParent *holder,const char *type);
void haed_object_set_level_editor_icons(EksParent *thisObject);
void haed_object_set_imagebuff_from_bufholder(EksParent *thisObject);
char *haed_object_get_string(EksParent *object,const char *assignparam);
void haed_object_set_string(EksParent *object,const char *assignparam,const char *setparam,int delete);
intptr_t haed_object_get_int(EksParent *object,const char *assignparam);
void haed_object_set_int(EksParent *object,const char *assignparam,int setparam,int delete);
double haed_object_get_double(EksParent *object,const char *assignparam);
void haed_object_set_double(EksParent *object,const char *assignparam,double setparam,int delete);
void haed_object_list_visual_init(void);
void haed_object_list_visual_reset(void);

#endif
