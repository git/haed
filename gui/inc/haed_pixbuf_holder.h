/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

EksParent *haed_pixbufholder_new(void);

size_t haed_pixbufholder_count_amount(EksParent *holder);

GdkPixbuf *haed_pixbufholder_get_pixbuf_from_name(EksParent *holder,char *name);

EksParent *haed_pixbufholder_add_animation(EksParent *holder,const char *animationname,GdkPixbuf *inbuf);

void haed_pixbufholder_set_animation_from_pixbuf(EksParent *holder,char *animationname,GdkPixbuf *inbuf);

void haed_pixbufholder_fix_object(EksParent *holder, const char *holderObjName, EksParent *programParent);
