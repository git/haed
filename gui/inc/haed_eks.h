/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#ifndef HAED_EKS_SOURCE
#define HAED_EKS_SOURCE

#include "includes.h"

/*
Vid sparning eller körning så fuckas generate game parent upp!
eks_parent_insert kan vara anledningen
*/

#ifdef  MAIN_FILE
const char *MOV_TYPES[2]={"none","all_directions"};
#else
extern const char *MOV_TYPES[2];
#endif

void haed_visual_add_all_objects(EksParent *programParent);
void haed_visual_clean_other(void);
char *zip_get_file_contents(char *filename,struct zip *z,struct zip_stat *st,int *contentsLength);
void haed_eks_set_objects_from_parent(EksParent *rootParent);
int haed_open_filetype_hft(char *filename);
void haed_clean_everything(void);
void haed_new_program(GtkWidget *widget,gpointer data);
void haed_load_program(GtkWidget *widget,gpointer data);
void haed_save_program(char *fileName);
void haed_gui_save_as(GtkWidget *widget,gpointer data);
void haed_gui_save(GtkWidget *widget,gpointer data);
void haed_run(GtkWidget *widget,gpointer data);

#endif
