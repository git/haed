/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

#define haed_unit_is_marked(obj) (obj->params & 1)

typedef enum HaedPosObjectCustomData{
	HAED_POS_OBJECT_DECLARE,
	HAED_POS_OBJECT_MARKED
}HaedPosObjectCustomData;

#define HAED_UNIT_IS_UNMARKED ((void*)(intptr_t)0)
#define HAED_UNIT_IS_MARKED ((void*)(intptr_t)1)

/*
typedef struct _EksParent
{
	int coord[DIMENSION];
	uint8_t params; //1bit=isMarked
	EksParent *object;
	
	struct _EksParent *nextUnit;
	struct _EksParent *prevUnit;
}EksParent;
*/

EksParent *haed_unitlist_get_unit_from_position(EksParent *firstUnit, int pos);

void haed_unitlist_add(EksParent *firstUnit, EksParent *object, int x,int y);

void haed_unitlist_remove(EksParent *firstUnit, EksParent *remobj);

void haed_unitlist_remove_from_marked(EksParent *firstUnit);

size_t haed_unitlist_count(EksParent *firstUnit);

size_t haed_unitlist_object_count(EksParent *firstUnit,EksParent *object);

EksParent *haed_unitlist_get_positions(EksParent *level);

char *haed_unit_get_obj_name(EksParent *unit);

intptr_t haed_unit_get_coord_x(EksParent *unit);
intptr_t haed_unit_get_coord_y(EksParent *unit);
