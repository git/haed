/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

#ifdef  MAIN_FILE
char *PROGRAM_DIR="./../hattis/";
char *PROGRAM_DIR_IMG="./../hattis/img/";
#else
extern char *PROGRAM_DIR;
extern char *PROGRAM_DIR_IMG;
#endif

typedef enum HaedLevelInformation{
	HAED_LEVEL_TRIGGHANDLER
}HaedLevelInformation;

void haed_set_storyboard_mode(GtkWidget *widget,gpointer   data);
void haed_set_level_editor_mode(GtkWidget *widget,gpointer   data);
void haed_set_event_editor_mode(GtkWidget *widget,gpointer   data);
void haed_update_event_editor_grid(GtkWidget *widget,gpointer data);
int create_popup_menu(GtkWidget *menu,GdkEvent *event);
void haed_select_movement_active_object_button(GtkWidget *widget,gpointer data);
void haed_select_player_active_object_button(GtkWidget *widget,gpointer data);
void haed_level_editor_active_object_remove_from_draw_area(GtkWidget *widget,gpointer data);
void haed_level_editor_active_object_popup_menu(GtkWidget *widget,GdkEvent *event, gpointer data);
void haed_set_current_layout_object_pointer(GtkWidget *widget,gpointer data);
gboolean haed_level_editor_signal_draw(GtkWidget *widget, cairo_t *cr, gpointer data);
void haed_level_editor_signal_event(GtkWidget *widget,GdkEvent *event,gpointer data);
void haed_update_event_editor_placer(GtkRange *range,gpointer  user_data);
