/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

typedef struct
{
	const char *object;
	//-1 == infinity
	int numberOfLoops;
	bool writeEnable; ///< If you want to write to this object, 0=false, 1=true, if false, then you cannot set it anymore
}PgdTriggObjInf;

typedef struct
{
	int amount; 
	PgdTriggObjInf *objInf;
}PgdTriggObjInfVect;

void pgd_infvect_trigg_obj_add(PgdTriggObjInfVect *objInfVect,const char *object,int numberOfLoops,bool writeEnable);

void pgd_infvect_trigg_obj_add_from_trigger(PgdTriggObjInfVect *objInfVect,const char *trigger,EksParent *topEksParent);

char *pgd_trigger_as_string(EksParent *triggerEksParent,EksParent *topEksParent);
