/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

#ifndef PGD_TRIGGER_LEXCA_BUFFER
#define PGD_TRIGGER_LEXCA_BUFFER

typedef struct Pgd_trigger_lexca_buffer_node Pgd_trigger_lexca_buffer_node;

typedef enum Pgd_trigger_lexca_buffer_node_type
{
	PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_NONE,
	PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_STD,
	PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_FUNCTION,
	PGD_TRIGGER_LEXCA_BUFFER_NODE_TYPE_LENGTH
}Pgd_trigger_lexca_buffer_node_type;

typedef struct Pgd_trigger_lexca_buffer
{
	GPtrArray *a; ///< standard array
	GPtrArray *n; ///< nestled array
}Pgd_trigger_lexca_buffer;

struct Pgd_trigger_lexca_buffer_node
{
	Pgd_trigger_lexca_buffer_node_type obj_type;
	union
	{
		char *obj_str; 
		GPtrArray *obj_children;
	};
	LexcaStrType type;
	size_t index;
	Pgd_trigger_lexca_buffer_node *parent;
};

Pgd_trigger_lexca_buffer_node *pgd_trigger_lexca_buffer_node_init(Pgd_trigger_lexca_buffer_node *self,const char *obj, LexcaStrType type, size_t index);
Pgd_trigger_lexca_buffer_node *pgd_trigger_lexca_buffer_node_new(const char *obj, LexcaStrType type, size_t index);
void pgd_trigger_lexca_buffer_node_finalize(Pgd_trigger_lexca_buffer_node *self);
void pgd_trigger_lexca_buffer_node_free(Pgd_trigger_lexca_buffer_node *self);

Pgd_trigger_lexca_buffer *pgd_trigger_lexca_buffer_init(Pgd_trigger_lexca_buffer *self);
Pgd_trigger_lexca_buffer *pgd_trigger_lexca_buffer_new(void);
void pgd_trigger_lexca_buffer_finalize(Pgd_trigger_lexca_buffer *self);
void pgd_trigger_lexca_buffer_free(Pgd_trigger_lexca_buffer *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(Pgd_trigger_lexca_buffer,pgd_trigger_lexca_buffer_free)

Pgd_trigger_lexca_buffer *pgd_trigger_lexca_buffer_create(const char *c_code);
int pgd_trigger_lexca_buffer_print(Pgd_trigger_lexca_buffer *obj);
int pgd_trigger_lexca_buffer_get_object_from_pos(Pgd_trigger_lexca_buffer *obj, int object_pos);

int pgd_trigger_lexca_buffer_compare(Pgd_trigger_lexca_buffer *obj1,Pgd_trigger_lexca_buffer *obj2);

#endif
