/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

//BADASS GLOBALS

//======================RELATING TO GLOBALS=========================

typedef struct _HaedGlobalinfo
{
	//the current widget containing an object
	GtkWidget *levelEditorSelectedSideBarWidget;
	//the current unit selected
	EksParent *levelEditorSelectedUnits;
	//the current button clicked in the triggers
	GtkWidget *triggerEditorCurrentButton;
	
	HaedProgramInfo *firstProgram;
	
	TriggerHandler *triggerHandler;
}HaedGlobalinfo;


typedef enum HaedBuildPolicy
{
	GLOBAL_HAED_BUILD_POLICY_PROJECT_NAME, /// Standard, will generate a new folder for the project name in the USER_PATH.
	GLOBAL_HAED_BUILD_POLICY_USER_PATH, ///Use the haed_game directory only.
	GLOBAL_HAED_BUILD_POLICY_PGD_PATH ///Use pgd as-is
}HaedBuildPolicy;

#ifdef  MAIN_FILE
HaedGlobalinfo GLOBAL_HAED_INFORMATION={0};

char *GLOBAL_HAED_PGD_PATH=NULL;
char *GLOBAL_HAED_USER_PATH=NULL;
int GLOBAL_HAED_BUILD_POLICY=0;
#else
extern HaedGlobalinfo GLOBAL_HAED_INFORMATION;

extern char *GLOBAL_HAED_PGD_PATH;
extern char *GLOBAL_HAED_USER_PATH;
extern int GLOBAL_HAED_BUILD_POLICY;
#endif

//======================DEFINITION OF OBJECTS=====================

#ifdef  MAIN_FILE
const char *GLOBAL_TRIGGERS_SPECIAL_S="SpecialConditions";
const char *GLOBAL_TRIGGERS_SOUND_S="SoundConditions";
const char *GLOBAL_TRIGGERS_STORY_S="StoryConditions";
const char *GLOBAL_TRIGGERS_INPUT_S="InputConditions";

const char *GLOBAL_ACTIVE_OBJECT_S="active";
const char *GLOBAL_BACKDROP_OBJECT_S="BackdropObject";
#endif

//animations
#define GLOBAL_ANIMATION_STOPPED "stopped"
#define GLOBAL_ANIMATION_RUNNING "running"
#define GLOBAL_ANIMATION_DISAPPEARING "disappearing"
#define GLOBAL_ANIMATION_JUMPING "jumping"

#ifdef  MAIN_FILE
const char *GLOBAL_ANIMATION_VECTOR[4]={GLOBAL_ANIMATION_STOPPED,GLOBAL_ANIMATION_RUNNING,GLOBAL_ANIMATION_DISAPPEARING,GLOBAL_ANIMATION_JUMPING};
#endif

#define GLOBAL_ANIMATION_VECTOR_LEN (sizeof(GLOBAL_ANIMATION_VECTOR)/sizeof(GLOBAL_ANIMATION_VECTOR[0]))


//TEMP!!!
#define GLOBAL_HAED_TRIGGER_HANDLER GLOBAL_HAED_INFORMATION.triggerHandler
#define GLOBAL_TRIGGER_PARENT eks_parent_get_child_from_name(haed_programinfo_get_current_level(GLOBAL_HAED_INFORMATION.firstProgram->program),"triggers")


#ifdef  MAIN_FILE
//Program screen width
int GLOBAL_SCREEN_WIDTH=1024;
//Program screen height
int GLOBAL_SCREEN_HEIGHT=768;

int GLOBAL_MARGIN_SCREEN_WIDTH=100;
int GLOBAL_MARGIN_SCREEN_HEIGHT=100;

int GLOBAL_MARGIN_SCREEN_WIDTH_MIN=50;
int GLOBAL_MARGIN_SCREEN_HEIGHT_MIN=50;

double GLOBAL_PLACER_SCREEN_RANGE_X=0;
double GLOBAL_PLACER_SCREEN_RANGE_Y=0;

//======================ALL IMPORTANT TOPLEVEL WIDGETS=====================

GtkWidget *GLOBAL_MAIN_WINDOW;

//MAIN GIRD
GtkWidget *GLOBAL_MAIN_BOX;

//MENU ITEMS
GtkWidget *GLOBAL_MAIN_MENU;

//MID SECT WIDGETS
//GtkWidget *MAIN_MidSectWidgets;

GtkWidget *GLOBAL_HAED_STORY_BOARD=NULL;
GtkWidget *GLOBAL_MAIN_STORYBOARD=NULL;
	GtkWidget *GLOBAL_MAIN_STORYBOARD_INFO[3];
	
GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR;
	GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[3];
	GtkWidget *GLOBAL_HAED_LIST_OBJECTS=NULL;
	GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX=NULL;
	GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS;
	GtkWidget **GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS_ICONS_IMAGE;
	
	GtkWidget *GLOBAL_LEVEL_EDITOR_DRAWING_AREA;
	GtkAdjustment *GLOBAL_MAIN_LAYOUT_AX;
	GtkAdjustment *GLOBAL_MAIN_LAYOUT_AY;
	
	GtkWidget *GLOBAL_MAIN_ACTIVE_OBJECT_MENU=NULL;

GtkWidget *GLOBAL_HAED_TRIGGER_BOX=NULL;
	GtkWidget *GLOBAL_HAED_TRIGGER_GRID=NULL;
	GtkWidget *GLOBAL_MAIN_TOP_LEFT_INFO;
	
	GtkWidget *GLOBAL_LIST_FUNCTIONS[2];
	
	GtkWidget *GLOBAL_MAIN_ACTIVE_OBJECT_MENU_TRIGGERS[20];
	GtkWidget *GLOBAL_MAIN_ACTIVE_OBJECT_ACTIONS_MENU[20];

GtkWidget *GLOBAL_MAIN_NOTEBOOK;

//conditions box (or grid)
GtkWidget *GLOBAL_CONDITIONS_WINDOW;
GtkWidget *GLOBAL_MAIN_CONDITIONS_BOX;

//for the window getobjbox
GtkWidget *GLOBAL_MAIN_GET_OBJECT_BOX;

EksParent *GLOBAL_HAED_TEMP_HAEDOBJECT;

int GLOBAL_CURRENT_WINDOW_MODE=0;

char *GLOBAL_CURRENT_FILE_NAME=NULL;
#else

//BLERP

extern const char *GLOBAL_TRIGGERS_SPECIAL_S;
extern const char *GLOBAL_TRIGGERS_SOUND_S;
extern const char *GLOBAL_TRIGGERS_STORY_S;
extern const char *GLOBAL_TRIGGERS_INPUT_S;
extern const char *GLOBAL_ACTIVE_OBJECT_S;
extern const char *GLOBAL_BACKDROP_OBJECT_S;

extern const char *GLOBAL_ANIMATION_VECTOR[4];

extern int GLOBAL_SCREEN_WIDTH;
extern int GLOBAL_SCREEN_HEIGHT;

extern int GLOBAL_MARGIN_SCREEN_WIDTH;
extern int GLOBAL_MARGIN_SCREEN_HEIGHT;

extern int GLOBAL_MARGIN_SCREEN_WIDTH_MIN;
extern int GLOBAL_MARGIN_SCREEN_HEIGHT_MIN;

extern double GLOBAL_PLACER_SCREEN_RANGE_X;
extern double GLOBAL_PLACER_SCREEN_RANGE_Y;

extern GtkWidget *GLOBAL_MAIN_WINDOW;
extern GtkWidget *GLOBAL_MAIN_BOX;

extern GtkWidget *GLOBAL_MAIN_MENU;

//extern GtkWidget *MAIN_MidSectWidgets;

extern GtkWidget *GLOBAL_HAED_STORY_BOARD;
extern GtkWidget *GLOBAL_MAIN_STORYBOARD;
	extern GtkWidget *GLOBAL_MAIN_STORYBOARD_INFO[3];
	extern GtkWidget **GLOBAL_LEVEL_EDITOR_PREVIEWS;
	
extern GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR;
	extern GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR_SCROLLBARS[3];
	extern GtkWidget *GLOBAL_HAED_LIST_OBJECTS;
	extern GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR_INNER_BOX;
	extern GtkWidget *GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS;
	extern GtkWidget **GLOBAL_MAIN_LEVEL_EDITOR_LIST_OBJECTS_ICONS_IMAGE;
	
	extern GtkWidget *GLOBAL_LEVEL_EDITOR_DRAWING_AREA;
	extern GtkAdjustment *GLOBAL_MAIN_LAYOUT_AX;
	extern GtkAdjustment *GLOBAL_MAIN_LAYOUT_AY;
	
	extern GtkWidget *GLOBAL_MAIN_ACTIVE_OBJECT_MENU;

extern GtkWidget *GLOBAL_HAED_TRIGGER_BOX;
	extern GtkWidget *GLOBAL_HAED_TRIGGER_GRID;
	extern GtkWidget *GLOBAL_MAIN_TOP_LEFT_INFO;
	
	extern GtkWidget *GLOBAL_LIST_FUNCTIONS[2];
	
	extern GtkWidget *GLOBAL_MAIN_ACTIVE_OBJECT_MENU_TRIGGERS[20];
	extern GtkWidget *GLOBAL_MAIN_ACTIVE_OBJECT_ACTIONS_MENU[20];

extern GtkWidget *GLOBAL_MAIN_NOTEBOOK;

extern GtkWidget *GLOBAL_CONDITIONS_WINDOW;
extern GtkWidget *GLOBAL_MAIN_CONDITIONS_BOX;

extern GtkWidget *GLOBAL_MAIN_GET_OBJECT_BOX;

extern EksParent *GLOBAL_HAED_TEMP_HAEDOBJECT;

extern int GLOBAL_CURRENT_WINDOW_MODE;

extern char *GLOBAL_CURRENT_FILE_NAME;
#endif

//======GLOBAL FUNCTIONS======

int haed_animations_get_pos(char *animationname);

uint8_t haed_animations_is_animation(char *animationname);

//====FOR HAEDGLOBALINFO====

void haed_globalinfo_add_program(HaedGlobalinfo *thisinfo,const char *programname);

HaedProgramInfo *haed_globalinfo_get_program_from_position(HaedGlobalinfo *thisinfo,int position);

HaedProgramInfo *haed_globalinfo_get_program_from_name(HaedGlobalinfo *thisinfo,char *findname);

int haed_global_filename_set(const char *filename);
