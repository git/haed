/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#include "includes.h"

//add functions such as load, save, new game, game in tab? 

typedef enum HaedProgramInfoCustom{
	HAED_PROGRAM_CURRENT_LEVEL
}HaedProgramInfoCustom;

typedef struct _HaedProgramInfo
{
	EksParent *program;
	
	//----@deprecated
	//for the levels
	//----@/deprecated
	
	struct _HaedProgramInfo *nextProgram;
	struct _HaedProgramInfo *prevProgram;
}HaedProgramInfo;

EksParent *haed_programinfo_init(const char *programname);

EksParent *haed_programinfo_add_level(EksParent *thisProgram,const char *levelname);

void haed_programinfo_set_current_level(EksParent *programParent,EksParent *id);

EksParent *haed_programinfo_get_current_level(EksParent *programParent);

size_t haed_programinfo_count_levels(EksParent *thisProgram);

void haed_programinfo_select_level_from_pos(HaedProgramInfo *thisProgram,size_t pos);

EksParent *haed_programinfo_get_current_level(EksParent *programParent);
