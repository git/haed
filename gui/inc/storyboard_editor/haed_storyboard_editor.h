/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

void haed_storyboard_change_level(GtkWidget *widget, gpointer data);

void haed_storyboard_init_visual(void);

void haed_storyboard_add_visual(EksParent *level);

void haed_storyboard_visual_reset(EksParent *firstLevel);

void haed_storyboard_add_button(GtkWidget *widget, gpointer data);
