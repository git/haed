/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/**
	Header file for Haed_trigghandler
*/
#pragma once

#ifndef HAED_TRIGGHANDLER
#define HAED_TRIGGHANDLER

#include "includes.h"

///@deprecated, remove UITriggRow och TriggerHandler allt ska skötas med GtkWidget och EksParent.

/**
	what the row should contain.
	Note that all conditions should be placed within a EksParent structure. This is just meant to write it out.
*/
typedef struct UITriggRow
{
	//note that the number can relate to an object number.
	EksParent *to_condition;
	//the number shown to the left (remove)?
	//int number;
	GtkWidget *number_info;
	GtkWidget *condition_info;
	//columns
	
/*	GtkWidget **ActionButtons;*/
	GPtrArray *action_buttons; ///< GtkWidget
}UITriggRow;

/**
	A set of triggers
	Used when for example when copying a set of triggers or to display the common triggers.

	@param rowAmount
		the amount of rows
	@param columnAmount
		the amount of rows
		implement UITriggerColumn?
	@param row
		what the rows should contain @param see UITriggRow
*/
typedef struct TriggerHandler
{
	//all rows except the top one (the one with all objects)
/*	int rowAmount;*/
/*	int column_amount;*/
/*	UITriggRow **row;*/
/*	EksParent **objects;*/
	GPtrArray *objects; ///< gives number of columns (EksParent)

	GPtrArray *rows; ///< UITriggRow
}TriggerHandler;

TriggerHandler *haed_trigghandler_new(void);
void haed_trigghandler_free(void);
void haed_triggers_bind_object_to_button(GtkWidget *button,EksParent *condition,EksParent *object);
void haed_trigghandler_add_object(EksParent *object);
int haed_trigghandler_count_objects(const char *objectType);
EksParent *haed_trigghandler_get_object(const char *objectType,int pos);
EksParent *haed_triggerhandler_get_object_by_name(char *name);
void haed_trigghandler_remove_condition(GtkWidget *triggerGrid,EksParent *conditionIn);
void haed_trigghandler_clean(void);
int haed_triggerhandler_get_pos_from_object(EksParent *object);

#endif
