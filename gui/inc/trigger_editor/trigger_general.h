/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

void haed_triggers_visual_update(void);
void haed_triggers_remove_condition_press(GtkWidget *widget,gpointer data);
void haed_triggers_insert_button_press(GtkWidget *widget,GdkEvent *event,gpointer data);
void haed_triggers_insert_row(GtkWidget *triggerGrid,EksParent *conditionIn);
EksParent *haed_triggerparent_add_condition(int level,char *conditionString);
char *haed_triggerparent_add_action(EksParent *conditionEksParent,char *actionString);
void haed_triggers_add_visual_for_action(GtkWidget *widget,char *actionText);
void haed_triggers_create_popup_from_menu_list(GtkMenuListInner *theList,size_t listSize,GdkEvent *event);
int haed_triggers_populate_code(const char *input,const char *desc,intptr_t refers,EksParent *refered_object,size_t *result_len,char **result);
void haed_trigger_conditions_add_internal(GtkWidget *widget,gpointer user_data);
void haed_trigger_actions_add_internal(GtkWidget *widget,gpointer user_data);
GtkWidget *haed_triggers_create_popup_from_eks_description(EksParent *object,GtkWidget *parent,GdkEvent *event,int status,void(*funct)(GtkWidget *widget,gpointer user_data));

