/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#ifndef HAED_CONFIG
#define HAED_CONFIG

#include "includes.h"

typedef struct Haed_config
{
	char *main_config_file;
	EksParent *data;
	
}Haed_config;

/////////////////////////////

Haed_config *haed_config_init(Haed_config *self,const char *main_config_file);
Haed_config *haed_config_new(const char *main_config_file);
void haed_config_finalize(Haed_config *self);
void haed_config_free(Haed_config *self);

#ifndef HAED_CONFIG_MAIN
extern Haed_config *GLOBAL_HAED_CONFIGURATION;
#endif

#endif
