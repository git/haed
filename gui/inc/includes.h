/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <stdio.h>
#include <stdbool.h>

#include <libintl.h>
#include <locale.h>
//#define _(STRING) gettext(STRING)

#include <time.h>

#include <gtk/gtk.h>

#define GETTEXT_PACKAGE "gtk20"
#include <glib/gi18n-lib.h>

#include <zip.h>

#include "GtkMenuList.h"
//#define strdup(str) strndup(str,strlen(str))
#include <eksparent.h>
#include "lexca.h"
#include "haed_modify.h"

#include "blibc.h"

#include "haed_definitions.h"

//relating to gui
#include "haed_pixbuf_holder.h"
#include "haed_object.h"
#include "pgd_trigger_lexca_buffer.h"
#include "haed_trigghandler.h"
#include "trigger_general.h"

#include "haed_unit_t.h"
//#include "haed_levelinfo_t.h"
#include "haed_programinfo_t.h"
#include "haed_globals.h"

//dialogs
#include "haed_dialog.h"
#include "haed_dialog_select_movement.h"
#include "haed_dialog_edit_movement.h"
#include "haed_dialog_get_object.h"
#include "haed_dialog_object_settings.h"
#include "haed_dialog_drawing.h"
#include "haed_dialog_strings.h"
#include "haed_dialog_level_settings.h"
#include "haed_dialog_pick_level.h"
#include "haed_dialog_animation_editor.h"
#include "haed_dialog_level_editor_create.h"
#include "haed_dialog_sound_manager.h"
//#include "haed_marker_t.h"

#include "triggers.h"

#include "pgd_programcreator.h"
#include "pgd_triggerreader.h"

#include "popup_windows.h"
#include "haed_eks.h"

#include "haed_dialog_drawing.h"
#include "topmenu.h"
#include "menubuttons.h"

#include "level_editor_display.h"
#include "haed_storyboard_editor.h"

#include "haed_config.h"
//#include "sacm.h"

/*#include "minilang.h"*/
