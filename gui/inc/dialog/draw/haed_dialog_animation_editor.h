/**
	Haed a free program maker and generator
	Copyright (C) 2018  Florian Evaldsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "includes.h"

//gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer data);

//void haed_draw_main_function(GtkWidget *widget,GdkEvent *event,gpointer user_data);

//void haed_draw_set_brush_size(GtkWidget *widget,gpointer data);

//void haed_draw_set_alpha_level(GtkWidget *widget,gpointer data);

//void haed_draw_set_eraser(GtkWidget *widget,gpointer data);

//void haed_draw_set_brush_color(GtkWidget *widget,gpointer data);

//void haed_draw_set_screen_size(GtkWidget *widget,gpointer data);

//void haed_dialog_drawing(GtkWidget *widget,gpointer data);

void haed_draw_change_move_type(GtkWidget *widget,gpointer data);

void hai_add_animation(EksParent *tempEksParent,char *animationtype,int animationspeed,char **animationinfo,int animationinfolen);

void haed_draw_active_object_frame_editor(GtkWidget *widget,gpointer data);
