/**
	Configuration file for the Haed program

	The MIT License (MIT)

	Copyright (c) 2019 Florian Evaldsson <florian.evaldsson@telia.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE. 

	@author Florian Evaldsson 
*/

/**
	Make a language such as;
	FUNCTION(__OBJECT, __OBJECT) == __CALCULATION;
	where:
	* __OBJECT:: will turn into the object picking dialog
	*# Attributes:
	** NOITR -> will not produce a foreach every time its generated
	* ==,>=,<=,!=,__EQUIV:: will turn into the equivalence dialog
	* __CALCULATION:: will turn into the calculation dialog
	* __EQUIV_CALCULATION:: will turn into the equiv calculation dialog (turns into __EQUIV __CALCULATION)
	* __COORD:: Will turn into the X,Y coordinate dialog (turns into __COORD_X, __COORD_Y)
	* __COORD_X, __COORD_Y:: will turn into the X or Y dialog.
	* __LENGTH:: Length of something.
	* __AREA:: Mark an area on the screen. (turns into __COORD_X, __COORD_Y, __LENGTH, __LENGTH)
	* __EDGE_AREA:: Mark edges or similar on the screen.
	* __LEVEL:: pick a level dialog.
	* __DIRECTION:: direction selection dialog.
	
	** __REFERS, which object of the type is the one relating to the selected object
	** functions are used when making assumptions about existing functions etc
		also used when relating or working with the existing code, and checking syntax correctness
*/

#general
{
	#pgd_dir
		../pgd
	#haed_user_dir
		~/.haed/
	#haed_build_policy
		USER_PATH
		/*PGD_PATH*/
}

#triggers
{
	#functions
	{
		#conditions
		{
			#"Collision"
			{
				#_CODE
					COLLISION(__OBJECT,__OBJECT)
				#_RETURNS
					__BOOLEAN
			}
			#"Count"
			{
				#_CODE
					COUNT(__OBJECT)
				#_RETURNS
					__NUMBER
			}
			#"Get x"
			{
				#_CODE
					GET_X(__OBJECT)
				#_RETURNS
					__NUMBER
			}
			#"Get y"
			{
				#_CODE
					GET_Y(__OBJECT)
				#_RETURNS
					__NUMBER
			}
		}
		#actions
		{
			#"Stop"
			{
				#_CODE
					STOP(__OBJECT,__OBJECT)
				#_RETURNS
					__VOID
			}
		}
	}
	/*
		works like if(something) ...
	*/
	#conditions
	{
		#STD_OBJECT
		{
			#"Collisions"
			{
				#"Another object"
				{
					#_DESC
						__OBJECT collision between __OBJECT
					#_CODE
						COLLISION(__OBJECT,__OBJECT)
					#_REFERS
						1
				}
				#"Overlapping another object"
				{
					#_DESC
						__OBJECT overlapping object __OBJECT
				}
			}
			#"Movement"
			{
				#"Stopped"
				{
					#_DESC
						__OBJECT is stopped
				}
				#"Bouncing"
				{
					#_DESC
						__OBJECT is bouncing
				}
				#"Compare speed to value"
				{
					#_DESC
						__OBJECT speed is compared to ...
				}
				#"Compare acceleration to value"
				{
					#_DESC
						__OBJECT acceleration is compared to ...
				}
				#"Compare deacceleration to value"
				{
					#_DESC
						__OBJECT deacceleration is compared to ...
				}
			}
			#"Animation"
			{
				#"Animation is playing"
				{
					#_DESC
						__OBJECT animation is playing ...
				}
				#"Animation finnished"
				{
					#_DESC
						__OBJECT animation ... is finished
				}
				#"Compare animation value"
				{
					#_DESC
						__OBJECT animation value is ...
				}
			}
			#"Position"
			{
				#"Raw Position in level"
				{
					#_DESC
						__OBJECT position is ...
				}
				#"Getting closer to the levels edge"
				{
					#_DESC
						__OBJECT position is getting closer to levels edge
				}
				#"Compare X position"
				{
					#_DESC
						__OBJECT X position is __EQUIV_CALCULATION
				}
				#"Compare Y position"
				{
					#_DESC
						__OBJECT Y position is __EQUIV_CALCULATION
				}
			}
			#"Direction"
			{
				#"Compare direction of object"
				{
					#_DESC
						__OBJECT direction is ...
				}
			}
			#"Visibility"
			{
				#"Is visible"
				{
					#_DESC
						__OBJECT is visible
				}
			}
			#"Pick or Count"
			{
				#"Pick one at random"
				{
					#_DESC
						__OBJECT ...
				}
				#"Number of objects"
				{
					#_DESC
						Number of __OBJECT is __EQUIV_CALCULATION
					#_CODE
						COUNT(__OBJECT__NOITR) __EQUIV_CALCULATION
					#_REFERS
						1
				}
				#"Number of objects in zone"
				{
					#_DESC
						Number of __OBJECT in zone is __EQUIV_CALCULATION
				}
			}
			#"Values"
			{
				#"Get value"
				{
					#_DESC
						__OBJECT value is __EQUIV_CALCULATION
				}
				#"Get flag"
				{
					#_DESC
						__OBJECT flag is __EQUIV_CALCULATION
				}
			}
		}
		
		#STD_SPECIAL
		{
			#"Timing"
			{
				#"Is a timer a certain value"
				{
					#_DESC
						...
				}
				#"Ticks"
				{
					#_DESC
						ANIMATION_ROLLER is __EQUIV_CALCULATION
					#_CODE
						ANIMATION_ROLLER __EQUIV_CALCULATION
				}
			}
		}
		
		#STD_SOUND
		{
			#"Samples"
			{
				#"Is a sample not playing?"
				{
					#_DESC
						...
				}
				#"Is a specific sample not playing?"
				{
					#_DESC
						...
				}
			}
			#"Music"
			{
				#"Is a music not playing?"
				{
					#_DESC
						...
				}
				#"Is a specific music not playing?"
				{
					#_DESC
						...
				}
			}
		}
		
		#STD_STORYBOARD
		{
			#"End of level"
			{
				#_DESC
					...
			}
			#"Start of level"
			{
				#_DESC
					...
			}
			#"End game"
			{
				#_DESC
					...
			}
		}
		
		#STD_INPUT
		{
			#"Is a button pressed down"
			{
				#_DESC
					...
			}
			#"Is a button up"
			{
				#_DESC
					...
			}
			#"Read input state ..."
			{
				#_DESC
					...
			}
		}
		
		#STD_TEXT
		{
			#"Is text"
			{
				#_DESC
					...
			}
		}
	}
	
	/*
		works like do(something,...);
	*/
	#actions
	{
		#STD_OBJECT
		{
			#"Movement"
			{
				#"Stop"
				{
					#_DESC
						Stop __OBJECT
					#_CODE
						STOP(__OBJECT)
					#_REFERS
						1
				}
				#"Bounce"
				{
					#_DESC
						Bounce __OBJECT
					#_CODE
						BOUNCE(__OBJECT)
					#_REFERS
						1
				}
				#"Warp around level"
				{
					#_DESC
						...
				}
				#"Set speed"
				{
					#_DESC
						...
				}
				#"Set maximum speed"
				{
					#_DESC
						...
				}
				#"Reverse"
				{
					#_DESC
						...
				}
			}
			#"Animation"
			{
				#"Set animation to ..."
				{
					#_DESC
						...
				}
				#"Set animation frame to ..."
				{
					#_DESC
						...
				}
			}
			#"Position"
			{
				#"Set position"
				{
					#_DESC
						...
				}
				#"Swap position with other object"
				{
					#_DESC
						...
				}
				#"Set X position"
				{
					#_DESC
						Set __OBJECT X position
					#_CODE
						SET_X(__OBJECT,__COORD_X)
					#_REFERS
						1
				}
				#"Set Y position"
				{
					#_DESC
						Set __OBJECT Y position
					#_CODE
						SET_Y(__OBJECT,__COORD_Y)
					#_REFERS
						1
				}
			}
			#"Direction"
			{
				#"Set direction"
				{
					#_DESC
						...
				}
				#"Set direction to ..."
				{
					#_DESC
						...
				}
			}
			#"Visibility"
			{
				#"Make invisible"
				{
					#_DESC
						...
				}
				#"Make visible"
				{
					#_DESC
						...
				}
			}
			#"Shoot an object"
			{
				#_DESC
					...
			}
			#"Create"
			{
				#_DESC
					Create object __OBJECT
				#_CODE
					CREATE(__OBJECT,__COORD)
				#_REFERS
						1
			}
			#"Destroy"
			{
				#_DESC
					Destroy __OBJECT
				#_CODE
					DESTROY(__OBJECT)
				#_REFERS
						1
			}
			#"Values"
			{
				#"Set value"
				{
					#_DESC
						...
				}
				#"Set flag"
				{
					#_DESC
						...
				}
			}
		}
		
		#STD_SPECIAL
		{
			#"Timing"
			{
				#"Set time"
				{
					#_DESC
						...
				}
			}
		}
		
		#STD_STORYBOARD
		{
			#"Next level"
			{
				#_DESC
					Next level
				#_CODE
					NEXT_LEVEL()
			}
			#"Previous level"
			{
				#_DESC
					...
			}
			#"Jump to level"
			{
				#_DESC
					Set level to __LEVEL
				#_CODE
					SET_LEVEL(__LEVEL)
			}
			#"Restart the current level"
			{
				#_DESC
					Restart the current level
				#_CODE
					RESTART_LEVEL
			}
			#"Restart the game"
			{
				#_DESC
					Restart the game
				#_CODE
					RESTART_PROGRAM
			}
			#"End the game"
			{
				#_DESC
					...
			}
		}
		
		#STD_TEXT
		{
			#"Set text"
			{
				#_DESC
					...
			}
			#"Set font"
			{
				#_DESC
					...
			}
			#"Set size"
			{
				#_DESC
					...
			}
			#"Set position"
			{
				#_DESC
					...
			}
		}
		
		#STD_LIVES
		{
			#"Set lives"
			{
				#_DESC
					...
			}
			#"Set life picture"
			{
				#_DESC
					...
			}
			#"Set position"
			{
				#_DESC
					...
			}
		}
	}
}
