#######################################################
#
# Makefile for the Haed project
#
# @license GPLv3
# Haed a free program maker and generator
# Copyright (C) 2018  Florian Evaldsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# @author Florian Evaldsson 
#
#######################################################

HAED = gui

DEPENDENCIES = blibc/src gtk_menu_list lexca/src minilang/src

DEPENDENCIES_F = $(addprefix include/,$(DEPENDENCIES))

all: $(HAED)

$(HAED): $(DEPENDENCIES_F)
	$(MAKE) -C $@

.PHONY: $(DEPENDENCIES_F)
$(DEPENDENCIES_F):
	$(MAKE) -C $@

%.install:
	$(MAKE) -C $* install

%.uninstall:
	$(MAKE) -C $* uninstall

install_deps: $(DEPENDENCIES_F:=.install)

uninstall_deps: $(DEPENDENCIES_F:=.uninstall)
	
run: all
	$(MAKE) -C gui run

gdb: all
	$(MAKE) -C gui gdb
	

